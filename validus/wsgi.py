"""
WSGI config for validus project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/2.0/howto/deployment/wsgi/
"""

import os
import time
from django.core.wsgi import get_wsgi_application
import traceback
import signal
import sys

sys.path.append('/home/ubuntu/validus')

sys.path.append('/home/ubuntu/validus/django/lib/python3.5/site-packages')

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "validus.settings")

try:
	application = get_wsgi_application()
except Exception:
	if 'mod_wsgi' in sys.modules:
		traceback.print_exc()
		os.kill(os.getpid(), signal.SIGINT)
		time.sleep(2.5)

