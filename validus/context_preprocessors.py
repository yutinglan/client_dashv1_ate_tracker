"""
This context preprocessor is used to inject specific environment
variables into the templating language to allow things like the
ag-grid license to be included at a deployment level instead of
at a codebase level.
"""
import os

def export_variables(request):
    return {
        'AG_GRID_LICENSE': os.environ.get('AG_GRID_LICENSE')
    }
