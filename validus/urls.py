"""validus URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include, re_path
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.auth import views as admin_views
from django.views.generic import TemplateView
from django.views.generic.base import RedirectView



urlpatterns = [
    path('admin/', admin.site.urls),
    path('accounts/', include('accounts.urls')),
    path('logout/', admin_views.LogoutView.as_view(next_page='login')),
    path('contacts/', include('contacts.urls')),
    path('webapi/', include('webapi.urls')),
    path('links/', include('links.urls')),
   
    path('', include('client.urls')),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

# Here is a snippet you can uncomment to enable Django Debug Toolbar given
# you have the package installed.
#
# It can be a useful tool when trying to debug some performance related issues.
#
# https://django-debug-toolbar.readthedocs.io/en/latest/
#
# There is a corresponding snippet in settings.py
#
# if settings.DEBUG:
#     import debug_toolbar
#     urlpatterns = [
#         path('__debug__/', include(debug_toolbar.urls)),
#     ] + urlpatterns
