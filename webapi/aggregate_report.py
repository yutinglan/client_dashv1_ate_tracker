import calendar
import datetime
import json
import logging
import math
import os
import traceback
from datetime import date, timedelta, timezone
import operator
import numpy as np
import pandas as pd
import requests
from dateutil.relativedelta import relativedelta
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.core.paginator import Paginator
from django.db.models import Avg, F, Func, Max, Q, Value as V, DurationField, ExpressionWrapper
from django.db.models.aggregates import Min, Sum
from django.db.models.functions import Coalesce
from django.http import HttpResponse, HttpResponseBadRequest, JsonResponse
from django.utils.encoding import smart_str
from django.views import View
from numpy import min as np_min
from django.core.exceptions import PermissionDenied

from client.models import VRMClient, VRMContentTab, VRMDisplayTab
from database_schema.models import (VrmAggregateMtm, VrmCounterparty,
                                    VrmDailyNetExposure, VrmDealhedge1,
                                    VrmFund, VrmHedge1CopyCopy, VrmMtm1)
from dss_schema.models import (FixingdataTempBridgeTable,
                               LatestSpotRelatedData, TempBridgeTable)
from general.models import Ccys, Clients, Counterparties, Funds, Underlyings
from hedges.models import InTheMoneyProbability, Lar
from marketdata1.models import Cpmonitor, Vrmfrdcurvedata
from webapi.utils import (divide_normalize, get_first_day, get_last_day,
                          multiply_normalize)
from webapi.common import user_is_allowed, module_loader
from django.contrib.auth.mixins import LoginRequiredMixin

logger = logging.getLogger(__name__)


@login_required(login_url='login')
@user_is_allowed
def ajax_client_spot_request(request):
    underlying = list()
    spot_rate = dict()
    # additional_underlying = list()
    valuation_date_max = None
    try:
        valuation_date_max = request.POST['valuation_date']
    except:
        pass

    try:
        if request.is_ajax() and request.method == 'POST':
            client_type = request.POST.get('client', '')
            if valuation_date_max is None or valuation_date_max == '':
                valuation_date_max = FixingdataTempBridgeTable.objects.aggregate(Max('valuation_date'))['valuation_date__max']
            underlying = list(
                VrmHedge1CopyCopy.objects.filter(
                    client=client_type,
                    cancellation__iexact='FALSE',
                    dummy__iexact='FALSE'
                ).values_list(
                    'underlying',
                    flat=True
                ).distinct()
            )
            spot_rate = list(FixingdataTempBridgeTable.objects.filter( underlying__in=underlying,
                                                                        valuation_date=valuation_date_max).order_by(
                                                            'valuation_date').values('underlying', 'mid').distinct())


    except Exception as e:
        logger.exception('{}'.format(traceback.format_exc()))
    finally:
        res_data = {
            'spot_rate': spot_rate
        }
        return JsonResponse(res_data, content_type='application/json')


@login_required(login_url='login')
@user_is_allowed
def ajax_client_fund_mtm(request):
    if request.is_ajax() is False or request.method != 'POST':
        return HttpResponseBadRequest()
    try:
        resp_data = []
        date_list = []
        valuation_date = None
        resp_sum = 0
        resp_currency = None
        try:
            client = VRMClient.objects.get(name=request.POST['client'])
        except VRMClient.DoesNotExist:
            raise Exception('Unable to find the client to process!')

        try:
            module = __import__(client.name.lower())
        except Exception as e:
            try:
                module = __import__(client.label.lower())
            except Exception as e:
                raise Exception('Unable to find the client to process!')

        if request.POST['valuation_date'] == '':
            valuation_date = VrmMtm1.objects.filter(trade__client=client,
                                                    trade__cancellation__iexact='FALSE',
                                                    trade__dummy__iexact='FALSE',
                                                    ). \
                                        aggregate(Max('valuation_date'))['valuation_date__max']
        else:
            valuation_date = datetime.datetime.strptime(request.POST['valuation_date'], "%Y-%m-%d")
        if VrmMtm1.objects.filter(trade__client=client,
                                  trade__cancellation__iexact='FALSE',
                                  trade__dummy__iexact='FALSE',
                                  trade__asset_class__iexact='FX',
                                  trade__value_date__gte=valuation_date,
                                  trade__trade_date__lte=valuation_date,
                                  valuation_date=valuation_date,
                                  ).exists():
            '''
            Example query
            SELECT
                  HEDGE.Client,HEDGE.Fund, MTM.MTM_Currency, SUM(MTM.MTM) MTM, SUM(MTM.Net_MTM) Net_MTM
                FROM database_schema.VRM_Hedge1_Copy_Copy HEDGE LEFT JOIN database_schema.VRM_MTM1 MTM
                    ON HEDGE.Trade_ID = MTM.Trade_ID  WHERE CLient = 'Permira'    AND MTM.Valuation_Date = '2019-06-06'
                GROUP BY HEDGE.Client,  HEDGE.Fund,  MTM.MTM_Currency;
            '''
            mtm = VrmMtm1.objects.filter(
                                       trade__client=client,
                                       trade__cancellation__iexact='FALSE',
                                       trade__dummy__iexact='FALSE',
                                       trade__asset_class__iexact='FX',
                                       trade__value_date__gte=valuation_date,
                                       trade__trade_date__lte=valuation_date,
                                       valuation_date=valuation_date,
                                      ).values('trade__fund', 'mtm_currency').distinct().annotate(
                                           mtm_sum=Sum('mtm'),
                                           net_mtm_sum=Sum('net_mtm')
                                      ).order_by('trade__fund')

            cp_mtm = list(VrmMtm1.objects.filter(
                                                trade__client=client,
                                                trade__cancellation__iexact='FALSE',
                                                trade__dummy__iexact='FALSE',
                                                trade__asset_class__iexact='FX',
                                                trade__value_date__gte=valuation_date,
                                                trade__trade_date__lte=valuation_date,
                                                valuation_date=valuation_date,
                                            ).values('trade__counterparty', 'mtm_currency').distinct().annotate(
                                                mtm_sum=Sum('mtm'),
                                                net_mtm_sum=Sum('net_mtm')
                                            ).order_by('trade__counterparty'))

            resp_data = list(mtm)
            date_list = list(VrmMtm1.objects.filter(
                trade__client=client,
                trade__cancellation__iexact='FALSE',
                trade__dummy__iexact='FALSE',
                trade__asset_class__iexact='FX',
                valuation_date__gte=date.today() + relativedelta(months=-6),
            ).values_list('valuation_date', flat=True).distinct().order_by('-valuation_date'))

    except ObjectDoesNotExist as exception:
        print(traceback.format_exc())
    except Exception as exception:
        print(traceback.format_exc())
    finally:
        resp = {
            'data': resp_data,
            'cp_data': cp_mtm,
            'date_list': date_list,
            'valuation_date': valuation_date
        }
        return JsonResponse(resp)


@login_required(login_url='login')
@user_is_allowed
def ajax_client_fund_cp_mtm(request):
    if request.is_ajax() is False or request.method != 'POST':
        return HttpResponseBadRequest()
    try:
        resp_data = []
        date_list = []
        valuation_date = None
        resp_sum = 0
        resp_currency = None
        fund = request.POST.get('fund')
        try:
            client = VRMClient.objects.get(name=request.POST['client'])
        except VRMClient.DoesNotExist:
            raise Exception('Unable to find the client to process!')

        try:
            module = __import__(client.name.lower())
        except Exception as e:
            try:
                module = __import__(client.label.lower())
            except Exception as e:
                raise Exception('Unable to find the client to process!')

        if request.POST.get('valuation_date') is None or request.POST['valuation_date'] == '':
            valuation_date = VrmMtm1.objects.filter(trade__client=client,
                                                    trade__cancellation__iexact='FALSE',
                                                    trade__dummy__iexact='FALSE',
                                                    trade__asset_class__iexact='FX',
                                                    ). \
                                        aggregate(Max('valuation_date'))['valuation_date__max']
        else:
            valuation_date = datetime.datetime.strptime(request.POST['valuation_date'], "%Y-%m-%d")
        if VrmMtm1.objects.filter(trade__client=client,
                                  trade__cancellation__iexact='FALSE',
                                  trade__dummy__iexact='FALSE',
                                  trade__asset_class__iexact='FX',
                                  trade__fund__iexact=fund,
                                  trade__value_date__gte=valuation_date,
                                  trade__trade_date__lte=valuation_date,
                                  valuation_date=valuation_date,
                                  ).exists():
            '''
            Example query
            SELECT
                  HEDGE.Client,HEDGE.Fund, MTM.MTM_Currency, SUM(MTM.MTM) MTM, SUM(MTM.Net_MTM) Net_MTM
                FROM database_schema.VRM_Hedge1_Copy_Copy HEDGE LEFT JOIN database_schema.VRM_MTM1 MTM
                    ON HEDGE.Trade_ID = MTM.Trade_ID  WHERE CLient = 'Permira'    AND MTM.Valuation_Date = '2019-06-06'
                GROUP BY HEDGE.Client,  HEDGE.Fund,  MTM.MTM_Currency;
            '''
            all_mtm = list(VrmMtm1.objects.filter(
                                       trade__client=client,
                                       trade__cancellation__iexact='FALSE',
                                       trade__dummy__iexact='FALSE',
                                       trade__asset_class__iexact='FX',
                                       trade__fund__iexact=fund,
                                       trade__value_date__gte=valuation_date,
                                       trade__trade_date__lte=valuation_date,
                                       valuation_date=valuation_date,
                                      ).values('mtm_currency').distinct().annotate(
                                           mtm_sum=Sum('mtm'),
                                           net_mtm_sum=Sum('net_mtm')
                                      ))
            all_mtm[0]['trade__counterparty'] = 'All'

            resp_data = list(VrmMtm1.objects.filter(
                                                trade__client=client,
                                                trade__cancellation__iexact='FALSE',
                                                trade__dummy__iexact='FALSE',
                                                trade__asset_class__iexact='FX',
                                                trade__fund__iexact=fund,
                                                trade__value_date__gte=valuation_date,
                                                trade__trade_date__lte=valuation_date,
                                                valuation_date=valuation_date,
                                            ).values('trade__counterparty', 'mtm_currency').distinct().annotate(
                                                mtm_sum=Sum('mtm'),
                                                net_mtm_sum=Sum('net_mtm')
                                            ).order_by('trade__counterparty'))
            resp_data.append(all_mtm[0])
    except ObjectDoesNotExist as exception:
        print(traceback.format_exc())
    except Exception as exception:
        print(traceback.format_exc())
    finally:
        resp = {
            'data': resp_data,
        }
        return JsonResponse(resp)


@login_required(login_url='login')
@user_is_allowed
def ajax_client_trade_mtm(request):
    if request.is_ajax() is False or request.method != 'POST':
        return HttpResponseBadRequest()
    try:
        resp_data = []
        valuation_date = None
        fund = request.POST.get('fund')
        try:
            client = VRMClient.objects.get(name=request.POST['client'])
        except VRMClient.DoesNotExist:
            raise Exception('Unable to find the client to process!')

        try:
            module = __import__(client.name.lower())
        except Exception as e:
            try:
                module = __import__(client.label.lower())
            except Exception as e:
                raise Exception('Unable to find the client to process!')

        if request.POST.get('valuation_date') is None  or request.POST['valuation_date'] == '':
            valuation_date = VrmMtm1.objects.filter(trade__client=client,
                                                    trade__cancellation__iexact='FALSE',
                                                    trade__dummy__iexact='FALSE', trade__asset_class__iexact='FX',

                                                    ). \
                                        aggregate(Max('valuation_date'))['valuation_date__max']
        else:
            valuation_date = datetime.datetime.strptime(request.POST['valuation_date'], "%Y-%m-%d")
        if VrmMtm1.objects.filter(trade__client=client,
                                  trade__cancellation__iexact='FALSE',
                                  trade__dummy__iexact='FALSE',
                                  trade__asset_class__iexact='FX',
                                  trade__value_date__gte=valuation_date,
                                  trade__trade_date__lte=valuation_date,
                                  valuation_date=valuation_date,
                                  ).exists():
            '''
            Example query
            SELECT
                  HEDGE.Client,HEDGE.Fund, MTM.MTM_Currency, SUM(MTM.MTM) MTM, SUM(MTM.Net_MTM) Net_MTM
                FROM database_schema.VRM_Hedge1_Copy_Copy HEDGE LEFT JOIN database_schema.VRM_MTM1 MTM
                    ON HEDGE.Trade_ID = MTM.Trade_ID  WHERE CLient = 'Permira'    AND MTM.Valuation_Date = '2019-06-06'
                GROUP BY HEDGE.Client,  HEDGE.Fund,  MTM.MTM_Currency;
            '''
            resp_data = list(VrmMtm1.objects.filter(
                                                trade__client=client,
                                                trade__cancellation__iexact='FALSE',
                                                trade__dummy__iexact='FALSE',
                                                trade__asset_class__iexact='FX',
                                                trade__value_date__gte=valuation_date,
                                                trade__trade_date__lte=valuation_date,
                                                valuation_date=valuation_date,
                                            ).values('trade__trade_id',
                                                     'trade__fund',
                                                     'trade__trade_date',
                                                     'trade__value_date',
                                                     'trade__delivery_date',
                                                     'trade__style',
                                                     'trade__underlying',
                                                     'trade__direction',
                                                     'trade__notional_currency',
                                                     'trade__notional_amounts',
                                                     'trade__strike',
                                                     'trade__premium',
                                                     'trade__premium_payment_date',
                                                     'trade__counterparty',
                                                     'valuation_date',
                                                     'outright_forward',
                                                     'spot_ref',
                                                     'mtm_currency',
                                                     'mtm',
                                                     'net_mtm').order_by('-trade__trade_id'))
    except ObjectDoesNotExist as exception:
        print(traceback.format_exc())
    except Exception as exception:
        print(traceback.format_exc())
    finally:
        resp = {
            'data': resp_data,
        }
        return JsonResponse(resp)
