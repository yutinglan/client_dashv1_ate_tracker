from general.models import Funds
from database_schema.models import VrmHedge1CopyCopy
from webapi.common import module_loader, unique
from django.core.mail import send_mail
from django.conf import settings

def sync_fundassociation(vrm_client):
    module = module_loader(vrm_client)

    rows_to_create = []
    rows_to_cancel = []

    general_funds = list(Funds.objects.filter(
        client__client_name=vrm_client.name
    ).extra(
        select={'fund_id': 'Funds.id'}
    ).values(
        'fund_id',
        'fund_name'
    ))

    trades = list(VrmHedge1CopyCopy.objects.filter(
        client=vrm_client.name
    ).extra(
        select={'fund_name': 'fund'}
    ).values(
        'trade_id',
        'fund_name'
    ))

    deal_trades = list(module.models.Dealtrade.objects.exclude(
        cancellation=True
    ).values('deal_id', 'trade_id'))

    cashflows = list(module.models.Cashflows.objects.exclude(
        cancellation=True
    ).values('deal_id', 'fund__fund_name'))

    deal_valuations = list(module.models.DealValuation.objects.values('deal_id', 'fund__fund_name'))

    fund_associations = list(module.models.FundAssociation.objects.exclude(
        deal_id__isnull=True
    ).values('deal_id', 'fund_id'))

    for deal_trade in deal_trades:
        deal_trade['fund_name'] = \
            [trade['fund_name']
             for trade in trades
             if trade['trade_id'] == deal_trade['trade_id']][0]

        deal_trade['fund_id'] =\
            [general_fund['fund_id']
             for general_fund in general_funds
             if general_fund['fund_name'] == deal_trade['fund_name']][0]

        del deal_trade['trade_id']
        del deal_trade['fund_name']

    for cashflow in cashflows:
        cashflow['fund_id'] = \
            [general_fund['fund_id']
             for general_fund in general_funds
             if general_fund['fund_name'] == cashflow['fund__fund_name']][0]

        del cashflow['fund__fund_name']

    for deal_valuation in deal_valuations:
        deal_valuation['fund_id'] = \
            [general_fund['fund_id']
             for general_fund in general_funds
             if general_fund['fund_name'] == deal_valuation['fund__fund_name']][0]

        del deal_valuation['fund__fund_name']

    current_associations = unique(deal_trades + cashflows + deal_valuations)

    for current_association in current_associations:
        if current_association not in fund_associations:
            if current_association not in rows_to_create:
                rows_to_create += [current_association]

    for fund_association in fund_associations:
        if fund_association not in current_associations:
            if current_association not in rows_to_cancel:
                rows_to_cancel += [current_association]

    objects_to_create = [module.models.FundAssociation(**element) for element in rows_to_create]
    module.models.FundAssociation.objects.bulk_create(objects_to_create)

    if len(rows_to_cancel):
        title = "Fund Association Sync - Rows to Delete for {}".format(vrm_client.name)
        message = u"The following fund association rows are not present anywhere in {} client tables:\n\n".format(vrm_client)
        from_email = settings.EMAIL_HOST_USER
        to_email = settings.EMAIL_NOTIFICATIONS_DESTINATION

        for row in rows_to_cancel:
            message += str(row) + u"\n"

        send_mail(
            title,
            message,
            from_email,
            [to_email]
        )
