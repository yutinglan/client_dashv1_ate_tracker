import datetime
import logging
import traceback
from datetime import timezone
import pandas as pd
from dateutil.relativedelta import relativedelta
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Avg, Max
from django.http import HttpResponse, HttpResponseBadRequest, JsonResponse
from marketdata1.models import Vrmirdata, Vrmfrdcurvedata
from webapi.common import user_is_allowed
from database_schema.models import (VrmHedge1CopyCopy)
import requests
from client.models import VRMClient

logger = logging.getLogger(__name__)


@login_required(login_url='login')
@user_is_allowed
def api_get_interest_curve(request):
    if request.is_ajax() is False or request.method != 'POST':
        return HttpResponseBadRequest()
    try:
        underlying_list = []
        underlying = request.POST['underlying']
        valuation_date = Vrmirdata.objects.aggregate(Max('valuation_date'))[
                'valuation_date__max']
        underlying_list = list(Vrmirdata.objects.filter(
            valuation_date=valuation_date).distinct().values_list('underlying', flat=True))
        if underlying is None or underlying == '':
            underlying = (underlying_list or [None])[0]
        interest_curve_data = list(Vrmirdata.objects.filter(valuation_date=valuation_date, underlying=underlying,
                                                            security__lte=(datetime.datetime.now() + relativedelta(years=10)).
                                                            strftime("%Y-%m-%d"), security__gte=valuation_date)
                                   .values('security').annotate(Avg('value')).order_by('security'))

    except ObjectDoesNotExist as exception:
        print(traceback.format_exc())
    except Exception as exception:
        print(traceback.format_exc())
    finally:
        resp = {
            'underlying_list':underlying_list,
            'underlying':underlying,
            'data': interest_curve_data,
        }
        return JsonResponse(resp)


@login_required(login_url='login')
@user_is_allowed
def api_prepare_interest_curve_data(request):
    if request.is_ajax() is False or request.method != 'POST':
        return HttpResponseBadRequest()

    # creating workbook
    file_name_wb = 'interest_curve_'+str(int(datetime.datetime.now(tz=timezone.utc).timestamp() * 1000))+'.xlsx'

    writer = pd.ExcelWriter(file_name_wb, engine="xlsxwriter", date_format="dd/MM/yyyy")
    workbook = writer.book

    head_style = workbook.add_format({
        'bold': 1,
        'font_size': 26
    })

    column_style = workbook.add_format({
        'font_size': 11,
        'bold':1,
        'top':1,
        'bottom':5,
        'align':'center',
        'text_wrap': True
    })

    comment_style = workbook.add_format({
        'font_size': 9,
        'italic':1,
        'bold':1
    })

    num_style = workbook.add_format(
        {
            'font_size': 11,
            'num_format':'#,##0.00',
            'align':'right'
        }
    )

    strike_style = workbook.add_format(
        {
            'font_size': 11,
            'num_format':'0.000000',
            'align':'right'
        }
    )

    try:
        underlying_list = []
        valuation_date = Vrmirdata.objects.aggregate(Max('valuation_date'))[
                'valuation_date__max']
        underlying_list = list(Vrmirdata.objects.filter(
            valuation_date=valuation_date).distinct().values_list('underlying', flat=True))
        interest_curve_data = list(Vrmirdata.objects.filter(valuation_date=valuation_date, underlying__in=underlying_list,
                                                            security__lte=(datetime.datetime.now() + relativedelta(years=10)).
                                                            strftime("%Y-%m-%d"), security__gte=valuation_date)
                                   .values('security','underlying').annotate(Avg('value')).order_by('security','underlying'))
        interest_curve_data_df = pd.DataFrame(interest_curve_data)
        interest_curve_data_df['Valuation_Date'] = valuation_date
        interest_curve_data_df = interest_curve_data_df[['Valuation_Date', 'underlying', 'security', 'value__avg']]
        interest_curve_data_df.columns = ['Valuation Date', 'Underlying', 'Reset Date', 'Value']
        sheet = 'Interest Curve'
        interest_curve_data_df.to_excel(writer, sheet_name=sheet, index=False, header=True, startrow=0,
                                        engine='xlsxwriter')


    except ObjectDoesNotExist as exception:
        print(traceback.format_exc())
    except Exception as exception:
        print(traceback.format_exc())

    # # column header names, you can use your own headers here
    writer.save()
    writer.close()
    return JsonResponse({'data': file_name_wb}, content_type='application/json')


@login_required(login_url='login')
@user_is_allowed
def api_prepare_forward_curve_data(request):
    if request.is_ajax() is False or request.method != 'POST':
        return HttpResponseBadRequest()

    # creating workbook
    file_name_wb = 'Forward_Curve_'+str(int(datetime.datetime.now(tz=timezone.utc).timestamp() * 1000))+'.xlsx'

    writer = pd.ExcelWriter(file_name_wb, engine="xlsxwriter", date_format="dd/MM/yyyy")

    try:
        client = request.POST.get('client', '')
        underlying = list(VrmHedge1CopyCopy.objects.filter(client=client,
                                                      cancellation__iexact='FALSE',
                                                      dummy__iexact='FALSE',
                                                      asset_class__iexact='FX',
                                                      unwind__iexact='FALSE',).values_list('underlying',flat=True).distinct())
        try:
            client = VRMClient.objects.get(name=client)
            module = __import__(client.name.lower())
        except Exception as e:
            try:
                module = __import__(client.label.lower())
            except Exception as e:
                raise Exception('Unable to find the client to process!')

        if module:
            try:
                additional_underlying_obj = getattr(module.models, 'AdditionalUnderlying')
            except Exception as e:
                additional_underlying_obj = None

        if additional_underlying_obj:
            additional_underlying = list(
                additional_underlying_obj.objects.all().exclude(
                    underlying__in=underlying
                ).values('underlying').distinct()
            )
        for item in additional_underlying:
            underlying.append(item['underlying'])

        valuation_date = Vrmfrdcurvedata.objects.aggregate(Max('valuation_date'))[
                'valuation_date__max']
        forward_curve_data = list(Vrmfrdcurvedata.objects.filter(valuation_date=valuation_date, underlying__in=underlying)
                                   .values('valuation_date','underlying', 'delivery_date', 'spot_ref', 'outright_forward').order_by('underlying', 'delivery_date'))
        forward_curve_data_df = pd.DataFrame(forward_curve_data)
        try:
            forward_curve_data_df['valuation_date'] = pd.to_datetime(forward_curve_data_df['valuation_date']).dt.strftime('%d/%m/%Y')
            forward_curve_data_df['delivery_date'] = pd.to_datetime(forward_curve_data_df['delivery_date']).dt.strftime('%d/%m/%Y')
        except:
            pass
        forward_curve_data_df = forward_curve_data_df[['valuation_date','underlying', 'delivery_date', 'spot_ref', 'outright_forward']]
        forward_curve_data_df.columns = ['Valuation Date', 'Underlying', 'Delivery Date', 'Spot Ref', 'Outright Forward']
        sheet = 'Forward Curve'
        forward_curve_data_df.to_excel(writer, sheet_name=sheet, index=False, header=True, startrow=0,
                                        engine='xlsxwriter')
    except ObjectDoesNotExist as exception:
        print(traceback.format_exc())
    except Exception as exception:
        print(traceback.format_exc())

    # # column header names, you can use your own headers here
    writer.save()
    writer.close()
    return JsonResponse({'data': file_name_wb}, content_type='application/json')
