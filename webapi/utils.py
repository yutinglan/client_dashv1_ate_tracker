"""
Utility functions for the webapi
"""

from decimal import Decimal
from datetime import date, timedelta
from django.db.models import Aggregate, CharField

def multiply_normalize(value, multiple):
    """
    Converts floats to a Decimal object, multiplies them by the
    provided value. After this it truncates any trailing empty precision with
    the normalize function.
    """
    val = Decimal("{}".format(value)) * multiple
    normalized = val.normalize()
    return "{:f}".format(normalized)


def divide_normalize(value, divider):
    """
    Converts floats to a Decimal object, divides them by the
    provided value. After this it truncates any trailing empty precision with
    the normalize function.
    """
    val = Decimal("{}".format(value)) / divider
    normalized = val.normalize()
    return "{:f}".format(normalized)


# Common Reusable functions

def get_first_day(dt, d_years=0, d_months=0):
    """
     d_years, d_months are "deltas" to apply to dt

    :param dt:
    :param d_years:
    :param d_months:
    :return: first day of the month
    """
    y, m = dt.year + d_years, dt.month + d_months
    a, m = divmod(m-1, 12)
    return date(y+a, m+1, 1)


def get_last_day(dt):
    """

    :param dt:
    :return: last day of the month
    """
    return get_first_day(dt, 0, 1) + timedelta(-1)

class Concat(Aggregate):
    function = 'GROUP_CONCAT'
    template = '%(function)s(%(distinct)s%(expressions)s)'

    def __init__(self, expression, distinct=False, **extra):
        super(Concat, self).__init__(
            expression,
            distinct='DISTINCT ' if distinct else '',
            output_field=CharField(),
            **extra)