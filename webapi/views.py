import calendar
import datetime
import json
import logging
import math
import os
import traceback
from datetime import date, timedelta, timezone
from datetime import datetime as dt
import operator
import numpy as np
import pandas as pd
import requests
import boto3
import yaml
from openpyxl import load_workbook
from dateutil.relativedelta import relativedelta
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.core.paginator import Paginator
from django.db.models import (Avg, F, Func, Max, Q, Value as V, DurationField, ExpressionWrapper, FloatField, Value,
DateTimeField, CharField)
from django.db.models.functions import (TruncDate, TruncDay, TruncHour, TruncMinute, TruncSecond, Cast )
from django.db.models.aggregates import Min, Sum
from django.db.models.functions import Coalesce
from django.http import HttpResponse, HttpResponseBadRequest, JsonResponse
from django.utils.encoding import smart_str
from django.views import View
from django.core.files.base import ContentFile
from django.core.files.storage import default_storage
from numpy import min as np_min
from django.core.exceptions import PermissionDenied
from django.db import transaction

from client.models import VRMClient, VRMContentTab, VRMDisplayTab, VRMContentPermission
from database_schema.models import (VrmAggregateMtm, VrmCounterparty,
                                    VrmDailyNetExposure, VrmDealhedge1,
                                    VrmFund, VrmHedge1CopyCopy, VrmMtm1)
from dss_schema.models import (FixingdataTempBridgeTable,
                               LatestSpotRelatedData, TempBridgeTable, LiveSpotRateFeed)
from general.models import Ccys, Clients, Counterparties, Funds, Underlyings, SchemaFundCp, SchemaLegalCp
from hedges.models import InTheMoneyProbability, Lar, IndividualFutureTrade, Trades, Futuretrade, DealHedges, Deals
from marketdata1.models import Cpmonitor, Vrmfrdcurvedata
from audit.models import Action, LogCashflows, LogDealvaluation
from webapi.utils import (divide_normalize, get_first_day, get_last_day,
                          multiply_normalize)
from webapi.common import user_is_allowed, module_loader
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import Http404
from validus import settings
from common import get_last_business_day
import audit
from webapi.syncs import sync_fundassociation


logger = logging.getLogger(__name__)

MTM_DEFAULT_VALUE_SET = ['trade__trade_id',
                          'trade__fund',
                          'trade__trade_date',
                          'trade__value_date',
                          'trade__delivery_date',
                          'trade__style',
                          'trade__underlying',
                          'trade__direction',
                          'trade__notional_currency',
                          'trade__notional_amounts',
                          'trade__strike',
                          'trade__premium',
                          'trade__premium_currency',
                          'trade__premium_payment_date',
                          'trade__counterparty',
                          'trade__legal_entity',
                          'valuation_date',
                          'outright_forward',
                          'spot_ref',
                          'mtm_currency',
                          'mtm',
                          'net_mtm'
]

HEDGE1_DEFAULT_QUERYSET = {
    'cancellation__iexact': 'FALSE',
    'asset_class__iexact': 'FX',
    'dummy__iexact': 'FALSE',
}
PROPORTIONAL_TRADE_VALUE_SET = [
    'trade_id',
    'fund__fund_name',
    'trade_date',
    'value_date',
    'delivery_date',
    'style',
    'underlying',
    'direction',
    'notional_currency',
    'notional_amounts_proportional',
    'strike',
    'premium',
    'premium_currency',
    'premium_payment_date',
    # 'cp__counterparty_name',
    'entity__entity_name',
    'outright_forward_ref',
    'cp_id',
    'entity_id',
    'fund_id',
]

PROPORTIONAL_MTM_VALUE_SET = [
    'fund_id',
    'cp_id',
    'entity_id',
    'trade_id',
    'spot_ref',
    'outright_forward',
    'valuation_date',
    'mtm_currency',
    'mtm_proportional',
    'net_mtm_proportional',
]

REGULAR_TO_PROPORTIONAL_MTM_NAME_MAPPING = {
    'trade__trade_id': 'trade_id',
    'trade__fund': 'fund__fund_name',
    'trade__trade_date': 'trade_date',
    'trade__value_date': 'value_date',
    'trade__delivery_date': 'delivery_date',
    'trade__style': 'style',
    'trade__underlying': 'underlying',
    'trade__direction': 'direction',
    'trade__notional_currency': 'notional_currency',
    'trade__notional_amounts': 'notional_amounts_proportional',
    'trade__strike': 'strike',
    'trade__premium': 'premium',
    'trade__premium_currency': 'premium_currency',
    'trade__premium_payment_date': 'premium_payment_date',
    'trade__counterparty': 'cp__counterparty_name',
    'trade__legal_entity': 'entity__entity_name',
    'spot_ref': 'spot_ref',
    'outright_forward': 'outright_forward',
    'mtm': 'mtm_proportional',
    'net_mtm': 'net_mtm_proportional',
    'valuation_date':'valuation_date'
}

def market_dashboard_table(underlying, valuation_date):
    # if request.method == 'POST' and request.is_ajax():
    security_list = ['spot', '1m forward', '2m forward', '3m forward', '6m forward', '9m forward', '12m forward', '2y forward', '3y forward', '5y forward',
                     '10y forward', '20y forward']
    if valuation_date == '':
        valuation_date_max = TempBridgeTable.objects.aggregate(Max('valuation_date'))['valuation_date__max']
    else:
        valuation_date_max = valuation_date
    data = TempBridgeTable.objects.filter(underlying=underlying, security__in=security_list,
                                          valuation_date=valuation_date_max).values()
    if len(data)==0:
        valuation_date_max = TempBridgeTable.objects.aggregate(Max('valuation_date'))['valuation_date__max']
        data = TempBridgeTable.objects.filter(underlying=underlying, security__in=security_list,
                                          valuation_date=valuation_date_max).values()
    try:
        underlying_decimals = pd.DataFrame(list(Underlyings.objects.filter(asset_class='FX').values('underlying', 'forward_point_decimals_tr')))
        underlying_decimals['division'] = 10 ** (underlying_decimals['forward_point_decimals_tr'] * 1.0)
    except:
        pass
    spot = data.get(security='spot')
    data = list(data)
    security_dict = {
        'spot': 0,
        '1m forward': 0.083,
        '2m forward': 0.167,
        '3m forward': 0.25,
        '6m forward': 0.5,
        '9m forward': 0.75,
        '12m forward': 1,
        '2y forward': 2,
        '3y forward': 3,
        '5y forward': 5,
        '10y forward': 10,
        '20y forward': 20,
    }
    for item in data:
        item['pricing_type'] = 'Mid'
        if item['security'].lower() == 'spot':
            item['forward_points'] = 0
            item['outright_forward'] = '%.4f' % (spot['mid'] + (item['mid'] / 10000))
            item['outright_forward'] = float(item['outright_forward'])
            item['duration'] = security_dict[item['security'].lower()]
            item['annualised_bps_left'] = 0
            item['annualised_bps_right'] = 0
            item['outright_forward'] = '%.4f' % spot['mid']
            item['valuation_date'] = str(item['valuation_date'])
        else:
            item['forward_points'] = float(item['mid'])
            try:
                conversion = underlying_decimals.loc[underlying_decimals.underlying == item['underlying'], 'division'].values[0]
            except:
                conversion = 10000.0
            try:
                if item['underlying'] in ['USDINR', 'USDJPY', 'USDTHB', 'USDKRW']:
                    item['outright_forward'] = '%.2f' % (spot['mid'] + item['forward_points'] / conversion)
                else:
                    item['outright_forward'] = '%.4f' % (spot['mid'] + item['forward_points'] / conversion)
                # if item['underlying'] in ['USDBRL']:
                #     item['outright_forward'] = '%.4f' % (spot['mid'] + item['forward_points'])
                #     item['forward_points'] = item['forward_points'] * 10000
                # elif item['underlying'] in ['USDINR', 'USDJPY', 'USDTHB', 'USDKRW']:
                #     item['outright_forward'] = '%.2f' % (spot['mid'] + item['forward_points'] / 100)
                # elif item['underlying'] in ['USDCLP', 'USDIDR']:
                #     item['outright_forward'] = '%.4f' % (spot['mid'] + item['forward_points'] / 1)
                # else:
                #     item['outright_forward'] = '%.4f' % (spot['mid'] + item['forward_points'] / 10000)
                item['outright_forward'] = float(item['outright_forward'])
                item['duration'] = security_dict[item['security'].lower()]
                item['annualised_bps_left'] = round(math.log(spot['mid'] / item['outright_forward']) *
                                                    10000 / item['duration'])
                item['annualised_bps_right'] = round(math.log(item['outright_forward'] / spot['mid']) *
                                                     10000 / item['duration'])
                item['valuation_date'] = str(item['valuation_date'])
            except Exception as e:
                print(e)
    data = sorted(data, key=lambda k: k['duration'])
    columns = [
        {'headerName': 'As of', 'field': 'valuation_date','width': 90},
        {'headerName': 'Underlying', 'field': 'underlying','width': 110},
        {'headerName': 'Security', 'field': 'security','width': 120},
        {'headerName': 'Pricing Type', 'field': 'pricing_type','width': 110},
        {'headerName': 'Forward Points', 'field': 'forward_points','width': 130},
        {'headerName': 'Outright Forward', 'field': 'outright_forward','width': 140},
        {'headerName': 'Annualised bps - ' + spot['underlying'][:3] + ' Fund', 'field': 'annualised_bps_left', 'width': 200,},
        {'headerName': 'Annualised bps - ' + spot['underlying'][3:] + ' Fund', 'field': 'annualised_bps_right', 'width': 200},
    ]
    response = {
        'as_of_list': valuation_date_max,
        'data': data,
        'columns': columns,
    }
    return response


@login_required(login_url='login')
@user_is_allowed
def api_get_table_underlying(request):
    # if request.method == 'POST' and request.is_ajax():
    if not request.POST['underlying']:
        return HttpResponseBadRequest()
    underlying = request.POST['underlying']
    try:
        valuation_date = request.POST['valuation_date']
    except:
        pass
    response = market_dashboard_table(underlying, valuation_date)
    return JsonResponse(response)


@login_required(login_url='login')
@user_is_allowed
def api_get_forward_Curve(request):
    if request.is_ajax() is False or request.method != 'POST':
        return HttpResponseBadRequest()
    try:
        fwd_curve_data = []
        underlying = request.POST['underlying']

        if Vrmfrdcurvedata.objects.filter(underlying=underlying).exists():
            latest_valuation_date = Vrmfrdcurvedata.objects.filter(
                underlying=underlying).order_by('valuation_date'
                                                ).values('valuation_date').last()['valuation_date']
            fwd_curve_data = list(Vrmfrdcurvedata.objects.filter(
                underlying=underlying,
                valuation_date=latest_valuation_date
            ).values('delivery_date', 'outright_forward').order_by('delivery_date'))

    except ObjectDoesNotExist as exception:
        print(traceback.format_exc())
    except Exception as exception:
        print(traceback.format_exc())
    finally:
        resp = {
            'data': fwd_curve_data,
        }
        return JsonResponse(resp)


@login_required(login_url='login')
@user_is_allowed
def api_historic_spot_rates(request):
    if request.method == 'POST' and request.is_ajax():
        if not request.POST['underlying']:
            return HttpResponseBadRequest()
        underlying = request.POST['underlying']
        try:
            data = FixingdataTempBridgeTable.objects.filter(underlying=underlying, valuation_date__gte=(
                        datetime.date.today() - datetime.timedelta(days=3600)), pricing_type='mid').order_by(
                        'valuation_date').exclude(mid__isnull=True).values('valuation_date', 'mid')
        except ObjectDoesNotExist:
            data = False
        if data:
            data = list(data)
            give = list()
            for item in data:
                obj = dict()
                obj['x'] = str(item['valuation_date'])
                obj['y'] = round(item['mid'],4)
                give.append(obj)
            response = {'data': give}
        else:
            response = {'data': 'False'}
        return JsonResponse(response, content_type='application/json')
    else:
        return HttpResponseBadRequest()


@login_required(login_url='login')
@user_is_allowed
def api_historic_spot_rates_highcharts(request):
    if request.method == 'POST' and request.is_ajax():
        if not request.POST['underlying']:
            return HttpResponseBadRequest()
        underlying = request.POST['underlying']
        try:
            data = FixingdataTempBridgeTable.objects.filter(underlying=underlying, valuation_date__gte=(
                    datetime.date.today() - datetime.timedelta(days=3600))).order_by(
                    'valuation_date').exclude(mid__isnull=True).values('valuation_date', 'mid')
        except ObjectDoesNotExist:
            data = False
        if data:
            give = list()
            for item in data:
                obj = [item['valuation_date'], round(item['mid'],4) ]
                give.append(obj)
            response = {'data': give}
        else:
            response = {'data': 'False'}
        return JsonResponse(response, content_type='application/json')
    else:
        return HttpResponseBadRequest()


@login_required(login_url='login')
@user_is_allowed
def in_the_money_probability(request):
    if request.method == 'POST': #and request.is_ajax():   # TODO remove the comment when we don't need to use this function in R
        underlying = request.POST['underlying']
        try:
            underlying = Underlyings.objects.get(underlying=underlying).pk
        except ObjectDoesNotExist:
            underlying = False
        if underlying:
            duration = InTheMoneyProbability.objects.filter(underlying_id=underlying).order_by('duration').values_list('duration', flat=True)
            probability = InTheMoneyProbability.objects.filter(underlying_id=underlying).order_by('duration').values_list('probability', flat=True)
            dict_duration = {
                30: "1 Month",
                60: "2 Months",
                90: "3 Months",
                120: "4 Months",
                180: "6 Months",
                270: "9 Months",
                360: "1 Year",
                540: "1 1/2 Year",
                720: "2 Years",
                1080: "3 Years",
                1440: "4 Years",
                1800: "5 Years",
                2520: "7 Years",
                3600: "10 Years"
            }
            dur_list = []
            for item in duration:
                dur_list.append(dict_duration[item])
            probability = list(probability)
            response = {'duration': dur_list, 'probability': probability, 'empty': 'False'}
        else:
            response = {'empty': 'True'}
        return JsonResponse(response, content_type='application/json')
    else:
        return HttpResponseBadRequest()


@login_required(login_url='login')
@user_is_allowed
def in_the_money_highchart(request):
    if request.method == 'POST': #and request.is_ajax():   # TODO remove the comment when we don't need to use this function in R
        underlying = request.POST['underlying']
        try:
            underlying = Underlyings.objects.get(underlying=underlying).pk
        except ObjectDoesNotExist:
            underlying = False
        if underlying:
            data = InTheMoneyProbability.objects.filter(underlying_id=underlying).order_by('duration').values('duration', 'probability')
            dict_duration = {
                30: "1 Month",
                60: "2 Months",
                90: "3 Months",
                120: "4 Months",
                180: "6 Months",
                270: "9 Months",
                360: "1 Year",
                540: "1 1/2 Year",
                720: "2 Years",
                1080: "3 Years",
                1440: "4 Years",
                1800: "5 Years",
                2520: "7 Years",
                3600: "10 Years"
            }
            result = list()
            label = list()
            for item in data:
                label.append(dict_duration[item['duration']])
                result.append(item['probability'])
            response = {'data': result, 'category': label, 'empty': 'False'}
        else:
            response = {'empty': 'True'}
        return JsonResponse(response, content_type='application/json')
    else:
        return HttpResponseBadRequest()


@login_required(login_url='login')
@user_is_allowed
def ajax_client_request(request):
    # Expect an auto 'type' to be passed in via Ajax and POST
    if request.is_ajax() and request.method == 'POST':
        fund = SchemaFundCp.objects.filter(client=request.POST.get('client', '')).distinct()
        fund_dict = dict()
        res_data = dict()
        for fund_item in fund:
            fund_dict[fund_item.fund] = fund_item.fund
        res_data['fund'] = fund_dict

        return JsonResponse(res_data, content_type="application/json")
    else:
        return HttpResponseBadRequest()


@login_required(login_url='login')
@user_is_allowed
def ajax_underlying_request(request):
    underlying_dict = dict()
    spot_rate = dict()
    additional_underlying = list()
    module = None

    try:
        if request.is_ajax() and request.method == 'POST':
            client_type = request.POST.get('client', '')
            fund = request.POST.get('fund', '')
            user = request.user
            user.profile.recent_fund = fund
            user.save()

            try:
                client = VRMClient.objects.get(name=client_type)
                module = __import__(client.name.lower())
            except Exception as e:
                try:
                    module = __import__(client.label.lower())
                except Exception as e:
                    raise Exception('Unable to find the client to process!')

            if module:
                if not VRMContentTab.objects.filter(
                        label='proportional_mtm',
                        client__name=client
                ).exists():
                    underlying = list(
                        VrmHedge1CopyCopy.objects.filter(
                            client=client_type,
                            fund=fund,
                            **HEDGE1_DEFAULT_QUERYSET
                        ).values_list(
                            'underlying',
                            flat=True
                        ).distinct()
                    )
                else:
                    underlying = list(getattr(module.models, 'ProportionalTrade').
                                      objects.filter(fund_id= getattr(module.models, 'Fund').objects.get(fund_name=fund).pk,
                                      cancellation__iexact = 'FALSE').values_list('underlying', flat=True).distinct())
                try:
                    additional_underlying_obj = getattr(module.models, 'AdditionalUnderlying')
                except Exception as e:
                    additional_underlying_obj = None

            if additional_underlying_obj:
                additional_underlying = list(
                    additional_underlying_obj.objects.all().exclude(
                        underlying__in=underlying
                    ).values_list(
                        'underlying',
                        flat=True
                    ).distinct()
                )
            # Reason for adding the 2 list for few clients , we display additional underlying which is not part
            # of the trade.
            full_underlying_list = underlying + additional_underlying

            spot_rate_pairs = list(
                LatestSpotRelatedData.objects.filter(
                    underlying__in=full_underlying_list,
                    security="spot"
                ).values(
                    "underlying",
                    "valuation_date"
                ).distinct()
            )

            for pair in spot_rate_pairs:
                query = Q(underlying=pair['underlying'], valuation_date=pair['valuation_date'], security="spot")
                spot_rate[pair['underlying']] = LatestSpotRelatedData.objects.get(query).mid

    except Exception as e:
        print(traceback.format_exc())
        return HttpResponseBadRequest()
    finally:
        res_data = {
            'spot_rate': spot_rate
        }
        return JsonResponse(res_data, content_type='application/json')


@login_required(login_url='login')
@user_is_allowed
def ajax_notional_request(request):
    # Expect an auto 'type' to be passed in via Ajax and POST
    if request.is_ajax() and request.method == 'POST':
        client = request.POST.get('client', '')
        fund = request.POST.get('fund', '')
        user = User.objects.get(username=request.user.username)
        user.profile.recent_fund = fund
        user.save()

        fund_currency = VrmFund.objects.filter(client=request.POST.get('client', ''),
                                               fund=request.POST.get('fund', '')).distinct()

        notional = request.POST.get('underlying')
        if notional:
            notional = [notional[0:3], notional[3:]]

        if fund_currency[0].fund_currency in notional:
            ## Exception client to consider fund_cuurenrcy in setting files
            if client.lower() not in ['cordet']:
                for item in notional:
                    if item == fund_currency[0].fund_currency:
                        notional.remove(item)

        notional_dict = dict()
        res_data = dict()
        for notional_item in notional:
            notional_dict[notional_item] = notional_item
        res_data['notional_currency'] = notional_dict
        res_data['premium_currency'] = fund_currency[0].fund_currency


@login_required(login_url="login")
@user_is_allowed
def ajax_counterparties(request):
    if request.is_ajax() and request.method == "GET":
        try:
            client = request.GET["client"]
            fund = request.GET["fund"]
            entity = request.GET.get('entity')
            guarantor = request.GET.get('guarantor')
            # Not including expired is default
            include_expired_counterparties = request.GET.get(
                'include_expired_counterparties',
                'False'
            ).lower() == 'true'
        except KeyError:
            return HttpResponseBadRequest()

        condition = {}
        try:
            vrm_client = VRMClient.objects.get(name=client)
        except VRMClient.DoesNotExist as e:
            return HttpResponseBadRequest()
        try:
            module = module_loader(vrm_client)
        except Exception as e:
            raise Exception('Unable to find the client to process!')
        if not include_expired_counterparties:
            condition["value_date__gte"] = date.today()
        query_set = {'client': client}
        if entity and entity != '':
            query_set['legal_entity'] = entity
        elif guarantor and guarantor != '':
            trade_guarantor = getattr(module.models, 'Tradeguarantor')
            try:
                guarantor_obj = getattr(module.models, 'Guarantor')
                g_id = guarantor_obj.objects.get(guarantor_name=guarantor)
            except ObjectDoesNotExist as e:
                return HttpResponseBadRequest()
            query_set['trade_id__in'] = list(trade_guarantor.objects.filter(guarantor_id=g_id).values_list('trade_id',
                                                                                                       flat=True))
        else:
            query_set['fund'] = fund
        if VRMContentTab.objects.filter(label='proportional_mtm', client__name=vrm_client).exists():

            general_client = Clients.objects.get(client_name=client)
            general_fund = Funds.objects.get(fund_name=fund, client=general_client.pk)
            lar_query_set = {'fund_id': general_fund.pk}
            if entity and entity != '':
                try:
                    lar_query_set['entity_id'] = getattr(module.models, 'Entity').objects.get(entity_name=entity).pk
                except ObjectDoesNotExist as e:
                    pass
                except Exception as e:
                    pass

            counterparties = [Counterparties.objects.get(pk=id).counterparty_name for id in
                              getattr(module.models, 'ProportionalTrade').objects.filter(
                              **lar_query_set,
                              cancellation='FALSE',
                              **condition,
                              ).values_list("cp", flat=True).distinct()]
        else:
            counterparties = list(VrmHedge1CopyCopy.objects.filter(
                    **query_set,
                    dummy__iexact='FALSE',
                    cancellation='FALSE',
                    **condition,
                ).values_list("counterparty", flat=True).distinct())
            # else:
            #     counterparties = list(SchemaFundCp.objects.filter(client=client, fund=fund)
            #                           .values_list("counterparty", flat=True).distinct())

        return JsonResponse({
            "counterparties": counterparties
        })

@login_required(login_url="login")
@user_is_allowed
def ajax_future_counterparties(request):
    if request.is_ajax() and request.method == "GET":
        try:
            client = request.GET["client"]
            fund = request.GET["fund"]
            entity = request.GET.get('entity')
            guarantor = request.GET.get('guarantor')
            # Not including expired is default
            include_expired_counterparties = request.GET.get(
                'include_expired_counterparties',
                'False'
            ).lower() == 'true'
        except KeyError:
            return HttpResponseBadRequest()
        fund = request.GET.get('fund', '')
        user = User.objects.get(username=request.user.username)
        user.profile.recent_fund = fund
        user.save()

        condition = {}
        if not include_expired_counterparties:
            condition["value_date__gte"] = date.today()
        # trades for future are avaiable in UTAM
        try:
            vrm_client = VRMClient.objects.get(name=client)
        except VRMClient.DoesNotExist as e:
            return HttpResponseBadRequest()
        try:
            module = module_loader(vrm_client)
        except Exception as e:
            raise Exception('Unable to find the client to process!')
        # at the moment it is generic
        counterparties = [ Counterparties.objects.get(pk=broker).counterparty_name  for broker in
                          Trades.objects.filter(fund=Funds.objects.get(fund_name=fund)).
                              distinct().values_list("broker", flat=True) if broker is not None]
        return JsonResponse({
            "counterparties": counterparties
        })


@login_required(login_url="login")
@user_is_allowed
def ajax_user_recent_fund(request):
    if request.is_ajax() and request.method == 'POST':
        try:
            fund = request.POST.get('fund')
        except KeyError:
            return HttpResponseBadRequest()

        user = User.objects.get(username=request.user.username)
        user.profile.recent_fund = fund
        user.save()

        return JsonResponse({})


@login_required(login_url='login')
@user_is_allowed
def ajax_cpmontior(request):
    if request.is_ajax() and request.method == 'POST':
        counterparty = request.POST.get('counterparty', '')
        client = request.POST.get('client', '')
        fund = request.POST.get('fund', '')
        user = User.objects.get(username=request.user.username)
        user.profile.recent_fund = fund
        user.save()
        elements = None
        include_expired = VRMContentTab.objects.filter(
            label='include_expired_counterparties',
            client__name=client
        ).exists()
        query = {'client' : client,
                 'fund': fund,
                 **HEDGE1_DEFAULT_QUERYSET
                 }
        if include_expired:
            if counterparty == 'all':
                elements = list(set(cp['counterparty'] for cp in
                                    VrmHedge1CopyCopy.objects.filter( **query ).values('counterparty')))

            else:
                elements = list(set(cp['counterparty'] for cp in
                                    VrmHedge1CopyCopy.objects.filter(counterparty=counterparty, **query).values('counterparty')))
        else:
            if counterparty == 'all':
                elements = list(set(cp['counterparty'] for cp in
                                    SchemaFundCp.objects.filter(client=client,fund=fund).values('counterparty')))

            else:
                elements = list(set(cp['counterparty'] for cp in
                                    SchemaFundCp.objects.filter(counterparty=counterparty, client=client,fund=fund).values(
                                        'counterparty')))
        res_data = {}
        data_dict = []
        for item in Cpmonitor.objects.filter(counterparty__in=elements).values():
            data_dict.append(item)

        res_data['row'] = data_dict
        res_data['columns'] =[ {'headerName': x.column, 'field': x.attname}  for x in Cpmonitor._meta.get_fields() if x.attname !='moodys']
        count = 0
        for item in res_data['row']:
            if item['number_5y_cds']:
                item['number_5y_cds'] = round(item['number_5y_cds'])
            if item['number_1y_default_pro']:
                item['number_1y_default_pro'] = "{}%".format( round(100 *item['number_1y_default_pro'],3))
            if 'moodys' in item.keys():
                del res_data['row'][count]['moodys']
            count += 1
        count =0
        for item in res_data['columns']:
            if item['headerName'] in ['Counterparty','1Y_Default_Pro']:
                if item['headerName']=='1Y_Default_Pro':
                    item['headerName'] = '1Y Default Probability'
                item['width'] = 100
            elif  item['headerName'] in ['SP']:
                item['width'] = 60
            else:
                item['width'] = 65
            item['headerName'] = item['headerName'].replace('_', ' ')
            count += 1
        return JsonResponse(res_data, safe=False)
    else:
        return HttpResponseBadRequest()


@login_required(login_url='login')
@user_is_allowed
def ajax_cpsummation(request):
    """AJAX Counterparty Summation

    Valid HTTP Methods: POST

    Returns the payload to render a heatmap graph.

    The request has 4 valid parameters:
        - counterparty
        - client
        - fund
        - include_expired_counterparties [true|false]

    :param object request: The request objects.
    :return: A JSON response object
    :rtype: object
    """
    if request.is_ajax() is False or request.method != 'POST':
        return HttpResponseBadRequest()

    counterparty = request.POST.get('counterparty')
    fund = request.POST.get('fund')
    client = request.POST.get('client')
    entity = request.POST.get('entity')
    guarantor = request.POST.get('guarantor')
    include_expired_counterparties = request.POST.get(
        'include_expired_counterparties',
        'False'
    ).lower() == 'true'

    if None in [counterparty, client, fund]:
        return HttpResponseBadRequest()

    try:
        counterparties = []
        color = None
        data_list = []
        currency = None
        vrm_client, module, LAR, general_client, general_fund, general_entity, lar_queryset, individual_set, hedge_set = _get_module_lar(
            request)
        lar_query_set = {}
        condition = {}
        if not include_expired_counterparties:
            condition["value_date__gte"] = date.today()
        if vrm_client.entity_summary:
            agg_set = {}
            query_set = {'client': client}
            if counterparty != 'all':
                agg_set = {'cp': Counterparties.objects.get(counterparty_name=counterparty)}
                query_set = {'counterparty': counterparty}
                lar_query_set['cp_id'] = Counterparties.objects.get(counterparty_name=counterparty).pk
            if entity and entity != '':
                agg_set['entity'] = general_entity
                lar_query_set['entity_id'] = general_entity.pk
                query_set['legal_entity'] = entity

            else:
                agg_set['fund'] = getattr(module.models, 'Fund').objects.get(fund_name=general_fund.fund_name)
                lar_query_set['fund_id'] = general_fund.pk
                query_set['fund'] = fund

            # counterparties = [Counterparties.objects.get(pk=item).counterparty_name
            #                   for item in getattr(module.models, 'LegalCounterparty').objects.filter(
            #         **agg_set).values_list('cp', flat=True).distinct()]

            counterparties = []
            if VRMContentTab.objects.filter(label='proportional_mtm', client__name=vrm_client).exists():
                counterparties = [Counterparties.objects.get(pk=id).counterparty_name for id in  getattr(module.models, 'ProportionalTrade').objects.filter(
                    **lar_query_set,
                    cancellation='FALSE',
                    **condition,
                ).values_list("cp", flat=True).distinct()]
            else:
                counterparties = list(VrmHedge1CopyCopy.objects.filter(
                    **query_set,
                    dummy__iexact='FALSE',
                    cancellation='FALSE',
                    **condition,
                ).values_list("counterparty", flat=True).distinct())

            currency = getattr(module.models, 'LegalCounterparty').objects.filter(**agg_set).first().base_currency
        elif vrm_client.guarantor_summary and guarantor and guarantor != '':
            query_set = {'client': client}
            if counterparty != 'all':
                query_set = {'counterparty': counterparty}
            try:
                vrm_client = VRMClient.objects.get(name=client)
            except VRMClient.DoesNotExist as e:
                return HttpResponseBadRequest()
            try:
                module = module_loader(vrm_client)
            except Exception as e:
                raise Exception('Unable to find the client to process!')
            trade_guarantor = getattr(module.models, 'Tradeguarantor')
            try:
                guarantor_obj = getattr(module.models, 'Guarantor').objects.get(guarantor_name=guarantor)
                g_id = guarantor_obj.guarantor_id
            except ObjectDoesNotExist as e:
                return HttpResponseBadRequest()
            query_set['trade_id__in'] = list(trade_guarantor.objects.filter(guarantor_id=g_id).values_list('trade_id',
                                                                                                           flat=True))
            lar_query_set['guarantor_id'] = g_id
            counterparties = list(VrmHedge1CopyCopy.objects.filter(
                **query_set,
                dummy__iexact='FALSE',
                cancellation='FALSE',
                **condition,
            ).values_list("counterparty", flat=True).distinct())
            currency = guarantor_obj.local_ccy
        else:
            counterparties_query = None
            # VRM FUND no longer used.
            try:
                currency = getattr(module.models, 'Fund').objects.\
                    get(fund_name=general_fund.fund_name).ccy
            except ObjectDoesNotExist as e:
                currency = ''
            counterparties_query = VrmHedge1CopyCopy.objects.filter(
                client=client,
                fund=fund,
                **HEDGE1_DEFAULT_QUERYSET
            )
            lar_query_set = {
                'fund_id': general_fund.pk,
            }
            if include_expired_counterparties is False:
                counterparties_query = SchemaFundCp.objects.filter(client=client, fund=fund)
            counterparties = list(counterparties_query.values_list(
                'counterparty',
                flat=True
            ).distinct())

        list_val = []
        for index, item in enumerate(counterparties):
            general_counterparty = Counterparties.objects.get(counterparty_name=item, cancellation=0)
            liquidity_all = None
            if vrm_client.entity_summary and entity and entity != '':
                lar_query_set['counterparty_id'] = general_counterparty.pk
                liquidity_all = LAR.objects.filter(**lar_query_set)
            elif vrm_client.guarantor_summary and guarantor and guarantor != '':
                lar_query_set['counterparty_id'] = general_counterparty.pk
                liquidity_all = LAR.objects.filter(**lar_query_set)
            else:
                lar_query_set['counterparty_id'] = general_counterparty.pk
                try:
                    liquidity_all = LAR.objects.filter(**lar_query_set,
                                                       client = general_client.pk)
                except Exception as exception:
                    # Assuming it is client schema.
                    liquidity_all = LAR.objects.filter(**lar_query_set)
            liquidity_at_risk_insert_timestamp = liquidity_all.filter(
                insert_timestamp=liquidity_all.aggregate(Max('insert_timestamp'))['insert_timestamp__max'])
            liquidity_at_risk_max_cl = liquidity_at_risk_insert_timestamp.filter(
                confidence_level=liquidity_at_risk_insert_timestamp.aggregate(Max('confidence_level'))[
                    'confidence_level__max'])

            value = float("{0:.2f}".format(liquidity_at_risk_max_cl[0].lar if len(liquidity_at_risk_max_cl) > 0 else 0.00 / 10e6))
            list_val.append(value)
            item_list = round(value)

            if counterparty == "all" or counterparty == item:
                data_list.append(item_list)

        color = min(list_val) if len(list_val) > 0  else None
    except Exception as exception:
        print(traceback.format_exc())
    finally:
        cp = [counterparty] if counterparty != "all" else counterparties

        res_data = {
            'counterparty': cp,
            'color': color,
            'cp': data_list,
            'currency': currency,
        }
        return JsonResponse(res_data, content_type='application/json')


@login_required(login_url='login')
@user_is_allowed
def ajax_historicalmtm(request):
    if request.is_ajax() is False or request.method != 'POST':
        return HttpResponseBadRequest()

    fund = request.POST.get('fund')
    entity = request.POST.get('entity') or ''
    guarantor = request.POST.get('guarantor') or ''
    counterparty = request.POST.get('counterparty')
    client = request.POST.get('client')

    if None in [fund, counterparty, client]:
        return HttpResponseBadRequest()

    try:
        vrm_client = VRMClient.objects.get(name=client)
        try:
            module = module_loader(vrm_client)
        except Exception as e:
            raise Exception('Unable to find the client to process!')
        general_client = Clients.objects.get(client_name=client)
        general_fund = None
        query_set = {'client_id': general_client.id}
        agg_set = {}
        res_data = {}
        data_list = []
        target_dt = date.today() + relativedelta(months=-6)

        res_data['start_month'] = target_dt
        res_data['end_month'] = date.today()

        today = date.today()
        data = None
        if entity and entity != '':
            entity = module.models.Entity.objects.get(entity_name=entity)
            query_set['entity_id'] = entity.entity_id
            agg_set['entity'] = entity
        elif guarantor and guarantor != '':
            guarantor_obj = module.models.Guarantor.objects.get(guarantor_name=guarantor)
            query_set['guarantor_id'] = guarantor_obj.guarantor_id
        else:
            general_fund = Funds.objects.get(fund_name=fund, client=general_client.pk)
            query_set['fund_id'] = general_fund.id
            query_set['entity_id'] = None
            agg_set['fund'] = general_fund

        if counterparty == 'all':
            if not VRMContentTab.objects.filter(
                    label='proportional_mtm',
                    client__name=vrm_client
            ).exists():
                data = VrmAggregateMtm.objects.filter(**query_set,
                                                      cp_id=0,
                                                      valuation_date__gte=target_dt,
                                                      valuation_date__lte=date.today()).values('valuation_date',
                                                                                               'agg_mtm').order_by(
                    'valuation_date')
            else:
                if (entity and entity != ''):
                    data = VrmAggregateMtm.objects.filter(**query_set,
                                                          cp_id=0,
                                                          valuation_date__gte=target_dt,
                                                          valuation_date__lte=date.today()).values(
                        'valuation_date').annotate(agg_mtm=Sum('agg_mtm')).order_by('valuation_date')
                else:
                    data = VrmAggregateMtm.objects.filter(**query_set,
                                                          cp_id=0,
                                                          valuation_date__gte=target_dt,
                                                          valuation_date__lte=date.today()).\
                        values('valuation_date', 'agg_mtm').order_by('valuation_date')

        else:
            query_set['cp_id'] = Counterparties.objects.get(counterparty_name=counterparty, cancellation=0).pk
            agg_set['cp_id'] = Counterparties.objects.get(counterparty_name=counterparty, cancellation=0).pk
            if not VRMContentTab.objects.filter(
                                label='proportional_mtm',
                                client__name=vrm_client
                        ).exists():
                data = VrmAggregateMtm.objects.filter(**query_set,
                                                  valuation_date__gte=target_dt,
                                                  valuation_date__lte=date.today()).values('valuation_date', 'agg_mtm').order_by('valuation_date')
            else:
                if ( entity and entity != ''):
                    data = VrmAggregateMtm.objects.filter(**query_set,
                                                  valuation_date__gte=target_dt,
                                                  valuation_date__lte=date.today()).values('valuation_date').annotate(agg_mtm=Sum('agg_mtm')).order_by('valuation_date')
                else:
                    data = VrmAggregateMtm.objects.filter(**query_set,
                                                          valuation_date__gte=target_dt,
                                                          valuation_date__lte=date.today()).\
                        values('valuation_date', 'agg_mtm').order_by('valuation_date')
        if vrm_client.entity_summary:
            # client schema then use the client fund, fund in the request
            agg_set['fund'] = getattr(module.models,'Fund').\
                objects.get(fund_name=request.POST.get('fund'))
            res_data['currency'] = getattr(module.models, 'LegalCounterparty').\
                objects.filter(**agg_set).first().base_currency
        elif guarantor and guarantor != '':
            res_data['currency'] = guarantor_obj.local_ccy
        else:
            try:
                res_data['currency'] = getattr(module.models, 'Fund').objects. \
                    get(fund_name=general_fund.fund_name).ccy
            except ObjectDoesNotExist as e:
                res_data['currency'] = ''
            # res_data['currency'] = VrmFund.objects.filter(
            #     client= general_client.client_name,
            #     fund=general_fund.fund_name
            # ).first().fund_currency

        for item in data:
            data_list.append([
                            item['valuation_date'],
                            round(item['agg_mtm'], 2)
                        ])

        res_data['hmtm'] = data_list
        return JsonResponse(res_data)
    except ObjectDoesNotExist as exception:
        print(traceback.format_exc())
        return JsonResponse({
            'hmtm':[],
            'start_month': date.today(),
            'end_month': date.today(),
            'currency': None
        })
    except Exception as exception:
        print(traceback.format_exc())
        return JsonResponse({
            'hmtm':[],
            'start_month': date.today(),
            'end_month': date.today(),
            'currency': None
        })


@login_required(login_url='login')
@user_is_allowed
def ajax_future_historicalmtm(request):
    if request.is_ajax() is False or request.method != 'POST':
        return HttpResponseBadRequest()

    fund = request.POST.get('fund')
    entity = request.POST.get('entity') or ''
    guarantor = request.POST.get('guarantor') or ''
    counterparty = request.POST.get('counterparty')
    client = request.POST.get('client')

    if None in [fund, counterparty, client]:
        return HttpResponseBadRequest()

    try:
        vrm_client = VRMClient.objects.get(name=client)
        try:
            module = module_loader(vrm_client)
        except Exception as e:
            raise Exception('Unable to find the client to process!')
        general_client = Clients.objects.get(client_name=client)
        general_fund = None
        query_set = {'client_id': general_client.id}
        agg_set = {}
        res_data = {}
        data_list = []
        target_dt = date.today() + relativedelta(months=-6)

        res_data['start_month'] = target_dt
        res_data['end_month'] = date.today()
        futuresummary = getattr(module.models, 'FutureSummary')
        today = date.today()
        data = None
        if counterparty == 'all':
            data = futuresummary.objects.filter(fund=fund, valuation_date__gte=target_dt, valuation_date__lte
                                    =date.today()).values('valuation_date').distinct().order_by('valuation_date').\
                                    annotate(agg_mtm=Sum('mtm'))

        else:
            # missing counterparty
            data = futuresummary.objects.filter(fund=fund, counterparty=counterparty, valuation_date__gte=target_dt,
                                                valuation_date__lte=date.today()).values(
                'valuation_date').distinct().order_by('valuation_date'). \
                annotate(agg_mtm=Sum('mtm'))
            #   fund=general_fund.fund_name
        res_data['currency'] = futuresummary.objects.filter().first().mtm_currency

        for item in data:
            data_list.append([
                            item['valuation_date'],
                            round(item['agg_mtm'], 2)
                        ])

        res_data['hmtm'] = data_list
        return JsonResponse(res_data)
    except ObjectDoesNotExist as exception:
        print(traceback.format_exc())
        return JsonResponse({
            'hmtm':[],
            'start_month': date.today(),
            'end_month': date.today(),
            'currency': None
        })
    except Exception as exception:
        print(traceback.format_exc())
        return JsonResponse({
            'hmtm':[],
            'start_month': date.today(),
            'end_month': date.today(),
            'currency': None
        })


def _get_module_lar(request):
    try:
        vrm_client, module, LAR, general_client, \
        general_fund, general_entity, lar_queryset, individual_set, hedge_set = None, None, None, None, None, \
                                                                                None, {}, {}, {}
        if 'label' in request.POST:
            vrm_client = VRMClient.objects.get(label=request.POST['label'])
        else:
            vrm_client = VRMClient.objects.get(name=request.POST['client'])
        try:
            module = module_loader(vrm_client)
        except Exception as e:
            raise Exception('Unable to find the client to process!')
        # TODO remove this once what exactly is required to be processed for LAR level
        # Assume this will become the schema level model for timebeing switch between hedges Lar and schema Lar
        general_client = Clients.objects.get(client_name=vrm_client.name)
        general_fund = None
        if request.POST['fund'] != '':
            general_fund = Funds.objects.get(fund_name=request.POST['fund'], client=general_client.pk)
            lar_queryset = {'fund': general_fund}
            individual_set = {'fund': general_fund}
        if vrm_client.entity_summary:
            LAR = getattr(module.models, 'Lar')
            if request.POST.get('entity') and request.POST.get('entity') != '':
                general_entity = module.models.Entity.objects.get(entity_name=request.POST.get('entity'))
                lar_queryset['entity'] = general_entity
                individual_set = {'entity': general_entity}
                hedge_set = {'legal_entity': general_entity.entity_name}
        elif vrm_client.guarantor_summary:
            LAR = getattr(module.models, 'Lar')
            if request.POST.get('guarantor') and request.POST.get('guarantor') != '':
                guarantor_obj = module.models.Guarantor.objects.get(guarantor_name=request.POST.get('guarantor'))
                lar_queryset['guarantor'] = guarantor_obj
                individual_set = {'guarantor': guarantor_obj}
                hedge_set = {'trade_id__in': list(module.models.Tradeguarantor.objects.filter(guarantor_name=request.POST.get('guarantor')).values_list('trade_id', flat=True))}
        else:
            # already imported
            LAR = getattr(__import__('hedges').models, 'Lar')
            lar_queryset = {'fund': general_fund.pk, 'client': general_client.pk}
            hedge_set = {'fund': general_fund.fund_name}
    except ObjectDoesNotExist as exception:
        raise Exception('Unable to find the client to process!')
    except Exception as e:
        print(e)
    finally:
        return vrm_client, module, LAR, general_client, general_fund, general_entity, lar_queryset, individual_set, hedge_set


@login_required(login_url='login')
@user_is_allowed
def liquidity_at_risk(request):
    if request.is_ajax() is False or request.method != 'POST':
        return HttpResponseBadRequest()

    client_label = request.POST.get('label')
    fund = request.POST.get('fund')
    entity_name = request.POST.get('entity')
    # TODO need to address this issue.
    user = User.objects.get(username=request.user.username)
    user.profile.recent_fund = fund
    user.save()
    if None in [client_label, fund]:
        response = {
            'total_lar': None,
            'total': None,
            'confidence_level': '95%',
            'data': [],
            'margin_txt': None
        }
        return JsonResponse(response)
    select_hedging_list = []
    value_hedging_list = []
    hedge_cost = None
    module = None
    lar_list = []
    data = []
    summary_list_option = []
    # Overall (44) and related fund (43) overall
    try:
        overall_cp = Counterparties.objects.get(pk=44)
    except ObjectDoesNotExist as exception:
        overall_cp = None
    try:
        related_cp = Counterparties.objects.get(pk=43)
    except ObjectDoesNotExist as exception:
        related_cp = None
    try:
        vrm_client, module, LAR, general_client, general_fund, general_entity, lar_queryset,individual_set, hedge_set = _get_module_lar(request)
        # time to set if any trade summary list option
        if vrm_client.label.lower() in ['cvc']:
            # This needs to be updated for the client in admin panel not to be hardcoded.
            summary_list_option = {1: 'Asset', 2: 'Subscription'}
        elif vrm_client.label.lower() in ['permira']:
            # This needs to be updated for the client in admin panel not to be hardcoded.
            summary_list_option = {1: 'Principal only', 2:'Interest Only'}
        # 43 and 44 are Overall and Related Fund Counterparties should be ignored
        counterparties = [Counterparties.objects.get(pk=item['counterparty_id'], cancellation=0) for item in
                          LAR.objects.filter(
                **lar_queryset,
                confidence_level__gt=0.9,
            ).values('counterparty_id').exclude(counterparty__in=[overall_cp, related_cp]).annotate(Max('insert_timestamp'))]

        counterparties = list(sorted(counterparties, key=operator.attrgetter('counterparty_name')))
        # Now LAR is calculated at Fund level and Related Fund level as well. but below code exist to support
        # the previous flow.
        if LAR.objects.filter(**lar_queryset, counterparty=overall_cp,
                              confidence_level__gt=0.9).exists():

            # TODO need to  inform the clients before turning this feature , but database will be
            #  updated by daily script as this will affect all the clients.
            # try:
            #     total = Lar.objects.filter(fund=general_fund.pk, client=general_client.pk, counterparty=overall_cp,
            #                                confidence_level__gt=0.9).latest('valuation_date').lar
            #     total = "{0:.2f}".format(total/ 10e5)
            #     lar_list.append({'id': general_fund.fund_name, 'value': total})
            # except ObjectDoesNotExist as exception:
            #     total = 1

            # TODO Until client agrees need to display the sum of counterparties probablity.
            # expecting this might be added to visual after confirmation

            liquidity_all = LAR.objects.filter(**lar_queryset).exclude(counterparty__in=[overall_cp, related_cp])
            #  TODO: 404 is not an appropriate response code
            if liquidity_all.count() == 0:
                # return JsonResponse({}, status=404)
                response = {
                    'total_lar': None,
                    'total': None,
                    'confidence_level': '95%',
                    'data': [],
                    'margin_txt': None
                }
                return JsonResponse(response)

            liquidity_at_risk_insert_timestamp = liquidity_all.filter(
                insert_timestamp=liquidity_all.aggregate(
                    Max('insert_timestamp')
                )['insert_timestamp__max']
            )

            liquidity_at_risk_max_cl = liquidity_at_risk_insert_timestamp.filter(
                confidence_level=liquidity_at_risk_insert_timestamp.aggregate(
                    Max('confidence_level')
                )['confidence_level__max']
            )

            total = liquidity_at_risk_max_cl.aggregate(Sum('lar'))
            total = "{0:.2f}".format(total['lar__sum'] / 10e5)
            if general_client.client_name == 'CVC':
                lar_list.append({'id': general_fund.fund_name, 'value': total})
                try:
                    related_total = LAR.objects.filter(**lar_queryset,
                                                       counterparty=related_cp,
                                                       confidence_level__gt=0.9).latest('valuation_date').lar
                    related_total = "{0:.2f}".format(related_total / 10e5)
                    lar_list.append({'id': 'Overall', 'value': related_total})
                except ObjectDoesNotExist as exception:
                    related_total = 1
        else:
            liquidity_all = LAR.objects.filter(**lar_queryset).exclude(counterparty__in=[overall_cp, related_cp])
            #  TODO: 404 is not an appropriate response code
            if liquidity_all.count() == 0:
                # return JsonResponse({}, status=404)
                response = {
                    'total_lar': 0,
                    'total': 0,
                    'confidence_level': '95%',
                    'data': [],
                    'margin_txt': None,
                    'summary_list_option':summary_list_option,
                }
                return JsonResponse(response)

            liquidity_at_risk_insert_timestamp = liquidity_all.filter(
                insert_timestamp=liquidity_all.aggregate(
                    Max('insert_timestamp')
                )['insert_timestamp__max']
            )

            liquidity_at_risk_max_cl = liquidity_at_risk_insert_timestamp.filter(
                confidence_level=liquidity_at_risk_insert_timestamp.aggregate(
                    Max('confidence_level')
                )['confidence_level__max']
            )

            total = liquidity_at_risk_max_cl.aggregate(Sum('lar'))
            total = "{0:.2f}".format(total['lar__sum']/10e5)
            if general_client.client_name == 'CVC':
                lar_list.append({'id': general_fund.fund_name, 'value': total})

        # TOdo need to check everything works fine here.
        margin_txt = ''
        for item in counterparties:
            margin_call = LAR.objects.filter(
                **lar_queryset,
                counterparty=item.id,
                confidence_level__gt=0.9
            ).order_by('-insert_timestamp')[0]
            data.append({'counterparty':item.counterparty_name,
                'probability': str(round(margin_call.probability * 100, 2)) + '%'})
            if margin_call.probability > 0:
                margin_txt += '{} : {}% to  get a margin  call in the  next  6   months  from {}'.\
                                format(fund, margin_call.probability, item.counterparty_name)
        if not margin_txt:
            margin_txt = 'No margin call is expected in the next 6 months.'
        try:
            try:
                module = __import__(vrm_client.name.lower())
            except Exception as e:
                try:
                    module = __import__(vrm_client.label.lower())
                except Exception as e:
                    raise Exception('Unable to find the client to process!')
            SummaryTable = getattr(module.models, 'SummaryTable')
            if SummaryTable:
                if general_client.client_name == 'CVC':
                    # get all different type of underlying
                    hedge_list = ['Overall','Exposure']
                    underlying_list = VrmHedge1CopyCopy.objects.filter(client=vrm_client.name, fund=fund,
                                                                       **HEDGE1_DEFAULT_QUERYSET,
                                                                       value_date__gte=date.today()). \
                        values( 'underlying').order_by('underlying').distinct()
                    for ul in underlying_list:
                        hedge_list.append(ul['underlying'])
                    # annulaised bps / monthly bps
                    value_hedging_list = [{'abps': [], 'mbps': []}]
                    count = 0
                    for actual_deal in hedge_list:
                        # 0 - Annualised bps
                        abps_val = 0
                        if SummaryTable.objects.filter(fund_id=general_fund.id, deal_name=actual_deal, bps=0).exists():
                             abps_val = SummaryTable.objects.filter(fund_id=general_fund.id, deal_name=actual_deal, bps=0). \
                                         order_by('-update_date').values('hedging_cost')[0]['hedging_cost'] or 0
                        value_hedging_list[0]['abps'].append({'id': count, 'val': abps_val})
                        # 0 - Monthly bps
                        mbps_val = 0
                        if SummaryTable.objects.filter(fund_id=general_fund.id, deal_name=actual_deal, bps=1).exists():
                            mbps_val = \
                            SummaryTable.objects.filter(fund_id=general_fund.id, deal_name=actual_deal, bps=1). \
                                order_by('-update_date').values('hedging_cost')[0]['hedging_cost'] or 0
                        value_hedging_list[0]['mbps'].append({'id': count, 'val': mbps_val})
                        count = count+1
                    count = 0
                    for hl_item in hedge_list:
                        select_hedging_list.append({'id': count, 'val': hl_item })
                        count = count + 1

                else:
                    #Todo need to validate if heding cost exist and we have started to use ID TODO
                    try:
                        if SummaryTable.objects.filter(fund=general_fund, deal_name='Overall').exists():
                            hedge_cost = SummaryTable.objects.filter(fund=general_fund, deal_name='Overall').\
                                                order_by('-update_date').values('hedging_cost')[0]['hedging_cost'] or 0
                        else:
                            hedge_cost = 0
                    except:
                        hedge_cost = 0

        except Exception as e:
            print(traceback.format_exc())

        response = {
            'total_lar': total,
            'total': float(total),
            'confidence_level': '95%',
            'data': data,
            'margin_txt': margin_txt,
            'hedge_cost': hedge_cost,
            'select_hedging_list': select_hedging_list,
            'value_hedging_list': value_hedging_list,
            'lar_list': lar_list,
            'summary_list_option': summary_list_option,

        }

        return JsonResponse(response)
    except ObjectDoesNotExist as exception:
        print(traceback.format_exc())
        response = {
            'total_lar': 0,
            'total': 0,
            'confidence_level': '95%',
            'data': [],
            'margin_txt': None,
            'select_hedging_list': [],
            'value_hedging_list': [],
            'lar_list': [],
            'summary_list_option': [],

        }
        return JsonResponse(response)
    except Exception as e:
        print(e)
        response = {
            'total_lar': 0,
            'total': 0,
            'confidence_level': '95%',
            'data': [],
            'margin_txt': None,
            'select_hedging_list': [],
            'value_hedging_list': [],
            'lar_list': [],
            'summary_list_option': [],
        }
        return JsonResponse(response)
    return HttpResponseBadRequest()


@login_required(login_url='login')
@user_is_allowed
def liquity_summary(request):
    """AJAX Trade Mark-to-Market Table

    Valid HTTP Methods: POST

    Returns the payload to render a 'Hedge Liquidity' column chart,
    and a table of all trades.

    The request has 4 valid parameters:
        - client
        - fund
        - include_expired_counterparties [true|false]

    :param object request: The request objects.
    :return: A JSON response object
    :rtype: object
    """

    if request.is_ajax() is False or request.method != 'POST':
        return HttpResponseBadRequest()
    resp_sum = 0
    fund = request.POST.get('fund')
    client = request.POST.get('client')
    entity = request.POST.get('entity') or ''
    if None in [fund, client]:
            return HttpResponseBadRequest()
    include_expired_counterparties = request.POST.get(
        'include_expired_counterparties',
        'False'
    ).lower() == 'true'
    # Getting all relevant fields
    vrm_client, module, LAR, general_client, general_fund, general_entity, lar_queryset, \
    individual_set, hedge_set = _get_module_lar(request)

    # Overall (44) and related fund (43) overall must be excluded unless total is required
    try:
        overall_cp = Counterparties.objects.get(pk=44)
    except ObjectDoesNotExist as exception:
        overall_cp = None
    try:
        related_cp = Counterparties.objects.get(pk=43)
    except ObjectDoesNotExist as exception:
        related_cp = None
    liquidity_all = LAR.objects.filter(**lar_queryset).exclude(
        counterparty__in=[overall_cp, related_cp])

    counterparties_query = VrmHedge1CopyCopy.objects.filter(
        client=client,
        **hedge_set,
        **HEDGE1_DEFAULT_QUERYSET
    )
    if include_expired_counterparties is False:
        if 'legal_entity' not in hedge_set.keys():
            counterparties_query = SchemaFundCp.objects.filter(client=client, fund=fund)
        else:
            counterparties_query = counterparties_query.filter(
                value_date__gte=date.today()
            )

    counterparties = [Counterparties.objects.get(counterparty_name=item['counterparty'], cancellation=0) for item in
                      counterparties_query.values('counterparty').distinct()]
    counterparties = list(sorted(counterparties, key=operator.attrgetter('counterparty_name')))
    threshold = []
    total_lar = []
    individual_mtm = []

    if liquidity_all.count() != 0:
        for item in counterparties:
            try:
                # if vrm_client.entity_summary:
                if 'fund' in individual_set.keys():
                    individual_set['fund'] = getattr(module.models, 'Fund'). \
                        objects.get(fund_name=individual_set['fund'].fund_name)
                cp_obj = getattr(module.models, 'LegalCounterparty').objects.filter(**individual_set,
                                                                                    cp=Counterparties.
                                                                                    objects.get(
                                                                                        counterparty_name=
                                                                                        item.counterparty_name, cancellation=0)
                                                                                    )
                # else:
                #     cp_obj = VrmCounterparty.objects.filter(fund=general_fund.fund_name,
                #                                             client=general_client.client_name,
                #                                             counterparty=item.counterparty_name)
                if len(cp_obj) > 0 and client != "UTAM":
                    if cp_obj[0].threshold and cp_obj[0].mta:
                        threshold.append(round((cp_obj[0].threshold - cp_obj[0].mta) / 1000000, 2))
                    else:
                        if cp_obj[0].threshold:
                            threshold.append(round(cp_obj[0].threshold / 1000000, 2))
                        elif cp_obj[0].mta:
                            threshold.append(round(cp_obj[0].mta / 1000000, 2) * -1)
                        else:
                            threshold.append(None)
                else:
                    threshold.append(None)
                iliquidity_all = LAR.objects.filter(**lar_queryset,
                                                    counterparty=Counterparties.objects.get(
                                                        counterparty_name=item.counterparty_name, cancellation=0),
                                                    confidence_level__gt=0.9)
                iliquidity_at_risk_insert_timestamp = iliquidity_all.filter(
                    insert_timestamp=iliquidity_all.aggregate(
                        Max('insert_timestamp')
                    )['insert_timestamp__max']
                )

                iliquidity_at_risk_max_cl = iliquidity_at_risk_insert_timestamp.filter(
                    confidence_level=iliquidity_at_risk_insert_timestamp.aggregate(
                        Max('confidence_level')
                    )['confidence_level__max']
                )
                itotal = iliquidity_at_risk_max_cl.aggregate(lar__sum=Coalesce(Sum('lar'), V(0)))

                try:
                    if general_entity:
                        if not VRMContentTab.objects.filter(
                                label='proportional_mtm',
                                client__name=vrm_client
                        ).exists():
                            agg_mtm = VrmAggregateMtm.objects.filter(
                                client_id=general_client.pk,
                                entity_id=general_entity.pk,
                                cp_id=Counterparties.objects.get(counterparty_name=item.counterparty_name, cancellation=0).id
                            ).latest('valuation_date').agg_mtm
                        else:
                            agg_mtm = VrmAggregateMtm.objects.filter(
                                client_id=general_client.pk,
                                entity_id=general_entity.pk,
                                fund_id=general_fund.pk,
                                cp_id=Counterparties.objects.get(counterparty_name=item.counterparty_name, cancellation=0).id
                            ).latest('valuation_date').agg_mtm
                    else:
                        agg_mtm = VrmAggregateMtm.objects.filter(
                            client_id=general_client.pk,
                            fund_id=general_fund.pk,
                            cp_id=Counterparties.objects.get(counterparty_name=item.counterparty_name, cancellation=0).id
                        ).latest('valuation_date').agg_mtm
                except ObjectDoesNotExist:
                    agg_mtm = 0
                if agg_mtm != 0 or total_lar!= 0:
                    individual_mtm.append(
                        round(agg_mtm /1000000, 2))
                    total_lar.append(round(itotal['lar__sum'] / 1000000, 2))
            except Exception as e:
                print(traceback.format_exc())
                threshold = []
                total_lar = []
                individual_mtm = []
    return JsonResponse({

        'mtm_sum': resp_sum,
        'total_lar': total_lar,
        'threshold': threshold,
        'individual_mtm': individual_mtm,
        'counterparties': [cp.counterparty_name for cp in counterparties]

    })

@login_required(login_url='login')
@user_is_allowed
def trade_mtm_table(request):
    """AJAX Trade Mark-to-Market Table

    Valid HTTP Methods: POST

    Returns the payload to render a 'Hedge Liquidity' column chart,
    and a table of all trades.

    The request has 4 valid parameters:
        - client
        - fund
        - include_expired_counterparties [true|false]

    :param object request: The request objects.
    :return: A JSON response object
    :rtype: object
    """

    if request.is_ajax() is False or request.method != 'POST':
        return HttpResponseBadRequest()
    resp_sum = 0
    fund = request.POST.get('fund')
    client = request.POST.get('client')
    entity = request.POST.get('entity') or ''
    if None in [fund, client]:
            return HttpResponseBadRequest()
    include_expired_counterparties = request.POST.get(
        'include_expired_counterparties',
        'False'
    ).lower() == 'true'

    trade_summary_options = 0
    summary_list_option = []
    if 'trade_summary_options' in request.POST:
        trade_summary_options = int(request.POST.get('trade_summary_options'))
    # Getting all relevant fields
    vrm_client, module, LAR, general_client, general_fund, general_entity, lar_queryset,\
        individual_set, hedge_set = _get_module_lar(request)

    # Overall (44) and related fund (43) overall must be excluded unless total is required
    try:
        overall_cp = Counterparties.objects.get(pk=44)
    except ObjectDoesNotExist as exception:
        overall_cp = None
    try:
        related_cp = Counterparties.objects.get(pk=43)
    except ObjectDoesNotExist as exception:
        related_cp = None

    query = {
        'trade__fund': fund,
        'trade__client': client,
        'trade__value_date__gte': date.today(),
        'trade__cancellation__iexact': 'FALSE',
        'trade__dummy__iexact': 'FALSE',
        'trade__asset_class__iexact':'FX',

    }
    if entity and entity != '':
        query['trade__legal_entity'] = entity

    exclude_trade = {}
    if vrm_client.trade_summary_options:
        if trade_summary_options == 1:
            if client.lower() in ['cvc']:
                # Asset trade hedges
                pass
            elif client.lower() in ['permira']:
                # principal hedges
                # Expected to be inverse for value_date - tradedate > = 14 and value_date >=20
                exclude_trade['trade_id__in'] = [item['trade_id'] for item in
                                                 VrmHedge1CopyCopy.objects.annotate(diff=ExpressionWrapper(
                                                                                    F('value_date')-F('trade_date'),
                                                                                    output_field=DurationField())).
                                                 filter(Q(client=client,
                                                         **HEDGE1_DEFAULT_QUERYSET,
                                                         diff__gte=datetime.timedelta(days=14),
                                                         value_date__day__gte=14)|Q(client=client,
                                                         **HEDGE1_DEFAULT_QUERYSET,
                                                         diff__gte=datetime.timedelta(days=14),
                                                         value_date__day__lte=7)).values('trade_id')]
        elif trade_summary_options == 2:
            if client.lower() in ['CVC']:
                # Subscription trade hedges
                pass
            elif client.lower() in ['permira']:
                # Interest hedges
                # Expected to value_date - tradedate > = 14 and value_date >=20
                query['trade_id__in'] = list(
                                         VrmHedge1CopyCopy.objects.annotate(diff=ExpressionWrapper(
                                             F('value_date')-F('trade_date'), output_field=DurationField())).filter(
                                             Q(client=client,
                                             **HEDGE1_DEFAULT_QUERYSET,
                                             diff__gte=datetime.timedelta(days=14),
                                             value_date__day__gte=14)|Q(client=client,
                                             **HEDGE1_DEFAULT_QUERYSET,
                                             diff__gte=datetime.timedelta(days=14),
                                             value_date__day__lte=7)
                                             ).values_list('trade_id', flat=True))

    counterparties_query = VrmHedge1CopyCopy.objects.filter(
        client=client,
        **hedge_set,
        **HEDGE1_DEFAULT_QUERYSET
    )
    if include_expired_counterparties is False:
        counterparties_query = counterparties_query.filter(
            value_date__gte=date.today()
        )

    counterparties = [Counterparties.objects.get(counterparty_name=item['counterparty'], cancellation=0) for item in
                      counterparties_query.values('counterparty').distinct()]
    counterparties = list(sorted(counterparties, key=operator.attrgetter('counterparty_name')))
    if not VRMContentTab.objects.filter(
            label='proportional_mtm',
            client__name=vrm_client
    ).exists() :
        mtm = VrmMtm1.objects.filter(**query).filter(Q(trade__unwind__iexact='FALSE') |
                                                     Q(trade__npv_date__gte=date.today()) |
                                                     Q(trade__npv_date__isnull=True))
        if len(exclude_trade) > 0:
            mtm = mtm.exclude(**exclude_trade)

        # liquidity_all = LAR.objects.filter(**lar_queryset).exclude(
        #     counterparty__in=[overall_cp, related_cp])

        #
        # threshold = []
        # total_lar = []
        # individual_mtm = []

        try:

            mtm = mtm.filter(
                valuation_date=mtm.aggregate(Max('valuation_date'))['valuation_date__max'])
            mtm_summation = mtm.aggregate(Sum('mtm'))
            # mtm_sum = mtm_summation['mtm__sum']
            # resp_sum = mtm_sum / 10e5 if mtm_sum is not None else None
            mtm_values = mtm.values(
                'trade__trade_id',
                'trade__trade_date',
                'trade__value_date',
                'trade__delivery_date',
                'trade__style',
                'trade__underlying',
                'trade__direction',
                'trade__notional_currency',
                'trade__notional_amounts',
                'trade__strike',
                'trade__premium',
                'trade__counterparty',
                'mtm',
                'net_mtm',
                'valuation_date'
            )

            resp_data = list(mtm_values)
            # check whether any new trade is added and not included here. As reverse lookup will not pick the objects.
            # These new trades will not have any market value
            if 'trade_id__in' not in [query, exclude_trade]:
                trade_queryset = {'cancellation__iexact': 'FALSE'}
                for key, value in query.items():
                    trade_queryset[key.replace('trade__', '')] = value
                hedge_list = list(VrmHedge1CopyCopy.objects.filter(**trade_queryset).
                                  filter(Q(unwind__iexact='FALSE') |
                                         Q(npv_date__gte=date.today()) |
                                         Q(npv_date__isnull=True)).exclude(**exclude_trade).
                                  values_list('trade_id', flat=True))
                for trade in VrmHedge1CopyCopy.objects.filter(trade_id__in=
                                                              list(set(hedge_list).difference(set(list(
                                                                  mtm.values_list('trade_id', flat=True)))))):
                    resp_data.append({ 'trade__trade_date': trade.trade_date,
                                       'trade__value_date': trade.value_date,
                                       'trade__delivery_date': trade.delivery_date,
                                       'trade__style': trade.style,
                                       'trade__underlying': trade.underlying,
                                       'trade__direction': trade.direction,
                                       'trade__notional_currency': trade.notional_currency,
                                       'trade__notional_amounts': trade.notional_amounts,
                                       'trade__strike': trade.strike,
                                       'trade__premium': trade.premium,
                                       'trade__counterparty': trade.counterparty,
                                       'mtm': 0.0,
                                       'net_mtm': 0.0,
                                       'trade__trade_id': trade.trade_id,
                                       'valuation_date':str(date.today())
                                       })
                # These extra checks are added to prevent an exception
                mtm_sum = mtm_summation['mtm__sum']
                resp_sum = mtm_sum / 10e5 if mtm_sum is not None else None
                next_date = mtm.aggregate(Min('trade__value_date'))['trade__value_date__min']
        except Exception as e:
            print(traceback.format_exc())
            resp_data = []
            resp_sum = None
            next_date = None
    else:
        try:
            mtm_query_set = {}
            for key, value in query.items():
                if key in ['trade__fund']:
                    mtm_query_set[key] = value
                # if key in ['trade__value_date__gte']:
                #     mtm_query_set['value_date__gte'] = value
            valuation_date, mtm, proportional_trades, mtm_query_set = \
                _get_proportional_mtm_list(request, query_set=mtm_query_set)

            if valuation_date is None:
                return HttpResponseBadRequest()
            mtm_values = []
            if mtm.count() > 0:
                mtm = mtm.filter(valuation_date=mtm.aggregate(Max('valuation_date'))['valuation_date__max'])
                mtm_sum = round(mtm.aggregate(Sum('mtm_proportional'))['mtm_proportional__sum'])
            mtm = list(mtm)
            if 'entity_id' in mtm_query_set.keys():
                for mtm_item in mtm:
                    current_mtm = getattr(module.models, 'ProportionalMtm').objects.filter(trade_id=mtm_item['trade_id']).first()
                    add_list = []
                    for key in PROPORTIONAL_MTM_VALUE_SET:
                        if key not in ['trade_id', 'mtm_proportional','net_mtm_proportional']:
                            mtm_item[key] = getattr(current_mtm, key)
            resp_data, mtm_sum = _construct_proportional_mtm_trade_table(mtm,
                                                                          proportional_trades,
                                                                          valuation_date)

            # These extra checks are added to prevent an exception
            resp_sum = mtm_sum/10e5 if mtm_sum is not None else None
            next_date = proportional_trades.aggregate(Min('value_date'))['value_date__min']
        except Exception as e:
            print(traceback.format_exc())
            resp_data = []
            resp_sum = None
            next_date = None

    return JsonResponse({
        'data': resp_data,
        'next_date': next_date,
        'mtm_sum': resp_sum,
        # 'total_lar': total_lar,
        # 'threshold': threshold,
        # 'individual_mtm': individual_mtm,
        'summary_list_option': summary_list_option,
        'counterparties': [cp.counterparty_name for cp in counterparties]

    })


@login_required(login_url='login')
@user_is_allowed
def api_hedge_summary(request):
    if request.is_ajax() is False or request.method != 'POST':
        return HttpResponseBadRequest()
    resp_sum = 0
    fund = request.POST.get('fund')
    client = request.POST.get('client')

    if None in [fund, client]:
        return HttpResponseBadRequest()

    resp_data = list()
    value_date_list = VrmHedge1CopyCopy.objects.filter(client=client, fund=fund,
                                                       **HEDGE1_DEFAULT_QUERYSET,
                                                       value_date__gte = date.today()).\
        values('value_date', 'underlying').order_by('underlying','value_date').distinct()
    resp_data1 = []
    total_summation = dict()
    try:
        for item in value_date_list:
            item_list = dict()
            item_list = item
            buy_amount = VrmHedge1CopyCopy.objects.filter(client=client, fund=fund, **HEDGE1_DEFAULT_QUERYSET,
                                                          direction='Buy',
                                                          underlying=item['underlying'],
                                                          value_date=item['value_date']).\
                aggregate(Sum('notional_amounts'))
            sell_amount = VrmHedge1CopyCopy.objects.filter(client=client, fund=fund, **HEDGE1_DEFAULT_QUERYSET,
                                                           underlying=item['underlying'],
                                                           direction='Sell',
                                                           value_date=item['value_date']).\
                aggregate(Sum('notional_amounts'))
            item_list['notional_amounts'] = (buy_amount['notional_amounts__sum'] or 0.0) - (
                    sell_amount['notional_amounts__sum'] or 0.0)
            buy_strike_sum = VrmHedge1CopyCopy.objects.filter(client=client,
                                                          fund=fund, **HEDGE1_DEFAULT_QUERYSET,
                                                          underlying=item['underlying'],
                                                          direction='Buy',
                                                          value_date=item['value_date']).\
                aggregate(total=Sum(F('notional_amounts') / F('strike'), output_field=FloatField()))['total']
            sell_strike_sum = VrmHedge1CopyCopy.objects.filter(client=client,
                                                             fund=fund, **HEDGE1_DEFAULT_QUERYSET,
                                                             underlying=item['underlying'],
                                                             direction='Sell',
                                                             value_date=item['value_date']). \
                aggregate(total=Sum(F('notional_amounts') / F('strike'), output_field=FloatField()))['total']
            strike_sum = (buy_strike_sum or 0.0) - (sell_strike_sum or 0.0)
            if item_list['notional_amounts'] == 0:
                strike_sum = 0.0
            else:
                strike_sum = abs(item_list['notional_amounts'] / strike_sum)
            item_list['strike'] = strike_sum
            if not item_list['notional_amounts'] == 0:
                if item['underlying'] in total_summation.keys():
                    total_summation[item['underlying']][0] += item_list['notional_amounts']
                    total_summation[item['underlying']][1] += item_list['notional_amounts']/item_list['strike']
                else:
                    total_summation[item['underlying']] = list()
                    total_summation[item['underlying']].append(item_list['notional_amounts'])
                    total_summation[item['underlying']].append(item_list['notional_amounts']/item_list['strike'])

                resp_data1.append(item_list)
            total_summary_table = list()

        for keys, values in total_summation.items():
            resp_data.append({'section': 'big-title', 'underlying': keys})
            for item  in resp_data1:
                if item['underlying'] == keys:
                    resp_data.append(item)
            resp_data.append({'section': 'quarters', 'underlying': 'Total  in ' +
                                                                   VrmHedge1CopyCopy.objects.filter(client=client,
                                                                                                    fund=fund,
                                                                                                    **HEDGE1_DEFAULT_QUERYSET,
                                                                                                    underlying=keys).order_by('-trade_id').
                             first().notional_currency, 'notional_amounts': values[0], 'strike': values[0] / values[1]})
    except Exception as e:
        print(traceback.format_exc())


    return JsonResponse({
        'data': resp_data,

    })


@login_required(login_url='login')
@user_is_allowed
def api_settlement_summary(request):
    if request.is_ajax() is False or request.method != 'POST':
        return HttpResponseBadRequest()
    requestedfund = request.POST.get('fund')
    client = request.POST.get('client')
    fund = list()

    if None in [requestedfund, client]:
        return HttpResponseBadRequest()
    # add the requested fund
    fund.append(requestedfund)
    if request.POST['selectedFund_overall'] == '1':
        # we need to change the fund to list
        try:
            client = VRMClient.objects.get(name=request.POST['client'])
        except VRMClient.DoesNotExist:
            raise Exception('Unable to find the client to process!')
        try:
            module = __import__(client.name.lower())
        except Exception as e:
            try:
                module = __import__(client.label.lower())
            except Exception as e:
                raise Exception('Unable to find the client to process!')
        fund_association = getattr(module.models,'FundAssociation').objects.filter(fund=Funds.objects.get(fund_name =requestedfund)).\
            exclude(related_fund__isnull=True)
        for fund_item in fund_association:
            fund.append(Funds.objects.get(pk=fund_item.related_fund_id).fund_name)

    resp_data = []
    try:
        value_date_list = VrmHedge1CopyCopy.objects.filter(client=client, fund__in=fund,
                                                           unwind__iexact='FALSE',
                                                           **HEDGE1_DEFAULT_QUERYSET,
                                                           value_date__gte = date.today()).\
            values('value_date', 'underlying','counterparty').order_by('value_date').distinct()
        # No need to worry here as hedge table has only single fund currency, but overall settlement
        # will have single currency


        value_notional_list = {}
        for item in value_date_list:
            item_list = item
            item_list['notional_amounts'] = 0.0
            for fund_item in fund:
                fund_currency = SchemaFundCp.objects.get(client=client, fund=fund_item, counterparty=item['counterparty']).base_currency
                if item['underlying'][3:] == fund_currency:
                    buy_strike_sum = VrmHedge1CopyCopy.objects.filter(client=client,
                                                                      fund=fund_item, unwind__iexact='FALSE',
                                                                      **HEDGE1_DEFAULT_QUERYSET,
                                                                      underlying=item['underlying'],
                                                                      counterparty=item['counterparty'],
                                                                      direction='Buy',
                                                                      value_date=item['value_date']).\
                        aggregate(total=Sum(F('notional_amounts') * F('strike'), output_field=FloatField()))['total']
                    sell_strike_sum = VrmHedge1CopyCopy.objects.filter(client=client,
                                                                       fund=fund_item, unwind__iexact='FALSE',
                                                                       **HEDGE1_DEFAULT_QUERYSET,
                                                                       underlying=item['underlying'],
                                                                       counterparty=item['counterparty'],
                                                                       direction='Sell',
                                                                       value_date=item['value_date']). \
                        aggregate(total=Sum(F('notional_amounts') * F('strike'), output_field=FloatField()))['total']
                else:
                    buy_strike_sum = VrmHedge1CopyCopy.objects.filter(client=client,
                                                                      fund=fund_item, unwind__iexact='FALSE',
                                                                      **HEDGE1_DEFAULT_QUERYSET,
                                                                      underlying=item['underlying'],
                                                                      counterparty=item['counterparty'],
                                                                      direction='Buy',
                                                                      value_date=item['value_date']). \
                        aggregate(total=Sum(F('notional_amounts') / F('strike'), output_field=FloatField()))['total']
                    sell_strike_sum = VrmHedge1CopyCopy.objects.filter(client=client,
                                                                       fund=fund_item, unwind__iexact='FALSE',
                                                                       **HEDGE1_DEFAULT_QUERYSET,
                                                                       underlying=item['underlying'],
                                                                       counterparty=item['counterparty'],
                                                                       direction='Sell',
                                                                       value_date=item['value_date']). \
                        aggregate(total=Sum(F('notional_amounts') / F('strike'), output_field=FloatField()))['total']
                item_list['notional_amounts'] = item_list['notional_amounts'] + (buy_strike_sum or 0.0) - (sell_strike_sum or 0.0)
            if item['value_date'] in value_notional_list.keys():
                value_notional_list[item['value_date']].append(item_list)
            else:
                value_notional_list[item['value_date']] = [item_list]

        for value_date_key,item_list in value_notional_list.items():
            cp = dict()
            for value_list in item_list:
                if value_list['counterparty'] in cp.keys():
                    cp[value_list['counterparty']] += value_list['notional_amounts']
                else:
                    cp[value_list['counterparty']] = value_list['notional_amounts']

            for cp_key, value in cp.items():
                summation_data = dict()
                summation_data['value_date'] = value_date_key
                summation_data['counterparty'] = cp_key
                summation_data['notional_amounts'] = abs(value)
                summation_data['settlement_limit'] = 0
                summation_data['settlement_usage'] = 0
                temp_settlement_line = VrmCounterparty.objects.filter(client=client,
                                                                      fund__in=fund,
                                                                      counterparty=cp_key)
                if len(temp_settlement_line) > 0:
                    if (temp_settlement_line.first().settlement_line_currency and
                            (temp_settlement_line.first().base_currency !=
                             temp_settlement_line.first().settlement_line_currency)):
                        try:
                            temp_currency = temp_settlement_line.first().base_currency + temp_settlement_line.first().settlement_line_currency
                            if LatestSpotRelatedData.objects.filter(underlying=temp_currency, security='spot').exists():
                                temp_settlement_line.first().settlement_line /= LatestSpotRelatedData.objects.filter(underlying=temp_currency,
                                                                                                                     security='spot').latest('valuation_date').mid
                            else:
                                try:
                                    temp_currency = temp_settlement_line.first().settlement_line_currency +\
                                                    temp_settlement_line.first().base_currency
                                    temp_spot_rate = LatestSpotRelatedData.objects.filter(underlying=temp_currency,
                                                                                          security='spot').latest(
                                        'valuation_date').mid
                                    temp_settlement_line.first().settlement_line *= temp_spot_rate
                                except:
                                    logger.exception("Unable to match the underlying - {}, {}".format(client, fund))
                                    pass
                        except:
                            logger.exception("Unable to match the underlying {}, {}".format(client, fund))
                            pass

                    summation_data['settlement_limit'] = temp_settlement_line.first().settlement_line or 0
                    if summation_data['notional_amounts'] == 0 or summation_data['settlement_limit'] == 0:
                        summation_data['settlement_usage'] = 0.0
                    else:
                        summation_data['settlement_usage'] = round(
                            summation_data['notional_amounts'] / summation_data['settlement_limit'], 2) * 100
                resp_data.append(summation_data)
    except Exception as e:
        print(traceback.format_exc())
    return JsonResponse({
        'data': resp_data,

    })


@login_required(login_url='login')
@user_is_allowed
def api_parent_summary(request):
    if request.is_ajax() is False or request.method != 'POST':
        return HttpResponseBadRequest()
    requestedfund = request.POST.get('fund')
    client = request.POST.get('client')
    date = request.POST.get('date')
    date_obj = dt.strptime(date,'%m/%d/%Y').date()
    fund = list()
    resp_data = None
    resp_currency= None
    total_realised_pnl = None
    total_unrealised_pnl = None
    if None in [requestedfund, client]:
        return HttpResponseBadRequest()

    vrm_client = VRMClient.objects.get(name=request.POST['client'])
    general_client = Clients.objects.get(client_name=vrm_client.name)
    general_fund = Funds.objects.get(fund_name=requestedfund, client=general_client.pk)
    try:
        module = module_loader(vrm_client)
    except Exception as e:
        raise Exception('Unable to find the client to process!')
    psummary = getattr(module.models, "ParentSummaryDeal")
    extra_parent = getattr(module.models, "ExtraParent")
    try:
        resp_data = list(psummary.objects.filter(fund=requestedfund, valuation_date=date_obj).values())
        extra_id_list = list(psummary.objects.filter(fund=requestedfund, valuation_date=date_obj, audited=1).values_list('id', flat=True))
        extra_parent_data = list(extra_parent.objects.filter(summary_id__in=extra_id_list).values())
        
        for data in extra_parent_data:
            for index, data2 in enumerate(resp_data,0):
                if data['summary_id'] == data2['id']:
                    resp_data[index].update(data)
                    

        total_realised_pnl = psummary.objects.filter(fund=requestedfund, valuation_date=date_obj).aggregate(Sum('realised_pnl'))
        total_unrealised_pnl = psummary.objects.filter(fund=requestedfund, valuation_date=date_obj).aggregate(Sum('unrealised_pnl'))
        if total_realised_pnl:
            total_realised_pnl = total_realised_pnl['realised_pnl__sum'] or 0.0/1e6
        if total_unrealised_pnl:
            total_unrealised_pnl = total_unrealised_pnl['unrealised_pnl__sum'] or 0.0/1e6
        try:
            resp_currency = getattr(module.models, 'Fund').objects. \
                get(fund_name=general_fund.fund_name).ccy
        except ObjectDoesNotExist as e:
            resp_currency = ''
    except Exception as e:
        print(traceback.format_exc())
    return JsonResponse({
        'data': resp_data,
        'currency': resp_currency,
        'total_realised_pnl': total_realised_pnl or 0.00,
        'total_unrealised_pnl': total_unrealised_pnl or 0.00
    })

@login_required(login_url='login')
@user_is_allowed
def api_forecast_data(request):
    if request.is_ajax() is False or request.method != 'POST':
        return HttpResponseBadRequest()
    fund = request.POST.get('fund')
    client = request.POST.get('client')

    if None in [fund, client]:
        return HttpResponseBadRequest()

    resp_data = list()

    return JsonResponse({
        'data': resp_data,

    })


class Round(Func):
    function = 'ROUND'
    template = '%(function)s(%(expressions)s)'


@login_required(login_url='login')
@user_is_allowed
def trade_mtm_counterparty_table(request):
    if request.is_ajax() is False or request.method != 'POST':
        return HttpResponseBadRequest()

    try:
        mtm_values = []
        currency = None
        mtm_sum = 0
        fund = request.POST.get('fund')
        counterparty = request.POST.get('counterparty')
        entity = request.POST.get('entity') or ''
        guarantor = request.POST.get('guarantor') or ''
        vrm_client = VRMClient.objects.get(name=request.POST['client'])
        general_client = Clients.objects.get(client_name=vrm_client.name)
        try:
            module = module_loader(vrm_client)
        except Exception as e:
            raise Exception('Unable to find the client to process!')

        if fund is None or counterparty is None :
            return HttpResponseBadRequest()

        mtm_queryset = {'trade__fund': fund}
        if entity and entity != '':
            mtm_queryset = {'trade__legal_entity': entity}
        elif guarantor and guarantor != '':
            pass
        else:
            mtm_queryset = {'trade__fund': fund}
        if vrm_client.entity_summary:
            agg_set = {'cp': Counterparties.objects.get(counterparty_name=counterparty)} if counterparty != 'all' else {}
            if entity and entity != '':
                agg_set['entity_id'] = getattr(module.models, 'Entity').objects.get(entity_name=entity).pk
            else:
                general_fund = Funds.objects.get(fund_name=fund, client_id=general_client.pk)
                # using the related name
                agg_set['fund'] = getattr(module.models, 'Fund').objects.get(fund_name=general_fund.fund_name)
            currency = getattr(module.models, 'LegalCounterparty'). \
                objects.filter(**agg_set).first().base_currency
        else:
            currency = SchemaFundCp.objects.get(
                client=request.POST['client'], fund=fund, counterparty=counterparty
            ).base_currency
        if request.POST.get('counterparty', '') != 'all':
            mtm_queryset['trade__counterparty'] = counterparty
        mtm = None
        valuation_date = None
        # fetch mtm based trades
        valuation_date, mtm = _get_mtm_list(request, query_set=mtm_queryset)

        if valuation_date is None:
            return HttpResponseBadRequest()
        mtm_values = []
        if not isinstance(mtm, list) and mtm.count() > 0:
            mtm = mtm.filter(valuation_date=mtm.aggregate(Max('valuation_date'))['valuation_date__max'])
            mtm_sum = round(mtm.aggregate(Sum('mtm'))['mtm__sum'])
            mtm_values = list(mtm.values(*MTM_DEFAULT_VALUE_SET))
            _add_new_trades(request, mtm_values, currency, mtm, valuation_date, query_set=mtm_queryset)

    except ObjectDoesNotExist as exception:
        print(traceback.format_exc())
    except Exception as exception:
        print(traceback.format_exc())
    finally:
        resp = {
            'data': mtm_values,
            'mtm_sum': mtm_sum,
            'currency': currency,
        }
        return JsonResponse(resp)


@login_required(login_url='login')
@user_is_allowed
def trade_table(request):
    if request.is_ajax() and request.method == 'POST':
        try:
            value_date = None
            if 'valuation_date' in request.POST and not request.POST['valuation_date'] == '':
                value_date = datetime.datetime.strptime(request.POST['valuation_date'], "%Y-%m-%d")
            else:

                value_date = VrmHedge1CopyCopy.objects.filter(client=request.POST['client'],
                                                              **HEDGE1_DEFAULT_QUERYSET,
                                                              value_date__gte=date.today()).order_by('value_date'). \
                                                            aggregate(value_date=Min('value_date'))['value_date']
            fund_deselection = []
            cp_deselection = []
            rollpnl = request.POST.get('rollpnl', 'False') == 'true'
            nettingval = request.POST.get('nettingval', 'false') == 'true'
            if 'fund_deselection' in request.POST and not request.POST.get('fund_deselection') == '[]':
                list_int = [int(item) for item in json.loads(request.POST.get('fund_deselection'))]
                fund_deselection = [fund.fund_name for fund in Funds.objects.filter(pk__in=list_int).distinct()]
            if 'cp_deselection' in request.POST and not request.POST.get('cp_deselection') == '[]':
                cp_deselection = [item for item in json.loads(request.POST.get('cp_deselection'))]
            trade_data = VrmHedge1CopyCopy.objects.filter(client=request.POST['client'],
                                                          **HEDGE1_DEFAULT_QUERYSET,
                                                          unwind__iexact='FALSE',
                                                          value_date__in= [value_date]).\
                                                          values('trade_id', 'client','fund', 'cp_ref','trade_date', 'value_date',\
                                                                 'delivery_date', 'style', 'underlying', 'direction', \
                                                                 'notional_currency', 'notional_amounts', 'strike',\
                                                                 'premium', 'premium_payment_date', 'counterparty',\
                                                                 'legal_entity', 'unwind', 'unwind_proceeds').\
                                                            exclude(fund__in=fund_deselection).\
                                                            exclude(counterparty__in=cp_deselection)
            for trade in trade_data:
                try:
                    trade['cp_ref'] = VrmCounterparty.objects.get(client=trade['client'],
                                                                  fund=trade['fund'],
                                                                  counterparty=trade['counterparty']).counterparty_short_code
                except ObjectDoesNotExist:
                    trade['cp_ref'] = None
            query_list = ['fund', 'underlying', 'counterparty', 'notional_currency', 'legal_entity']
            if not nettingval:
                query_list.append('trade_id')

            rolling_period = VrmHedge1CopyCopy.objects.filter(client=request.POST['client'],
                                                              unwind__iexact='FALSE',
                                                              **HEDGE1_DEFAULT_QUERYSET,
                                                              value_date__in=[value_date]).\
                                                              values(*query_list).\
                                                              exclude(fund__in=fund_deselection).\
                                                              exclude(counterparty__in=cp_deselection).distinct()
            list_rolling_period = list()
            value_date = trade_data[0]['value_date']
            for item in list(rolling_period):
                rolling_table = dict(item)
                change_value_date = value_date + datetime.timedelta(days=91)
                while (calendar.day_name[change_value_date.weekday()] != "Wednesday"):
                    change_value_date += datetime.timedelta(days=1)
                rolling_table.update({'strike': 0.00, 'value_date': change_value_date})
                # TODO Incase of parallel fund need to work with legal counterparty currency not on the fund currency
                # legal_counterparty is not fully migrated. Once migrated then rollover will be done based on the
                # config if legal_counterparty currency or fund currency.
                # check the common setting not based on fund name
                buy_direction = 0.0
                sell_direction = 0.0
                if nettingval:

                    buy_direction = VrmHedge1CopyCopy.objects.filter(client=request.POST['client'],
                                                                     **HEDGE1_DEFAULT_QUERYSET,
                                                                     unwind__iexact='FALSE',
                                                                     fund=item['fund'],
                                                                     direction__iexact='Buy',
                                                                     underlying = item['underlying'],
                                                                     notional_currency=item['notional_currency'],
                                                                     counterparty=item['counterparty'],
                                                                     legal_entity=item['legal_entity'],
                                                                     value_date__in=[value_date])
                    sell_direction = VrmHedge1CopyCopy.objects.filter(client=request.POST['client'],
                                                                      **HEDGE1_DEFAULT_QUERYSET,
                                                                      unwind__iexact='FALSE',
                                                                      fund=item['fund'],
                                                                      notional_currency=item['notional_currency'],
                                                                      underlying=item['underlying'],
                                                                      legal_entity=item['legal_entity'],
                                                                      direction='Sell',
                                                                      counterparty=item['counterparty'],
                                                                      value_date__in=[value_date])
                else:
                    buy_direction = VrmHedge1CopyCopy.objects.filter(client=request.POST['client'],
                                                                     **HEDGE1_DEFAULT_QUERYSET,
                                                                     unwind__iexact='FALSE',
                                                                     fund=item['fund'],
                                                                     direction__iexact='Buy',
                                                                     underlying = item['underlying'],
                                                                     notional_currency=item['notional_currency'],
                                                                     counterparty=item['counterparty'],
                                                                     legal_entity=item['legal_entity'],
                                                                     value_date__in=[value_date],
                                                                     trade_id = item['trade_id'])
                    sell_direction = VrmHedge1CopyCopy.objects.filter(client=request.POST['client'],
                                                                      **HEDGE1_DEFAULT_QUERYSET,
                                                                      unwind__iexact='FALSE',
                                                                      fund=item['fund'],
                                                                      notional_currency=item['notional_currency'],
                                                                      underlying=item['underlying'],
                                                                      direction='Sell',
                                                                      counterparty=item['counterparty'],
                                                                      legal_entity=item['legal_entity'],
                                                                      value_date__in=[value_date],
                                                                      trade_id = item['trade_id'])
                if nettingval:
                    trade_id_list = list(buy_direction.values_list('trade_id', flat=True)) or []
                    trade_id_list += sell_direction.values_list('trade_id', flat=True) or []
                else:
                    trade_id_list = [item['trade_id']]
                # if 'parallel' in item['fund'].lower():
                # try:
                #     parallel_currency = True if SchemaFundCp.objects.get(client=request.POST['client'],
                #                                  counterparty=item['counterparty'],
                #                                  fund=item['fund']).base_currency != SchemaLegalCp.objects.get(
                #                                 client=request.POST['client'],
                #                                 legal_entity=item['legal_entity'],
                #                                 fund=item['fund']).base_currency else False
                # except:
                #     parallel_currency = True  if 'parallel' in item['fund'].lower() else False
                '''
                  Please find below for some cases. The end goal is to change notional_currency 
                  to non entity currency.
                  Underlying from hedge_copy_copy	Notional_Currency from hedge_copy_copy	entity_currency 
                  from entity table	
                  Action
                  ------------------------------------------------------------------------------------------------
                  EURUSD	EUR	EUR	change notional_currency to USD,change direction and notional_amounts 
                  EURUSD	USD	EUR	no action taken
                  EURUSD	EUR	USD	no action taken
                  EURUSD	USD	USD	change notional_currency to EUR, and change direction and notional_amounts 
                  EURGBP	EUR	EUR	change notional_currency to GBP, and change direction and notional_amounts 
                  EURGBP	GBP	EUR	no action taken
                  EURGBP	EUR	GBP	no action taken
                  EURGBP	GBP	GBP	change notional_currency to EUR, and change direction and notional_amounts 
                '''
                # if SchemaLegalCp.objects.get(fund=datum['fund'],
                #                              client=datum['client'],
                #                              legal_entity=datum['legal_entity']).base_currency == datum[
                #     'notional_currency']:
                #     if datum['notional_currency'] == datum['underlying'][0:3]:
                #         datum['notional_currency'] = datum['underlying'][3:]
                #         datum['notional_amounts'] = round(datum['notional_amounts'] *
                #                                           datum['strike'], 2)
                #     else:
                #         datum['notional_currency'] = datum['underlying'][0:3]
                #         datum['notional_amounts'] = round(datum['notional_amounts'] /
                #                                           datum['strike'], 2)
                #     datum['direction'] = 'Sell' if (datum['direction']
                #                                     == "Buy") else 'Buy'
                # if parallel_currency:
                if rollpnl and SchemaLegalCp.objects.get(client=request.POST['client'],
                                                         fund=item['fund'],
                                                         legal_entity=item['legal_entity']).base_currency == item[
                    'notional_currency']:
                    if item['notional_currency'] == item['underlying'][0:3]:
                        sell_direction = sell_direction.aggregate(notional_amounts=Sum(F("notional_amounts")*F('strike') , output_field=FloatField()))
                        buy_direction = buy_direction.aggregate(notional_amounts=Sum(F("notional_amounts")*F('strike') , output_field=FloatField()))
                        rolling_table['notional_currency'] = item['underlying'][3:]
                    else:
                        sell_direction = sell_direction.aggregate(
                            notional_amounts=Sum(F("notional_amounts") / F('strike'), output_field=FloatField()))
                        buy_direction = buy_direction.aggregate(
                            notional_amounts=Sum(F("notional_amounts") / F('strike'), output_field=FloatField()))
                        rolling_table['notional_currency'] = item['underlying'][0:3]

                else:
                        sell_direction = sell_direction.aggregate(
                            notional_amounts=Sum("notional_amounts"))
                        buy_direction = buy_direction.aggregate(
                            notional_amounts=Sum("notional_amounts"))
                rolling_amount = (buy_direction['notional_amounts'] or 0.00) - (sell_direction['notional_amounts'] or 0.00)
                if rolling_amount > 0.00:
                    rolling_table['direction'] = 'Buy'
                else:
                    rolling_table['direction'] = 'Sell'
                try:
                    rolling_table['cp_ref'] = VrmCounterparty.objects.get(client=request.POST['client'],
                                                                          fund=item['fund'],
                                                                          counterparty=item['counterparty']).counterparty_short_code
                except ObjectDoesNotExist:
                    rolling_table['cp_ref'] = None
                schema_entry = SchemaLegalCp.objects.get(client=request.POST['client'],
                                                             fund=item['fund'],
                                                             legal_entity=item['legal_entity'])
                if rollpnl and schema_entry.base_currency == item[
                        'notional_currency']:
                    rolling_table['direction'] = 'Buy' if rolling_table['direction'] == "Sell" else 'Sell'
                rolling_table['notional_amounts__sum'] = math.fabs(rolling_amount)
                rolling_table['notional_amounts__newsum'] = math.fabs(rolling_amount)
                rolling_table['trade_id'] = trade_id_list
                rolling_table['legal_entity'] = item['legal_entity']
                list_rolling_period.append(rolling_table)
            return JsonResponse({'data': list(trade_data), 'list_rolling_period': list_rolling_period, 'value_date': str(value_date) },
                                content_type='application/json')
        except Exception as e:
            print(e)
            return JsonResponse({'data': [], 'mtm_sum': None,'currency': None, 'value_date': str(date.today())}, content_type='application/json')
        return JsonResponse(res_data, content_type='application/json')
    else:
        return HttpResponseBadRequest()


@login_required(login_url='login')
@user_is_allowed
def live_spot_rates(request):
    table_update = list()
    if request.is_ajax() and request.method == 'POST':
        client = request.POST.get('client', '')
        value_date = None
        additional_underlying= []
        cond_underlying= request.POST.get('additional_underlying','')
        if 'valuation_date' in request.POST and not request.POST['valuation_date'] == '':
            value_date = datetime.datetime.strptime(request.POST['valuation_date'], "%Y-%m-%d")
        else:

            value_date = VrmHedge1CopyCopy.objects.filter(client=request.POST['client'],
                                                          cancellation=False, dummy=False). \
                latest('value_date').value_date
        underlying = VrmHedge1CopyCopy.objects.filter(client=client,
                                                      value_date=value_date,
                                                      **HEDGE1_DEFAULT_QUERYSET,
                                                      unwind__iexact='FALSE',).values('underlying').distinct()
        if cond_underlying=='true':

            underlying = VrmHedge1CopyCopy.objects.filter(client=client,
                                                          **HEDGE1_DEFAULT_QUERYSET,
                                                          unwind__iexact='FALSE', ).values('underlying').distinct()
            try:
                client = VRMClient.objects.get(name=client)
                module = __import__(client.name.lower())
            except Exception as e:
                try:
                    module = __import__(client.label.lower())
                except Exception as e:
                    raise Exception('Unable to find the client to process!')

            if module:
                try:
                    additional_underlying_obj = getattr(module.models, 'AdditionalUnderlying')
                except Exception as e:
                    additional_underlying_obj = None

            if additional_underlying_obj:
                additional_underlying =  additional_underlying_obj.objects.all().exclude(
                        underlying__in=[item['underlying'] for item in underlying]
                    ).values('underlying').distinct()

            # Reason for adding the 2 list for few clients , we display additional underlying which is not part
            # of the trade.

            if len(additional_underlying) > 0:
                underlying = list(underlying) + list(additional_underlying)
            else:
                underlying = list(underlying)
            # for item in additional_underlying:
            #     underlying.append(item['underlying'])

        underlying_format = []
        for count,item in enumerate(underlying):
            if item['underlying']  in ['GBPEUR']:
                underlying_format.append(item['underlying'][3:] + '/' + item['underlying'][:3])
            else:
                underlying_format.append(item['underlying'][:3] + '/' + item['underlying'][3:])
        mid_cur = dict()
        data = []
        try:
            for item in underlying_format:
                try:
                    live_rate = LiveSpotRateFeed.objects.filter(currency=item).latest(
                            'timestamp')
                # if live_rate is None:
                #
                #     live_rate = FixingdataTempBridgeTable.objects.filter(security='Spot',
                #                                                          underlying=item).values('underlying',
                #                                                                                  'bid','ask').\
                #                                                         annotate(valuation_date=Max('valuation_date'))
                #
                #     mid_cur[live_rate.underlying] = {'mid': (live_rate.bid + live_rate.ask) / 2,
                #                                    'timestamp': str(live_rate.valuation_date)}
                # else:
                    mid_cur[live_rate.currency] = {'mid': (live_rate.rate) or 0.0,
                                         'timestamp': str(live_rate.timestamp).split('+')[0]}
                except Exception as e:
                    print("missing underlying in live feed {}".format(item))
        except Exception as e:
            print(e)
        check_exist = []
        for currpair, values in mid_cur.items():
            for count, item in enumerate(underlying):
                if currpair.replace('/', '') == 'EURGBP' and item['underlying'] == 'GBPEUR':
                    found_currency = 'GBPEUR'
                    table_update.append({'underlying': found_currency, 'timestamp': values['timestamp'],
                                         'mid': str(1 / float(values['mid']))})
                    check_exist.append('GBPEUR')
                    # if currpair.replace('/', '') == 'SEKGBP' and item['underlying'] == 'GBPSEK':
                    #     found = True
                    #     found_currency = 'GBPSEK'
                    #     break
            if currpair.replace('/', '') not in check_exist:
                check_exist.append(currpair.replace('/', ''))
                table_update.append({'underlying':currpair.replace('/',''), 'mid': str(values['mid']),
                                     'timestamp': values['timestamp']})
        return JsonResponse({'data': table_update}, content_type='application/json')


def _process_cash_settlement(client, value_date, rolling_item, adding_parallel,fund_deselection, cp_deselection,
                             rollpnl, nettingval, current_livespotrates,  cash_settlement):
    rolling_table = dict(rolling_item)
    list_of_trades = []
    queryset = {
        'client': client,
        'unwind__iexact': 'FALSE',
        **HEDGE1_DEFAULT_QUERYSET,
        'fund': rolling_table['fund'],
        'underlying': rolling_table['underlying'],
        'counterparty': rolling_table['counterparty'],
        'legal_entity': rolling_table['legal_entity'],
        'value_date__in': [value_date]
    }
    if not nettingval:
        # if not nettingval, then it means per trade.
        queryset['trade_id'] = rolling_table['trade_id'][0]

    # try:
    #     parallel_currency =  False if SchemaFundCp.objects.get(client=client,
    #                                                  counterparty=rolling_table['counterparty'],
    #                                                  fund=rolling_table['fund']).base_currency != SchemaLegalCp.objects.get(
    #         client=client, legal_entity=rolling_table['legal_entity'], fund=rolling_table['fund']).base_currency else True
    # except:
    #     parallel_currency = True if not 'parallel' in rolling_table['fund'].lower() else False
    # # if not 'parallel' in rolling_table['fund'].lower():
    # if parallel_currency:
    if SchemaLegalCp.objects.get(
            client=client, legal_entity=rolling_table['legal_entity'], fund=rolling_table['fund']).base_currency == \
            rolling_table['notional_currency'] and rollpnl:
        queryset['notional_currency'] = rolling_table['notional_currency']
    list_of_trades = (VrmHedge1CopyCopy.objects.filter(**queryset).
                      exclude(fund__in=fund_deselection).
                      exclude(counterparty__in=cp_deselection))
    for trade_item in list_of_trades:
        new_cash_settlement = dict()
        buy_direction = 0.0
        sell_direction = 0.0
        new_cash_settlement['trade_id'] = trade_item.trade_id
        new_cash_settlement['underlying'] = trade_item.underlying
        new_cash_settlement['fund'] = trade_item.fund
        new_cash_settlement['counterparty'] = trade_item.counterparty
        new_cash_settlement['strike'] = trade_item.strike
        new_cash_settlement['legal_entity'] = trade_item.legal_entity
        schema_entry = SchemaLegalCp.objects.get(client=trade_item.client,
                                                 fund=trade_item.fund,
                                                 legal_entity=trade_item.legal_entity)
        new_cash_settlement['fxall_fund_name'] = schema_entry.fxall_fund_name
        try:
            new_cash_settlement['cp_ref'] = VrmCounterparty.objects.get(client=trade_item.client,
                                                                        fund=trade_item.fund,

                                                                        counterparty=trade_item.counterparty).counterparty_short_code
        except ObjectDoesNotExist:
            new_cash_settlement['cp_ref'] = None
        underlying = trade_item.underlying
        notional_amounts = trade_item.notional_amounts
        notional_currency = trade_item.notional_currency
        direction = trade_item.direction
        # TODO Incase of parallel fund need to work with legal counterparty currency not on the fund currency
        # legal_counterparty is not fully migrated. Once migrated then rollover will be done based on the
        # config if legal_counterparty currency or fund currency.
        # check the common setting not based on fund name
        # try:
        #     parallel_currency_local = True if SchemaFundCp.objects.get(client=trade_item.client,
        #                                                         counterparty=trade_item.counterparty,
        #                                                         fund=trade_item.fund).base_currency != SchemaLegalCp.objects.get(
        #         client=trade_item.client, legal_entity=trade_item.legal_entity, fund=trade_item.fund).base_currency else False
        # except:
        #     parallel_currency_local = True if 'parallel' in trade_item.fund.lower() else False
        # if parallel_currency_local:
        new_cash_settlement['parallel_ccy'] = 'false'
        if SchemaLegalCp.objects.get(
                client=trade_item.client, legal_entity=trade_item.legal_entity,
                fund=trade_item.fund).base_currency == trade_item.notional_currency and rollpnl:
            direction = 'Buy' if trade_item.direction == 'Sell' else 'Sell'
            if trade_item.underlying[3:] == rolling_table['notional_currency']:
                notional_amounts = trade_item.notional_amounts * trade_item.strike
            else:
                notional_amounts = trade_item.notional_amounts / trade_item.strike
            new_cash_settlement['parallel_ccy'] = 'true'

        if direction == 'Buy':
            if trade_item.underlying[3:] == rolling_table['notional_currency']:
                new_cash_settlement['notional_currency'] = trade_item.underlying[3:]
                new_cash_settlement['notional_amounts'] = notional_amounts
                new_cash_settlement['in_notional_currency'] = underlying[0:3]
                # You are selling and the amount is different currency
                new_cash_settlement['in_notional_amounts'] = -1 * notional_amounts * (1 / trade_item.strike)
                new_cash_settlement['style'] = trade_item.style
                new_cash_settlement['direction'] = 'Buy'
            else:
                new_cash_settlement['notional_currency'] = trade_item.underlying[0:3]
                new_cash_settlement['notional_amounts'] = notional_amounts
                new_cash_settlement['in_notional_currency'] = underlying[3:]
                # You are selling and the amount is different currency
                new_cash_settlement['in_notional_amounts'] = -1 * notional_amounts * trade_item.strike
                new_cash_settlement['style'] = trade_item.style
                new_cash_settlement['direction'] = 'Buy'
        elif direction == 'Sell':
            if trade_item.underlying[3:] == rolling_table['notional_currency']:
                new_cash_settlement['notional_currency'] = trade_item.underlying[3:]
                new_cash_settlement['notional_amounts'] = notional_amounts * (-1)
                new_cash_settlement['in_notional_currency'] = underlying[0:3]
                # You are selling and the amount is different currency
                new_cash_settlement['in_notional_amounts'] = notional_amounts * (1 / trade_item.strike)
                new_cash_settlement['style'] = trade_item.style
                new_cash_settlement['direction'] = 'Sell'
            else:
                new_cash_settlement['notional_currency'] = trade_item.underlying[0:3]
                new_cash_settlement['notional_amounts'] = notional_amounts * (-1)
                new_cash_settlement['in_notional_currency'] = underlying[3:]
                # You are selling and the amount is different currency
                new_cash_settlement['in_notional_amounts'] = notional_amounts * trade_item.strike
                new_cash_settlement['style'] = trade_item.style
                new_cash_settlement['direction'] = 'Sell'

        cash_settlement.append(new_cash_settlement)

    new_cash_settlement = dict()
    strike = float(
        current_livespotrates[rolling_table['underlying']] if rolling_table['strike'] == 0.00 else rolling_table[
            'strike'])
    if rolling_table['direction'] == 'Sell':
        if rolling_table['underlying'][3:] == rolling_table['notional_currency']:
            new_cash_settlement['notional_currency'] = rolling_table['underlying'][3:]
            new_cash_settlement['notional_amounts'] = float(rolling_table['notional_amounts__newsum'])
            new_cash_settlement['in_notional_currency'] = rolling_table['underlying'][0:3]
            # You are selling and the amount is different currency
            new_cash_settlement['in_notional_amounts'] = -1 * float(rolling_table['notional_amounts__newsum']) * (
                        1 / strike)
            new_cash_settlement['direction'] = 'Sell'
            new_cash_settlement['style'] = 'Spot'
        else:
            new_cash_settlement['notional_currency'] = rolling_table['underlying'][0:3]
            new_cash_settlement['notional_amounts'] = float(rolling_table['notional_amounts__newsum'])
            new_cash_settlement['in_notional_currency'] = rolling_table['underlying'][3:]
            # You are selling and the amount is different currency
            new_cash_settlement['in_notional_amounts'] = -1 * float(
                rolling_table['notional_amounts__newsum']) * strike
            new_cash_settlement['direction'] = 'Sell'
            new_cash_settlement['style'] = 'Spot'
    elif rolling_table['direction'] == 'Buy':
        if rolling_table['underlying'][3:] == rolling_table['notional_currency']:
            new_cash_settlement['notional_currency'] = rolling_table['underlying'][3:]
            new_cash_settlement['notional_amounts'] = float(rolling_table['notional_amounts__newsum']) * (-1)
            new_cash_settlement['in_notional_currency'] = rolling_table['underlying'][0:3]
            # You are selling and the amount is different currency
            new_cash_settlement['in_notional_amounts'] = float(rolling_table['notional_amounts__newsum']) * (
                        1 / strike)
            new_cash_settlement['direction'] = 'Buy'
            new_cash_settlement['style'] = 'Spot'
        else:
            new_cash_settlement['notional_currency'] = rolling_table['underlying'][0:3]
            new_cash_settlement['notional_amounts'] = float(rolling_table['notional_amounts__newsum']) * (-1)
            new_cash_settlement['in_notional_currency'] = rolling_table['underlying'][3:]
            # You are selling and the amount is different currency
            new_cash_settlement['in_notional_amounts'] = float(rolling_table['notional_amounts__newsum']) * strike
            new_cash_settlement['direction'] = 'Buy'
            new_cash_settlement['style'] = 'Spot'
    new_cash_settlement['trade_id'] = "New Roll"
    new_cash_settlement['fund'] = rolling_table['fund']
    new_cash_settlement['underlying'] = rolling_table['underlying']
    new_cash_settlement['counterparty'] = rolling_table['counterparty']
    new_cash_settlement['strike'] = strike
    new_cash_settlement['cp_ref'] = rolling_table['cp_ref']
    new_cash_settlement['legal_entity'] = ''
    schema_entry = SchemaLegalCp.objects.get(client=trade_item.client,
                                             fund=trade_item.fund,
                                             legal_entity=trade_item.legal_entity)
    new_cash_settlement['fxall_fund_name'] = schema_entry.fxall_fund_name
    cash_settlement.append(new_cash_settlement)


@login_required(login_url='login')
@user_is_allowed
def cash_settlement(request):
    if request.is_ajax() and request.method == 'POST':
        try:
            current_rollingvalues = json.loads(request.POST['current_rollingvalues'] or {})
            current_livespotrates = json.loads(request.POST['current_livespotrates'] or {})
            fund_deselection = []
            cp_deselection = []
            rollpnl = request.POST.get('rollpnl', 'False') == 'true'
            nettingval = request.POST.get('nettingval', 'False') == 'true'
            if 'fund_deselection' in request.POST and not request.POST.get('fund_deselection') == '[]':
                list_int = [int(item) for item in json.loads(request.POST.get('fund_deselection'))]
                fund_deselection = [fund.fund_name for fund in Funds.objects.filter(pk__in=list_int).distinct()]
            if 'cp_deselection' in request.POST and not request.POST.get('cp_deselection') == '[]':
                cp_deselection = [item for item in json.loads(request.POST.get('cp_deselection'))]
            value_date = None
            if 'valuation_date' in request.POST and not request.POST['valuation_date'] == '':
                value_date = datetime.datetime.strptime(request.POST['valuation_date'], "%Y-%m-%d")
            else:

                value_date = VrmHedge1CopyCopy.objects.filter(client=request.POST['client'],
                                                              **HEDGE1_DEFAULT_QUERYSET,
                                                              unwind__iexact='FALSE').\
                                                          latest('value_date').value_date
            cash_settlement = list()
            cash_summation = list()
            adding_parallel = dict()
            for key,rolling_item in current_rollingvalues.items():
                try:
                    _process_cash_settlement(request.POST['client'], value_date, rolling_item, adding_parallel, fund_deselection,
                                         cp_deselection, rollpnl, nettingval,current_livespotrates, cash_settlement)
                except Exception as e:
                    print(e)
                    print("Exception")
            cash_sum = dict()
            cash_settlement_sorted = sorted(cash_settlement, key=lambda i: i['fund'])
            for cash_in in cash_settlement_sorted:
                    key_find_underlying = cash_in['fund'] + '_' + cash_in['counterparty'] + '_' + cash_in[
                        'notional_currency']
                    key_find_underlying2 = cash_in['fund'] + '_' + cash_in['counterparty'] + '_' + cash_in[
                        'in_notional_currency']
                    if key_find_underlying in cash_sum.keys():
                        cash_sum[key_find_underlying]['ccy_amunt'] += cash_in['notional_amounts']
                    else:
                        cash_sum[key_find_underlying] = {}
                        cash_sum[key_find_underlying]['fund'] = cash_in['fund']
                        cash_sum[key_find_underlying]['counterparty'] = cash_in['counterparty']
                        cash_sum[key_find_underlying]['notional_currency'] = cash_in['notional_currency']
                        cash_sum[key_find_underlying]['ccy_amunt'] = cash_in['notional_amounts']
                        cash_sum[key_find_underlying]['ccy'] = cash_in['notional_currency']
                        cash_sum[key_find_underlying]['strike'] = cash_in['strike']
                        cash_sum[key_find_underlying]['cp_ref'] = cash_in['cp_ref']
                    if key_find_underlying2 in cash_sum.keys():
                        cash_sum[key_find_underlying2]['ccy_amunt'] += cash_in['in_notional_amounts']
                    else:
                        cash_sum[key_find_underlying2] = {}
                        cash_sum[key_find_underlying2]['fund'] = cash_in['fund']
                        cash_sum[key_find_underlying]['notional_currency'] = cash_in['notional_currency']
                        cash_sum[key_find_underlying2]['counterparty'] = cash_in['counterparty']
                        cash_sum[key_find_underlying2]['ccy_amunt'] = cash_in['in_notional_amounts']
                        cash_sum[key_find_underlying2]['ccy'] = cash_in['in_notional_currency']
                        cash_sum[key_find_underlying2]['strike'] = cash_in['strike']
                        cash_sum[key_find_underlying2]['cp_ref'] = cash_in['cp_ref']
            for key,value in cash_sum.items():
                cash_summation.append(value)
        except Exception as e:
            print(e)
            return JsonResponse({'data': [], }, content_type='application/json')
    return JsonResponse({'data': cash_settlement_sorted, 'cash_summation': cash_summation}, content_type='application/json')


@login_required(login_url='login')
@user_is_allowed
def prepare_rolling_data(request):
    # creating workbook
    file_name_wb = 'settlementandrolling_'+str(int(datetime.datetime.now(tz=timezone.utc).timestamp() * 1000))+'.xlsx'

    writer = pd.ExcelWriter(file_name_wb, engine="xlsxwriter", date_format="dd/MM/yyyy")
    workbook = writer.book

    head_style = workbook.add_format({
        'bold': 1,
        'font_size': 26
    })

    column_style = workbook.add_format({
        'font_size': 11,
        'bold':1,
        'top':1,
        'bottom':5,
        'align':'center',
        'text_wrap': True
    })

    comment_style = workbook.add_format({
        'font_size': 9,
        'italic':1,
        'bold':1
    })

    num_style = workbook.add_format(
        {
            'font_size': 11,
            'num_format':'#,##0.00',
            'align':'right'
        }
    )

    strike_style = workbook.add_format(
        {
            'font_size': 11,
            'num_format':'0.000000',
            'align':'right'
        }
    )

    # # column header names, you can use your own headers here
    columns_ws = [ 'trade_id',
                'client',
                'fund',
                'cp_ref',
                'trade_date',
                'value_date',
                'delivery_date',
                'style',
                'underlying',
                'direction',
                'notional_currency',
                'notional_amounts',
                'strike',
                'counterparty',
                'legal_entity',
                'unwind',
                'unwind_proceeds'
    ]

    columns_ws1 = [ 'fund',
                    'value_date',
                    'underlying',
                    'direction',
                    'notional_currency',
                    'counterparty',
                    'notional_amounts__sum',
                    'notional_amounts__newsum',
                    'physical_settlement_amounts'

    ]

    columns_ws2 = [ 'fund',
                    'cp_ref',
                    'value_date',
                    'p/r',
                    'ccy',
                    'ccy_amunt',
                    'counterparty'

    ]

    columns_ws3 = [ 'fund',
                    'fxall_fund_name',
                    'cp_ref',
                    'style',
                    'value_date',
                    'underlying',
                    'direction',
                    'counterparty',
                    'notional_currency',
                    'notional_amounts'

    ]
    fxall_columns = [
        'Order ID',
        'Allocation ID',
        'CCY Pair',
        'Value DT',
        'Account',
        'B/S',
        'Deal CCY',
        'Amount',
        'Bank 1',
        'Bank 2',
        'Bank 3',
        'Bank 4',
        'Bank 5',
        'Bank 6',
        'Bank 7',
        'Bank 8',
        'Bank 9',
        'Bank 10',
        'MT304 Flag',
        'SSI Flag',
        'Remarks',
        'Source ID',
        'Contract Type',
        'Owner ID',
        'ND Fix Date'
    ]

    data_ws = json.loads(request.POST["current_trades"])
    data_ws1 = json.loads(request.POST["current_rollingvalues"])
    data_ws2 = json.loads(request.POST["cash_summation"])
    data_ws3 = json.loads(request.POST["cashSettlementGridOptions"])

    columnLength = {}

    expiringTradeData = pd.DataFrame.from_dict(data_ws, orient="index")
    expiringTradeData = expiringTradeData[columns_ws]
    expiringTradeData["trade_date"] = pd.to_datetime(expiringTradeData["trade_date"],
                                                     format="%Y-%m-%d").dt.strftime('%d/%m/%Y')
    expiringTradeData["value_date"] = pd.to_datetime(expiringTradeData["value_date"],
                                                     format="%Y-%m-%d").dt.strftime('%d/%m/%Y')
    expiringTradeData["delivery_date"] = pd.to_datetime(expiringTradeData["delivery_date"],
                                                        format="%Y-%m-%d").dt.strftime('%d/%m/%Y')
    # expiringTradeData["legal_entity"] = expiringTradeData["fund"]
    columns_ws = map(lambda x: x.replace("_", " ").title(), columns_ws)
    expiringTradeData.columns = columns_ws
    expiringTradeData.rename({"Notional Amounts Sum":"Existing Amounts", "Notional Amounts Newsum": "Rolling Amounts",
                              "Cp Ref": "CP Short Code", "Trade Id": "Trade ID"}, axis="columns", inplace=True)
    columnLength["Expiring Trade Details"] = expiringTradeData.columns

    nettingSummaryData = pd.DataFrame.from_dict(data_ws1, orient="index")
    try:
        nettingSummaryData['notional_amounts__newsum'] = pd.to_numeric(nettingSummaryData['notional_amounts__newsum'])
        nettingSummaryData['notional_amounts__sum'] = pd.to_numeric(nettingSummaryData['notional_amounts__sum'])
    except Exception as e:
        logger.exception(e)
        print('Cannot convert object to numeric object')

    nettingSummaryData['weight'] = nettingSummaryData['notional_amounts__newsum'] / nettingSummaryData['notional_amounts__sum']
    weight_matrix = nettingSummaryData[['trade_id', 'weight']]
    weight_matrix_new = pd.DataFrame(columns=weight_matrix.columns)
    if len(weight_matrix) >= 1:
        try:
            trade_id_location = weight_matrix.columns.get_loc('trade_id')
            weight_location = weight_matrix.columns.get_loc('weight')
            for i in range(len(weight_matrix)):
                temp_weight_matrix_trade_id = weight_matrix.iloc[i, trade_id_location]
                temp_weight_matrix_weight = weight_matrix.iloc[i, weight_location]
                for j in range(len(temp_weight_matrix_trade_id)):
                    temp_weight_matrix_new = pd.DataFrame(
                        data=[[temp_weight_matrix_trade_id[j],temp_weight_matrix_weight]], columns=weight_matrix_new.columns)
                    weight_matrix_new = weight_matrix_new.append(temp_weight_matrix_new)
        except Exception as e:
            logger.exception("Unable to convert weight matrix")


    nettingSummaryData["physical_settlement_amounts"] = np.maximum(0, nettingSummaryData["notional_amounts__newsum"].astype(int) - nettingSummaryData["notional_amounts__sum"].astype(int))
    nettingSummaryData = nettingSummaryData[columns_ws1]
    nettingSummaryData["value_date"] = pd.to_datetime(nettingSummaryData["value_date"], format = "%Y-%m-%d").dt.strftime('%d/%m/%Y')
    columns_ws1 = map(lambda x:x.replace("_", " ").title().replace("  "," "), columns_ws1)
    nettingSummaryData.columns = columns_ws1
    nettingSummaryData.rename({"Notional Amounts Sum":"Existing Amounts", "Notional Amounts Newsum":"Rolling Amounts", "Cp Ref": "CP Short Code"}, axis="columns",inplace=True)
    nettingSummaryData= nettingSummaryData[nettingSummaryData["Rolling Amounts"].astype(int)>0]
    columnLength["Netting Summary"] = nettingSummaryData.columns

    tempDate = datetime.datetime.strptime(request.POST["valuation_date"], "%Y-%m-%d")
    newValuaDate = tempDate + datetime.timedelta(days=91)
    while(calendar.day_name[newValuaDate.weekday()] != "Wednesday"):
        newValuaDate+=datetime.timedelta(days=1)

    estimatingSettlementData = pd.DataFrame.from_dict(data_ws2, orient="index")
    if len(estimatingSettlementData) > 0:
        estimatingSettlementData['p/r'] = np.where(estimatingSettlementData['ccy_amunt'] > 0, 'Receives', "Pays")
        estimatingSettlementData["ccy_amunt"] = np.abs(estimatingSettlementData["ccy_amunt"])
        # estimatingSettlementData["cp_ref"] = None
        estimatingSettlementData["value_date"] = tempDate
        estimatingSettlementData["value_date"] = pd.to_datetime(estimatingSettlementData["value_date"], format = "%d/%m/%Y").dt.strftime('%d/%m/%Y')
        estimatingSettlementData = estimatingSettlementData[columns_ws2]
        columns_ws2 = map(lambda x:x.replace("_", " ").title().replace("  "," "), columns_ws2)
        estimatingSettlementData.columns = columns_ws2
        estimatingSettlementData.rename({"Cp Ref": "CP Short Code", "Ccy": "Currency", "Ccy Amunt": "Amount"}, axis="columns", inplace=True)
        estimatingSettlementData = estimatingSettlementData[estimatingSettlementData["Amount"] != 0]
        columnLength["Estimating Settlements"] = estimatingSettlementData.columns
        LENGTH = len(estimatingSettlementData)

    rollingTradeData = pd.DataFrame.from_dict(data_ws3, orient="index")

    rollpnl = request.POST.get('rollpnl', 'False') == 'true'
    farleg = request.POST.get('farleg', 'false') == 'true'
    nettingval = request.POST.get('nettingval', 'false') == 'true'
    if len(rollingTradeData) > 0:
        existingTradesOld = rollingTradeData[rollingTradeData["trade_id"] != "New Roll"]
        if len(existingTradesOld) == len(weight_matrix_new):
            weight_matrix_new.fillna(1, inplace=True)
            try:
                existingTradesOld = existingTradesOld.merge(weight_matrix_new, how='left', on=['trade_id'])
                existingTradesOld['notional_amounts'] = existingTradesOld['notional_amounts'] * existingTradesOld['weight']
                existingTradesOld['in_notional_amounts'] = existingTradesOld['in_notional_amounts'] * existingTradesOld['weight']
            except Exception as e:
                logger.exception(e)
                pass
        if rollpnl and farleg:
            try:
                if len(existingTradesOld) >= 1:
                    notional_currency_location = existingTradesOld.columns.get_loc('notional_currency')
                    notional_amounts_location = existingTradesOld.columns.get_loc('notional_amounts')
                    in_notional_currency_location = existingTradesOld.columns.get_loc('in_notional_currency')
                    in_notional_amounts_location = existingTradesOld.columns.get_loc('in_notional_amounts')
                    direction_location = existingTradesOld.columns.get_loc('direction')
                    parallel_ccy_location = existingTradesOld.columns.get_loc('parallel_ccy')
                    for i in range(len(existingTradesOld)):
                        if existingTradesOld.iloc[i, parallel_ccy_location] == 'true':
                            existingTradesOld.iloc[i, notional_currency_location], \
                            existingTradesOld.iloc[i, in_notional_currency_location] = \
                                existingTradesOld.iloc[i, in_notional_currency_location],  \
                                existingTradesOld.iloc[i, notional_currency_location]

                            existingTradesOld.iloc[i, notional_amounts_location], \
                            existingTradesOld.iloc[i, in_notional_amounts_location] = \
                                existingTradesOld.iloc[i, in_notional_amounts_location], \
                                existingTradesOld.iloc[i, notional_amounts_location]

                            existingTradesOld.iloc[i, direction_location] = 'Sell' \
                                if existingTradesOld.iloc[i, direction_location] == 'Buy' else 'Buy'

                    del existingTradesOld['parallel_ccy']

            except Exception as e:
                print(e)
        if 'parallel_ccy' in existingTradesOld.columns:
            del existingTradesOld['parallel_ccy']

        if 'parallel_ccy' in rollingTradeData.columns:
            del rollingTradeData['parallel_ccy']

        newTrades = rollingTradeData[rollingTradeData["trade_id"] =="New Roll"]
        if nettingval:
            existingTradesNEW = existingTradesOld.groupby(["fund", "legal_entity", "counterparty", "underlying"])["notional_amounts"].sum()
            existingTradesNEW = existingTradesNEW.reset_index()
            existingTradesOld.drop_duplicates(subset=["fund", "legal_entity", "counterparty","underlying"], keep="first", inplace=True)
            try:
                existingTradesOld = existingTradesOld.merge(existingTradesNEW, how="left",
                                                            on=["fund", "legal_entity", "counterparty", "underlying"])
            except:
                existingTradesOld = existingTradesOld.join(existingTradesNEW, how="left",
                                                           on=["fund","legal_entity",  "counterparty", "underlying"])

            existingTradesOld.rename({"notional_amounts_y":"notional_amounts"}, axis="columns",inplace=True)
            del existingTradesOld["notional_amounts_x"]

        rollingTradeData = newTrades.append(existingTradesOld, ignore_index=True)

        rollingTradeData["direction"] = np.where(rollingTradeData["notional_amounts"] > 0, "Buy", "Sell")
        rollingTradeData["notional_amounts"] = np.abs(rollingTradeData["notional_amounts"])
        rollingTradeData["style"] = np.where(rollingTradeData["trade_id"] == "New Roll", "Spot", "Forward")
        rollingTradeData["value_date"] = np.where(rollingTradeData["trade_id"] == "New Roll", tempDate, newValuaDate)
        rollingTradeData["value_date"] = pd.to_datetime(rollingTradeData["value_date"], format = "%d/%m/%Y").dt.strftime('%d/%m/%Y')
        fx_data = rollingTradeData.copy()

        # rollingTradeData["cp_ref"] = None
        # rollingTradeData.fillna(method = "bfill", inplace=True,limit=1)
        try:
            columns_ws3.remove('fxall_fund_name')
        except Exception as e:
            logger.info('fxall_fund_name not included into the columns of rolling_trade_data.')
        rollingTradeData = rollingTradeData[columns_ws3]

        columns_ws3 = map(lambda x:x.replace("_", " ").title().replace("  "," "), columns_ws3)
        rollingTradeData.columns = columns_ws3
        rollingTradeData["Strike"] = None
        rollingTradeData.rename({"Cp Ref": "CP Short Code", "Ccy": "Currency", "Ccy Amunt": "Amount"},
                                axis="columns", inplace=True)

    expiringTradeData.to_excel(writer, sheet_name="Expiring Trade Details",
                               index=False, header=False, startrow=2, engine="xlsxwriter")

    sheetNames = ["Expiring Trade Details", "Netting Summary", "Estimating Settlements"]
    if len(fx_data) >= 1:
        fx_data_output = pd.DataFrame(columns=fxall_columns)
        try:
            fx_data_output['CCY Pair'] = fx_data['underlying'].apply(lambda x:x[:3] + '.' + x[3:])
        except:
            logger.info('For some trades, the length of underlying is not equal to 6.')
            fx_data_output['CCY Pair'] = fx_data['underlying']
        fx_data_output['Value DT'] = pd.to_datetime(fx_data['value_date'], format='%d/%m/%Y')
        fx_data_output['Account'] = fx_data['fxall_fund_name']
        fx_data_output['B/S'] = fx_data['direction']
        fx_data_output['B/S'] = fx_data_output['B/S'].apply(lambda x: 'B' if x == 'Buy' else 'S')
        fx_data_output['Deal CCY'] = fx_data['notional_currency']
        fx_data_output['Amount'] = fx_data['notional_amounts'].abs()
        fx_data_output.sort_values(by=['Amount'], inplace=True)
    if len(nettingSummaryData) > 0:
        nettingSummaryData.to_excel(writer, sheet_name="Netting Summary",
                                    index=False, header=False, startrow=2, engine="xlsxwriter")
    else:
        sheetNames.remove("Netting Summary")

    if len(estimatingSettlementData) > 0:
        estimatingSettlementData.to_excel(writer, sheet_name="Estimating Settlements",
                                          index=False, header=False, startrow=2, engine="xlsxwriter")
    else:
        sheetNames.remove("Estimating Settlements")

    if len(rollingTradeData) > 0:
        counterparty_set = np.unique(rollingTradeData["Counterparty"][pd.notna(rollingTradeData["Counterparty"].values)])
        # commnents try to push this file. more todo might be
        for i in range(len(counterparty_set)):
            tempData = rollingTradeData[rollingTradeData["Counterparty"] == counterparty_set[i]]
            tempData.sort_values(by=["Fund", "Underlying", "Notional Amounts"], inplace=True)
            tempData.index = pd.Index(np.arange(len(tempData)))
            del tempData["Counterparty"]
            text = str("Rolling - "+ counterparty_set[i])[:31]
            index = tempData.index[tempData["Notional Amounts"] == 0]
            if(len(index)>0):
                additionIndex = index + 1
                try:
                    tempData.drop(index, axis = 0, inplace=True)
                except:
                    pass
                try:
                    tempData.drop(additionIndex, axis=0, inplace=True)
                except:
                    pass
            if len(tempData) > 0:
                tempData.to_excel(writer, sheet_name=text, index=False, header=False, startrow=2, engine="xlsxwriter")
                sheetNames.append(text)
                columnLength[text] = tempData.columns

    comment = "*Estimates are based on current spot price, these values are subject to change at execution"
    for i in sheetNames:
        worksheet = writer.sheets[i]
        worksheet.write("A1", i, head_style)
        tempColumns = columnLength[i]

        if i == "Estimating Settlements":
            worksheet.write(LENGTH + 2, 0, comment, comment_style)

        for j in range(len(tempColumns)):
            worksheet.write(1, j, columnLength[i][j], column_style)
            if columnLength[i][j].find('Amount') != -1:
                worksheet.set_column(j, j, None, num_style)
            if columnLength[i][j].find('Strike') != -1:
                worksheet.set_column(j, j, None, strike_style)

    if len(fx_data_output) >= 1:
        sheet_name = 'FXALL'
        fx_data_output['Value DT'] = pd.to_datetime(fx_data_output['Value DT']).apply(lambda x:x.strftime('%d-%b-%y'))
        fx_data_output.to_excel(writer, sheet_name=sheet_name, index=False, header=fxall_columns, engine="xlsxwriter")
        fx_work_sheet = writer.sheets[sheet_name]
        fx_value_dt_format = workbook.add_format({'num_format': 'dd-mmm-yy'})
        fx_work_sheet.set_column('H:H', None, num_style)
        fx_work_sheet.set_column('D:D', None, fx_value_dt_format)

    writer.save()
    writer.close()
    return JsonResponse({'data':file_name_wb}, content_type='application/json')


@login_required(login_url='login')
@user_is_allowed
def prepare_trade_info_data(request):
    file_name_wb = 'Hedge_Data_' + str(
        int(datetime.datetime.now(tz=timezone.utc).timestamp() * 1000)) + '.xlsx'

    if request.is_ajax() and request.method == 'POST':


        try:
            module = None
            client = VRMClient.objects.get(name=request.POST['client'])
            if client is None:
                raise Exception('Unable to find the client to process!')
            try:
                module = __import__(client.name.lower())
            except Exception as e:
                try:
                    module = __import__(client.label.lower())
                except Exception as e:
                    raise Exception('Unable to find the client to process!')

            DealTrade = getattr(module.models, 'Dealtrade')

            hedge_data = json.loads(request.POST['hedge_data'])
            #deal_hedge_data = json.loads(request.POST['deal_hedge_data'])
            deal_hedge_data_list = DealTrade.objects.filter(cancellation=0).values('id','trade__trade_id','deal__deal_name','percentage')
            deal_hedge_data = dict()
            count = len(deal_hedge_data_list)
            for item in deal_hedge_data_list:
                deal_hedge_data[len(deal_hedge_data_list)-count] = item
                count = count -1
            writer = pd.ExcelWriter(file_name_wb, engine="xlsxwriter", date_format="dd/MM/yyyy")
            workbook = writer.book

            head_style = workbook.add_format({
                'bold': 1,
                'font_size': 20,
                'valign': 'vcenter',
            })

            column_style = workbook.add_format({
                'font_size': 11,
                'bold': 1,
                'top': 1,
                'bottom': 5,
                'align': 'center',
                'text_wrap': True
            })

            additional_notes_style = workbook.add_format({
                'font_size': 11,
                'bold': 1,
                'text_wrap': True
            })

            num_style = workbook.add_format({
                    'font_size': 11,
                    'num_format': '#,##0.00',
                    'align': 'right'
            })

            strike_style = workbook.add_format({
                    'font_size': 11,
                    'num_format': '0.000000',
                    'align': 'right'
            })

            percentage_style = workbook.add_format({
                    'font_size': 11,
                    'num_format': '0.00%',
                    'align': 'right'
            })

            columns_ws_hedges = ['trade_id', 'client',
                          'fund',
                          'cp_ref',
                          'trade_date',
                          'value_date',
                          'delivery_date',
                          'direction',
                          'underlying',
                          'style',
                          'notional_currency',
                          'notional_amounts',
                          'strike',
                          'premium_currency',
                          'premium',
                          'premium_payment_date',
                          'counterparty'
            ]

            columns_ws_deal_hedges = ['id', 'trade__trade_id', 'deal__deal_name', 'percentage']

            hedge_df = pd.DataFrame.from_dict(hedge_data, orient="index")
            hedge_df = hedge_df[columns_ws_hedges]
            deal_hedge_df = pd.DataFrame.from_dict(deal_hedge_data, orient="index")
            deal_hedge_df = deal_hedge_df[columns_ws_deal_hedges]

            # deal_hedge_df.percentage = deal_hedge_df.percentage * 100;

            hedge_df.rename(
                {
                    "trade_id": "Trade ID",
                    'client': 'Client',
                    'fund': 'Fund',
                    'cp_ref': 'CP Ref',
                    'trade_date': 'Trade Date',
                    'value_date': 'Value Date',
                    'delivery_date': 'Delivery Date',
                    'direction': 'Direction',
                    'underlying': 'Underlying',
                    'style': 'Style',
                    'notional_currency': 'Notional Currency',
                    'notional_amounts': 'Notional Amounts',
                    'strike': 'Strike',
                    'premium_currency': 'Premium Currency',
                    'premium': 'Premium',
                    'premium_payment_date': 'Premium Payment Date',
                    'counterparty': 'Counterparty'
                },
                axis="columns",
                inplace=True)

            deal_hedge_df.rename(
                {
                    'id': 'ID',
                    'trade__trade_id': 'Trade ID',
                    'deal__deal_name': 'Deal Name',
                    'percentage': 'Percentage'
                },
                axis="columns",
                inplace=True)

            sheet_names = ['Hedges', 'Hedges per Deal']

            additional_notes = [
                "",
                """
Additional Notes:

- A negative Percentage indicates the client is selling the notional currency, a positive number indicates the client is buying the notional currency.
                """.strip()
            ]

            hedge_df.to_excel(writer, sheet_name=sheet_names[0], index=False, header=False, startrow=2, engine='xlsxwriter')
            deal_hedge_df.to_excel(writer, sheet_name=sheet_names[1], index=False, header=False, startrow=2, engine='xlsxwriter')

            columns = {
                sheet_names[0]: hedge_df.columns.values.tolist(),
                sheet_names[1]: deal_hedge_df.columns.values.tolist(),
            }

            for index, sheet in enumerate(sheet_names):
                worksheet = writer.sheets[sheet]
                worksheet.write("A1", sheet, head_style)

                # Additional note for a single sheet
                if sheet == "Hedges per Deal":
                    worksheet.set_column('F:F', 60)
                    worksheet.write("F1", additional_notes[index], additional_notes_style)

                tempColumns = columns[sheet]
                for col in range(len(tempColumns)):
                    worksheet.write(1, col, columns[sheet][col], column_style)
                    worksheet.set_column(col, col, 18, None)
                    if columns[sheet][col].find('Notional Amounts') != -1:
                        worksheet.set_column(col, col, 20, num_style)
                    if columns[sheet][col].find('Strike') != -1:
                        worksheet.set_column(col, col, 20, strike_style)
                    if columns[sheet][col].find('Percentage') != -1:
                        worksheet.set_column(col, col, 20, percentage_style)

            writer.save()
            writer.close()
        except Exception as exception:
            print(traceback.format_exc())
        finally:
            resp = {
                'file_name': file_name_wb
            }
            return JsonResponse(resp, content_type='application/json')


@login_required(login_url='login')
@user_is_allowed
def prepare_future_trade_info_data(request):
    file_name_wb = 'Future_Hedge_Data_' + str(
        int(datetime.datetime.now(tz=timezone.utc).timestamp() * 1000)) + '.xlsx'

    if request.is_ajax() and request.method == 'POST':


        try:
            module = None
            client = VRMClient.objects.get(name=request.POST['client'])
            if client is None:
                raise Exception('Unable to find the client to process!')
            try:
                module = __import__(client.name.lower())
            except Exception as e:
                try:
                    module = __import__(client.label.lower())
                except Exception as e:
                    raise Exception('Unable to find the client to process!')

            hedge_data = json.loads(request.POST['hedge_data'])
            deal_hedge_data_list = DealHedges.objects.filter(deal_cancellation=0,
                                                             trade__cancellation=0).values('id', 'trade__id',
                                                                                         'deal__deal',
                                                                                         'contract_number')
            deal_hedge_data = dict()
            count = len(deal_hedge_data_list)
            for item in deal_hedge_data_list:
                deal_hedge_data[len(deal_hedge_data_list)-count] = item
                count = count -1
            writer = pd.ExcelWriter(file_name_wb, engine="xlsxwriter", date_format="dd/MM/yyyy")
            workbook = writer.book

            head_style = workbook.add_format({
                'bold': 1,
                'font_size': 20,
                'valign': 'vcenter',
            })

            column_style = workbook.add_format({
                'font_size': 11,
                'bold': 1,
                'top': 1,
                'bottom': 5,
                'align': 'center',
                'text_wrap': True
            })

            additional_notes_style = workbook.add_format({
                'font_size': 11,
                'bold': 1,
                'text_wrap': True
            })

            num_style = workbook.add_format({
                    'font_size': 11,
                    'num_format': '#,##0.00',
                    'align': 'right'
            })

            strike_style = workbook.add_format({
                    'font_size': 11,
                    'num_format': '0.000000',
                    'align': 'right'
            })

            percentage_style = workbook.add_format({
                    'font_size': 11,
                    'num_format': '0.00%',
                    'align': 'right'
            })
            columns_ws_hedges = ['trade_id', 'fund', 'cp_ref', 'trade_date', 'style', 'counterparty', 'futurecode',
                                 'direction', 'contract_currency', 'contracts', 'contract_size', 'chain_code',
                                 'value_of_1_point', 'description', 'strike']
            columns_ws_deal_hedges = ['id', 'trade__id', 'deal__deal', 'contract_number']
            hedge_df = pd.DataFrame.from_dict(hedge_data, orient="index")
            hedge_df = hedge_df[columns_ws_hedges]
            deal_hedge_df = pd.DataFrame.from_dict(deal_hedge_data, orient="index")
            deal_hedge_df = deal_hedge_df[columns_ws_deal_hedges]

            # deal_hedge_df.percentage = deal_hedge_df.percentage * 100;

            hedge_df.rename({'trade_id': 'Trade ID',
                             'fund': 'Fund',
                             'cp_ref': 'CP Ref',
                             'trade_date': 'Trade Date',
                             'style': 'Style',
                             'counterparty': 'Counterparty',
                             'futurecode': 'Future Code',
                             'direction': 'Direction',
                             'contract_currency': 'Contract Currency',
                             'contracts': 'Contracts',
                             'contract_size': 'Contract Size',
                             'chain_code': 'Chain Code',
                             'value_of_1_point':'Value of 1 Point',
                             'description': 'Description',
                             'strike': 'Strike'
                            }, axis="columns", inplace=True)

            deal_hedge_df.rename(
                {
                    'id': 'ID',
                    'trade__id': 'Trade ID',
                    'deal__deal': 'Deal Name',
                    'contract_number': 'Contract Number'
                },
                axis="columns",
                inplace=True)

            sheet_names = ['Hedges', 'Hedges per Deal']

            additional_notes = [
                "",
                """
Additional Notes:

- A negative Percentage indicates the client is selling the notional currency, a positive number indicates the client is buying the notional currency.
                """.strip()
            ]

            hedge_df.to_excel(writer, sheet_name=sheet_names[0], index=False, header=False, startrow=2, engine='xlsxwriter')
            deal_hedge_df.to_excel(writer, sheet_name=sheet_names[1], index=False, header=False, startrow=2, engine='xlsxwriter')

            columns = {
                sheet_names[0]: hedge_df.columns.values.tolist(),
                sheet_names[1]: deal_hedge_df.columns.values.tolist(),
            }

            for index, sheet in enumerate(sheet_names):
                worksheet = writer.sheets[sheet]
                worksheet.write("A1", sheet, head_style)

                # Additional note for a single sheet
                if sheet == "Hedges per Deal":
                    worksheet.set_column('F:F', 60)
                    worksheet.write("F1", additional_notes[index], additional_notes_style)

                tempColumns = columns[sheet]
                for col in range(len(tempColumns)):
                    worksheet.write(1, col, columns[sheet][col], column_style)
                    worksheet.set_column(col, col, 18, None)
                    if columns[sheet][col].find('Notional Amounts') != -1:
                        worksheet.set_column(col, col, 20, num_style)
                    if columns[sheet][col].find('Strike') != -1:
                        worksheet.set_column(col, col, 20, strike_style)
                    if columns[sheet][col].find('Percentage') != -1:
                        worksheet.set_column(col, col, 20, percentage_style)

            writer.save()
            writer.close()
        except Exception as exception:
            print(traceback.format_exc())
        finally:
            resp = {
                'file_name': file_name_wb
            }
            return JsonResponse(resp, content_type='application/json')


@login_required(login_url='login')
@user_is_allowed
def prepare_deal_level_valuation_report(request):
    file_name_wb = 'Deal-level_Valuation_' + str(
        int(datetime.datetime.now(tz=timezone.utc).timestamp() * 1000)) + '.xlsx'
    report_df = None

    if request.is_ajax() and request.method == 'POST':
        try:
            client = VRMClient.objects.get(label=request.POST['label'])
            # fund = request.POST['fund']
            if request.POST['valuation_date'] == '':
                valuation_date = VrmMtm1.objects.filter(#trade__fund=fund,
                                                        trade__client=client,
                                                        trade__cancellation__iexact='FALSE',
                                                        trade__dummy__iexact='FALSE',
                                                        trade__asset_class__iexact='FX'
                                ). \
                    aggregate(Max('valuation_date'))['valuation_date__max']
            else:
                valuation_date = datetime.datetime.strptime(request.POST['valuation_date'], "%Y-%m-%d")


            # Fetch data from db:
            hedge_deal_data = VrmDealhedge1.objects.filter(
                trade__client=client,
                # trade__fund=fund,
                trade__cancellation__iexact='FALSE',
                trade__dummy__iexact='FALSE',
                trade__asset_class__iexact='FX'
                #trade__value_date__gte=date.today(),
            ).values(
                'trade__trade_id',
                'deal_name',
                'trade__client',
                'trade__fund',
                'trade__cp_ref',
                'trade__trade_date',
                'trade__value_date',
                'trade__delivery_date',
                'trade__direction',
                'trade__underlying',
                'trade__style',
                'trade__notional_currency',
                'trade__notional_amounts',
                'trade__strike',
                'trade__premium_currency',
                'trade__premium',
                'trade__premium_payment_date',
                'trade__counterparty',
                'percentage',
            )

            if hedge_deal_data.count() > 0:
                mtm_data = VrmMtm1.objects.filter(
                    valuation_date=valuation_date,
                    trade__client=client,
                    trade__trade_id__in=list(hedge_deal_data.all().values_list('trade__trade_id', flat=True)),
                ).values(
                    'trade__trade_id',
                    'valuation_date',
                    'spot_ref',
                    'outright_forward',
                    'mtm_currency',
                    'mtm',
                    'net_mtm',
                )

                hedge_deal_df = pd.DataFrame.from_records(hedge_deal_data)
                mtm_df = pd.DataFrame.from_records(mtm_data)
                report_df = pd.merge(hedge_deal_df, mtm_df, on='trade__trade_id', how='inner')

                report_df['sign'] = np.where(report_df.trade__direction == 'Sell', -1, 1)
                report_df['trade__notional_amounts'] = abs(report_df.trade__notional_amounts * report_df.percentage)
                report_df.mtm = report_df.mtm * report_df.percentage * report_df.sign
                report_df.net_mtm = report_df.net_mtm * report_df.percentage * report_df.sign
                report_df.loc[:, 'new_direction'] = np.where(report_df.percentage < 0, 'Sell', 'Buy')


            # Generate Excel file:
            writer = pd.ExcelWriter(file_name_wb, engine="xlsxwriter", date_format="dd/MM/yyyy")
            workbook = writer.book

            head_style = workbook.add_format({
                'bold': 1,
                'font_size': 20
            })

            column_style = workbook.add_format({
                'font_size': 11,
                'bold': 1,
                'top': 1,
                'bottom': 5,
                'align': 'center',
                'text_wrap': True
            })

            num_style = workbook.add_format({
                'font_size': 11,
                'num_format': '#,##0.00',
                'align': 'right'
            })

            strike_style = workbook.add_format({
                'font_size': 11,
                'num_format': '0.000000',
                'align': 'right'
            })

            original_columns = [
                'trade__trade_id',
                'deal_name',
                'trade__client',
                'trade__fund',
                'trade__cp_ref',
                'trade__trade_date',
                'trade__value_date',
                'trade__delivery_date',
                'new_direction',
                'trade__underlying',
                'trade__style',
                'trade__notional_currency',
                'trade__notional_amounts',
                'trade__strike',
                'trade__premium_currency',
                'trade__premium',
                'trade__premium_payment_date',
                'trade__counterparty',
                'valuation_date',
                'spot_ref',
                'outright_forward',
                'mtm_currency',
                'mtm',
                'net_mtm',
            ]
            if report_df is None:
                report_df = pd.DataFrame(columns=original_columns)
            else:
                report_df = report_df[original_columns]

            report_df.rename(
                {
                    'trade__trade_id': 'Trade ID',
                    'deal_name': 'Deal Name',
                    'trade__client': 'Client',
                    'trade__fund': 'Fund',
                    'trade__cp_ref': 'CP Ref',
                    'trade__trade_date': 'Trade Date',
                    'trade__value_date': 'Value Date',
                    'trade__delivery_date': 'Delivery Date',
                    'new_direction': 'Direction',
                    'trade__underlying': 'Underlying',
                    'trade__style': 'Style',
                    'trade__notional_currency': 'Notional Currency',
                    'trade__notional_amounts': 'Notional Amounts',
                    'trade__strike': 'Strike',
                    'trade__premium_currency': 'Premium Currency',
                    'trade__premium': 'Premium',
                    'trade__premium_payment_date': 'Premium Payment Date',
                    'trade__counterparty': 'Counterparty',
                    'valuation_date': 'Valuation Date',
                    'spot_ref': 'Spot Ref',
                    'outright_forward': 'Outright Forward',
                    'mtm_currency': 'MtM Currency',
                    'mtm': 'MtM',
                    'net_mtm': 'Net MtM',
                },
                axis="columns",
                inplace=True
            )

            sheet = 'Details'
            report_df.to_excel(
                writer,
                sheet_name=sheet,
                index=False,
                header=False,
                startrow=1,
                engine='xlsxwriter'
            )

            report_columns = report_df.columns.values.tolist()
            worksheet = writer.sheets[sheet]
            for col in range(len(report_columns)):
                worksheet.write(0, col, report_columns[col], column_style)
                worksheet.set_column(col, col, 18, None)
                if report_columns[col].find('Notional Amounts') != -1:
                    worksheet.set_column(col, col, 20, num_style)
                if report_columns[col].find('Premium') != -1:
                        worksheet.set_column(col, col, 20, num_style)
                if report_columns[col].find('MtM') != -1:
                    worksheet.set_column(col, col, 20, num_style)
                if report_columns[col].find('Net MtM') != -1:
                        worksheet.set_column(col, col, 20, num_style)
                if report_columns[col].find('Strike') != -1:
                    worksheet.set_column(col, col, 20, strike_style)
                if report_columns[col].find('Outright Forward') != -1:
                    worksheet.set_column(col, col, 20, strike_style)

            writer.save()
            writer.close()

        except Exception as exception:
            print(traceback.format_exc())
        finally:
            resp = {
                'file_name': file_name_wb
            }
            return JsonResponse(resp, content_type='application/json')


def download_rolling_data(request, data=None):

    with open(data, 'rb') as fh:
        response = HttpResponse(fh.read(), content_type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
        response['Content-Disposition'] = 'inline; filename=' + os.path.basename(data)
        return response


def download_booking_template(request, data=None):
    path = os.path.join("deal-booking-templates", os.path.basename(data), "deal-booking.xlsm")
    with open(path, 'rb') as fh:
        response = HttpResponse(fh.read(), content_type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
        response['Content-Disposition'] = 'inline; filename=' + 'deal-booking.xlsm'
        return response


@login_required(login_url='login')
@user_is_allowed
def database_hedge_data_table(request):
    if request.is_ajax() is False or request.method != 'POST':
        return HttpResponseBadRequest()
    try:
        resp_data = []
        fund = request.POST['fund']

        if fund == '' or fund is None:
            vrm_hedges = VrmHedge1CopyCopy.objects.filter(
                client=request.POST['client'],
                **HEDGE1_DEFAULT_QUERYSET).order_by('-trade_id')
            resp_data = list(vrm_hedges.values(
                'trade_id',
                'client',
                'fund',
                'cp_ref',
                'trade_date',
                'value_date',
                'delivery_date',
                'style',
                'underlying',
                'direction',
                'notional_currency',
                'notional_amounts',
                'strike',
                'premium_currency',
                'premium',
                'premium_payment_date',
                'counterparty',
                'legal_entity',
                'unwind',
                'unwind_proceeds',
            ))
        else:
            vrm_hedges = VrmHedge1CopyCopy.objects.filter(
                client=request.POST['client'],
                fund=fund,
                cancellation='FALSE',
                dummy='FALSE').order_by('-trade_id')
            resp_data = list(vrm_hedges.values(
                'trade_id',
                'client',
                'fund',
                'cp_ref',
                'trade_date',
                'value_date',
                'delivery_date',
                'style',
                'underlying',
                'direction',
                'notional_currency',
                'notional_amounts',
                'strike',
                'premium_currency',
                'premium',
                'premium_payment_date',
                'counterparty',
                'legal_entity',
                'unwind',
                'unwind_proceeds',
            ))

    except ObjectDoesNotExist as exception:
        print(traceback.format_exc())
    except Exception as exception:
        print(traceback.format_exc())
    finally:
        resp = {
          'data': resp_data,
        }
        return JsonResponse(resp)


@login_required(login_url='login')
@user_is_allowed
def database_deal_hedge_data_table(request):
    if request.is_ajax() is False or request.method != 'POST':
        return HttpResponseBadRequest()
    try:
        resp_data = []
        vrm_hedges = VrmHedge1CopyCopy.objects.filter(
            client=request.POST['client'], **HEDGE1_DEFAULT_QUERYSET
            )
        resp_data = list(VrmDealhedge1.objects.filter(trade__in=vrm_hedges).values(
            'id',
            'trade_id',
            'deal_id',
            'deal_name',
            'percentage',
        ))
    except ObjectDoesNotExist as exception:
        print(traceback.format_exc())
    except Exception as exception:
        print(traceback.format_exc())
    finally:
        resp = {
            'data': resp_data,
        }
        return JsonResponse(resp)


@login_required(login_url='login')
@user_is_allowed
def database_future_hedge_data_table(request):
    if request.is_ajax() is False or request.method != 'POST':
        return HttpResponseBadRequest()
    try:
        resp_data = []
        fund = request.POST['fund']

        if fund == '' or fund is None:
            resp_data = list(Futuretrade.objects.filter().order_by('-trade_id').values())
        else:
            resp_data = list(Futuretrade.objects.filter().order_by('-trade_id').values())
    except ObjectDoesNotExist as exception:
        print(traceback.format_exc())
    except Exception as exception:
        print(traceback.format_exc())
    finally:
        resp = {
          'data': resp_data,
        }
        return JsonResponse(resp)


@login_required(login_url='login')
@user_is_allowed
def database_future_deal_hedge_data_table(request):
    if request.is_ajax() is False or request.method != 'POST':
        return HttpResponseBadRequest()
    try:
        resp_data = []
        vrm_hedges = VrmHedge1CopyCopy.objects.filter(
            client=request.POST['client'], **HEDGE1_DEFAULT_QUERYSET)
        resp_data = list(VrmDealhedge1.objects.filter(trade__in=vrm_hedges).values(
            'id',
            'trade_id',
            'deal_id',
            'deal_name',
            'percentage',
        ))
    except ObjectDoesNotExist as exception:
        print(traceback.format_exc())
    except Exception as exception:
        print(traceback.format_exc())
    finally:
        resp = {
            'data': resp_data,
        }
        return JsonResponse(resp)


@login_required(login_url='login')
@user_is_allowed
def analytics_spotrates_table(request):
    if request.is_ajax() is False or request.method != 'POST':
        return HttpResponseBadRequest()

    client = request.POST['client']
    fund = request.POST['fund']
    valuation_date = None
    if request.POST['valuation_date'] =='':
        valuation_date = VrmMtm1.objects.filter(trade__fund=fund,
                                                trade__client=request.POST['client'],
                                                trade__cancellation__iexact='FALSE',
                                                trade__asset_class__iexact='FX',
                                                trade__dummy__iexact='FALSE'). \
                                    aggregate(Max('valuation_date'))['valuation_date__max']
    else:
        valuation_date = datetime.datetime.strptime(request.POST['valuation_date'], "%Y-%m-%d")
    if None in [fund] or None in [client] or None in [valuation_date]:
        return HttpResponseBadRequest()

    try:
        resp_data = []
        resp_data = list(VrmMtm1.objects.filter(
            trade__fund=fund,
            trade__client=client,
            trade__value_date__gte=valuation_date,
            trade__cancellation__iexact='FALSE',
            trade__dummy__iexact='FALSE',
            trade__asset_class__iexact='FX',
            valuation_date=valuation_date,
        ).values(
            'trade__underlying',
        ).annotate(average_spot_rate=Avg('spot_ref')))
    except ObjectDoesNotExist as exception:
        print(traceback.format_exc())
    except Exception as exception:
        print(traceback.format_exc())
    finally:
        resp = {
            'data': resp_data,
        }
        return JsonResponse(resp)


def _get_mtm_list(request, query_set=None):
    mtm = []
    guarantor_obj = None
    trade_guarantor = None
    g_id = None

    mtm_query_set = {
                     'trade__client': request.POST['client'],
                     'trade__cancellation__iexact': 'FALSE',
                     'trade__asset_class__iexact' : 'FX',
                     'trade__dummy__iexact': 'FALSE'}
    if query_set is not None:
        for key, value in query_set.items():
            mtm_query_set[key] = value
    if request.POST.get('guarantor') and request.POST.get('guarantor') != '':
        try:
            vrm_client = VRMClient.objects.get(name=request.POST['client'])
        except VRMClient.DoesNotExist as e:
            return HttpResponseBadRequest()
        try:
            module = module_loader(vrm_client)
        except Exception as e:
            raise Exception('Unable to find the client to process!')
        trade_guarantor = getattr(module.models, 'Tradeguarantor')
        try:
            guarantor_obj = getattr(module.models, 'Guarantor')
            g_id = guarantor_obj.objects.get(guarantor_name=request.POST.get('guarantor'))
        except ObjectDoesNotExist as e:
            return HttpResponseBadRequest()
        mtm_query_set['trade_id__in'] = list(trade_guarantor.objects.filter(guarantor_id=g_id).values_list('trade_id',
                                                                                                           flat=True))
    elif query_set is None:
        mtm_query_set['trade__fund'] = request.POST['fund']
    else:
        for key, value in query_set.items():
            mtm_query_set[key] = value
    if request.POST['valuation_date'] == '':
        valuation_date = VrmMtm1.objects.filter(**mtm_query_set) \
            .aggregate(Max('valuation_date'))['valuation_date__max']

    else:
        valuation_date = datetime.datetime.strptime(request.POST['valuation_date'], "%Y-%m-%d")

    if valuation_date is None or valuation_date == date.today():
        # is it possible to reach here. may be if no MTM exist for the client.
        valuation_date = get_last_business_day()

    if valuation_date:
        mtm_query_set['trade__value_date__gte'] = valuation_date
        mtm_query_set['trade__trade_date__lte'] = valuation_date
        mtm_query_set['valuation_date'] = valuation_date

    if VrmMtm1.objects.filter(**mtm_query_set).exists():
        mtm = VrmMtm1.objects.filter(**mtm_query_set).filter(Q(trade__unwind__iexact='FALSE') |
                                                             Q(trade__npv_date__gte=valuation_date) |
                                                             Q(trade__npv_date__isnull=True)) \
                                                      .values(*MTM_DEFAULT_VALUE_SET)
    else:
        mtm = VrmMtm1.objects.filter(**mtm_query_set)
    return valuation_date, mtm


def _get_proportional_mtm_list(request, query_set=None):
    valuation_date = None

    try:
        vrm_client = VRMClient.objects.get(name=request.POST['client'])
    except VRMClient.DoesNotExist as e:
        return HttpResponseBadRequest()
    try:
        module = module_loader(vrm_client)
    except Exception as e:
        raise Exception('Unable to find the client to process!')

    if request.POST.get('valuation_date', None)  in ['', None]:
        valuation_date = module.models.ProportionalMtm.objects.filter() \
            .aggregate(Max('valuation_date'))['valuation_date__max']
    else:
        valuation_date = datetime.datetime.strptime(request.POST['valuation_date'], "%Y-%m-%d")
        if valuation_date == date.today():
            valuation_date = module.models.ProportionalMtm.objects.filter() \
                .aggregate(Max('valuation_date'))['valuation_date__max']
    if valuation_date is None or valuation_date == date.today():
        # is it possible to reach here. may be if no MTM exist for the client.
        valuation_date = module.models.ProportionalMtm.objects.filter() \
            .aggregate(Max('valuation_date'))['valuation_date__max']
        if valuation_date is None:
            valuation_date = get_last_business_day()
    mtm_query_set = {}
    if query_set is not None:
        for key, value in query_set.items():
            if key == "trade__fund":
                key = "fund_id"
                if Funds.objects.filter(fund_name__iexact=value).exists():
                    fund_id = Funds.objects.filter(fund_name__iexact=value).first().pk
                else:
                    fund_id = 0
                value = fund_id
            elif key =="trade__legal_entity":
                key = "entity_id"
                if module.models.Entity.objects.filter(entity_name__iexact=value).exists():
                    entity_id = module.models.Entity.objects.filter(entity_name__iexact=value).first().pk
                else:
                    entity_id = 0
                value = entity_id
            elif key == "trade__counterparty":
                key = "cp_id"
                if Counterparties.objects.filter(counterparty_name__iexact=value).exists():
                    cp_id = Counterparties.objects.filter(counterparty_name__iexact=value).first().pk
                else:
                    cp_id = 0
                value = cp_id
            mtm_query_set[key] = value

    ProportionalMtm = getattr(module.models, 'ProportionalMtm')
    proportional_trades = None
    if ProportionalMtm:
        proportional_trades = getattr(module.models, 'ProportionalTrade').objects.filter(**mtm_query_set,
                                                                                         value_date__gte=valuation_date,
                                                                                        ).filter(
                                                Q(unwind__iexact='FALSE') |
                                                Q(npv_date__gt=valuation_date) |
                                                Q(npv_date__isnull=True)).exclude(notional_amounts_proportional__isnull=True) \
            .values(*PROPORTIONAL_TRADE_VALUE_SET)
        if len(proportional_trades) > 0:
            if 'trade_id__in' in mtm_query_set.keys():
                mtm_query_set['trade_id__in'] = set(mtm_query_set['trade_id__in']+
                                                    list(proportional_trades.values_list('trade_id', flat=True)))
            else:
                mtm_query_set['trade_id__in'] = list(proportional_trades.values_list('trade_id', flat=True))
            mtm = ProportionalMtm.objects.filter(**mtm_query_set, valuation_date=valuation_date).values(*PROPORTIONAL_MTM_VALUE_SET)
        else:
            mtm = ProportionalMtm.objects.none()
        if "entity_id" in mtm_query_set.keys():
            value_set_prop = [x for x in PROPORTIONAL_TRADE_VALUE_SET if
                              x not in ['notional_amounts_proportional', 'trade_id']]
            if len(proportional_trades) > 0:
                prop_trades = list(proportional_trades.values('trade_id'). \
                    annotate(notional_amounts_proportional=Sum( 'notional_amounts_proportional')))
                for ppt in prop_trades:
                    current_trade = proportional_trades.filter(
                        trade_id=ppt['trade_id']).values(*value_set_prop).first()
                    ppt.update(current_trade)
                proportional_trades = prop_trades
            else:
                proportional_trades = []
    return valuation_date, mtm, proportional_trades, mtm_query_set


def _construct_proportional_mtm_trade_table(proportional_mtm, proportional_trades, valuation_date, deal_table=None):
    table = []
    mtm_sum = 0
    for trade in proportional_trades:
        try:
            mtm_row = None
            for element in proportional_mtm:
                if element['trade_id'] == trade['trade_id'] and \
                   element['fund_id'] == trade['fund_id'] and \
                   element['entity_id'] == trade['entity_id'] and \
                   element['cp_id'] == trade['cp_id']:
                    mtm_row = element
                    proportional_mtm.remove(element)
                    break
            if mtm_row is None:
                mtm_row = {
                    'valuation_date': str(valuation_date),
                    'mtm_currency': trade['premium_currency'],
                    'mtm_proportional': 0,
                    'net_mtm_proportional': 0,
                }
            #TODO: Fix foriegn key problem in model so that counterparty name can be retrieved directly
            counterparty_name = {
                'cp__counterparty_name': list(Counterparties.objects.filter(id=trade['cp_id'])
                                              .values('counterparty_name'))[0]['counterparty_name']
            }
            row = {**trade, **counterparty_name, **mtm_row}
            for gen_field, prop_field in REGULAR_TO_PROPORTIONAL_MTM_NAME_MAPPING.items():
                try:
                    if gen_field == 'valuation_date':
                        row[gen_field] = str(row[prop_field])
                    else:
                        row[gen_field] = row[prop_field]
                    if prop_field != gen_field:
                        del row[prop_field]
                except Exception as e:
                    row[gen_field] = None
                    print(e)
            # row['valuation_date'] = row['valuation_date'].strftime("%Y-%m-%d")
            if deal_table:
                percentage = 0
                if row['trade__trade_id'] in list(deal_table.values_list('trade_id', flat=True)):
                    try:
                        percentage = 0
                        for deal_trade in deal_table:
                            if deal_trade.trade_id == row['trade__trade_id']:
                                percentage = deal_trade.percentage
                                break
                        row['trade__notional_amounts'] = row['trade__notional_amounts'] * percentage
                        row['mtm'] = row['mtm'] * percentage
                        mtm_sum += row['mtm']
                        table += [row]
                    except Exception as e:
                        percentage = 0
            else:
                table += [row]
                mtm_sum += row['mtm']
        except Exception as e:
            logger.error(e)
    return table, mtm_sum


def _add_new_trades(request, resp_data, currency, mtm, valuation_date, query_set=None):
    '''
    # check whether any new trade is added and not included here. As reverse lookup will not pick the objects.
    # These new trades will not have any market value.
    # Added the condition to include unwind or npv_date conditonality
    '''
    if request.POST.get('guarantor') and request.POST.get('guarantor') != '':
        if query_set is None:
            query_set = dict()
        else:
            new_dict = dict()
            for key, value in query_set.items():
                if 'trade__' in key:
                    new_dict[key.replace('trade__', '')] = value
                else:
                    new_dict[key] = value
            query_set.clear()
            query_set = new_dict
        try:
            vrm_client = VRMClient.objects.get(name=request.POST['client'])
        except VRMClient.DoesNotExist as e:
            return HttpResponseBadRequest()
        try:
            module = module_loader(vrm_client)
        except Exception as e:
            raise Exception('Unable to find the client to process!')
        trade_guarantor = getattr(module.models, 'Tradeguarantor')
        try:
            guarantor_obj = getattr(module.models, 'Guarantor')
            g_id = guarantor_obj.objects.get(guarantor_name=request.POST.get('guarantor'))
        except ObjectDoesNotExist as e:
            return HttpResponseBadRequest()
        query_set['trade_id__in'] = list(trade_guarantor.objects.filter(guarantor_id=g_id).values_list('trade_id',
                                                                                                           flat=True))
    elif query_set is None:
        query_set = dict()
        query_set['fund'] = request.POST['fund']
    else:
        # Make sure if MTM queryset is used it is reverse lookup that mean we need to correct the given queryset
        new_dict = dict()
        for key, value in query_set.items():
            if 'trade__' in key:
                new_dict[key.replace('trade__', '')] = value
            else:
                new_dict[key] = value
        query_set.clear()
        query_set = new_dict
    # npv_date should be greater than valuation_date to be included as a new Trade
    hedge_list = list(VrmHedge1CopyCopy.objects.filter(client=request.POST['client'],
                                                       **HEDGE1_DEFAULT_QUERYSET,
                                                       trade_date__lte=valuation_date,
                                                       value_date__gte=valuation_date,
                                                       **query_set).
                      filter(Q(unwind__iexact='FALSE') |
                             Q(npv_date__gt=valuation_date) |
                             Q(npv_date__isnull=True)).
                      values_list('trade_id', flat=True))
    for trade in VrmHedge1CopyCopy.objects.filter(trade_id__in=list(set(hedge_list).difference(set(list(
                                                  mtm.values_list('trade_id', flat=True)))))):
        resp_data.append({'trade__trade_id': trade.trade_id,
                          'trade__fund': trade.fund,
                          'trade__trade_date': trade.trade_date,
                          'trade__value_date': trade.value_date,
                          'trade__delivery_date': trade.delivery_date,
                          'trade__style': trade.style,
                          'trade__underlying': trade.underlying,
                          'trade__direction': trade.direction,
                          'trade__notional_currency': trade.notional_currency,
                          'trade__notional_amounts': trade.notional_amounts,
                          'trade__strike': trade.strike,
                          'trade__premium': trade.premium,
                          'trade__premium_payment_date': trade.premium_payment_date,
                          'trade__counterparty': trade.counterparty,
                          'valuation_date': valuation_date.strftime("%Y-%m-%d"),
                          'outright_forward': 0.0,
                          'spot_ref': 0.0,
                          'mtm_currency': currency,
                          'mtm': 0.0,
                          'net_mtm': 0.0
                          })


@login_required(login_url='login')
@user_is_allowed
def analytics_mtm_table(request):
    if request.is_ajax() is False or request.method != 'POST':
        return HttpResponseBadRequest()
    try:
        mtm_values = []
        currency = None
        trade_guarantor = None
        guarantor_obj = None
        valuation_date = None
        g_id = None
        mtm_sum = 0
        date_list = []
        fund = request.POST.get('fund')
        counterparty = request.POST.get('counterparty')
        entity = request.POST.get('entity') or ''
        guarantor = request.POST.get('guarantor') or ''
        vrm_client = VRMClient.objects.get(name=request.POST['client'])
        general_client = Clients.objects.get(client_name=vrm_client.name)
        general_fund = Funds.objects.get(fund_name=fund, client=general_client.pk)
        is_proportional_tab = request.POST.get('is_proportional_tab') or False

        try:
            module = module_loader(vrm_client)
        except Exception as e:
            raise Exception('Unable to find the client to process!')

        if fund is None :
            return HttpResponseBadRequest()

        if entity and entity != '':
            mtm_queryset = {'trade__legal_entity': entity}
        elif guarantor and guarantor != '':
            try:
                vrm_client = VRMClient.objects.get(name=request.POST.get('client'))
            except VRMClient.DoesNotExist as e:
                return HttpResponseBadRequest()
            try:
                module = module_loader(vrm_client)
            except Exception as e:
                raise Exception('Unable to find the client to process!')
            trade_guarantor = getattr(module.models, 'Tradeguarantor')
            try:
                guarantor_obj = getattr(module.models, 'Guarantor').objects.get(guarantor_name=guarantor)
                g_id = guarantor_obj.guarantor_id
                currency = guarantor_obj.local_ccy
            except ObjectDoesNotExist as e:
                return HttpResponseBadRequest()
            mtm_queryset = {'trade_id__in': list(trade_guarantor.objects.filter(guarantor_id=g_id).
                                                 values_list('trade_id', flat=True))}
        else:
            mtm_queryset = {'trade__fund': fund}
        if vrm_client.entity_summary and entity and entity != '':
            agg_set = {
                'cp': Counterparties.objects.get(counterparty_name=counterparty)} if counterparty != 'all' else {}
            if entity and entity != '':
                agg_set['entity_id'] = getattr(module.models, 'Entity').objects.get(entity_name=entity).pk
            else:
                # general_fund = Funds.objects.get(fund_name=fund, client_id=general_client.pk)
                # using the related name
                agg_set['fund'] = getattr(module.models, 'Fund').objects.get(fund_name=general_fund.fund_name)
            currency = getattr(module.models, 'LegalCounterparty'). \
                objects.filter(**agg_set).first().base_currency
        elif vrm_client.guarantor_summary and guarantor and guarantor != '':
            #currency is already set
            pass
        else:
            try:
                currency = getattr(module.models, 'Fund').objects. \
                    get(fund_name=general_fund.fund_name).ccy
            except ObjectDoesNotExist as e:
                currency = ''
            # currency = VrmFund.objects.filter(
            #     client=request.POST['client'], fund=fund
            # ).first().fund_currency
        if request.POST.get('counterparty', '') != 'all' and request.POST.get('counterparty', '') != '':
            mtm_queryset['trade__counterparty'] = counterparty
        mtm = None
        valuation_date = None
        # fetch mtm based trades
        if not VRMContentTab.objects.filter(
                label='proportional_mtm',
                client__name=vrm_client
        ).exists() or not is_proportional_tab:
            valuation_date, mtm = _get_mtm_list(request, query_set=mtm_queryset)

            if valuation_date is None:
                return HttpResponseBadRequest()
            mtm_values = []
            if mtm.count() > 0:
                mtm = mtm.filter(valuation_date=mtm.aggregate(Max('valuation_date'))['valuation_date__max'])
                mtm_sum = round(mtm.aggregate(Sum('mtm'))['mtm__sum']or 0.0)
                mtm_values = list(mtm.values(*MTM_DEFAULT_VALUE_SET))
                if vrm_client.guarantor_summary and guarantor and guarantor != '':
                    new_mtm_sum = 0
                    for guarantor_item in mtm_values:
                        percentage = 0
                        if trade_guarantor.objects.filter(guarantor_id=g_id,
                                                          trade_id=guarantor_item['trade__trade_id']).exists():
                            percentage = trade_guarantor.objects.filter(guarantor_id=g_id,
                                                                        trade_id=guarantor_item['trade__trade_id'])[0].\
                                percentage
                        guarantor_item['mtm'] = guarantor_item['mtm'] * percentage
                        guarantor_item['net_mtm'] = guarantor_item['net_mtm'] * percentage
                        guarantor_item['trade__notional_amounts'] = guarantor_item['trade__notional_amounts'] * percentage
                        new_mtm_sum += guarantor_item['mtm']
                    mtm_sum = round(new_mtm_sum)
            _add_new_trades(request, mtm_values, currency, mtm, valuation_date, query_set=mtm_queryset)

            date_list = list(VrmMtm1.objects.filter(trade__client=request.POST['client'],
                                                    trade__fund=fund,
                                                    trade__cancellation__iexact='FALSE',
                                                    trade__asset_class__iexact='FX',
                                                    trade__dummy__iexact='FALSE',
                                                    trade__unwind__iexact='FALSE',
                                                    **mtm_queryset)\
                                            .values_list('valuation_date', flat=True)\
                                            .distinct().order_by('-valuation_date'))
        else:
            valuation_date, mtm, proportional_trades, mtm_query_set = \
                _get_proportional_mtm_list(request, query_set=mtm_queryset)

            if valuation_date is None:
                return HttpResponseBadRequest()
            mtm_values = []
            if mtm.count() > 0:
                mtm = mtm.filter(valuation_date=mtm.aggregate(Max('valuation_date'))['valuation_date__max'])
                mtm_sum = round(mtm.aggregate(Sum('mtm_proportional'))['mtm_proportional__sum'])

            if 'entity_id' in mtm_query_set.keys():
                mtm = mtm.values('trade_id').annotate(mtm_proportional=Sum('mtm_proportional'),
                                                      net_mtm_proportional=Sum('net_mtm_proportional'))
                current_mtm = getattr(module.models, 'ProportionalMtm').objects.filter(
                    trade_id__in=mtm.values_list('trade_id', flat=True), valuation_date=valuation_date).order_by(
                    '-updated_timestamp').values('fund_id',
                                                 'cp_id',
                                                 'entity_id',
                                                 'trade_id',
                                                 'spot_ref',
                                                 'outright_forward',
                                                 'valuation_date',
                                                 'mtm_currency')
                mtm = list(mtm)
                for mtm_item in mtm:
                    merge_mtm = current_mtm.filter(trade_id=mtm_item['trade_id']).first()
                    mtm_item.update(merge_mtm)
            else:
                mtm = list(mtm)
            mtm_values, mtm_sum = _construct_proportional_mtm_trade_table(mtm,
                                                                 proportional_trades,
                                                                 valuation_date)

            date_list = list(module.models.ProportionalMtm.objects.filter(**mtm_query_set)
                             .values_list('valuation_date', flat=True)
                             .distinct().order_by('-valuation_date'))
            agg_set = {
                'cp': Counterparties.objects.get(counterparty_name=counterparty)} if counterparty != 'all' else {}
            if entity and entity != '':
                agg_set['entity_id'] = getattr(module.models, 'Entity').objects.get(entity_name=entity).pk
            else:
                general_fund = Funds.objects.get(fund_name=fund, client_id=general_client.pk)
                # using the related name
                agg_set['fund'] = getattr(module.models, 'Fund').objects.get(fund_name=general_fund.fund_name)
            currency = getattr(module.models, 'LegalCounterparty'). \
                objects.filter(**agg_set).first().base_currency


    except ObjectDoesNotExist as exception:
        print(traceback.format_exc())
    except Exception as exception:
        print(traceback.format_exc())
    finally:
        resp = {
            'data': mtm_values,
            'mtm_sum': mtm_sum,
            'currency': currency,
            'date_list': date_list,
            'valuation_date': valuation_date if valuation_date else date.today()
        }
        return JsonResponse(resp)


@login_required(login_url='login')
@user_is_allowed
def analytic_futures_mtm_table(request):
    if request.is_ajax() is False or request.method != 'POST':
        return HttpResponseBadRequest()
    try:
        new_mtm_values = []
        currency = None
        mtm_sum = 0
        date_list = []
        fund = request.POST.get('fund')
        counterparty = request.POST.get('counterparty')
        vrm_client = VRMClient.objects.get(name=request.POST['client'])
        general_client = Clients.objects.get(client_name=vrm_client.name)
        try:
            module = module_loader(vrm_client)
        except Exception as e:
            raise Exception('Unable to find the client to process!')

        if fund is None:
            return HttpResponseBadRequest()
        # Assuming this will be UTAM used.
        currency = 'CAD'
        # missing counterparty
        # if request.POST.get('counterparty', '') != 'all':
        #     mtm_queryset['trade__counterparty'] = counterparty


        mtm = None
        valuation_date = None
        future_summary = getattr(module.models, 'FutureSummary')
        if request.POST['valuation_date'] == '':
            valuation_date = future_summary.objects.filter(fund=fund) \
                .aggregate(Max('valuation_date'))['valuation_date__max']
            if valuation_date is None:
                # is it possible to reach here. may be if no MTM exist for the client.
                valuation_date = date.today()
        else:
            valuation_date = datetime.datetime.strptime(request.POST['valuation_date'], "%Y-%m-%d")
            if valuation_date == date.today():
                valuation_date = future_summary.objects.filter(fund=fund) \
                    .aggregate(Max('valuation_date'))['valuation_date__max']

        if valuation_date is None:
            return HttpResponseBadRequest()
        queryset = {
                    'fund': fund, 'spot_ref_date': valuation_date,
                    'trade_date__lte': valuation_date,
                    'lasttradedate__gte': valuation_date
        }
        update_timestamp = future_summary.objects.filter(valuation_date=valuation_date).order_by('-update_timestamp').first().update_timestamp
        if counterparty != 'all':
            queryset['counterparty'] = counterparty
            summary_value = list(future_summary.objects.filter(fund=fund, contracts__gt=0, counterparty=counterparty,
                                                               valuation_date=valuation_date,
                                                               update_timestamp=update_timestamp).values())
        else:
            summary_value = list(future_summary.objects.filter(fund=fund, contracts__gt=0,
                                                               valuation_date=valuation_date,
                                                               update_timestamp=update_timestamp
                                                               ).values())
        mtm_values = IndividualFutureTrade.objects.filter(**queryset).values()
        new_mtm_values = []
        total_gross = 0
        for mtm in mtm_values:
            new_dict = mtm
            try:
                if mtm['ccy_underlying']:
                    new_dict['gross_mtm'] = mtm['gross_mtm_contract_ccy']/ mtm['ccy_underlying']
                elif mtm['ccy_underlying_reverse']:
                    new_dict['gross_mtm'] = mtm['gross_mtm_contract_ccy']* mtm['ccy_underlying_reverse']
                else:
                    new_dict['gross_mtm'] = mtm['gross_mtm_contract_ccy']

                total_gross += new_dict['gross_mtm']
                new_mtm_values.append(new_dict)
            except Exception as e:
                logger.exception(new_dict)
        # mtm_values = mtm_values.annotate(gross_mtm=F('gross_mtm_contract_ccy')*F('ccy_underlying')
        # if ('ccy_underlying') else F('gross_mtm_contract_ccy') / F('ccy_underlying_reverse')
        # if ('ccy_underlying_reverse') else F('gross_mtm_contract_ccy'))
        mtm_sum = round(total_gross)
        date_list = list(IndividualFutureTrade.objects.filter(fund=fund).values_list('spot_ref_date', flat=True).
                         distinct().order_by('-spot_ref_date'))

    except ObjectDoesNotExist as exception:
        print(traceback.format_exc())
    except Exception as exception:
        print(traceback.format_exc())
    finally:
        resp = {
            'data': new_mtm_values if len(new_mtm_values) > 0 else [],
            'summary_data': summary_value,
            'mtm_sum': mtm_sum,
            'currency': currency,
            'date_list': date_list,
            'valuation_date': valuation_date if valuation_date else date.today()
        }
        return JsonResponse(resp)


@login_required(login_url='login')
@user_is_allowed
def recalculate_mtm_on_spotref_url(request):
    if request.is_ajax() is False or request.method != 'POST':
        return HttpResponseBadRequest()

    try:
        totalPages = 1
        mtm = []
        resp_data = []
        resp_sum = 0
        resp_currency = None
        mtm = None
        valuation_date = None
        # fetch mtm based trades
        valuation_date, mtm = _get_mtm_list(request)
        spot_ref_data = pd.DataFrame(json.loads(request.POST['spot_ref_data'] or '[]'))
        mtm_data = pd.DataFrame(list(mtm) or '[]')
        mtm_data['new_spot_ref'] = np.nan
        for index, row in spot_ref_data.iterrows():
            match = mtm_data['trade__underlying'] == row['trade__underlying']
            if match.any():
                mtm_data.loc[match, 'new_spot_ref'] = \
                    float(row['average_spot_rate'])

        for index, row in mtm_data.iterrows():
            fwd_points = row['outright_forward'] - row['spot_ref']
            new_outright_forward = fwd_points + row['new_spot_ref']

            ccy1 = row['trade__underlying'][:3]
            ccy2 = row['trade__underlying'][3:]

            new_mtm = 0
            if row['trade__style'] in ["Spot", "Put", "Call", "Forward", "Swap"]:
                if row['trade__notional_currency'] == ccy1:
                    if row['mtm_currency'] == ccy2:
                        if row['trade__direction'].lower() == "buy":
                            new_mtm = row['trade__notional_amounts'] * (new_outright_forward - row['trade__strike'])
                        else:
                            new_mtm = -row['trade__notional_amounts'] * (new_outright_forward - row['trade__strike'])
                    else:
                        if row['trade__direction'].lower() == "buy":
                            new_mtm = row['trade__notional_amounts'] * (new_outright_forward - row['trade__strike']) /\
                                      row['new_spot_ref']
                        else:
                            new_mtm = -row['trade__notional_amounts'] * (new_outright_forward - row['trade__strike']) /\
                                      row['new_spot_ref']
                else:
                    if row['mtm_currency'] == ccy2:
                        if row['trade__direction'].lower() == "buy":
                            new_mtm = row['trade__notional_amounts'] *\
                                      (1/new_outright_forward - 1/row['trade__strike']) * \
                                      row['new_spot_ref']
                        else:
                            new_mtm = -row['trade__notional_amounts'] *\
                                      (1/new_outright_forward - 1/row['trade__strike']) * \
                                      row['new_spot_ref']
                    else:
                        if not new_outright_forward == 0:
                            if row['trade__direction'].lower() == "buy":
                                new_mtm = row['trade__notional_amounts'] *\
                                          (1/new_outright_forward - 1/row['trade__strike'])
                            else:
                                new_mtm = -row['trade__notional_amounts'] *\
                                          (1/new_outright_forward - 1/row['trade__strike'])
                        else:
                            new_mtm = 0

            row['spot_ref'] = row['new_spot_ref']
            row['outright_forward'] = new_outright_forward
            row['mtm'] = new_mtm
            resp_sum += new_mtm
            resp_data += [row.to_dict()]
        if resp_sum != 0:
            resp_sum = resp_sum / 1e6
        resp_currency = resp_data[0]['trade__premium_currency'] if len(resp_data) > 0 else None
        # include missing trades
        _add_new_trades(request, resp_data, resp_currency, mtm, valuation_date)

    except ObjectDoesNotExist as exception:
        print(traceback.format_exc())
    except Exception as exception:
        print(traceback.format_exc())
    finally:
        resp = {
            'data': resp_data,
            'mtm_sum': resp_sum,
            'currency': resp_currency
        }
        return JsonResponse(resp)


@login_required(login_url='login')
@user_is_allowed
def pickup_cost_analytics_mtm_table(request):
    if request.is_ajax() is False or request.method != 'POST':
        return HttpResponseBadRequest()
    try:
        fund = request.POST.get('fund')
        counterparty = request.POST.get('counterparty')

        resp_data = []
        date_list = []
        pick_up_total = 0
        credit_cost_total = 0
        resp_currency = None
        start_valuation_date = None
        end_valuation_date = None
        resp_sum = None
        date_list = list(VrmHedge1CopyCopy.objects.filter(
                            client=request.POST['client'],
                            fund=fund,
                            **HEDGE1_DEFAULT_QUERYSET,
                        ).values_list('trade_date', flat=True).distinct().order_by('-value_date'))
        if request.POST['start_valuation_date'] =='':
            start_valuation_date = VrmHedge1CopyCopy.objects.filter(fund=fund,
                                                                    client=request.POST['client'],
                                                                    **HEDGE1_DEFAULT_QUERYSET). \
                                        aggregate(Min('trade_date'))['trade_date__min']
        else:
            start_valuation_date = datetime.datetime.strptime(request.POST['start_valuation_date'], "%Y-%m-%d")
        if request.POST['end_valuation_date'] =='':
            end_valuation_date = VrmHedge1CopyCopy.objects.filter(fund=fund,
                                                                  client=request.POST['client'],
                                                                  **HEDGE1_DEFAULT_QUERYSET,
                                                                  value_date__lte=date.today()
                                                                  ). \
                                        aggregate(Max('trade_date'))['trade_date__max']
        else:
            end_valuation_date = datetime.datetime.strptime(request.POST['end_valuation_date'], "%Y-%m-%d")

        if VrmHedge1CopyCopy.objects.filter(fund=fund,
                                            client=request.POST['client'],
                                            **HEDGE1_DEFAULT_QUERYSET,
                                            trade_date__gte=start_valuation_date,
                                            trade_date__lte=end_valuation_date,
                                            ).exists():

            mtm = VrmHedge1CopyCopy.objects.filter(fund=fund,
                                                   client=request.POST['client'],
                                                   **HEDGE1_DEFAULT_QUERYSET,
                                                   trade_date__gte=start_valuation_date,
                                                   trade_date__lte=end_valuation_date,
                                                   value_date__lte=date.today()
                                                   ).values(
                                                    'trade_date',
                                                    'value_date',
                                                    'underlying',
                                                    'direction',
                                                    'notional_currency',
                                                    'notional_amounts',
                                                    'traded_spot',
                                                    'traded_forward',
                                                    'strike',
                                                    'premium',
                                                    'premium_currency'
                                                    ).order_by('-value_date')
            resp_data = list(mtm)

            for item in resp_data:
                if item['underlying'] == item['notional_currency']+item['premium_currency']:
                    item['pick_up'] = (item['notional_amounts'] * item['strike']) - (item['notional_amounts'] * item['traded_spot'])
                    item['credit_cost'] = (item['notional_amounts'] * item['strike']) - (
                                item['notional_amounts'] * item['traded_forward'])
                else:
                    try:
                        item['pick_up'] = (item['notional_amounts'] / item['strike']) - (
                                    item['notional_amounts'] / item['traded_spot'])
                    except:
                        item['pick_up'] = 0.0
                    try:
                        item['credit_cost'] = (item['notional_amounts'] / item['strike']) - (
                                item['notional_amounts'] / item['traded_forward'])
                    except:
                        item['credit_cost'] = 0.0
                pick_up_total = pick_up_total + item['pick_up']
                credit_cost_total = credit_cost_total + item['credit_cost']
            resp_currency = VrmHedge1CopyCopy.objects.filter(
                client=request.POST['client'],
                fund=fund,
            ).first().premium_currency
    except ObjectDoesNotExist as exception:
        print(traceback.format_exc())
    except Exception as exception:
        print(traceback.format_exc())
    finally:
        resp = {
            'data': resp_data,
            'pick_up_total': pick_up_total,
            'credit_cost_total': credit_cost_total,
            'currency': resp_currency,
            'date_list': date_list,
            'start_valuation_date': start_valuation_date if start_valuation_date else date.today(),
            'end_valuation_date': end_valuation_date if end_valuation_date else date.today()
        }
        return JsonResponse(resp)


@login_required(login_url='login')
@user_is_allowed
def summary_base_case_risk(request):
    if request.is_ajax() is False or request.method != 'POST':
        return HttpResponseBadRequest()

    try:
        base_case = 0
        current_fx_gain_loss = 0
        at_risk = 0
        hedging_effect = 0
        cost = 0
        worst_case = 0

        fund = request.POST['fund']
        confidence_level = request.POST['confidence_level']

        module = None
        client = VRMClient.objects.get(name=request.POST['client'])
        if client is None:
            raise Exception('Unable to find the client to process!')
        try:
            module = __import__(client.name.lower())
        except Exception as e:
            try:
                module = __import__(client.label.lower())
            except Exception as e:
                raise Exception('Unable to find the client to process!')
        IrrAtRisk = getattr(module.models, 'IrrAtRisk')
        IrrAtRiskNoCost = getattr(module.models, 'IrrAtRiskNoCost')
        SummaryTable = getattr(module.models, 'SummaryTable')
        general_client = Clients.objects.get(client_name=client)
        general_fund = Funds.objects.get(fund_name=request.POST['fund'], client=general_client.pk)
        query = None
        fund_type_use = request.POST['fund']
        try:
            fund_exist = IrrAtRisk.objects.filter(fund_0=fund_type_use)
            query = {'fund_0': fund_type_use}
        except:
            fund_exist = IrrAtRisk.objects.filter(fund=general_fund)
            fund_type_use = general_fund
            query = {'fund': fund_type_use}

        irr_at_risk = IrrAtRisk.objects.filter(**query, deal_name='Overall', cl=confidence_level).order_by('-update_date')
        irr_at_risk_no_cost = IrrAtRiskNoCost.objects.filter(**query, deal_name='Overall', cl=confidence_level).order_by('-update_date')
        #TODO correct this as well to above to keep in sync
        summary_table = SummaryTable.objects.filter(fund=fund_type_use, deal_name='Overall').order_by('-update_date')
        current_spot = 0.0
        base_case =0.0
        current_fx_gain_loss = 0.0
        filter_value = 0.0
        at_risk = 0.0
        irr_unhedged_value = 0.0
        hedge_irr_value = 0.0
        irr_value = 0.0
        irr_nocost_hedge_value= 0.0
        if len(summary_table) > 0 :
            base_case = summary_table.values('expected_irr')[0]['expected_irr'] * 100
            current_fx_gain_loss = (summary_table.values('irr_current_spot')[0]['irr_current_spot'] - \
                                   summary_table.values('expected_irr')[0]['expected_irr']) * 100

            current_spot = summary_table.values('irr_current_spot')[0]['irr_current_spot'] * 100
        if len(irr_at_risk) > 0 :
            filter_value = irr_at_risk.filter(type='Unhedged', cl=confidence_level).values('value')[0]['value']
            irr_unhedged_value = irr_at_risk.filter(type='Unhedged', cl=confidence_level).values('value')[0]['value']
            hedge_irr_value = irr_at_risk.filter(type='Hedged', cl=confidence_level).values('value')[0]['value']

        at_risk = filter_value - current_spot
        if len(irr_at_risk_no_cost) > 0:
            irr_value = irr_at_risk_no_cost.filter(type='Hedged', cl=confidence_level).values('value')[0]['value']
            irr_nocost_hedge_value = irr_at_risk_no_cost.filter(type='Hedged', cl=confidence_level).values('value')[0]['value']

        hedging_effect = (irr_value - irr_unhedged_value) * 100

        cost = (hedge_irr_value - irr_nocost_hedge_value)* 100

        worst_case = cost+base_case+current_fx_gain_loss+at_risk+hedging_effect
    except ObjectDoesNotExist as exception:
        print(traceback.format_exc())
    except Exception as exception:
        print(traceback.format_exc())
    finally:
        resp = {
               'base_case': base_case,
               'current_fx_gain_loss': current_fx_gain_loss,
               'at_risk': at_risk,
               'hedging_effect': hedging_effect,
               'cost': cost,
               'worst_case': worst_case
                }
        return JsonResponse(resp)


@login_required(login_url='login')
@user_is_allowed
def moic_base_case_risk(request):
    if request.is_ajax() is False or request.method != 'POST':
        return HttpResponseBadRequest()

    try:
        base_case = 0
        current_fx_gain_loss = 0
        at_risk = 0
        hedging_effect = 0
        cost = 0

        fund = request.POST['fund']
        confidence_level = request.POST['confidence_level']

        module = None
        client = VRMClient.objects.get(name=request.POST['client'])
        if client is None:
            raise Exception('Unable to find the client to process!')
        try:
            module = __import__(client.name.lower())
        except Exception as e:
            try:
                module = __import__(client.label.lower())
            except Exception as e:
                raise Exception('Unable to find the client to process!')
        MoicAtRisk = getattr(module.models, 'MoicAtRisk')
        MoicAtRiskNoCost = getattr(module.models, 'MoicAtRiskNoCost')
        SummaryTable = getattr(module.models, 'SummaryTable')
        Moic_at_risk = MoicAtRisk.objects.filter(fund=fund, deal_name='Overall', cl=confidence_level).order_by('-update_date')
        Moic_at_risk_no_cost = MoicAtRiskNoCost.objects.filter(fund=fund, deal_name='Overall', cl=confidence_level).order_by('-update_date')
        summary_table = SummaryTable.objects.filter(fund=fund, deal_name='Overall').order_by('-update_date')
        current_spot = 0.0
        base_case =0.0
        current_fx_gain_loss = 0.0
        filter_value = 0.0
        at_risk = 0.0
        Moic_unhedged_value = 0.0
        hedge_Moic_value = 0.0
        Moic_value = 0.0
        Moic_nocost_hedge_value= 0.0
        if len(summary_table) > 0 :
            base_case = summary_table.values('expected_Moic')[0]['expected_Moic'] * 100
            current_fx_gain_loss = (summary_table.values('Moic_current_spot')[0]['Moic_current_spot'] - \
                                   summary_table.values('expected_Moic')[0]['expected_Moic']) * 100

            current_spot = summary_table.values('Moic_current_spot')[0]['Moic_current_spot'] * 100
        if len(Moic_at_risk) > 0 :
            filter_value = Moic_at_risk.filter(type='Unhedged', cl=confidence_level).values('value')[0]['value']
            Moic_unhedged_value = Moic_at_risk.filter(type='Unhedged', cl=confidence_level).values('value')[0]['value']
            hedge_Moic_value = Moic_at_risk.filter(type='Hedged', cl=confidence_level).values('value')[0]['value']

        at_risk = filter_value - current_spot
        if len(Moic_at_risk_no_cost) > 0:
            Moic_value = Moic_at_risk_no_cost.filter(type='Hedged', cl=confidence_level).values('value')[0]['value']
            Moic_nocost_hedge_value = Moic_at_risk_no_cost.filter(type='Hedged', cl=confidence_level).values('value')[0]['value']

        hedging_effect = (Moic_value - Moic_unhedged_value) * 100

        cost = (hedge_Moic_value - Moic_nocost_hedge_value)* 100

        worst_case = cost+base_case+current_fx_gain_loss+at_risk+hedging_effect
    except ObjectDoesNotExist as exception:
        print(traceback.format_exc())
    except Exception as exception:
        print(traceback.format_exc())
    finally:
        resp = {
               'base_case': base_case,
               'current_fx_gain_loss': current_fx_gain_loss,
               'at_risk': at_risk,
               'hedging_effect': hedging_effect,
               'cost': cost,
               'worst_case': worst_case
                }
        return JsonResponse(resp)


@login_required(login_url='login')
@user_is_allowed
def rebalancing_table(request):
    if request.is_ajax() is False or request.method != 'POST':
        return HttpResponseBadRequest()
    try:
        resp_data = []
        date_list = []
        maxdate = None
        module = None
        fund = request.POST.get('fund')
        try:
            user = User.objects.get(username=request.user.username)
            user.profile.recent_fund = fund
            user.save()
        except:
            raise Exception('Unable to find the fund to process!')
        try:
            client = VRMClient.objects.get(name=request.POST['client'])
        except VRMClient.DoesNotExist:
            raise Exception('Unable to find the client to process!')
        level = request.POST.get('level')
        level_value = request.POST.get('level_value')
        if None in [level, level_value]:
            raise Exception('Unable to find the valuation level to process!')
        try:
            module = __import__(client.name.lower())
        except Exception as e:
            try:
                module = __import__(client.label.lower())
            except Exception as e:
                raise Exception('Unable to find the client to process!')
        general_client = Clients.objects.get(client_name=client)
        general_fund = Funds.objects.get(fund_name=request.POST['fund'], client=general_client.pk)
        level_relation_fields = []
        level_filter = {'fund_id': general_fund.id}

        #todo modify the tables for old clients to work with the fund_id format
        RebalancingTrade = getattr(module.models, 'RebalancingTrade')
        if RebalancingTrade.objects.all().count() > 0:
            fund_type_use = request.POST['fund']
            query = None
            try:
                fund_exist = RebalancingTrade.objects.filter(fund_0=fund_type_use)
                query = {'fund_0':fund_type_use}
                level_relation_fields.append('fund_0')
            except:
                fund_exist = RebalancingTrade.objects.filter(fund=general_fund)
                fund_type_use = general_fund
                query = {'fund': fund_type_use}
                level_relation_fields.append('fund')
            if level and level != 'Fund'and level_value:
                dynamic_field = {level.lower() + '_name': level_value}
                try:
                    table_access = getattr(module.models, level.title()).objects.get(**dynamic_field)
                    query[level.lower() + '_id'] = getattr(table_access, level.lower() + '_id')
                    level_relation_fields.append(level.lower() + '_id')
                except ObjectDoesNotExist:
                    print(traceback.format_exc())
            else:
                query['entity_id__isnull'] = True
                if client.investor:
                    query['investor_id__isnull'] = True
                query['deal_id__isnull'] = True
            if not request.POST['valuation_date'] == '':
                maxdate = request.POST['valuation_date']
            else:
                maxdate = RebalancingTrade.objects.filter(**query).aggregate(Max('date'))['date__max']
            resp_data = list(RebalancingTrade.objects.filter(
                **query,
                date=maxdate).values(
                *level_relation_fields,
                'style',
                'underlying',
                'direction',
                'notional_currency',
                'notional_amounts',
                'nav_percentage'
            ))
            for item in resp_data:
                if 'fund_0' in item.keys():
                    item['fund'] = item['fund_0']
                    del item['fund_0']
                elif 'fund' in item.keys():
                    item['fund']= general_fund.fund_name
                elif level in item.keys():
                    item['fund'] = general_fund.fund_name
            date_list = list(RebalancingTrade.objects.filter(**query).order_by('-date').distinct().annotate(
                                    date_str=Cast(
                                        TruncSecond('date', DateTimeField()), CharField()
                                    )).values('date_str'))
            for item in date_list:
                item['date']=item.pop('date_str')


    except ObjectDoesNotExist as exception:
        print(exception)
        print(traceback.format_exc())
    except Exception as exception:
        print(exception)
        print(traceback.format_exc())
    finally:
        resp = {
            'data': resp_data,
            'valuedate': date_list
        }
        return JsonResponse(resp)


@login_required(login_url='login')
@user_is_allowed
def fund_investment_summary(request):
    if request.is_ajax() is False or request.method != 'POST':
        return HttpResponseBadRequest()
    try:
        res_data = []
        fund_ccy = ''
        fund_sum = 0

        client = VRMClient.objects.get(name=request.POST['client'])
        fund = request.POST.get('fund')
        user = User.objects.get(username=request.user.username)
        user.profile.recent_fund = fund
        user.save()
        general_client = Clients.objects.get(client_name=client)
        general_fund = Funds.objects.get(fund_name=request.POST['fund'], client=general_client.pk)
        if client is None:
            raise Exception('Unable to find the client to process!')
        try:
            module = __import__(client.name.lower())
        except Exception as e:
            try:
                module = __import__(client.label.lower())
            except Exception as e:
                raise Exception('Unable to find the client to process!')
        HedgeRatio = getattr(module.models, 'HedgeRatio')
        query = None
        fund_type_use = fund
        try:
            fund_exist = HedgeRatio.objects.filter(fund_0=fund_type_use)
            query = {'fund_0': fund_type_use}
        except:
            fund_exist = HedgeRatio.objects.filter(fund=general_fund)
            fund_type_use = general_fund
            query = {'fund': fund_type_use}
        if request.POST['select_date'] == '':
            maxDate = HedgeRatio.objects.filter(**query).aggregate(Max('date'))['date__max']
        else:
            maxDate = HedgeRatio.objects.filter(**query, date =request.POST['select_date'])
        fund_sum = HedgeRatio.objects.filter(**query, date=maxDate).\
             aggregate(Sum('nav_fund_ccy'))['nav_fund_ccy__sum'] or 0.0
        for fund_item in HedgeRatio.objects.filter(**query, date=maxDate):
            if fund_item.nav_fund_ccy > 1:
                res_data.append({'ccy': fund_item.hedges_currency,'percentage': round(abs(fund_item.nav_fund_ccy/\
                                fund_sum)*100, 2), 'value': fund_item.nav_fund_ccy})
                fund_ccy = fund_item.fund_currency

    except ObjectDoesNotExist as exception:
        print(traceback.format_exc())
    except Exception as exception:
        print(traceback.format_exc())
    finally:
        return JsonResponse({
         'data':res_data,
         'fund_ccy':fund_ccy,
         'fund_sum':round(fund_sum, 2)
        })


@login_required(login_url='login')
@user_is_allowed
def portfolio_deal_hedge_mtm_chart(request):
    if request.is_ajax() is False or request.method != 'POST':
        return HttpResponseBadRequest()

    try:
        series_dates = []
        series_unhedged = []
        series_hedged = []
        ccy = None

        client = request.POST['client']
        fund = request.POST['fund']

        module = None
        client = VRMClient.objects.get(name=request.POST['client'])
        general_client = Clients.objects.get(client_name=client)
        general_fund = Funds.objects.get(fund_name=request.POST['fund'], client=general_client.pk)
        if client is None:
            raise Exception('Unable to find the client to process!')
        try:
            module = __import__(client.name.lower())
        except Exception as e:
            try:
                module = __import__(client.label.lower())
            except Exception as e:
                raise Exception('Unable to find the client to process!')
        HedgeRatio = getattr(module.models, 'HedgeRatio')
        query = None
        fund_type_use = request.POST['fund']
        try:
            fund_exist = HedgeRatio.objects.filter(fund_0=fund_type_use)
            query = {'fund_0': fund_type_use}
        except:
            fund_exist = HedgeRatio.objects.filter(fund=general_fund)
            fund_type_use = general_fund
            query = {'fund': fund_type_use}

        if HedgeRatio.objects.filter(**query).count() > 0:
            ccy = list(HedgeRatio.objects.filter(**query).values_list('fund_currency'))[0]

            unhedged = HedgeRatio.objects.filter(
                **query,).order_by('date').values('date').annotate(nav_sum=Round(Sum('nav_fund_ccy')))

            for tuple_row in list(unhedged.values_list('date', 'nav_sum')):
                series_dates.append(tuple_row[0])
                series_unhedged.append(tuple_row[1])

            hedged = VrmMtm1.objects.filter(
                trade__client=client,
                trade__fund=fund,
                trade__cancellation__iexact='FALSE',
                trade__dummy__iexact='FALSE',
                trade__asset_class__iexact='FX',
                valuation_date__in=series_dates
            ).order_by('valuation_date').values('valuation_date').annotate(mtm_sum=Round(Sum('mtm')))

            if hedged is not None:
                i = 0
                for tuple_row in list(hedged.values_list('valuation_date', 'mtm_sum')):
                    current_date_data_point = series_dates[i]
                    if tuple_row[0] != current_date_data_point:
                        if i > 0:
                            while tuple_row[0] != current_date_data_point:
                                series_hedged.append(series_hedged[i-1])
                                current_date_data_point = series_dates[i + 1]
                                i += 1
                    series_hedged.append(tuple_row[1] + series_unhedged[i])
                    i += 1


    except ObjectDoesNotExist as exception:
        print(traceback.format_exc())
    except Exception as exception:
        print(traceback.format_exc())
    finally:
        resp = {
                 'time': series_dates,
                 'unhedged': series_unhedged,
                 'hedged': series_hedged,
                 'ccy': ccy,
                }
        return JsonResponse(resp)


@login_required(login_url='login')
@user_is_allowed
def hedgeratio_table(request):
    if request.is_ajax() is False or request.method != 'POST':
        return HttpResponseBadRequest()
    try:
        resp_data = list()
        tempdictdata = dict()
        date_list = list()
        hedge_ratio_series = list()
        maxdate = None
        module = None
        client = VRMClient.objects.get(name=request.POST['client'])
        general_client = Clients.objects.get(client_name=client)
        general_fund = Funds.objects.get(fund_name=request.POST['fund'], client=general_client.pk)
        level = None
        level_value = None
        if 'level' in request.POST:
            level = request.POST['level']
        if 'level_value' in request.POST:
            level_value = request.POST['level_value']
        query = None
        level_relation_fields = []
        try:
            module = __import__(client.name.lower())
        except Exception as e:
            try:
                module = __import__(client.label.lower())
            except Exception as e:
                raise Exception('Unable to find the client to process!')
        HedgeRatio = getattr(module.models, 'HedgeRatio')
        fund_type_use = request.POST['fund']
        try:
            fund_exist = HedgeRatio.objects.filter(fund_0=fund_type_use)
            query = {'fund_0': fund_type_use}
            level_relation_fields.append('fund_0')
        except:
            fund_exist = HedgeRatio.objects.filter(fund=general_fund)
            fund_type_use = general_fund
            query = {'fund': fund_type_use}
            level_relation_fields.append('fund')

        if level and level!= 'Fund' and level_value:
            dynamic_field = {level.lower()+'_name':level_value}
            try:
                table_access = getattr(module.models, level.title()).objects.get(**dynamic_field)
                query[level.lower() + '_id'] = getattr(table_access,level.lower()+'_id')
                level_relation_fields.append(level.lower() + '_id')
            except ObjectDoesNotExist:
                print(traceback.format_exc())
        else:
            query['entity_id__isnull'] = True
            if client.investor:
                query['investor_id__isnull'] = True
            query['deal_id__isnull'] = True
        if not request.POST['valuation_date'] == '':
            maxdate = request.POST['valuation_date']
        else:
            maxdate = HedgeRatio.objects.filter(**query).aggregate(Max('date'))['date__max'].strftime('%Y-%m-%d %H:%M')

        resp_data = list(HedgeRatio.objects.filter(
            **query,
            date=maxdate,
            nav_fund_ccy__gte=1).order_by('-date').values(
            *level_relation_fields,
            'fund_currency',
            'nav_fund_ccy',
            'hedges_currency',
            'nav_foreign_ccy',
            'hedges_amount',
            'hedge_ratio',
            'hedges_amount_fund_ccy',
            "hedges_currency"
        ))

        if len(resp_data) > 0:
            tempdictdata = {'names': [], 'unhedge': [], 'investment': [], 'hedge_ratio': []}
            navSum = 0
            hedgeSum = 0
            for item in resp_data:
                if 'fund_0' in item.keys():
                    item['fund'] = item['fund_0']
                    del item['fund_0']
                elif 'fund' in item.keys():
                    item['fund'] = general_fund.fund_name
                navSum += item["nav_fund_ccy"]
                hedgeSum += item["hedges_amount_fund_ccy"]
                if item["fund_currency"] == item["hedges_currency"]:
                    tempdictdata["investment"].append(100)
                    tempdictdata["unhedge"].append(0)
                    tempdictdata["hedge_ratio"].append(0)
                else:
                    tempdictdata["investment"].append(0)
                    tempdictdata["unhedge"].append((1.0-item['hedge_ratio'])*100
                                                   if (1.0-item['hedge_ratio'])*100 > 1
                                                   else 0.0)
                    tempdictdata["hedge_ratio"].append(item['hedge_ratio']*100)
                tempdictdata['names'].append(item["hedges_currency"])
            overallHedgeRatio = hedgeSum/ navSum
            tempdictdata['names'].append('OVERALL')
            tempdictdata['investment'].append(0)
            tempdictdata['unhedge'].append((1.0-overallHedgeRatio)*100  if overallHedgeRatio < 1 else 0.0)
            tempdictdata['hedge_ratio'].append(overallHedgeRatio * 100)
        date_list = list(HedgeRatio.objects.filter(**query).order_by('-date').distinct().values('date').annotate(
                                    date_str=Cast(
                                        TruncSecond('date', DateTimeField()), CharField()
                                    )).values('date_str'))
        for item in date_list:
            item['date'] = item.pop('date_str')
        latest_date = []
        for unhedgeitem in HedgeRatio.objects.filter(
                **query).order_by('date__date').distinct():
            latest_date.append(HedgeRatio.objects.filter(
                **query, date__date=unhedgeitem.date.date()).order_by('-date')[0].date)
        hedge_ratio_time = list(HedgeRatio.objects.filter( date=maxdate,
            **query, nav_fund_ccy__gte=1, date__in=latest_date).values(
            'hedges_currency',
            'hedge_ratio',
            'date'
        ).order_by('date'))
        dict_hedge_ratio = {}
        for item in hedge_ratio_time:
            if item['hedges_currency'] in dict_hedge_ratio.keys():
                dict_hedge_ratio[item['hedges_currency']].append([item['date'], item['hedge_ratio']*100])
            else:
                dict_hedge_ratio[item['hedges_currency']]=[]
                dict_hedge_ratio[item['hedges_currency']].append([item['date'], item['hedge_ratio']*100])

        for key, value in dict_hedge_ratio.items():
            hedge_ratio_series.append({'names': key, 'dates': value})

    except ObjectDoesNotExist as exception:
        print(traceback.format_exc())
    except Exception as exception:
        print(traceback.format_exc())
    finally:
        resp = {
            'data': resp_data,
            'chartdata': [tempdictdata],
            'valuedate': date_list,
            'hedge_ratio_series': hedge_ratio_series
        }
        return JsonResponse(resp)


@login_required(login_url='login')
@user_is_allowed
def cashflow_specific_tables(request):
    if request.is_ajax() is False or request.method != 'POST':
        return HttpResponseBadRequest()
    try:
        cashflow = []
        backwardCashflow = []
        forwardCashflow = []
        documentCashflow = []
        direct_deals = []
        indirect_deals= []
        deal_list = []
        entity_list = []
        currency_list= []

        client = __import__(request.POST['client'].lower().replace(" ", "_"))
        Deal =  getattr(client.models, 'Deal')
        Entity = getattr(client.models, 'Entity')
        cashflowTable = getattr(client.models, 'Cashflow')
        dealsTable = getattr(client.models, 'DirectDeal')
        indirect_dealsTable = getattr(client.models, 'IndirectDeal')
        backwardCashflowTable = getattr(client.models, 'BackwardCashflows')
        forwardCashflowTable = getattr(client.models, 'ForwardCashflows')
        documentCashflowTable = getattr(client.models, 'DocumentCashFlow')

        deal_list = [dealid.deal_name  for dealid in Deal.objects.all()]
        entity_list = [entityid.entity_name for entityid in Entity.objects.all()]
        currency_list = [ccyid.identifier for ccyid in  Ccys.objects.all()]

        cashflow = list(cashflowTable.objects.filter(cancellation=0).order_by("-id").values())
        direct_deals = list(dealsTable.objects.filter(cancellation=0).order_by("-id").values('entity__entity_name',
                                                                                     'deal__deal_name',
                                                                                     'direct_local_currency',
                                                                                     ))
        indirect_deals = list(indirect_dealsTable.objects.filter(cancellation=0).order_by("-id").values('deal__deal_name',
                                                                                                       'indirect_local_currency',
                                                                                                       'percentage'))
        backwardCashflow = list(backwardCashflowTable.objects.filter(cancellation=0).order_by("-transaction_id").
                                values('deal__deal_name','entity__entity_name',
                                       'cashflows','cf_type','fx_rate','value_date'))
        forwardCashflow = list(forwardCashflowTable.objects.filter(cancellation=0).order_by("-transaction_id").values('deal__deal_name','cashflows','value_date','cf_type'))


        documentCashflowMaxdate = documentCashflowTable.objects.all().aggregate(Max('current_date'))['current_date__max']
        documentCashflow = list(documentCashflowTable.objects.filter(current_date=documentCashflowMaxdate).order_by("id").values())

    except ObjectDoesNotExist as exception:
        print(traceback.format_exc())
    except Exception as exception:
        print(traceback.format_exc())
    finally:
        resp = {
            'deal_list': deal_list,
            'entity_list': entity_list,
            'cashflow': cashflow,
            'deals': direct_deals,
            'indirect_deals' : [],
            'backward_cashflow': backwardCashflow,
            'forward_cashflow': forwardCashflow,
            'document_cashflow': documentCashflow,
            'currency_list': currency_list
        }
        return JsonResponse(resp)


@login_required(login_url='login')
@user_is_allowed
def forward_specific_tables(request):
    if request.is_ajax() is False or request.method != 'POST':
        return HttpResponseBadRequest()
    try:
        forwardCashflow = []
        client = __import__(request.POST['client'].lower().replace(" ", "_"))
        forwardCashflowTable = getattr(client.models, 'ForwardCashflows')
        forwardCashflow = list(forwardCashflowTable.objects.filter(cancellation=0).order_by("-transaction_id").values('deal__deal_name','cashflows','value_date','cf_type'))
    except ObjectDoesNotExist as exception:
        print(traceback.format_exc())
    except Exception as exception:
        print(traceback.format_exc())
    finally:
        resp = {
            'forward_cashflow': forwardCashflow,
        }
        return JsonResponse(resp)


@login_required(login_url='login')
@user_is_allowed
def indirect_specific_tables(request):
    if request.is_ajax() is False or request.method != 'POST':
        return HttpResponseBadRequest()
    try:
        deal_name =request.POST['deal_name']
        cashflow = []
        backwardCashflow = []
        forwardCashflow = []
        documentCashflow = []
        direct_deals = []
        indirect_deals= []
        deal_list = []
        entity_list = []
        currency_list= []

        client = __import__(request.POST['client'].lower().replace(" ", "_"))
        Deal =  getattr(client.models, 'Deal')
        Entity = getattr(client.models, 'Entity')
        cashflowTable = getattr(client.models, 'Cashflow')
        dealsTable = getattr(client.models, 'DirectDeal')
        indirect_dealsTable = getattr(client.models, 'IndirectDeal')
        backwardCashflowTable = getattr(client.models, 'BackwardCashflows')
        forwardCashflowTable = getattr(client.models, 'ForwardCashflows')
        documentCashflowTable = getattr(client.models, 'DocumentCashFlow')

        deal_list = [dealid.deal_name  for dealid in Deal.objects.all()]
        entity_list = [entityid.entity_name for entityid in Entity.objects.all()]
        currency_list = [ccyid.identifier for ccyid in  Ccys.objects.all()]

        cashflow = list(cashflowTable.objects.filter(cancellation=0).order_by("-id").values())
        deal = Deal.objects.get(deal_name= deal_name, cancellation=0)
        if deal:
            if indirect_dealsTable.objects.filter(deal_id=deal.deal_id,cancellation=0).count() > 0:
                indirect_deals = list(indirect_dealsTable.objects.filter(deal_id=deal.deal_id,cancellation=0).
                                      order_by("-deal_id").values('id', 'deal__deal_name',
                                                                       'indirect_local_currency',
                                                                       'percentage'))
                for item in indirect_deals:
                    item['percentage'] = float(item['percentage'])*100
                    item['indirect_local_currency__identifier'] = Ccys.objects.get(id= item['indirect_local_currency']).identifier


    except ObjectDoesNotExist as exception:
        print(traceback.format_exc())
    except Exception as exception:
        print(traceback.format_exc())
    finally:
        resp = {
            'deal_list': deal_list,
            'entity_list': entity_list,
            'cashflow': cashflow,
            'indirect_deals' : indirect_deals,
            'currency_list': currency_list
        }
        return JsonResponse(resp)


@login_required(login_url='login')
@user_is_allowed
def fetch_funds_data(request):
    if request.is_ajax() is False or request.method != 'POST':
        return HttpResponseBadRequest()
    try:
        funds_data = []

        client = VRMClient.objects.get(name=request.POST['client'])
        if client is None:
            raise Exception('Unable to find the client to process!')

        funds_data = list(VrmFund.objects.filter(
            client=client,
        ).values(
            'fund_id',
            'fund',
            'fund_currency',
            'fund_type',
            'short_code',
            ))

    except ObjectDoesNotExist as exception:
        print(traceback.format_exc())
    except Exception as exception:
        print(traceback.format_exc())
    finally:
        resp = {
            'funds_data': funds_data,
        }
        return JsonResponse(resp)


@login_required(login_url='login')
@user_is_allowed
def fetch_deals_data(request):
    if request.is_ajax() is False or request.method != 'POST':
        return HttpResponseBadRequest()
    try:
        deals = []

        client = VRMClient.objects.get(name=request.POST['client'])
        if client is None:
            raise Exception('Unable to find the client to process!')
        try:
            module = __import__(client.name.lower())
        except Exception as e:
            try:
                module = __import__(client.label.lower())
            except Exception as e:
                raise Exception('Unable to find the client to process!')

        fund = request.POST['fund']
        trade_id = request.POST['trade_id']
        if fund is not None:
            Deals = getattr(module.models, 'Deals')

            if trade_id == '':
                deals = list(Deals.objects.filter(
                    fund=fund,
                    cancellation=False,
                ).values(
                    'deal_id',
                    'deal_name',
                    'local_currency',
                    'fund',
                    'cancellation',
                ))
            else:
                deals = list(Deals.objects.filter(
                    fund=fund,
                    cancellation=False,
                    trade_id=trade_id,
                ).values(
                    'deal_id',
                    'deal_name',
                    'local_currency',
                    'fund',
                    'cancellation',
                ))

    except ObjectDoesNotExist as exception:
        print(traceback.format_exc())
    except Exception as exception:
        print(traceback.format_exc())
    finally:
        resp = {
            'deals': deals,
        }
        return JsonResponse(resp)


@login_required(login_url='login')
@user_is_allowed
def fetch_deal_hedge_data(request):
    if request.is_ajax() is False or request.method != 'POST':
        return HttpResponseBadRequest()

    try:
        deal_hedge = []
        fund_hedge = []
        deal_entity_data = []
        entity_fund_data = []
        entity_trade_data = []
        entity_list = []

        client = VRMClient.objects.get(name=request.POST['client'])
        if client is None:
            raise Exception('Unable to find the client to process!')
        try:
            module = __import__(client.name.lower())
        except Exception as e:
            try:
                module = __import__(client.label.lower())
            except Exception as e:
                raise Exception('Unable to find the client to process!')

        # fund = request.POST['fund']
        try:
            trade_id = request.POST['trade_id']
        except:
            pass

        # if fund is not None:
        if trade_id is not None:
            if client == 'UTAM':

                if VrmDealhedge1.objects.filter(
                    trade__client=client,
                    # trade__fund=fund,
                    trade__trade_id=trade_id,
                ).exists():
                    deal_hedge = list(VrmDealhedge1.objects.filter(
                        trade__client=client,
                        # trade__fund=fund,
                        trade__trade_id=trade_id,
                        trade__cancellation='FALSE',
                        trade__dummy='FALSE',
                    ).values(
                        'id',
                        'trade__trade_id',
                        'deal_name',
                        'percentage',
                    ))
                    for deal_hedge_each in deal_hedge:
                        deal_hedge_each['percentage'] = multiply_normalize(
                            deal_hedge_each['percentage'],
                            100
                        )
            else:
                try:
                    deal_entity_module = getattr(module.models, 'Dealentity')
                    deal_entity_data = list(deal_entity_module.objects.filter(cancellation=0).values(
                        'id', 'entity_id','entity__entity_name', 'deal__deal_name','percentage', 'cancellation').
                                            order_by('-id'))
                    for deal_entity_data_each in deal_entity_data:
                        deal_entity_data_each['percentage'] = multiply_normalize(
                            deal_entity_data_each['percentage'],
                            100
                        )

                except Exception:
                    logger.exception('Deal_Entity table does not exist')

                try:
                    entity_fund_module = getattr(module.models, 'Entityfund')
                    entity_fund_data = list(entity_fund_module.objects.filter( cancellation=0 ).
                                            values('id',
                                                   'entity_id',
                                                   'entity__entity_name',
                                                   'fund_id', 'percentage',
                                                   'cancellation').order_by('-id'))
                    for entity_fund_data_each in entity_fund_data:
                        fund_info = list(Funds.objects.filter(id=entity_fund_data_each['fund_id']).
                                         values('id', 'fund_name'))
                        entity_fund_data_each['fund_name'] = fund_info[0]['fund_name']
                        entity_fund_data_each['percentage'] = multiply_normalize(
                            entity_fund_data_each['percentage'],
                            100
                        )
                except Exception:
                    entity_fund_data = []
                    logger.exception('Entity_Fund table does not exist')

                try:
                    entity_trade_module = getattr(module.models, 'Entitytrade')
                    entity_trade_data = list(
                        entity_trade_module.objects.filter(cancellation=0).values(
                            'id',
                            'entity_id',
                            'entity__entity_name',
                            'trade_id',
                            'percentage',
                            'cancellation').order_by('-id'))
                    for entity_trade_data_each in entity_trade_data:
                        entity_trade_data_each['percentage'] = multiply_normalize(
                            entity_trade_data_each['percentage'],
                            100
                        )
                except Exception:
                    entity_trade_data= []
                    logger.exception('Entity_Trade table does not exist')

                try:
                    if module:
                        Dealtrade = getattr(module.models, 'Dealtrade')
                        fund_list = [fund.fund for fund in SchemaFundCp.objects.filter(client=client)]
                        trade_id_list = [trade.trade_id for trade in VrmHedge1CopyCopy.objects.filter(client=client)]
                        Deal = getattr(module.models, 'Deal')
                        deal_list = []
                        if Deal.objects.all().exists():
                            deal_list = [dealid.deal_name  for dealid in Deal.objects.all()]
                        Entity = getattr(module.models, 'Entity')
                        entity_list = []
                        if Entity.objects.all().exists():
                            entity_list = [entityid.entity_name for entityid in Entity.objects.all()]
                        if Dealtrade.objects.filter(
                            trade_id=int(trade_id)
                            #,cancellation=0, for timebeign hold on
                         ).exists():
                            deal_hedge = list(Dealtrade.objects.filter(
                                trade_id=int(trade_id)
                                # , cancellation=0,or timebeign hold on
                            ).values(
                                'id',
                                'trade__trade_id',
                                'deal__deal_name',
                                'percentage',
                            ))
                            for deal_hedge_each in deal_hedge:
                                deal_hedge_each['percentage'] = multiply_normalize(
                                    deal_hedge_each['percentage'],
                                    100
                                )
                except ObjectDoesNotExist as exception:
                    print(traceback.format_exc())
                    deal_hedge = []
                try:
                    content_tabs = VRMContentTab.objects.filter(client=client)
                    # TODO need to remove unnecessary logic need a revisit.
                    content_tab_list = []
                    if content_tabs:
                        content_tab_list = [content.name for content in content_tabs]

                    if module and "Fund-Hedge Editor" in content_tab_list:
                        FundHedge = getattr(module.models, 'Tradefund')
                        trade_id_list = [trade.trade_id for trade in
                                         VrmHedge1CopyCopy.objects.filter(client=client)]
                        Fund = getattr(module.models, 'Fund')
                        fund_list = []
                        if Fund.objects.all().exists():
                            fund_list = [fundid.fund_name for fundid in Fund.objects.all()]
                        if FundHedge.objects.filter(
                                trade_id=int(trade_id)
                                # ,cancellation=0, for timebeign hold on
                        ).exists():
                            fund_hedge = list(FundHedge.objects.filter(
                                trade_id=int(trade_id)
                                # , cancellation=0,or timebeign hold on
                            ).values(
                                'id',
                                'trade__trade_id',
                                'fund__fund_name',
                                'percentage',
                            ))
                            for fund_hedge_each in fund_hedge:
                                fund_hedge_each['percentage'] = multiply_normalize(
                                    fund_hedge_each['percentage'],
                                    100
                                )
                except ObjectDoesNotExist as exception:
                    print(traceback.format_exc())
                    fund_hedge = []
    except ObjectDoesNotExist as exception:
        print(traceback.format_exc())
    except Exception as exception:
        print(traceback.format_exc())
    finally:
        resp = {
            'deal': deal_list,
            'fund':fund_list,
            'trade_id_list':trade_id_list,
            'entity':entity_list,
            'deal_hedge': deal_hedge,
            'fund_hedge' : fund_hedge,
            'deal_entity_data': deal_entity_data,
            'entity_fund_data': entity_fund_data,
            'entity_trade_data': entity_trade_data,
        }
        return JsonResponse(resp)


@login_required(login_url='login')
@user_is_allowed
def save_deal_data(request):
    msg =' Successfully added the '
    errorcode = 1
    try:
        client = request.POST["client"]
        data_ws = json.loads(request.POST["current_dealvalues"])
        module = None
        try:
            client = VRMClient.objects.get(name=client)
            module = __import__(client.name.lower())

        except Exception as e:
            try:
                module = __import__(client.label.lower())
            except Exception as e:
                raise Exception('Unable to find the client to process!')
        for item,values in data_ws.items():
            if module:
                Dealtrade = getattr(module.models, 'Dealtrade')
                Deal = getattr(module.models, 'Deal')
                deal_id = Deal.objects.get(deal_name=values['deal__deal_name']).deal_id
                try:
                    if Dealtrade.objects.filter(trade_id= values['trade__trade_id'],deal_id=deal_id).exists():
                        deal_trade =  Dealtrade.objects.get(trade_id= values['trade__trade_id'],deal_id=deal_id)
                        deal_trade.percentage=float(values['percentage']) / 100
                        deal_trade.save()
                    else:
                        dt, created = Dealtrade.objects.get_or_create(trade_id=values['trade__trade_id'],
                                                                      deal_id=deal_id, percentage=float(values['percentage']) / 100)
                except Exception as exception:
                    print(traceback.format_exc())
    except ObjectDoesNotExist as exception:
        msg = 'Unable to add the '
        errorcode = 0
        print(traceback.format_exc())
    except Exception as exception:
        msg = 'Unable to add the '
        errorcode = 0
        print(traceback.format_exc())
    finally:
        resp = {
            'msg': msg,
            'errorcode': errorcode
        }
        return JsonResponse(resp)


@login_required(login_url='login')
@user_is_allowed
def save_fund_hedge_data(request):
    msg =' Successfully added the '
    errorcode = 1
    try:
        client = request.POST["client"]
        data_ws = json.loads(request.POST["current_fundvalues"])
        module = None
        try:
            client = VRMClient.objects.get(name=client)
            module = __import__(client.name.lower())

        except Exception as e:
            try:
                module = __import__(client.label.lower())
            except Exception as e:
                raise Exception('Unable to find the client to process!')
        for item,values in data_ws.items():
            if module:
                Tradefund = getattr(module.models, 'Tradefund')
                Fund = getattr(module.models, 'Fund')
                fund_id = Fund.objects.get(fund_name=values['fund__fund_name']).fund_id
                try:
                    if Tradefund.objects.filter(trade_id= values['trade__trade_id'],fund_id=fund_id).exists():
                        fund_trade = Tradefund.objects.get(trade_id= values['trade__trade_id'], fund_id=fund_id)
                        fund_trade.percentage = float(values['percentage']) / 100
                        fund_trade.save()
                    else:
                        dt, created = Tradefund.objects.get_or_create(trade_id=values['trade__trade_id'],
                                                                      fund_id=fund_id,
                                                                      percentage=float(values['percentage']) / 100)
                except Exception as exception:
                    print(traceback.format_exc())
    except ObjectDoesNotExist as exception:
        msg = 'Unable to add the '
        errorcode = 0
        print(traceback.format_exc())
    except Exception as exception:
        msg = 'Unable to add the '
        errorcode = 0
        print(traceback.format_exc())
    finally:
        resp = {
            'msg': msg,
            'errorcode': errorcode
        }
        return JsonResponse(resp)

@login_required(login_url='login')
@user_is_allowed
def save_future_deal_data(request):
    msg =' Successfully added the '
    errorcode = 1
    try:
        client = request.POST["client"]
        data_ws = json.loads(request.POST["current_dealvalues"])
        for item, values in data_ws.items():
            deal_id = Deals.objects.get(deal=values['deal__deal'])
            try:
                if DealHedges.objects.filter(trade_id=values['trade'], deal=deal_id).exists():
                    deal_trade = DealHedges.objects.get(trade_id= values['trade'], deal=deal_id)
                    deal_trade.contract_number=values['contract_number']
                    deal_trade.save()
                else:
                    dt, created = DealHedges.objects.get_or_create(trade_id=values['trade'], deal=deal_id,
                                                                   contract_number=values['contract_number'])
            except Exception as exception:
                print(traceback.format_exc())
    except ObjectDoesNotExist as exception:
        msg = 'Unable to add the '
        errorcode = 0
        print(traceback.format_exc())
    except Exception as exception:
        msg = 'Unable to add the '
        errorcode = 0
        print(traceback.format_exc())
    finally:
        resp = {
            'msg': msg,
            'errorcode': errorcode
        }
        return JsonResponse(resp)


@login_required(login_url='login')
@user_is_allowed
def save_deal_entity(request):
    msg =' Successfully added the '
    errorcode = 1
    try:
        client = request.POST["client"]
        data_ws = json.loads(request.POST["current_dealentity"])
        module = None
        try:
            client = VRMClient.objects.get(name=client)
            module = __import__(client.name.lower())
        except Exception as e:
            try:
                module = __import__(client.label.lower())
            except Exception as e:
                raise Exception('Unable to find the client to process!')
        for item,values in data_ws.items():
            if module:
                dealEntity = getattr(module.models, 'Dealentity')
                Deal = getattr(module.models, 'Deal')
                Entity = getattr(module.models, 'Entity')
                deal_id = Deal.objects.get(deal_name=values['deal__deal_name']).deal_id
                entity_id = Entity.objects.get(entity_name=values['entity__entity_name']).entity_id
                try:
                    if dealEntity.objects.filter(deal_id=deal_id, entity_id=entity_id).exists():
                        deal_trade = dealEntity.objects.get(deal_id=deal_id, entity_id=entity_id)
                        deal_trade.percentage = float(values['percentage']) / 100
                        deal_trade.save()
                    else:
                        dt, created = dealEntity.objects.get_or_create(deal_id=deal_id, entity_id=entity_id,
                                                                       percentage=float(values['percentage']) / 100)
                except  Exception as exception:
                    print(traceback.format_exc())
    except (ObjectDoesNotExist, Exception) as exception:
        msg = 'Unable to add the '
        errorcode = 0
        print(traceback.format_exc())
    finally:
        resp = {
            'msg': msg,
            'errorcode': errorcode
        }
        return JsonResponse(resp)


@login_required(login_url='login')
@user_is_allowed
def save_entity_fund(request):

    try:
        client = request.POST["client"]
        data_ws = json.loads(request.POST["current_entityfund"])
        module = None
        try:
            client = VRMClient.objects.get(name=client)
            module = __import__(client.name.lower())
        except Exception as e:
            try:
                module = __import__(client.label.lower())
            except Exception as e:
                logger.exception('Unable to find the client to process!')
        for item,values in data_ws.items():
            if module:
                entityFund = getattr(module.models, 'Entityfund')
                Entity = getattr(module.models, 'Entity')
                entity_id = Entity.objects.get(entity_name=values['entity__entity_name']).entity_id
                fund_id = list(Funds.objects.filter(fund_name=values['fund_name']).values('id'))[0]['id']
                try:
                    if entityFund.objects.filter(fund_id=fund_id, entity_id=entity_id).exists():
                        entity_fund = entityFund.objects.get(fund_id=fund_id, entity_id=entity_id)
                        entity_fund.percentage = float(values['percentage']) / 100
                        entity_fund.save()
                        msg = 'Updated '
                    else:
                        dt, created = entityFund.objects.get_or_create(fund_id=fund_id, entity_id=entity_id,
                                                                       percentage=float(values['percentage']) / 100)
                        msg = 'Created'

                except Exception as exception:
                    print(traceback.format_exc())

        msg = ' Successfully added the '
        errorcode = 1
    except (ObjectDoesNotExist, Exception) as exception:
        msg = 'Unable to add the '
        errorcode = 0
        print(traceback.format_exc())
    finally:
        resp = {
            'msg': msg,
            'errorcode': errorcode
        }
        return JsonResponse(resp)


@login_required(login_url='login')
@user_is_allowed
def save_entity_trade(request):

    msg = 'Unable to add the '
    errorcode = 0
    try:
        client = request.POST["client"]
        data_ws = json.loads(request.POST["current_entityhedge"])
        module = None
        try:
            client = VRMClient.objects.get(name=client)
            module = __import__(client.name.lower())
        except Exception as e:
            try:
                module = __import__(client.label.lower())
            except Exception as e:
                raise Exception('Unable to find the client to process!')
        for item, values in data_ws.items():
            if module:
                entityTrade = getattr(module.models, 'Entitytrade')
                Entity = getattr(module.models, 'Entity')
                entity_id = Entity.objects.get(entity_name=values['entity__entity_name']).entity_id
                trade_id = values['trade_id']
                try:
                    if entityTrade.objects.filter(id=values['id'], trade_id=trade_id, entity_id=entity_id).exists():
                        entity_trade = entityTrade.objects.get(trade_id=trade_id, entity_id=entity_id)
                        entity_trade.percentage = float(values['percentage']) / 100
                        entity_trade.save()
                    else:
                        dt, created = entityTrade.objects.get_or_create(trade_id=trade_id, entity_id=entity_id,
                                                                        percentage=float(values['percentage']) / 100)

                except  Exception as exception:
                    print(traceback.format_exc())
        msg = ' Successfully added the '
        errorcode = 1
    except (ObjectDoesNotExist, Exception) as exception:
        msg = 'Unable to add the '
        errorcode = 0
        print(traceback.format_exc())
    finally:
        resp = {
            'msg': msg,
            'errorcode': errorcode
        }
        return JsonResponse(resp)


@login_required(login_url='login')
@user_is_allowed
def fetch_future_deal_hedge_data(request):
    if request.is_ajax() is False or request.method != 'POST':
        return HttpResponseBadRequest()

    try:
        deal_hedge = []
        deal_entity_data = []
        entity_fund_data = []
        entity_trade_data = []
        entity_list = []

        client = VRMClient.objects.get(name=request.POST['client'])
        if client is None:
            raise Exception('Unable to find the client to process!')
        try:
            module = __import__(client.name.lower())
        except Exception as e:
            try:
                module = __import__(client.label.lower())
            except Exception as e:
                raise Exception('Unable to find the client to process!')

        # fund = request.POST['fund']
        try:
            trade_id = request.POST['trade_id']
        except:
            pass

        # if fund is not None:
        if trade_id is not None:
            if client.name == 'UTAM':
                try:
                    if DealHedges.objects.filter(
                        trade_id=trade_id,
                    ).exists():
                        deal_hedge = list(DealHedges.objects.filter(
                            trade_id=trade_id
                        ).values(
                            'id',
                            'trade',
                            'deal__deal',
                            'contract_number',
                        ))
                    if Deals.objects.all().exists():
                        deal_list = [dealid.deal for dealid in Deals.objects.all()]
                except ObjectDoesNotExist as exception:
                    print(traceback.format_exc())
                    deal_hedge = []
    except ObjectDoesNotExist as exception:
        print(traceback.format_exc())
    except Exception as exception:
        print(traceback.format_exc())
    finally:
        resp = {
            'deal': deal_list,
            'deal_hedge': deal_hedge
        }
        return JsonResponse(resp)


@login_required(login_url='login')
@user_is_allowed
def fetch_valuation_level_options(request):
    if request.is_ajax() is False or request.method != 'POST':
        return HttpResponseBadRequest()
    try:
        data = []
        client = VRMClient.objects.get(name=request.POST['client'])
        level = request.POST['level']
        if client is None:
            raise Exception('Unable to find the client to process!')
        if level is None:
            raise Exception('Unable to find the valuation level to process!')
        try:
            module = __import__(client.name.lower())
        except Exception as e:
            try:
                module = __import__(client.label.lower())
            except Exception as e:
                raise Exception('Unable to find the client to process!')

        if level == 'fund':
            if SchemaFundCp.objects.filter(client=client).exists():
                data = list(SchemaFundCp.objects.filter(client=client).extra(select={'value': 'fund'}).values('value'))
        elif level == 'deal':
            if module.models.Deal.objects.all().exists():
                data = list(module.models.Deal.objects.all().extra(select={'value': 'deal_name'}).values('value'))
        elif level == 'entity':
            if module.models.Entity.objects.all().exists():
                data = list(module.models.Entity.objects.all().extra(select={'value': 'entity_name'}).values('value'))

    except ObjectDoesNotExist as exception:
        print(traceback.format_exc())
    except Exception as exception:
        print(traceback.format_exc())
    finally:
        resp = {
            'data': data,
        }
        return JsonResponse(resp)


@login_required(login_url='login')
@user_is_allowed
def fetch_fund_level_options(request):
    if request.is_ajax() is False or request.method != 'POST':
        return HttpResponseBadRequest()
    try:
        data = []
        level_choice = []
        client = VRMClient.objects.get(name=request.POST['client'])
        level = request.POST.get('level')
        fund = request.POST.get('fund', '')

        general_client = Clients.objects.get(client_name=client.name)
        general_fund = Funds.objects.get(fund_name=request.POST['fund'], client=general_client.pk)
        if client is None:
            raise Exception('Unable to find the client to process!')
        if level is None:
            raise Exception('Unable to find the valuation level to process!')
        user = User.objects.get(username=request.user.username)
        user.profile.recent_fund = fund
        user.save()

        module = module_loader(client)
        fund_association = module.models.FundAssociation
        if level == 'Entity':
            if fund_association.objects.filter(fund_id=general_fund.pk).exclude(entity_id__isnull=True).exists():
                level_choice = fund_association.objects.filter(fund_id=general_fund.pk).exclude(
                    entity_id__isnull=True).order_by(level.lower() + '_id').\
                    values_list(level.lower() + '__entity_name', flat=True)
        elif level == 'Investor' and client.investor:
            if fund_association.objects.filter(fund_id=general_fund.pk).exclude(investor_id__isnull=True).exists():
                level_choice = fund_association.objects.filter(fund_id=general_fund.pk).exclude(
                    investor_id__isnull=True).order_by(level.lower() + '_id').\
                    values_list(level.lower() + '__investor_name', flat=True)
        elif level == 'Deal':
            if fund_association.objects.filter(fund_id=general_fund.pk).exclude(deal_id__isnull=True).exists():
                level_choice = fund_association.objects.filter(fund_id=general_fund.pk).exclude(
                    deal_id__isnull=True).order_by(level.lower() + '_id').\
                    values_list(level.lower() + '__deal_name', flat=True)

    except ObjectDoesNotExist as exception:
        print(traceback.format_exc())
    except Exception as exception:
        print(traceback.format_exc())
    finally:
        resp = {
            'data': list(level_choice),
        }
        return JsonResponse(resp)


@login_required(login_url='login')
@user_is_allowed
def fetch_multi_valuation_level_options(request):
    if request.is_ajax() is False or request.method != 'POST':
        return HttpResponseBadRequest()
    try:
        data = None
        client = VRMClient.objects.get(name=request.POST['client'])
        fund = request.POST['fund']
        user = User.objects.get(username=request.user.username)
        user.profile.recent_fund = fund
        user.save()
        general_client = Clients.objects.get(client_name=client.name)
        general_fund = Funds.objects.get(fund_name=request.POST['fund'], client=general_client.pk)
        level_list = [{'id':'Fund', 'value': [general_fund.fund_name]}]
        if client is None:
            raise Exception('Unable to find the client to process!')
        module = module_loader(client)
        for level in ['Deal', 'Investor']:#, 'Entity']:
            level_choice = []
            if level == 'Entity':
                entity_fund = module.models.Entityfund
                if entity_fund.objects.filter(fund=general_fund).exists():
                    level_choice = entity_fund.objects.filter(fund=general_fund).\
                        values('entity_id').order_by(level.lower() + '_id')
            elif level == 'Investor':
                # Todo need to change as mya be a guarantor or investor
                # level_choice = fund_association.objects.filter(fund_id=general_fund.pk).exclude(
                #     investor_id__isnull=True).values(level.lower() + '_id').order_by(level.lower() + '_id')
                pass
            elif level == 'Deal':
                ## Assuming 100 % association
                entity_fund = module.models.Entityfund
                entity_list = entity_fund.objects.filter(fund=general_fund).order_by('entity_id')
                dealentity = module.models.Dealentity
                for entity in entity_list:
                    if dealentity.objects.filter(entity=module.models.Entity(entity_id=entity.id)).exists():
                        for deal in dealentity.objects.filter(entity=module.models.Entity(
                                entity_id=entity.id)).values('deal_id'):
                            level_choice.append(deal)

            if len(level_choice) > 0:
                append_list = []
                for item in level_choice:
                    if level == 'Entity':
                        if getattr(module.models, level).objects.get(entity_id=item['entity_id']):
                            append_list.append(
                                getattr(module.models, level).objects.get(entity_id=item['entity_id']).entity_name)
                    elif level == 'Investor':
                        if getattr(module.models, level).objects.get(investor_id=item['investor_id']):
                            append_list.append(
                                getattr(module.models, level).objects.get(investor_id=item['investor_id']).investor_name)
                    elif level == 'Deal':
                        if getattr(module.models, level).objects.get(deal_id=item['deal_id']):
                            append_list.append(
                                getattr(module.models, level).objects.get(deal_id=item['deal_id']).deal_name)
                if len(append_list) > 0:
                    level_list.append({'id':level, 'value':append_list})
    except ObjectDoesNotExist as exception:
        print(traceback.format_exc())
    except Exception as exception:
        print(traceback.format_exc())
    finally:
        resp = {
            'data': level_list,
        }
        return JsonResponse(resp)


@login_required(login_url='login')
@user_is_allowed
def fetch_hedge_valuation_level_options(request):
    if request.is_ajax() is False or request.method != 'POST':
        return HttpResponseBadRequest()
    try:
        data = None
        client = VRMClient.objects.get(name=request.POST['client'])
        fund = request.POST['fund']
        user = User.objects.get(username=request.user.username)
        user.profile.recent_fund = fund
        user.save()
        general_client = Clients.objects.get(client_name=client.name)
        general_fund = Funds.objects.get(fund_name=request.POST['fund'], client=general_client.pk)
        level_list = [{'id': 'Fund', 'value': [general_fund.fund_name]}]
        if client is None:
            raise Exception('Unable to find the client to process!')
        module = module_loader(client)
        fund_association = module.models.FundAssociation
        for level in ['Deal', 'Investor', 'Entity']: #,'Guarantor']:
            append_list = []
            if level == 'Entity':
                if fund_association.objects.filter(fund_id=general_fund.pk).exclude(entity_id__isnull=True).exists():
                    append_list = fund_association.objects.filter(fund_id=general_fund.pk).exclude(
                        entity_id__isnull=True).order_by(level.lower() + '_id').values_list(
                        level.lower()+'__entity_name', flat=True)
            elif level == 'Investor' and client.investor:
                if fund_association.objects.filter(fund_id=general_fund.pk).exclude(investor_id__isnull=True).exists():
                    append_list = fund_association.objects.filter(fund_id=general_fund.pk).exclude(
                        investor_id__isnull=True).order_by(level.lower() + '_id').values_list(
                        level.lower()+'__investor_name', flat=True)
            elif level == 'Guarantor' and client.guarantor_summary:
                # No fund assosication for Guarantor , user vrm client and fetch all the guarantor
                try:
                    module = module_loader(client)
                except Exception as e:
                    raise Exception('Unable to find the client to process!')
                guarantor = getattr(module.models, 'Guarantor')
                if guarantor.objects.all().exists():
                    append_list = guarantor.objects.filter(cancellation=0).order_by(level.lower() + '_id').values_list(
                        'guarantor_name', flat=True)
            elif level == 'Deal':
                if fund_association.objects.filter(fund_id=general_fund.pk).exclude(deal_id__isnull=True).exists():
                    append_list = fund_association.objects.filter(fund_id=general_fund.pk).exclude(
                        deal_id__isnull=True).order_by(level.lower() + '_id').values_list(
                        level.lower() + '__deal_name', flat=True)
            if len(append_list) > 0:
                level_list.append({'id': level, 'value': list(append_list)})
    except ObjectDoesNotExist as exception:
        print(traceback.format_exc())
    except Exception as exception:
        print(traceback.format_exc())
    finally:
        resp = {
            'data': level_list,
        }
        return JsonResponse(resp)


@login_required(login_url='login')
@user_is_allowed
def multi_investment_summary(request):
    if request.is_ajax() is False or request.method != 'POST':
        return HttpResponseBadRequest()
    try:
        res_data = []
        fund_ccy = ''
        fund_sum = 0
        module = None
        client = VRMClient.objects.get(name=request.POST['client'])
        level = request.POST['level']
        level_value = request.POST['level_value']
        if client is None:
            raise Exception('Unable to find the client to process!')
        if level is None:
            raise Exception('Unable to find the valuation level to process!')
        try:
           module = module_loader(client)
        except Exception as e:
            raise Exception('Unable to find the client to process!')

        HedgeRatio = getattr(module.models, 'HedgeRatio')
        if level == 'fund':
            fund_list = list(Funds.objects.filter(client__client_name=client).values('id', 'fund_name'))
            fund_id = [x['id'] for x in fund_list if x['fund_name'] == level_value][0]
            level_filter = {'fund_id': fund_id}
        elif level == 'deal':
            level_filter = {'deal__deal_name': level_value}
        elif level == 'entity':
            level_filter = {'entity__entity_name': level_value}
        if request.POST['select_date'] == '':
            maxDate = HedgeRatio.objects.filter(**level_filter).aggregate(Max('date'))['date__max']
        else:
            maxDate = HedgeRatio.objects.filter(**level_filter, date =request.POST['select_date'])
        fund_sum = HedgeRatio.objects.filter(**level_filter, date=maxDate).\
             aggregate(Sum('nav_fund_ccy'))['nav_fund_ccy__sum'] or 0.0
        for fund_item in HedgeRatio.objects.filter(**level_filter, date=maxDate):
            if fund_item.nav_fund_ccy > 1:
                res_data.append({'ccy': fund_item.hedges_currency, 'percentage': round(abs(fund_item.nav_fund_ccy/\
                                fund_sum)*100, 2), 'value': fund_item.nav_fund_ccy})
                fund_ccy = fund_item.fund_currency

    except ObjectDoesNotExist as exception:
        print(traceback.format_exc())
    except Exception as exception:
        print(traceback.format_exc())
    finally:
        return JsonResponse({
         'data':res_data,
         'fund_ccy':fund_ccy,
         'fund_sum':round(fund_sum, 2)
        })


@login_required(login_url='login')
@user_is_allowed
def fundlevel_investment_summary(request):
    if request.is_ajax() is False or request.method != 'POST':
        return HttpResponseBadRequest()
    try:
        res_data = []
        fund_ccy = ''
        fund_sum = 0
        try:
            client = VRMClient.objects.get(name=request.POST['client'])
        except VRMClient.DoesNotExist:
            raise Exception('Unable to find the client to process!')
        level = request.POST.get('level')
        level_value = request.POST.get('level_value')
        if level is None:
            raise Exception('Unable to find the valuation level to process!')
        try:
            module = __import__(client.name.lower())
        except Exception as e:
            try:
                module = __import__(client.label.lower())
            except Exception as e:
                raise Exception('Unable to find the client to process!')
        general_client = Clients.objects.get(client_name=client)
        general_fund = Funds.objects.get(fund_name=request.POST['fund'], client=general_client.pk)
        HedgeRatio = getattr(module.models, 'HedgeRatio')
        level_relation_fields = []
        level_filter = {'fund_id': general_fund.id}
        level_relation_fields.append('fund_id')
        if level and level != 'fund' and level_value:
            dynamic_field = {level.lower() + '_name': level_value}
            try:
                class_obj = getattr(module.models, level.title())
                table_access = class_obj.objects.get(**dynamic_field)
                level_filter[level.lower() + '_id'] = getattr(table_access, level.lower() + '_id')
                level_relation_fields.append(level.lower() + '_id')
            except ObjectDoesNotExist:
                print(traceback.format_exc())
        else:
            # Remeber this is Fund selection only no deal or entity or investor should be selected.
            level_filter['entity_id__isnull'] = True
            if client.investor:
                level_filter['investor_id__isnull'] = True
            level_filter['deal_id__isnull'] = True
        if request.POST['select_date'] == '':
            maxDate = HedgeRatio.objects.filter(**level_filter).aggregate(Max('date'))['date__max']
        else:
            maxDate = HedgeRatio.objects.filter(**level_filter, date =request.POST['select_date'])
        fund_sum = HedgeRatio.objects.filter(**level_filter, date=maxDate).\
             aggregate(Sum('nav_fund_ccy'))['nav_fund_ccy__sum'] or 0.0
        for fund_item in HedgeRatio.objects.filter(**level_filter, date=maxDate):
            if fund_item.nav_fund_ccy > 1:
                res_data.append({'ccy': fund_item.hedges_currency, 'percentage': round(abs(fund_item.nav_fund_ccy/\
                                fund_sum)*100, 2), 'value': fund_item.nav_fund_ccy})
                fund_ccy = fund_item.fund_currency

    except ObjectDoesNotExist as exception:
        print(traceback.format_exc())
    except Exception as exception:
        print(traceback.format_exc())
    finally:
        return JsonResponse({
         'data':res_data,
         'fund_ccy':fund_ccy,
         'fund_sum':round(fund_sum, 2)
        })


@login_required(login_url='login')
@user_is_allowed
def save_fwd_deal_data(request):
    msg =' Successfully added the '
    errorcode = 1
    forwardCashflow = []
    try:
        client = request.POST["client"]
        data_ws = json.loads(request.POST["current_dealvalues"])
        module = None
        try:
            client = VRMClient.objects.get(name=client)
            module = __import__(client.name.lower())

        except Exception as e:
            try:
                module = __import__(client.label.lower())
            except Exception as e:
                raise Exception('Unable to find the client to process!')
        for item,values in data_ws.items():
            if module:
                ForwardCashflowTable = getattr(module.models, 'ForwardCashflows')
                Deal = getattr(module.models, 'Deal')
                deal_id = Deal.objects.get(deal_name=values['deal__deal_name']).deal_id
                try:
                    if ForwardCashflowTable.objects.filter(cf_type=values['cf_type'], deal_id=deal_id,
                                                           value_date= values['value_date']).exists():
                        fwd_data =  ForwardCashflowTable.objects.get(cf_type=values['cf_type'], deal_id=deal_id)
                        fwd_data.value_date= values['value_date']
                        fwd_data.cashflows = values['cashflows']
                        # todo need to update date
                        fwd_data.save()
                    else:
                        dt, created = ForwardCashflowTable.objects.get_or_create(
                                                                      deal_id=deal_id,
                                                                      cf_type=values['cf_type'],
                                                                value_date=values['value_date'])
                        if created:
                            dt.cashflows = values['cashflows']
                            dt.save()
                except Exception as exception:
                    print(traceback.format_exc())
        forward_cashflow = list(ForwardCashflowTable.objects.filter(cancellation=0).
                                order_by("-transaction_id").values('deal__deal_name',
                                                                   'cashflows',
                                                                   'value_date',
                                                                    'cf_type'))
    except ObjectDoesNotExist as exception:
        msg = 'Unable to add the '
        errorcode = 0
        print(traceback.format_exc())
    except Exception as exception:
        msg = 'Unable to add the '
        errorcode = 0
        print(traceback.format_exc())
    finally:
        resp = {
            'msg': msg,
            'errorcode': errorcode,
            'forward_cashflow': forward_cashflow,
        }
        return JsonResponse(resp)


@login_required(login_url='login')
@user_is_allowed
def save_indirect_deal_data(request):
    msg =' Successfully added the '
    errorcode = 1
    try:
        client = request.POST["client"]
        data_ws = json.loads(request.POST["current_dealvalues"])
        module = None
        try:
            client = VRMClient.objects.get(name=client)
            module = __import__(client.name.lower())

        except Exception as e:
            try:
                module = __import__(client.label.lower())
            except Exception as e:
                raise Exception('Unable to find the client to process!')
        for item,values in data_ws.items():
            if module:
                IndirectDeal = getattr(module.models, 'IndirectDeal')
                Deal = getattr(module.models, 'Deal')
                deal_id = Deal.objects.get(deal_name=values['deal__deal_name']).deal_id
                currency_id = Ccys.objects.get(identifier=values['indirect_local_currency__identifier'])
                try:
                    if IndirectDeal.objects.filter(indirect_local_currency= currency_id,deal_id=deal_id).exists():
                        deal_trade =  IndirectDeal.objects.get(indirect_local_currency= currency_id,deal_id=deal_id)
                        deal_trade.percentage=values['percentage']
                        # todo need to update date
                        deal_trade.save()
                    else:
                        dt, created = IndirectDeal.objects.get_or_create(indirect_local_currency = currency_id,
                                                                      deal_id=deal_id)
                        if created:
                            dt.percentage = values['percentage']
                            dt.save()
                except  Exception as exception:
                    print(traceback.format_exc())
                    dt, created = IndirectDeal.objects.get_or_create(indirect_local_currency = currency_id,
                                                                      deal_id=deal_id)
                    if created:
                        dt.percentage = values['percentage']
                        dt.save()

    except ObjectDoesNotExist as exception:
        msg = 'Unable to add the '
        errorcode = 0
        print(traceback.format_exc())
    except Exception as exception:
        msg = 'Unable to add the '
        errorcode = 0
        print(traceback.format_exc())
    finally:
        resp = {
            'msg': msg,
            'errorcode': errorcode

        }
        return JsonResponse(resp)


@login_required(login_url='login')
@user_is_allowed
def multi_portfolio_mtm_chart(request):
    if request.is_ajax() is False or request.method != 'POST':
        return HttpResponseBadRequest()

    try:
        series_dates = []
        series_unhedged = []
        series_hedged = []
        ccy = ''

        level = request.POST['level']
        level_value = request.POST['level_value']

        client = VRMClient.objects.get(name=request.POST['client'])
        if client is None:
            raise Exception('Unable to find the client to process!')
        if level is None:
            raise Exception('Unable to find the level to process!')
        if level_value is None:
            raise Exception('Unable to find the value of level to process!')
        try:
            module = __import__(client.name.lower())
        except Exception as e:
            try:
                module = __import__(client.label.lower())
            except Exception as e:
                raise Exception('Unable to find the client to process!')
        HedgeRatio = getattr(module.models, 'HedgeRatio')
        level_filter = None
        level_relation_fields = None
        if level == 'fund':
            fund_list = list(Funds.objects.filter(client__client_name=client).values('id', 'fund_name'))
            fund_id = [x['id'] for x in fund_list if x['fund_name'] == level_value][0]
            level_filter = {'fund_id': fund_id}
            level_filter['entity_id__isnull'] = True
            if client.investor:
                level_filter['investor_id__isnull'] = True
            level_filter['deal_id__isnull'] = True
            # level_filter = {'fund__fund_name': level_value}
            level_relation_fields = ['fund_id']
        elif level == 'deal':
            level_filter = {'deal__deal_name': level_value}
            level_relation_fields = ['deal_id','entity_id']
        elif level == 'entity':
            level_filter = {'entity__entity_name': level_value}
            level_relation_fields = ['deal_id','entity_id']

        HedgeUnhedge = getattr(module.models, 'HedgeUnhedge')
        DirectDeal = getattr(module.models, 'DirectDeal')

        if HedgeUnhedge.objects.filter(**level_filter).exists() \
                and level_filter is not None\
                and level_relation_fields is not None:
            hedge_unhdege_data = HedgeUnhedge.objects.filter(**level_filter).values(
                'valuation_date',
                'hedged_amounts',
                'unhedged_amounts',
                'fund_id',
                *level_relation_fields,
            )

            series_dates = [item['valuation_date'] for item in hedge_unhdege_data]
            series_hedged = [item['hedged_amounts'] for item in hedge_unhdege_data]
            series_unhedged = [item['unhedged_amounts'] for item in hedge_unhdege_data]

            if level == 'fund':
                ccy = Ccys.objects.get(id= Funds.objects.get(id =hedge_unhdege_data[0]['fund_id']).ccy_id).identifier
            else:
                level_relations = dict()
                for level_relation_field in level_relation_fields:
                    level_relations[level_relation_field] = hedge_unhdege_data[0][level_relation_field]
                ccy = DirectDeal.objects.filter(**level_relations).values('direct_local_currency').\
                    first()['direct_local_currency'] if DirectDeal.objects.filter(**level_relations).count() > 0 \
                    else ''

    except ObjectDoesNotExist as exception:
        print(traceback.format_exc())
    except Exception as exception:
        print(traceback.format_exc())
    finally:
        resp = {
                 'time': series_dates,
                 'unhedged': series_unhedged,
                 'hedged': series_hedged,
                 'ccy': ccy,
                }
        return JsonResponse(resp)


@login_required(login_url='login')
@user_is_allowed
def multi_summary_base_case_risk(request):
    if request.is_ajax() is False or request.method != 'POST':
        return HttpResponseBadRequest()
    try:

        results = {'Base': 0,
                   'FX_Gain_Loss': 0,
                  'At_Risk': 0,
                  'Hedged_Effect': 0,
                  'Cost': 0,
                  'Worst_Case': 0,
        }

        level = request.POST['level']
        level_value = request.POST['level_value']
        confidence_level = request.POST['confidence_level']

        client = VRMClient.objects.get(name=request.POST['client'])
        if client is None:
            raise Exception('Unable to find the client to process!')
        if level is None:
            raise Exception('Unable to find the level to process!')
        if level_value is None:
            raise Exception('Unable to find the value of level to process!')
        try:
            module = __import__(client.name.lower())
        except Exception as e:
            try:
                module = __import__(client.label.lower())
            except Exception as e:
                raise Exception('Unable to find the client to process!')

        if level == 'fund':
            fund_list = list(Funds.objects.filter(client__client_name=client).values('id', 'fund_name'))
            fund_id = [x['id'] for x in fund_list if x['fund_name'] == level_value][0]
            for parameter, value in results.items():
                if module.models.IrrAtRisk.objects.filter(type=parameter, fund_id=fund_id).exists():
                    results[parameter] = module.models.IrrAtRisk.objects.filter(
                        type=parameter,
                        cl=confidence_level,
                        fund_id=fund_id,
                    ).order_by('update_date').values('value').last()['value']

        if level == 'deal':
            for parameter, value in results.items():
                if module.models.IrrAtRisk.objects.filter(type=parameter, deal__deal_name=level_value).exists():
                    results[parameter] = module.models.IrrAtRisk.objects.filter(
                        type=parameter,
                        cl=confidence_level,
                        deal__deal_name=level_value,
                    ).order_by('update_date').values('value').last()['value']

        if level == 'entity':
            for parameter, value in results.items():
                if module.models.IrrAtRisk.objects.filter(type=parameter, entity__entity_name=level_value).exists():
                    results[parameter] = module.models.IrrAtRisk.objects.filter(
                        type=parameter,
                        cl=confidence_level,
                        entity__entity_name=level_value,
                    ).order_by('update_date').values('value').last()['value']

    except ObjectDoesNotExist as exception:
        print(traceback.format_exc())
    except Exception as exception:
        print(traceback.format_exc())
    finally:
        resp = {
               'base_case': results['Base']*100,
               'current_fx_gain_loss': results['FX_Gain_Loss']*100,
               'at_risk': results['At_Risk']*100,
               'hedging_effect': results['Hedged_Effect']*100,
               'cost': results['Cost']*100,
               'worst_case': results['Worst_Case']*100,
                }
        return JsonResponse(resp)


@login_required(login_url='login')
@user_is_allowed
def multi_moic_base_case_risk(request):
    if request.is_ajax() is False or request.method != 'POST':
        return HttpResponseBadRequest()
    try:

        results = {'Base': 0,
                   'FX_Gain_Loss': 0,
                  'At_Risk': 0,
                  'Hedged_Effect': 0,
                  'Cost': 0,
                  'Worst_Case': 0,
        }

        level = request.POST['level']
        level_value = request.POST['level_value']
        confidence_level = request.POST['confidence_level']

        client = VRMClient.objects.get(name=request.POST['client'])
        if client is None:
            raise Exception('Unable to find the client to process!')
        if level is None:
            raise Exception('Unable to find the level to process!')
        if level_value is None:
            raise Exception('Unable to find the value of level to process!')
        try:
            module = __import__(client.name.lower())
        except Exception as e:
            try:
                module = __import__(client.label.lower())
            except Exception as e:
                raise Exception('Unable to find the client to process!')

        if level == 'fund':
            fund_list = list(Funds.objects.filter(client__client_name=client).values('id', 'fund_name'))
            fund_id = [x['id'] for x in fund_list if x['fund_name'] == level_value][0]
            for parameter, value in results.items():
                if module.models.MoicAtRisk.objects.filter(type=parameter, fund_id=fund_id).exists():
                    results[parameter] = module.models.MoicAtRisk.objects.filter(
                        type=parameter,
                        cl=confidence_level,
                        fund_id=fund_id,
                    ).order_by('update_date').values('value').last()['value']

        if level == 'deal':
            for parameter, value in results.items():
                if module.models.MoicAtRisk.objects.filter(type=parameter, deal__deal_name=level_value).exists():
                    results[parameter] = module.models.MoicAtRisk.objects.filter(
                        type=parameter,
                        cl=confidence_level,
                        deal__deal_name=level_value,
                    ).order_by('update_date').values('value').last()['value']

        if level == 'entity':
            for parameter, value in results.items():
                if module.models.MoicAtRisk.objects.filter(type=parameter, entity__entity_name=level_value).exists():
                    results[parameter] = module.models.MoicAtRisk.objects.filter(
                        type=parameter,
                        cl=confidence_level,
                        entity__entity_name=level_value,
                    ).order_by('update_date').values('value').last()['value']

    except ObjectDoesNotExist as exception:
        print(traceback.format_exc())
    except Exception as exception:
        print(traceback.format_exc())
    finally:
        resp = {
               'base_case': results['Base'],
               'current_fx_gain_loss': results['FX_Gain_Loss'],
               'at_risk': results['At_Risk']*100,
               'hedging_effect': results['Hedged_Effect'],
               'cost': results['Cost']*100,
               'worst_case': results['Worst_Case'],
                }
        return JsonResponse(resp)


@login_required(login_url='login')
@user_is_allowed
def multi_analytics_mtm_table(request):
    if request.is_ajax() is False or request.method != 'POST':
        return HttpResponseBadRequest()
    try:
        resp_data = []
        date_list = []
        valuation_date = None
        resp_sum = 0
        resp_currency = None
        is_proportional_tab = request.POST.get('is_proportional_tab') or False
        level = request.POST['level']
        level_value = request.POST['level_value']

        try:
            client = VRMClient.objects.get(name=request.POST['client'])
        except VRMClient.DoesNotExist:
            raise Exception('Unable to find the client to process!')
        ignore_proportional_mtm = not VRMContentTab.objects.filter(
                    label='proportional_mtm',
                    client__name=client
            ).exists() or not is_proportional_tab

        level = request.POST.get('level')
        level_value = request.POST.get('level_value')
        if None in [level, level_value]:
            raise Exception('Unable to find the valuation level to process!')
        try:
            module = __import__(client.name.lower())
        except Exception as e:
            try:
                module = __import__(client.label.lower())
            except Exception as e:
                raise Exception('Unable to find the client to process!')

        level_filter = None
        level_filter2 = None
        associated_trade_model = None
        associated_type = None
        if level == 'fund':
            level_filter = {'trade__fund': level_value}
            level_filter2 = {'fund': level_value}
        elif level == 'deal':
            associated_type = {'deal_id': module.models.Deal.objects.get(deal_name=level_value).deal_id}
            if module.models.Dealtrade.objects.filter(deal__deal_name=level_value).exists():
                trade_ids = list(module.models.Dealtrade.objects.filter(
                    deal__deal_name=level_value).values_list('trade_id', flat=True))

                if len(trade_ids) > 0:
                    level_filter = {'trade__trade_id__in': trade_ids}
                    level_filter2 = {'trade_id__in': trade_ids}
                try:
                    associated_trade_model = module.models.Dealtrade
                except:
                    associated_trade_model = None
        elif level == 'entity':
            associated_type = {'entity_id': module.models.Entity.objects.get(entity_name=level_value).entity_id}
            if module.models.Entitytrade.objects.filter(entity__entity_name=level_value).exists():
                trade_ids = list(module.models.Entitytrade.objects.filter(
                    entity__entity_name=level_value).values_list('trade_id', flat=True))
                if len(trade_ids) > 0:
                    level_filter = {'trade__trade_id__in': trade_ids}
                    level_filter2 = {'trade_id__in': trade_ids}
                try:
                    associated_trade_model = module.models.Entitytrade
                except:
                    associated_trade_model = None

        if level_filter is not None and level_filter2 is not None:
            mtm = None
            valuation_date = None
            # fetch mtm based trades
            resp_sum_total = 0.0
            if ignore_proportional_mtm:
                valuation_date, mtm = _get_mtm_list(request, query_set=level_filter)
                if valuation_date is None:
                    return HttpResponseBadRequest()
                resp_data = list(mtm)
                if mtm:

                    if associated_trade_model:
                        for item in resp_data:
                            try:
                                # Make sure incase of deal or entity  notional amount should be displayed according to the
                                # percentage
                                percentage = associated_trade_model.objects.get(trade=item['trade__trade_id'], **associated_type).percentage
                                item['mtm'] = item['mtm'] * percentage
                                item['trade__notional_amounts'] = item['trade__notional_amounts'] * percentage
                                item['trade__premium'] = item['trade__premium'] * percentage
                                resp_sum_total = resp_sum_total + item['mtm']
                            except Exception as exception:
                                print(traceback.format_exc())
                    else:
                        resp_sum_total = mtm.aggregate(Sum('mtm'))['mtm__sum']
            else:
                deal_table = None
                if level == "deal":
                    if module.models.Dealtrade.objects.filter(deal_id__deal_name=level_value).exists():
                        dealtrades = getattr(module.models, 'Dealtrade').objects.filter(deal__deal_name=level_value)
                        trade_ids = [element['trade_id'] for element in list(dealtrades.values('trade_id'))]
                        proportional_mtm_query = {'trade_id__in': trade_ids}
                    else:
                        proportional_mtm_query = {}
                else:
                    proportional_mtm_query = level_filter

                valuation_date, mtm, proportional_trades, mtm_query_set = \
                    _get_proportional_mtm_list(request, query_set=proportional_mtm_query)
                if valuation_date is None:
                    return HttpResponseBadRequest()

                resp_data, resp_sum_total = _construct_proportional_mtm_trade_table(
                    list(mtm),
                    list(proportional_trades),
                    valuation_date,
                    deal_table=deal_table
                )
                # if mtm:
                #     resp_sum_total = mtm.aggregate(Sum('mtm_proportional'))['mtm_proportional__sum']

            resp_sum = resp_sum_total / 1e6 if resp_sum_total is not None else None
            resp_currency = VrmHedge1CopyCopy.objects.filter(
                client=request.POST['client'],
                **level_filter2,
                value_date__gte = valuation_date
            ).first().premium_currency if VrmHedge1CopyCopy.objects.filter(
                client=request.POST['client'],
                **level_filter2,
                value_date__gte = valuation_date
            ).first() else  None
            # try:
            #     resp_currency = getattr(module.models, 'Fund').objects. \
            #         get(fund_name=general_fund.fund_name).ccy
            # except ObjectDoesNotExist as e:
            #     currency = ''

            if ignore_proportional_mtm and mtm:
                _add_new_trades(request, resp_data, resp_currency, mtm, valuation_date, query_set=level_filter)
            date_list = list(VrmMtm1.objects.filter(
                trade__client=client,
                trade__cancellation__iexact='FALSE',
                trade__dummy__iexact='FALSE',
                trade__asset_class__iexact='FX',
                **level_filter,
            ).values_list('valuation_date', flat=True).distinct().order_by('-valuation_date'))

    except ObjectDoesNotExist as exception:
        print(traceback.format_exc())
    except Exception as exception:
        print(traceback.format_exc())
    finally:
        resp = {
            'data': resp_data,
            'mtm_sum': resp_sum,
            'currency': resp_currency,
            'date_list': date_list,
            'valuation_date': valuation_date
        }
        return JsonResponse(resp)


@login_required(login_url='login')
@user_is_allowed
def multi_recalculate_analytics_mtm_table(request):
    if request.is_ajax() is False or request.method != 'POST':
        return HttpResponseBadRequest()
    try:
        resp_data = []
        resp_sum = 0
        resp_currency = ''
        associated_trade_model = None
        associated_type = None
        try:
            client = VRMClient.objects.get(name=request.POST['client'])
        except VRMClient.DoesNotExist:
            raise Exception('Unable to find the client to process!')
        level = request.POST.get('level')
        level_value = request.POST.get('level_value')
        if None in [level, level_value]:
            raise Exception('Unable to find the valuation level to process!')
        try:
            module = __import__(client.name.lower())
        except Exception as e:
            try:
                module = __import__(client.label.lower())
            except Exception as e:
                raise Exception('Unable to find the client to process!')

        level_filter = None
        level_filter2 = None
        trade_ids = []
        if level == 'fund':
            level_filter = {'trade__fund': level_value}
            level_filter2 = {'fund': level_value}
        elif level == 'deal':
            associated_type = {'deal_id': module.models.Deal.objects.get(deal_name=level_value).deal_id}
            if module.models.Dealtrade.objects.filter(deal__deal_name=level_value).exists():
                trade_ids = list(module.models.Dealtrade.objects.filter(
                    deal__deal_name=level_value).values_list('trade_id', flat=True))
                if len(trade_ids) > 0:
                    level_filter = {'trade__trade_id__in': trade_ids}
                    level_filter2 = {'trade_id__in': trade_ids}
                try:
                    associated_trade_model = module.models.Dealtrade
                except:
                    associated_trade_model = None
        elif level == 'entity':
            associated_type = {'entity_id': module.models.Entity.objects.get(entity_name=level_value).entity_id}
            if module.models.Entitytrade.objects.filter(entity__entity_name=level_value).exists():
                trade_ids = list(module.models.Entitytrade.objects.filter(
                    entity__entity_name=level_value).values_list('trade_id', flat=True))
                if len(trade_ids) > 0:
                    level_filter = {'trade__trade_id__in': trade_ids}
                    level_filter2 = {'trade_id__in': trade_ids}
                try:
                    associated_trade_model = module.models.Entitytrade
                except:
                    associated_trade_model = None

        if level_filter is not None and level_filter2 is not None:
            mtm = None
            valuation_date = None
            # fetch mtm based trades
            valuation_date, mtm = _get_mtm_list(request, query_set=level_filter)
            if valuation_date is None:
                return HttpResponseBadRequest()
            if mtm:
                spot_ref_data = pd.DataFrame(json.loads(request.POST['spot_ref_data'] or '[]'))
                mtm_data = pd.DataFrame(list(mtm) or '[]')

                mtm_data['new_spot_ref'] = np.nan
                for index, row in spot_ref_data.iterrows():
                    match = mtm_data['trade__underlying'] == row['trade__underlying']
                    if match.any():
                        mtm_data.loc[match, 'new_spot_ref'] = \
                            float(row['average_spot_rate'])

                for index, row in mtm_data.iterrows():
                    # set this percentage to 1
                    associated_percentage = 1
                    if associated_trade_model:
                        try:
                            associated_percentage = associated_trade_model.objects.get(trade=row['trade__trade_id'],
                                                                            **associated_type).percentage
                        except ObjectDoesNotExist as e:
                            associated_percentage = 1
                        row['trade__notional_amounts'] = row['trade__notional_amounts'] * associated_percentage
                        row['trade__premium'] = row['trade__premium'] * associated_percentage
                    fwd_points = row['outright_forward'] - row['spot_ref']
                    new_outright_forward = fwd_points + row['new_spot_ref']

                    ccy1 = row['trade__underlying'][:3]
                    ccy2 = row['trade__underlying'][3:]
                    new_mtm = 0
                    if row['trade__style'] in ["Spot", "Put", "Call", "Forward", "Swap"]:
                        if row['trade__notional_currency'] == ccy1:
                            if row['mtm_currency'] == ccy2:
                                if row['trade__direction'].lower() == "buy":
                                    new_mtm = row['trade__notional_amounts'] * (
                                                new_outright_forward - row['trade__strike'])
                                else:
                                    new_mtm = -row['trade__notional_amounts'] * (
                                                new_outright_forward - row['trade__strike'])
                            else:
                                if row['trade__direction'].lower() == "buy":
                                    new_mtm = row['trade__notional_amounts'] * (
                                                new_outright_forward - row['trade__strike']) / \
                                              row['new_spot_ref']
                                else:
                                    new_mtm = -row['trade__notional_amounts'] * (
                                                new_outright_forward - row['trade__strike']) / \
                                              row['new_spot_ref']
                        else:
                            if row['mtm_currency'] == ccy2:
                                if row['trade__direction'].lower() == "buy":
                                    new_mtm = row['trade__notional_amounts'] * \
                                              (1 / new_outright_forward - 1 / row['trade__strike']) * \
                                              row['new_spot_ref']
                                else:
                                    new_mtm = -row['trade__notional_amounts'] * \
                                              (1 / new_outright_forward - 1 / row['trade__strike']) * \
                                              row['new_spot_ref']
                            else:
                                if not new_outright_forward == 0:
                                    if row['trade__direction'].lower() == "buy":
                                        new_mtm = row['trade__notional_amounts'] * \
                                                  (1 / new_outright_forward - 1 / row['trade__strike'])
                                    else:
                                        new_mtm = -row['trade__notional_amounts'] * \
                                                  (1 / new_outright_forward - 1 / row['trade__strike'])
                                else:
                                    new_mtm = 0

                    row['spot_ref'] = row['new_spot_ref']
                    row['outright_forward'] = new_outright_forward
                    if associated_trade_model:
                        try:
                            new_mtm = new_mtm * associated_percentage
                        except:
                            print(traceback.format_exc())
                    row['mtm'] = new_mtm
                    resp_sum += new_mtm
                    resp_data += [row.to_dict()]
                if resp_sum != 0:
                    resp_sum = resp_sum / 1e6
                resp_currency = resp_data[0]['trade__premium_currency'] if len(resp_data) > 0 else None
                _add_new_trades(request, resp_data, resp_currency, mtm, valuation_date, query_set=level_filter)
    except ObjectDoesNotExist as exception:
        print(traceback.format_exc())
    except Exception as exception:
        print(traceback.format_exc())
    finally:
        resp = {
            'data': resp_data,
            'mtm_sum': resp_sum,
            'currency': resp_currency
        }
        return JsonResponse(resp)


@login_required(login_url='login')
@user_is_allowed
def multi_analytics_spotrates_table(request):
    if request.is_ajax() is False or request.method != 'POST':
        return HttpResponseBadRequest()
    try:
        resp_data = []
        try:
            client = VRMClient.objects.get(name=request.POST['client'])
        except VRMClient.DoesNotExist:
            raise Exception('Unable to find the client to process!')
        level = request.POST.get('level')
        level_value = request.POST.get('level_value')
        if None in [level, level_value]:
            raise Exception('Unable to find the valuation level to process!')

        try:
            module = __import__(client.name.lower())
        except Exception as e:
            try:
                module = __import__(client.label.lower())
            except Exception as e:
                raise Exception('Unable to find the client to process!')

        level_filter = None
        trade_ids = []
        if level == 'fund':
            level_filter = {'trade__fund': level_value}
        elif level == 'deal':
            if module.models.Dealtrade.objects.filter(deal__deal_name=level_value).exists():
                trade_ids = list(module.models.Dealtrade.objects.filter(
                    deal__deal_name=level_value).values_list('trade_id', flat=True))
                if len(trade_ids) > 0:
                    level_filter = {'trade__trade_id__in': trade_ids}
        elif level == 'entity':
            if module.models.Entitytrade.objects.filter(entity__entity_name=level_value).exists():
                trade_ids = list(module.models.Entitytrade.objects.filter(
                    entity__entity_name=level_value).values_list('trade_id', flat=True))
                if len(trade_ids) > 0 :
                    level_filter = {'trade__trade_id__in': trade_ids}

        if level_filter is not None:
            if request.POST['valuation_date'] == '':
                valuation_date = VrmMtm1.objects.filter(trade__client=request.POST['client'],
                                                        trade__cancellation__iexact='FALSE',
                                                        trade__dummy__iexact='FALSE',
                                                        trade__asset_class__iexact='FX',
                                                        **level_filter,
                                                        ).aggregate(Max('valuation_date'))['valuation_date__max']
            else:
                try:
                    valuation_date = datetime.datetime.strptime(request.POST['valuation_date'], "%Y-%m-%d")
                except:
                    valuation_date = VrmMtm1.objects.filter(trade__client=request.POST['client'],
                                                            trade__cancellation__iexact='FALSE',
                                                            trade__dummy__iexact='FALSE',
                                                            trade__asset_class__iexact='FX',
                                                            **level_filter,
                                                            ).aggregate(Max('valuation_date'))['valuation_date__max']

            resp_data = []
            if valuation_date:
                resp_data = list(VrmMtm1.objects.filter(
                    trade__client=client,
                    trade__value_date__gte=valuation_date,
                    trade__cancellation__iexact='FALSE',
                    trade__dummy__iexact='FALSE',
                    trade__asset_class__iexact='FX',
                    valuation_date=valuation_date,
                    **level_filter,
                ).values(
                    'trade__underlying',
                ).annotate(average_spot_rate=Avg('spot_ref')))

    except ObjectDoesNotExist as exception:
        print(traceback.format_exc())
    except Exception as exception:
        print(traceback.format_exc())
    finally:
        resp = {
            'data': resp_data,
        }
        return JsonResponse(resp)


@login_required(login_url='login')
@user_is_allowed
def fundlevel_portfolio_mtm_chart(request):
    if request.is_ajax() is False or request.method != 'POST':
        return HttpResponseBadRequest()

    try:
        series_dates = []
        series_unhedged = []
        series_hedged = []
        ccy = ''
        try:
            client = VRMClient.objects.get(name=request.POST['client'])
        except VRMClient.DoesNotExist:
            raise Exception('Unable to find the client to process!')
        level = request.POST.get('level')
        level_value = request.POST.get('level_value')
        if None in [level, level_value]:
            raise Exception('Unable to find the valuation level to process!')
        try:
            module = __import__(client.name.lower())
        except Exception as e:
            try:
                module = __import__(client.label.lower())
            except Exception as e:
                raise Exception('Unable to find the client to process!')
        general_client = Clients.objects.get(client_name=client)
        general_fund = Funds.objects.get(fund_name=request.POST['fund'], client=general_client.pk)
        HedgeRatio = getattr(module.models, 'HedgeRatio')
        level_relation_fields = []
        level_filter = {'fund_id': general_fund.id}
        level_relation_fields.append('fund_id')
        if level and level != 'fund' and level_value:
            dynamic_field = {level.lower() + '_name': level_value}
            try:
                class_obj = getattr(module.models, level.title())
                table_access = class_obj.objects.get(**dynamic_field)
                level_filter[level.lower() + '_id'] = getattr(table_access, level.lower() + '_id')
                level_relation_fields.append(level.lower() + '_id')
            except ObjectDoesNotExist:
                print(traceback.format_exc())
        else:
            level_filter['entity_id__isnull'] = True
            if client.investor:
                level_filter['investor_id__isnull'] = True
            level_filter['deal_id__isnull'] = True
        if HedgeRatio.objects.filter(**level_filter).count() > 0:
            ccy = list(HedgeRatio.objects.filter(**level_filter).values_list('fund_currency'))[0]
            latest_date = []
            for unhedgeitem in HedgeRatio.objects.filter(
                    **level_filter).order_by('date__date').distinct():
                latest_date.append(HedgeRatio.objects.filter(
                    **level_filter, date__date=unhedgeitem.date.date()).order_by('-date')[0].date)
            unhedged = HedgeRatio.objects.filter(
                **level_filter, date__in=latest_date).order_by('date').values('date__date').annotate(
                nav_sum=Round(Sum('nav_fund_ccy')))

            # unhedged = HedgeRatio.objects.filter(
            #     **level_filter,).order_by('date').values('date__date').annotate(nav_sum=Round(Sum('nav_fund_ccy')))

            for tuple_row in unhedged.values_list('date__date', 'nav_sum'):
                series_dates.append(tuple_row[0])
                series_unhedged.append(tuple_row[1])
            hedged = None
            if level and level != 'fund' and level_value:
                try:
                    trade_ids = []
                    if level == 'deal':
                        trade_ids = []
                        if client.investor:
                            if module.models.AssetTrade.objects.filter(deal__deal_name=level_value,
                                                                       fund=general_fund).exists():
                                trade_ids = list(module.models.AssetTrade.objects.filter(
                                    deal__deal_name=level_value, fund=general_fund).exclude(
                                    deal_id__isnull=True).values_list('trade_id', flat=True))
                        else:
                            if module.models.Dealtrade.objects.filter(deal__deal_name=level_value).exists():
                                trade_ids = list(module.models.Dealtrade.objects.filter(
                                    deal__deal_name=level_value).values_list('trade_id', flat=True))

                    elif level == 'entity':
                        if module.models.Entity.objects.filter(entity_name=level_value).exists():

                            if client.investor:
                                if module.models.AssetTrade.objects.filter(entity__entity_name=level_value,
                                                                           fund=general_fund).exists():
                                    trade_ids = list(module.models.AssetTrade.objects.filter(
                                        entity__entity_name=level_value, fund=general_fund).exclude(
                                        entity_id__isnull=True).values_list('trade_id', flat=True))
                            else:
                                if module.models.Entitytrade.objects.filter(entity__entity_name=level_value).exists():
                                    trade_ids = list(module.models.Entitytrade.objects.filter(
                                        entity__entity_name=level_value).values_list('trade_id', flat=True))
                    elif level == 'investor':
                        if module.models.AssetTrade.objects.filter(investor__investor_name=level_value,
                                                                   fund=general_fund).exists():
                            trade_ids = list(module.models.AssetTrade.objects.filter(
                                investor__investor_name=level_value, fund=general_fund).exclude(
                                investor_id__isnull=True).values_list('trade_id', flat='True'))
                except ObjectDoesNotExist as exception:
                    print(traceback.format_exc())
                except Exception as exception:
                    print(traceback.format_exc())
                hedged = VrmMtm1.objects.filter(
                        trade__trade_id__in=trade_ids,
                        trade__cancellation__iexact='FALSE',
                        trade__dummy__iexact='FALSE',
                        trade__asset_class__iexact='FX',
                        valuation_date__in=series_dates
                    ).order_by('valuation_date').values('valuation_date').annotate(mtm_sum=Round(Sum('mtm')))
            else:
                hedged = VrmMtm1.objects.filter(
                    trade__client=general_client.client_name,
                    trade__fund=general_fund.fund_name,
                    trade__cancellation__iexact='FALSE',
                    trade__dummy__iexact='FALSE',
                    trade__asset_class__iexact='FX',
                    valuation_date__in=series_dates
                ).order_by('valuation_date').values('valuation_date').annotate(mtm_sum=Round(Sum('mtm')))

            if hedged is not None:
                i = 0
                for tuple_row in list(hedged.values_list('valuation_date', 'mtm_sum')):
                    current_date_data_point = series_dates[i]
                    if tuple_row[0] != current_date_data_point:
                        if i > 0:
                            while tuple_row[0] != current_date_data_point:
                                series_hedged.append(series_hedged[i-1])
                                current_date_data_point = series_dates[i + 1]
                                i += 1
                    series_hedged.append(tuple_row[1] + series_unhedged[i])
                    i += 1
    except ObjectDoesNotExist as exception:
        print(traceback.format_exc())
    except Exception as exception:
        print(traceback.format_exc())
    finally:
        resp = {
                 'time': series_dates,
                 'unhedged': series_unhedged,
                 'hedged': series_hedged,
                 'ccy': ccy,
                }
        return JsonResponse(resp)

    #TODO need to check below content leave it for the moment
    #     HedgeUnhedge = getattr(module.models, 'HedgeUnhedge')
    #     DirectDeal = getattr(module.models, 'DirectDeal')
    #
    #     if HedgeUnhedge.objects.filter(**level_filter).exists() \
    #             and level_filter is not None\
    #             and level_relation_fields is not None:
    #         hedge_unhdege_data = HedgeUnhedge.objects.filter(**level_filter).values(
    #             'valuation_date',
    #             'hedged_amounts',
    #             'unhedged_amounts',
    #             'fund_id',
    #             *level_relation_fields,
    #         )
    #
    #         series_dates = [item['valuation_date'] for item in hedge_unhdege_data]
    #         series_hedged = [item['hedged_amounts'] for item in hedge_unhdege_data]
    #         series_unhedged = [item['unhedged_amounts'] for item in hedge_unhdege_data]
    #
    #         if level == 'fund':
    #             ccy = Ccys.objects.get(id= Funds.objects.get(id =hedge_unhdege_data[0]['fund_id']).ccy_id).identifier
    #         else:
    #             level_relations = dict()
    #             for level_relation_field in level_relation_fields:
    #                 level_relations[level_relation_field] = hedge_unhdege_data[0][level_relation_field]
    #             ccy = DirectDeal.objects.filter(**level_relations).values('direct_local_currency').\
    #                 first()['direct_local_currency'] if DirectDeal.objects.filter(**level_relations).count() > 0 \
    #                 else ''
    #
    # except ObjectDoesNotExist as exception:
    #     print(traceback.format_exc())
    # except Exception as exception:
    #     print(traceback.format_exc())
    # finally:
    #     resp = {
    #              'time': series_dates,
    #              'unhedged': series_unhedged,
    #              'hedged': series_hedged,
    #              'ccy': ccy,
    #             }
    #     return JsonResponse(resp)


@login_required(login_url='login')
@user_is_allowed
def fundlevel_investor_summary_analysis(request):

    '''
    :param request:  Investor analysis data contains the following in the request
            client : client
            fund :fund
            level :investor
            level name : investor name
            date : month and the year
            overall : checked or not
    :return:gross_fund_return and gross_fund_return_percentage table data for that month or overall based on the
            selection '''

    if request.is_ajax() is False or request.method != 'POST':
        return HttpResponseBadRequest()
    try:
        data = []
        results = {'return_invest_ccy': 0.0,
                   'fx_market_impact': 0.0,
                   'return_fund_ccy': 0.0,
                   'cost_of_hedging': 0.0,
                   'hedging_impact': 0.0,
                   'final_return': 0.0,
                   }
        table_display =  {'return_invest_ccy': 0,
                   'fx_market_impact': 0,
                   'return_fund_ccy': 0,
                   'cost_of_hedging': 0,
                   'hedging_impact': 0,
                   'final_return': 0,
                   }
        fund = request.POST['fund']
        module = None
        try:
            client = VRMClient.objects.get(name=request.POST['client'])
        except VRMClient.DoesNotExist:
            raise Exception('Unable to find the client to process!')
        level = request.POST.get('level')
        level_value = request.POST.get('level_value')
        input_date = request.POST.get('date')
        overall = request.POST.get('overall')
        if None in [level, level_value, date, overall]:
            raise Exception('Unable to find the valuation level to process!')
        try:
            module = __import__(client.name.lower())
        except Exception as e:
            try:
                module = __import__(client.label.lower())
            except Exception as e:
                raise Exception('Unable to find the client to process!')

        general_client = Clients.objects.get(client_name=client)
        general_fund = Funds.objects.get(fund_name=request.POST['fund'], client=general_client.pk)
        level_relation_fields = []
        level_filter = {'fund_id': general_fund.id}
        level_relation_fields.append('fund_id')
        input_date = datetime.date(int(input_date.split(' ')[0]), int(input_date.split(' ')[1]), date.today().day)
        if level and level != 'fund' and level_value:
            dynamic_field = {level.lower() + '_name': level_value}
            try:
                class_obj = getattr(module.models, level.title())
                table_access = class_obj.objects.get(**dynamic_field)
                level_filter[level.lower() + '_id'] = getattr(table_access, level.lower() + '_id')
                level_relation_fields.append(level.lower() + '_id')
            except Exception as e:
                print(traceback.format_exc())
        else:
            # Remeber this is Fund selection only no deal or entity or investor should be selected.
            level_filter['entity_id__isnull'] = True
            if client.investor:
                level_filter['investor_id__isnull'] = True
            level_filter['deal_id__isnull'] = True

        gross_fund_return = getattr(module.models, 'GrossFundReturn')
        gross_fund_return_percentage = getattr(module.models, 'GrossFundReturnPercentage')
        ## Javascript providing false
        if overall == 'false':
            level_filter[ 'date__gte'] = get_first_day(input_date)
            level_filter['date__lte'] = get_last_day(input_date)
        if  gross_fund_return.objects.filter(**level_filter).exists() and  \
                gross_fund_return_percentage.objects.filter(**level_filter).exists():
            gross_fund_return_obj = gross_fund_return.objects.filter(**level_filter).values()
            # Expecting to return only one row only.
            gross_fund_return_percentage_obj = gross_fund_return_percentage.objects.filter(**level_filter).values()
            # Summation of the investor value ignore the date
            # TODO correct this as well to above to keep in sync
            for item in gross_fund_return_obj:
                table_display['return_invest_ccy'] =  table_display['return_invest_ccy'] + item['return_invest_ccy']
                table_display['fx_market_impact'] = table_display['fx_market_impact'] +item['fx_market_impact']
                table_display['cost_of_hedging'] = table_display['cost_of_hedging'] + item['cost_of_hedging']
                table_display['return_fund_ccy'] = table_display['return_fund_ccy'] + item['return_fund_ccy']
                table_display['hedging_impact'] = table_display['hedging_impact'] + item['hedging_impact']
                table_display['final_return'] = table_display['final_return'] + item['final_return']

            for item in list(gross_fund_return_percentage_obj):
                results['return_invest_ccy'] =  results['return_invest_ccy'] + item['return_invest_ccy']
                results['fx_market_impact'] = results['fx_market_impact'] +item['fx_market_impact']
                results['cost_of_hedging'] = results['cost_of_hedging'] + item['cost_of_hedging']
                results['return_fund_ccy'] = results['return_fund_ccy'] + item['return_fund_ccy']
                results['hedging_impact'] = results['hedging_impact'] + item['hedging_impact']
                results['final_return'] = results['final_return'] + item['final_return']
        else:
            table_display = []
            results = []
        data.append(table_display)

    except ObjectDoesNotExist as exception:
        print(traceback.format_exc())
    except Exception as exception:
        data['table_display]'] = []
        data['results]'] = []
        print(traceback.format_exc())
    finally:
        resp = {
            'data': data,
            'results': results,
        }
        return JsonResponse(resp)


@login_required(login_url='login')
@user_is_allowed
def fundlevel_summary_base_case_risk(request):
    if request.is_ajax() is False or request.method != 'POST':
        return HttpResponseBadRequest()
    try:
        current_spot = 0.0
        base_case = 0.0
        current_fx_gain_loss = 0.0
        filter_value = 0.0
        at_risk = 0.0
        irr_unhedged_value = 0.0
        hedge_irr_value = 0.0
        hedging_effect = 0
        cost = 0.0
        worst_case = 0.0
        irr_value = 0.0
        irr_nocost_hedge_value = 0.0
        results = {'Base': 0,
                   'FX_Gain_Loss': 0,
                   'At_Risk': 0,
                   'Hedged_Effect': 0,
                   'Cost': 0,
                   'Worst_Case': 0,
        }

        confidence_level = request.POST['confidence_level']
        try:
            client = VRMClient.objects.get(name=request.POST['client'])
        except VRMClient.DoesNotExist:
            raise Exception('Unable to find the client to process!')
        level = request.POST.get('level')
        level_value = request.POST.get('level_value')
        if None in [level]:
            raise Exception('Unable to find the valuation level to process!')

        try:
            module = __import__(client.name.lower())
        except Exception as e:
            try:
                module = __import__(client.label.lower())
            except Exception as e:
                raise Exception('Unable to find the client to process!')
        general_client = Clients.objects.get(client_name=client)
        general_fund = Funds.objects.get(fund_name=request.POST['fund'], client=general_client.pk)
        level_relation_fields = []
        level_filter = {'fund_id': general_fund.id}
        level_relation_fields.append('fund_id')
        if level and level != 'fund' and level_value:
            dynamic_field = {level.lower() + '_name': level_value}
            try:
                if level.title() == 'Deal':
                    level_filter[level.lower() + '_id'] = getattr(module.models,
                                                                  'FundAssociation').objects.get(fund_id=general_fund.id,
                                                                  deal__deal_name=level_value).deal_id
                else:
                    table_access = getattr(module.models, level.title()).objects.get(**dynamic_field)
                    level_filter[level.lower() + '_id'] = getattr(table_access, level.lower() + '_id')
                level_relation_fields.append(level.lower() + '_id')
                level_filter['deal_name'] = level_value
            except Exception as e:
                print(traceback.format_exc())
        else:
            # Remeber this is Fund selection only no deal or entity or investor should be selected.
            level_filter['entity_id__isnull'] = True
            if client.investor:
                level_filter['investor_id__isnull'] = True
            level_filter['deal_id__isnull'] = True
            level_filter['deal_name'] = 'Overall'

        # for parameter, value in [']:
        #     if module.models.IrrAtRisk.objects.filter(type=parameter, **level_filter).exists():
        #         results[parameter] = module.models.IrrAtRisk.objects.filter(
        #             type=parameter,
        #             cl=confidence_level,
        #             **level_filter
        #         ).order_by('update_date').values('value').last()['value']

        IrrAtRisk = getattr(module.models, 'IrrAtRisk')
        IrrAtRiskNoCost = getattr(module.models, 'IrrAtRiskNoCost')
        SummaryTable = getattr(module.models, 'SummaryTable')
        query = None
        fund_type_use = request.POST['fund']
        query = {'fund': fund_type_use}

        irr_at_risk = IrrAtRisk.objects.filter(**level_filter,  cl=confidence_level).order_by(
            '-update_date')
        irr_at_risk_no_cost = IrrAtRiskNoCost.objects.filter(**level_filter,
                                                             cl=confidence_level).order_by('-update_date')
        # TODO correct this as well to above to keep in sync
        summary_table = SummaryTable.objects.filter(**level_filter).order_by('-update_date').values()

        if len(summary_table) > 0:
            base_case = summary_table[0]['expected_irr'] * 100
            current_fx_gain_loss = (summary_table[0]['irr_current_spot'] - \
                                    summary_table[0]['expected_irr']) * 100

            current_spot = summary_table.values('irr_current_spot')[0]['irr_current_spot'] * 100
        if len(irr_at_risk) > 0:
            filter_value = irr_at_risk.filter(**level_filter, type='Unhedged', cl=confidence_level).order_by('-update_date').values('value')[0]['value']*100
            irr_unhedged_value = irr_at_risk.filter(**level_filter, type='Unhedged', cl=confidence_level).order_by('-update_date').values('value')[0]['value'] *100
            hedge_irr_value = irr_at_risk.filter(**level_filter, type='Hedged', cl=confidence_level).order_by('-update_date').values('value')[0]['value'] *100

        at_risk = filter_value - current_spot
        if len(irr_at_risk_no_cost) > 0:
            irr_value = irr_at_risk_no_cost.filter(**level_filter, type='Hedged', cl=confidence_level).order_by('-update_date').values('value')[0]['value']   *100
            irr_nocost_hedge_value = irr_at_risk_no_cost.filter(**level_filter, type='Hedged', cl=confidence_level).values('value')[0][
                'value']  *100

        hedging_effect = (irr_value - irr_unhedged_value)

        cost = (hedge_irr_value - irr_nocost_hedge_value)

        worst_case = cost + base_case + current_fx_gain_loss + at_risk + hedging_effect
    except ObjectDoesNotExist as exception:
        print(traceback.format_exc())
    except Exception as exception:
        print(traceback.format_exc())
    finally:
        resp = {
            'base_case': base_case,
            'current_fx_gain_loss': current_fx_gain_loss,
            'at_risk': at_risk,
            'hedging_effect': hedging_effect,
            'cost': cost,
            'worst_case': worst_case
        }
        # resp = {
        #        'base_case': results['Base']*100,
        #        'current_fx_gain_loss': results['FX_Gain_Loss']*100,
        #        'at_risk': results['At_Risk']*100,
        #        'hedging_effect': results['Hedged_Effect']*100,
        #        'cost': results['Cost']*100,
        #        'worst_case': results['Worst_Case']*100,
        #         }
        return JsonResponse(resp)


@login_required(login_url='login')
@user_is_allowed
def fundlevel_moic_base_case_risk(request):
    if request.is_ajax() is False or request.method != 'POST':
        return HttpResponseBadRequest()
    try:

        results = {'Base': 0,
                   'FX_Gain_Loss': 0,
                  'At_Risk': 0,
                  'Hedged_Effect': 0,
                  'Cost': 0,
                  'Worst_Case': 0,
        }

        confidence_level = request.POST['confidence_level']
        try:
            client = VRMClient.objects.get(name=request.POST['client'])
        except VRMClient.DoesNotExist:
            raise Exception('Unable to find the client to process!')
        level = request.POST.get('level')
        level_value = request.POST.get('level_value')
        if None in [level, level_value]:
            raise Exception('Unable to find the valuation level to process!')

        try:
            module = __import__(client.name.lower())
        except Exception as e:
            try:
                module = __import__(client.label.lower())
            except Exception as e:
                raise Exception('Unable to find the client to process!')
        general_client = Clients.objects.get(client_name=client)
        general_fund = Funds.objects.get(fund_name=request.POST['fund'], client=general_client.pk)
        level_relation_fields = []
        level_filter = {'fund_id': general_fund.id}
        level_relation_fields.append('fund_id')
        if level and level != 'fund' and level_value:
            dynamic_field = {level.lower() + '_name': level_value}
            try:
                table_access = getattr(module.models, level.title()).objects.get(**dynamic_field)
                level_filter[level.lower() + '_id'] = getattr(table_access, level.lower() + '_id')
                level_relation_fields.append(level.lower() + '_id')
            except Exception as e:
                print(traceback.format_exc())
        else:
            # Remeber this is Fund selection only no deal or entity or investor should be selected.
            level_filter['entity_id__isnull'] = True
            if client.investor:
                level_filter['investor_id__isnull'] = True
            level_filter['deal_id__isnull'] = True
        for parameter, value in results.items():
            if module.models.MoicAtRisk.objects.filter(type=parameter, fund_id=general_fund.id).exists():
                results[parameter] = module.models.MoicAtRisk.objects.filter(
                    type=parameter,
                    cl=confidence_level,
                    **level_filter
                ).order_by('update_date').values('value').last()['value']

    except ObjectDoesNotExist as exception:
        print(traceback.format_exc())
    except Exception as exception:
        print(traceback.format_exc())
    finally:
        resp = {
               'base_case': results['Base'],
               'current_fx_gain_loss': results['FX_Gain_Loss'],
               'at_risk': results['At_Risk']*100,
               'hedging_effect': results['Hedged_Effect'],
               'cost': results['Cost']*100,
               'worst_case': results['Worst_Case'],
                }
        return JsonResponse(resp)


@login_required(login_url='login')
@user_is_allowed
def fundlevel_analytics_mtm_table(request):
    if request.is_ajax() is False or request.method != 'POST':
        return HttpResponseBadRequest()
    try:
        resp_data = []
        date_list = []
        valuation_date = None
        resp_sum = 0
        resp_currency = ''

        is_proportional_tab = request.POST.get('is_proportional_tab') or False
        # TODO repeated code need to modularise it.
        try:
            client = VRMClient.objects.get(name=request.POST['client'])
        except VRMClient.DoesNotExist:
            raise Exception('Unable to find the client to process!')

        ignore_proportional_mtm = not VRMContentTab.objects.filter(
                    label='proportional_mtm',
                    client__name=client
            ).exists() or not is_proportional_tab

        level = request.POST.get('level')
        level_value = request.POST.get('level_value')
        if None in [level, level_value]:
            raise Exception('Unable to find the valuation level to process!')
        try:
            module = __import__(client.name.lower())
        except Exception as e:
            try:
                module = __import__(client.label.lower())
            except Exception as e:
                raise Exception('Unable to find the client to process!')
        general_client = Clients.objects.get(client_name=client)
        general_fund = Funds.objects.get(fund_name=request.POST['fund'], client=general_client.pk)
        level_relation_fields = []
        level_filter = {'fund_id': general_fund.id}
        level_relation_fields.append('fund_id')
        if level and level != 'fund' and level_value:
            dynamic_field = {level.lower() + '_name': level_value}
            try:
                table_access = getattr(module.models, level.title()).objects.get(**dynamic_field)
                level_filter[level.lower() + '_id'] = getattr(table_access, level.lower() + '_id')
                level_relation_fields.append(level.lower() + '_id')
            except Exception as e:
                print(traceback.format_exc())
        level_filter = None
        level_filter2 = None
        associated_trade_model = None
        associated_type = None
        all_condition = False
        if level == 'fund':
            level_key = "fund_id__fund_name"
            level_filter = {'trade__fund': level_value}
            level_filter2 = {'fund': level_value}
            all_condition = True
        elif level == 'deal':
            level_key = ""
            try:
                trade_ids = []
                associated_type = {'deal_id': module.models.Deal.objects.get(deal_name=level_value).deal_id}
                if client.investor:
                    if module.models.AssetTrade.objects.filter(deal__deal_name=level_value, fund=general_fund).exists():
                        trade_ids = list(module.models.AssetTrade.objects.filter(
                            deal__deal_name=level_value, fund=general_fund).exclude(
                            deal_id__isnull=True).values_list('trade_id', flat=True))
                else:
                    if module.models.Dealtrade.objects.filter(deal__deal_name=level_value).exists():
                        trade_ids = list(module.models.Dealtrade.objects.filter(
                            deal__deal_name=level_value).values_list('trade_id', flat=True))
                if len(trade_ids) > 0:
                    level_filter = {'trade__trade_id__in': trade_ids}
                    level_filter2 = {'trade_id__in': trade_ids}
                try:
                    associated_trade_model = module.models.Dealtrade
                except:
                    associated_trade_model = None
                all_condition = True
            except ObjectDoesNotExist as exception:
                print(traceback.format_exc())
            except Exception as exception:
                print(traceback.format_exc())
        elif level == 'entity':
            level_key = "entity_id__entity_name"
            try:
                if module.models.Entity.objects.filter(entity_name=level_value).exists():
                    trade_ids = []
                    associated_type = {'entity_id': module.models.Entity.objects.get(entity_name=level_value).entity_id}
                    if client.investor:
                        if module.models.AssetTrade.objects.filter(entity__entity_name=level_value,
                                                                   fund=general_fund).exists():
                            trade_ids = list(module.models.AssetTrade.objects.filter(
                                        entity__entity_name=level_value, fund=general_fund).exclude(
                                        entity_id__isnull=True).values_list('trade_id', flat=True))
                    else:
                        if module.models.Entitytrade.objects.filter(entity__entity_name=level_value).exists():
                            trade_ids = list(module.models.Entitytrade.objects.filter(
                                entity__entity_name=level_value).values_list('trade_id', flat=True))
                    if len(trade_ids) > 0:
                        level_filter = {'trade__trade_id__in': trade_ids}
                        level_filter2 = {'trade_id__in': trade_ids}
                    try:
                        associated_trade_model = module.models.Entitytrade
                    except:
                        associated_trade_model = None
                    all_condition = True
            except ObjectDoesNotExist as exception:
                print(traceback.format_exc())
            except Exception as exception:
                print(traceback.format_exc())
        elif level == 'investor':
            level_key = ""
            try:
                associated_type = {'investor_id': module.models.Investor.objects.get(investor_name=level_value).investor_id}
                if module.models.AssetTrade.objects.filter(investor__investor_name=level_value,fund=general_fund).exists():
                    trade_ids = list(module.models.AssetTrade.objects.filter(
                        investor__investor_name=level_value,fund=general_fund).exclude(
                                      investor_id__isnull=True).values_list('trade_id', flat='True'))
                    if len(trade_ids) > 0:
                        level_filter = {'trade__trade_id__in': trade_ids}
                        level_filter2 = {'trade_id__in': trade_ids}
                    try:
                        associated_trade_model = module.models.AssetTrade
                    except:
                        associated_trade_model = None
                    all_condition = True
            except ObjectDoesNotExist as exception:
                print(traceback.format_exc())
            except Exception as exception:
                print(traceback.format_exc())

        if level_filter is not None and level_filter2 is not None and all_condition:
            mtm = None
            valuation_date = None
            # fetch mtm based trades
            if ignore_proportional_mtm:
                valuation_date, mtm = _get_mtm_list(request, query_set=level_filter)
                if valuation_date is None:
                    return HttpResponseBadRequest()
                if mtm:
                    resp_data = list(mtm)
                    resp_sum_total = 0.0
                    if associated_trade_model:
                        for item in resp_data:
                            try:
                                try:
                                    # ASsuming table with percentage exist if not assign 100%
                                    item['mtm'] = item['mtm'] * associated_trade_model.objects.get(
                                        trade=item['trade__trade_id'], **associated_type).percentage
                                    row['trade__notional_amounts'] = row['trade__notional_amounts'] * associated_trade_model.objects.get(
                                        trade=item['trade__trade_id'], **associated_type).percentage
                                except:
                                    item['mtm'] = 0.0
                                    item['trade__notional_amounts'] = 0.0
                                resp_sum_total = resp_sum_total + item['mtm']
                            except:
                                print(traceback.format_exc())
                    else:
                        resp_sum_total = mtm.aggregate(Sum('mtm'))['mtm__sum']

                    date_list = list(VrmMtm1.objects.filter(
                        trade__client=client,
                        trade__cancellation__iexact='FALSE',
                        trade__dummy__iexact='FALSE',
                        trade__asset_class__iexact='FX',
                        **level_filter,
                    ).values('valuation_date').distinct().order_by('-valuation_date')[:15])
            else:
                if level_key != "":
                    mtm_queryset = {level_key: level_value}
                else:
                    mtm_queryset = {}
                if level == "deal":
                    deal_table = getattr(module.models, 'Dealtrade').objects.filter(deal__deal_name=level_value)
                    mtm_queryset['trade_id__in'] = list(getattr(module.models, 'Dealtrade').objects.filter(deal__deal_name=level_value).values_list('trade_id',flat=True))
                    mtm_queryset['trade__fund'] = general_fund.fund_name
                valuation_date, mtm, proportional_trades, mtm_query_set = \
                    _get_proportional_mtm_list(request, query_set=mtm_queryset)

                if level_key == "":
                    mtm_queryset = {}
                if valuation_date is None:
                    return HttpResponseBadRequest()
                if mtm:
                    deal_table = []
                    if level == "deal":
                        deal_table = getattr(module.models, 'Dealtrade').objects.filter(deal__deal_name=level_value)
                    resp_data, resp_sum_total  = _construct_proportional_mtm_trade_table(
                        list(mtm),
                        list(proportional_trades),
                        valuation_date,
                        deal_table=deal_table
                    )

                    #resp_sum_total = mtm.aggregate(Sum('mtm_proportional'))['mtm_proportional__sum']

                    date_list = list(module.models.ProportionalMtm.objects.filter(**mtm_queryset if level_key != "" else {})
                                     .values('valuation_date').distinct().order_by('-valuation_date')[:15])
            resp_sum = resp_sum_total / 1e6 if resp_sum_total is not None else None
            try:
                resp_currency = getattr(module.models, 'Fund').objects. \
                    get(fund_name=general_fund.fund_name).ccy
            except ObjectDoesNotExist as e:
                resp_currency = ''
            # resp_currency = VrmHedge1CopyCopy.objects.filter(
            #     client=request.POST['client'],
            #     **level_filter2,
            # ).first().premium_currency


            if ignore_proportional_mtm:
                _add_new_trades(request, resp_data, resp_currency, mtm, valuation_date, query_set=level_filter2)


    except ObjectDoesNotExist as exception:
        print(traceback.format_exc())
    except Exception as exception:
        print(traceback.format_exc())
    finally:
        resp = {
            'data': resp_data,
            'mtm_sum': resp_sum,
            'currency': resp_currency,
            'date_list': [item['valuation_date'] for item in date_list],
            'valuation_date': valuation_date
        }
        return JsonResponse(resp)


@login_required(login_url='login')
@user_is_allowed
def fundlevel_recalculate_analytics_mtm_table(request):
    if request.is_ajax() is False or request.method != 'POST':
        return HttpResponseBadRequest()
    try:
        resp_data = []
        resp_sum = 0
        resp_currency = ''
        associated_trade_model = None
        associated_type = None
        try:
            client = VRMClient.objects.get(name=request.POST['client'])
        except VRMClient.DoesNotExist:
            raise Exception('Unable to find the client to process!')
        level = request.POST.get('level')
        level_value = request.POST.get('level_value')
        if None in [level, level_value]:
            raise Exception('Unable to find the valuation level to process!')
        try:
            module = __import__(client.name.lower())
        except Exception as e:
            try:
                module = __import__(client.label.lower())
            except Exception as e:
                raise Exception('Unable to find the client to process!')

        # TODO repeated code need to modularise it.
        general_client = Clients.objects.get(client_name=client)
        general_fund = Funds.objects.get(fund_name=request.POST['fund'], client=general_client.pk)
        level_filter = None
        level_filter2 = None
        all_condition = False
        if level == 'fund':
            level_filter = {'trade__fund': level_value}
            level_filter2 = {'fund': level_value}
            all_condition = True
        elif level == 'deal':
            try:
                trade_ids = []
                associated_type = {'deal_id': module.models.Deal.objects.get(deal_name=level_value).deal_id}
                if client.investor:
                    if module.models.AssetTrade.objects.filter(deal__deal_name=level_value, fund=general_fund).exists():
                        trade_ids = list(module.models.AssetTrade.objects.filter(
                            deal__deal_name=level_value, fund=general_fund).exclude(
                            deal_id__isnull=True).values_list('trade_id', flat=True))
                else:
                    if module.models.Dealtrade.objects.filter(deal__deal_name=level_value).exists():
                        trade_ids = list(module.models.Dealtrade.objects.filter(
                            deal__deal_name=level_value).values_list('trade_id', flat=True))
                if len(trade_ids) > 0:
                    level_filter = {'trade__trade_id__in': trade_ids}
                    level_filter2 = {'trade_id__in': trade_ids}
                try:
                    associated_trade_model = module.models.Dealtrade
                except:
                    associated_trade_model = None
                all_condition = True
            except ObjectDoesNotExist as exception:
                print(traceback.format_exc())
            except Exception as exception:
                print(traceback.format_exc())
        elif level == 'entity':
            try:
                if module.models.Entity.objects.get(entity_name=level_value).exists():
                    trade_ids = []
                    associated_type = {'entity_id': module.models.Entity.objects.get(entity_name=level_value).entity_id}
                    if client.investor:
                        if module.models.AssetTrade.objects.filter(entity__entity_name=level_value,
                                                                   fund=general_fund).exists():
                            trade_ids = list(module.models.AssetTrade.objects.filter(
                                        entity__entity_name=level_value, fund=general_fund).exclude(
                                        entity_id__isnull=True).values_list('trade_id', flat=True))
                    else:
                        if module.models.Entitytrade.objects.filter(entity__entity_name=level_value).exists():
                            trade_ids = list(module.models.Entitytrade.objects.filter(
                                entity__entity_name=level_value).values_list('trade_id', flat=True))
                    if len(trade_ids) > 0:
                        level_filter = {'trade__trade_id__in': trade_ids}
                        level_filter2 = {'trade_id__in': trade_ids}
                    try:
                        associated_trade_model = module.models.Entitytrade
                    except:
                        associated_trade_model = None
                    all_condition = True
            except ObjectDoesNotExist as exception:
                print(traceback.format_exc())
            except Exception as exception:
                print(traceback.format_exc())
        elif level == 'investor':
            try:
                associated_type = {'investor_id': module.models.Investor.objects.get(investor_name=level_value).investor_id}
                if module.models.AssetTrade.objects.filter(investor__investor_name=level_value,fund=general_fund).exists():
                    trade_ids = list(module.models.AssetTrade.objects.filter(
                        investor__investor_name=level_value,fund=general_fund).exclude(
                                      investor_id__isnull=True).values_list('trade_id', flat=True))
                    if len(trade_ids) > 0:
                        level_filter = {'trade__trade_id__in': trade_ids}
                        level_filter2 = {'trade_id__in': trade_ids}
                    try:
                        associated_trade_model = module.models.AssetTrade
                    except:
                        associated_trade_model = None
                    all_condition = True
            except ObjectDoesNotExist as exception:
                print(traceback.format_exc())
            except Exception as exception:
                print(traceback.format_exc())

        # TODO repeated code need to modularise it.
        if level_filter is not None and level_filter2 is not None and all_condition:
            mtm = None
            valuation_date = None
            # fetch mtm based trades
            valuation_date, mtm = _get_mtm_list(request, query_set=level_filter)
            if valuation_date is None:
                return HttpResponseBadRequest()
            if mtm:
                spot_ref_data = pd.DataFrame(json.loads(request.POST['spot_ref_data'] or '[]'))
                mtm_data = pd.DataFrame(list(mtm) or '[]')

                mtm_data['new_spot_ref'] = np.nan
                for index, row in spot_ref_data.iterrows():
                    match = mtm_data['trade__underlying'] == row['trade__underlying']
                    if match.any():
                        mtm_data.loc[match, 'new_spot_ref'] = \
                            float(row['average_spot_rate'])

                for index, row in mtm_data.iterrows():
                    fwd_points = row['outright_forward'] - row['spot_ref']
                    new_outright_forward = fwd_points + row['new_spot_ref']

                    ccy1 = row['trade__underlying'][:3]
                    ccy2 = row['trade__underlying'][3:]

                    new_mtm = 0
                    if row['trade__style'] in ["Spot", "Put", "Call", "Forward", "Swap"]:
                        if row['trade__notional_currency'] == ccy1:
                            if row['mtm_currency'] == ccy2:
                                if row['trade__direction'].lower() == "buy":
                                    new_mtm = row['trade__notional_amounts'] * (
                                                new_outright_forward - row['trade__strike'])
                                else:
                                    new_mtm = -row['trade__notional_amounts'] * (
                                                new_outright_forward - row['trade__strike'])
                            else:
                                if row['trade__direction'].lower() == "buy":
                                    new_mtm = row['trade__notional_amounts'] * (
                                                new_outright_forward - row['trade__strike']) / \
                                              row['new_spot_ref']
                                else:
                                    new_mtm = -row['trade__notional_amounts'] * (
                                                new_outright_forward - row['trade__strike']) / \
                                              row['new_spot_ref']
                        else:
                            if row['mtm_currency'] == ccy2:
                                if row['trade__direction'].lower() == "buy":
                                    new_mtm = row['trade__notional_amounts'] * \
                                              (1 / new_outright_forward - 1 / row['trade__strike']) * \
                                              row['new_spot_ref']
                                else:
                                    new_mtm = -row['trade__notional_amounts'] * \
                                              (1 / new_outright_forward - 1 / row['trade__strike']) * \
                                              row['new_spot_ref']
                            else:
                                if not new_outright_forward == 0:
                                    if row['trade__direction'].lower() == "buy":
                                        new_mtm = row['trade__notional_amounts'] * \
                                                  (1 / new_outright_forward - 1 / row['trade__strike'])
                                    else:
                                        new_mtm = -row['trade__notional_amounts'] * \
                                                  (1 / new_outright_forward - 1 / row['trade__strike'])
                                else:
                                    new_mtm = 0

                    row['spot_ref'] = row['new_spot_ref']
                    row['outright_forward'] = new_outright_forward
                    if associated_trade_model:
                        try:
                            # believe it is 100 % or deal trade schema table would be updated with percentage
                            percentage = 1
                            try:
                                percentage =  associated_trade_model.objects.get(trade=row['trade__trade_id'],
                                                                                         **associated_type).percentage
                            except ObjectDoesNotExist as e:
                                percentage = 1
                            new_mtm = new_mtm*percentage
                        except:
                            print(traceback.format_exc())
                    row['mtm'] = new_mtm
                    resp_sum += new_mtm
                    resp_data += [row.to_dict()]
                if resp_sum != 0:
                    resp_sum = resp_sum / 1e6
                resp_currency = resp_data[0]['trade__premium_currency'] if len(resp_data) > 0 else None
                _add_new_trades(request, resp_data, resp_currency, mtm, valuation_date, query_set=level_filter)
    except ObjectDoesNotExist as exception:
        print(traceback.format_exc())
    except Exception as exception:
        print(traceback.format_exc())
    finally:
        resp = {
            'data': resp_data,
            'mtm_sum': resp_sum,
            'currency': resp_currency
        }
        return JsonResponse(resp)


@login_required(login_url='login')
@user_is_allowed
def fundlevel_analytics_spotrates_table(request):
    if request.is_ajax() is False or request.method != 'POST':
        return HttpResponseBadRequest()
    try:
        resp_data = []

        try:
            client = VRMClient.objects.get(name=request.POST['client'])
        except VRMClient.DoesNotExist:
            raise Exception('Unable to find the client to process!')
        level = request.POST.get('level')
        level_value = request.POST.get('level_value')
        if None in [level, level_value]:
            raise Exception('Unable to find the valuation level to process!')
        try:
            module = __import__(client.name.lower())
        except Exception as e:
            try:
                module = __import__(client.label.lower())
            except Exception as e:
                raise Exception('Unable to find the client to process!')
        general_client = Clients.objects.get(client_name=client)
        general_fund = Funds.objects.get(fund_name=request.POST['fund'], client=general_client.pk)
        level_filter = None
        propotional_filter = None
        if level == 'fund':
            level_filter = {'trade__fund': level_value}
            propotional_filter = {'fund_id' : general_fund.pk}
        elif level == 'deal':
            try:
                trade_ids = []
                if client.investor:
                    if module.models.AssetTrade.objects.filter(deal__deal_name=level_value, fund=general_fund).exists():
                        trade_ids = list(module.models.AssetTrade.objects.filter(
                            deal__deal_name=level_value, fund=general_fund).exclude(
                            deal_id__isnull=True).values_list('trade_id', flat=True))
                else:
                    if module.models.Dealtrade.objects.filter(deal__deal_name=level_value).exists():
                        trade_ids = list(module.models.Dealtrade.objects.filter(
                            deal__deal_name=level_value).values_list('trade_id', flat=True))

                level_filter = {'trade__trade_id__in': trade_ids}
                propotional_filter = {'trade_id__in': trade_ids}
            except ObjectDoesNotExist as exception:
                print(traceback.format_exc())
            except Exception as exception:
                print(traceback.format_exc())
        elif level == 'entity':
            try:
                if module.models.Entity.objects.filter(entity_name=level_value).exists():

                    trade_ids = []
                    if client.investor:
                        if module.models.AssetTrade.objects.filter(entity__entity_name=level_value,
                                                                fund=general_fund).exists():
                            trade_ids = list(module.models.AssetTrade.objects.filter(
                                                        entity__entity_name=level_value, fund=general_fund).exclude(
                                                        entity_id__isnull=True).values('trade_id', flat=True))
                    else:
                        if module.models.Entitytrade.objects.filter(entity__entity_name=level_value).exists():
                            trade_ids = list(module.models.Entitytrade.objects.filter(
                                                            entity__entity_name=level_value).values_list('trade_id',
                                                                                                         flat=True))

                    level_filter = {'trade__trade_id__in': trade_ids}
                    propotional_filter = {'fund_id' : general_fund.pk, 'entity_id': module.models.Entitytrade.objects.filter(
                        entity__entity_name=level_value)[0]['entity_id']}
            except ObjectDoesNotExist as exception:
                print(traceback.format_exc())
            except Exception as exception:
                print(traceback.format_exc())
        elif level == 'investor':
            try:
                if module.models.Investor.objects.filter(investor_name=level_value).exists():
                    if module.models.AssetTrade.objects.filter(investor__investor_name=level_value,
                                                               fund=general_fund).exists():
                        trade_ids = list(module.models.AssetTrade.objects.filter(
                            investor__investor_name=level_value, fund=general_fund).exclude(
                            investor_id__isnull=True).values_list('trade_id', flat=True))
                        level_filter = {'trade__trade_id__in': trade_ids}
            except ObjectDoesNotExist as exception:
                print(traceback.format_exc())
            except Exception as exception:
                print(traceback.format_exc())

        if level_filter is not None:
            if request.POST['valuation_date'] == '':
                valuation_date = VrmMtm1.objects.filter(trade__client=request.POST['client'],
                                                        trade__cancellation__iexact='FALSE',
                                                        trade__dummy__iexact='FALSE',
                                                        trade__asset_class__iexact='FX',
                                                        **level_filter,
                                                        ).aggregate(Max('valuation_date'))['valuation_date__max']
            else:
                valuation_date = datetime.datetime.strptime(request.POST['valuation_date'], "%Y-%m-%d")
            if valuation_date is None or valuation_date == date.today():
                # is it possible to reach here. may be if no MTM exist for the client.
                if VRMContentTab.objects.filter(
                        label='proportional_mtm',
                        client__name=client ).exists():
                    valuation_date = getattr(module.models, 'ProportionalMtm').objects.filter(
                            **propotional_filter).aggregate(
                        Max('valuation_date'))['valuation_date__max']
                else:
                    valuation_date = VrmMtm1.objects.filter(**level_filter) \
                        .aggregate(Max('valuation_date'))['valuation_date__max']
                if valuation_date is None:
                    valuation_date = get_last_business_day()

            resp_data = []
            if valuation_date:
                if VRMContentTab.objects.filter(
                    label='proportional_mtm',
                    client__name=client
                ).exists():
                    data = list(getattr(module.models,'ProportionalTrade').objects.filter(
                        value_date__gte=valuation_date,
                        cancellation__iexact='FALSE',
                        **propotional_filter,
                    ).values(
                        'underlying',
                    ).annotate(average_spot_rate=Avg('spot_ref')))
                    resp_data = []
                    for item in data:
                        new_dict = {}
                        for res, value in item.items():
                            if res == 'underlying':
                                new_dict['trade__underlying'] = value
                            else:
                                new_dict[res] = value
                        resp_data.append(new_dict)


                else:
                    resp_data = list(VrmMtm1.objects.filter(
                        trade__client=client,
                        trade__value_date__gte=valuation_date,
                        trade__cancellation__iexact='FALSE',
                        trade__dummy__iexact='FALSE',
                        trade__asset_class__iexact='FX',
                        valuation_date=valuation_date,
                        **level_filter,
                    ).values(
                        'trade__underlying',
                    ).annotate(average_spot_rate=Avg('spot_ref')))

    except ObjectDoesNotExist as exception:
        print(traceback.format_exc())
    except Exception as exception:
        print(traceback.format_exc())
    finally:
        resp = {
            'data': resp_data,
        }
        return JsonResponse(resp)


@login_required(login_url='login')
@user_is_allowed
def save_indirect_deal_data(request):
    msg =' Successfully added the '
    errorcode = 1
    try:
        client = request.POST["client"]
        data_ws = json.loads(request.POST["current_dealvalues"])
        module = None
        try:
            client = VRMClient.objects.get(name=client)
            module = __import__(client.name.lower())

        except Exception as e:
            try:
                module = __import__(client.label.lower())
            except Exception as e:
                raise Exception('Unable to find the client to process!')
        for item,values in data_ws.items():
            if module:
                IndirectDeal = getattr(module.models, 'IndirectDeal')
                Deal = getattr(module.models, 'Deal')
                deal_id = Deal.objects.get(deal_name=values['deal__deal_name']).deal_id
                currency_id = Ccys.objects.get(identifier=values['indirect_local_currency__identifier'])
                try:
                    if currency_id:
                        if IndirectDeal.objects.filter(id=int(values['id']), indirect_local_currency= currency_id, deal_id=deal_id).exists():
                            deal_trade =  IndirectDeal.objects.get(id=int(values['id']), indirect_local_currency= currency_id, deal_id=deal_id)
                            deal_trade.percentage = float(values['percentage'])/100
                            # todo need to update date
                            deal_trade.save()
                        else:
                            if not IndirectDeal.objects.get_or_create(indirect_local_currency=currency_id,
                                                                             deal_id=deal_id).exists():
                                dt, created = IndirectDeal.objects.get_or_create(indirect_local_currency=currency_id,
                                                                                 deal_id=deal_id)
                                if created:
                                    dt.percentage = float(values['percentage'])/100
                                    dt.save()
                except  Exception as exception:
                    print(traceback.format_exc())


    except ObjectDoesNotExist as exception:
        msg = 'Unable to add the '
        errorcode = 0
        print(traceback.format_exc())
    except Exception as exception:
        msg = 'Unable to add the '
        errorcode = 0
        print(traceback.format_exc())
    finally:
        resp = {
            'msg': msg,
            'errorcode': errorcode

        }
        return JsonResponse(resp)


@login_required(login_url='login')
@user_is_allowed
def fetch_reports_list(request):
    if request.is_ajax() is False or request.method != 'POST':
        return HttpResponseBadRequest()
    report_list = []
    try:
        is_staff = request.POST['is_staff'] == 'True'
        client = VRMClient.objects.get(name=request.POST['client'])
        module = module_loader(client)
        if None in [module]:
            raise Exception('Unable to find the client to process!')
        report = module.models.Report
        kwargs = {'enabled_internal': 1} if is_staff else {'enabled_client': 1}
        if report.objects.all().exists():
            report_list = list(report.objects.all().filter(**kwargs).values('id', 'report_label'))
    except ObjectDoesNotExist as exception:
        print(traceback.format_exc())
    except Exception as exception:
        print(traceback.format_exc())
    finally:
        resp = {
            'reports': report_list,
        }
        return JsonResponse(resp)


@login_required(login_url='login')
@user_is_allowed
def prepare_report(request):
    if request.is_ajax() is False or request.method != 'POST':
        return HttpResponseBadRequest()
    try:
        file_name = None
        nrows = 0
        error_message = 'Error: Unexpected exception trying to generate the file.'

        report_id = request.POST['report_id']
        report_name = request.POST['report_name']
        export_type = request.POST['export_format']
        client = VRMClient.objects.get(name=request.POST['client'])
        if request.POST['start_date'] == '' and request.POST['start_date'] is None:
            raise Exception('Start date needs to be specified')
        if request.POST['end_date'] == '' and request.POST['end_date'] is None:
            raise Exception('End date needs to be specified')

        if 'position' in report_name.lower():
            valuation_date = datetime.datetime.strptime(request.POST['valuation_date'], '%d/%m/%Y')
            start_date = end_date = valuation_date
            selection_kwarg = {'valuation_date__range': (start_date, end_date)}
            dates_select = 'valuation_date'
        elif 'performance' in report_name.lower():
            valuation_date = datetime.datetime.strptime(request.POST['valuation_date'], '%d/%m/%Y')
            start_date = end_date = valuation_date
            selection_kwarg = {'end_valuation_date': valuation_date}
            dates_select = 'end_valuation_date'
        else:
            start_date = datetime.datetime.strptime(request.POST['start_date'], '%d/%m/%Y')
            end_date = datetime.datetime.strptime(request.POST['end_date'], '%d/%m/%Y')
            selection_kwarg = {'valuation_date__range': (start_date, end_date)}
            dates_select = 'valuation_date'
        try:
            module = __import__(client.label.lower())
            Report = module.models.Report
        except Exception as e:
            raise Exception('Unable to find the client to process!')

        if Report.objects.filter(id=report_id).exists():
            report_model = Report.objects.filter(id=report_id).values('model_name').first()['model_name']
            report_table = getattr(module.models, report_model)

            if report_table.objects.filter(**selection_kwarg).exists():
                report_data_all = report_table.objects.filter(**selection_kwarg)
                valuation_dates = np.unique([x[dates_select] for x in list(report_data_all.values(dates_select))])
                report_data = None
                for valuation_date in valuation_dates:
                    temp_kwargs1 = {dates_select: valuation_date}
                    last_timestamp = report_table.objects.filter(**temp_kwargs1).latest('update_timestamp').update_timestamp
                    temp_kwargs2 = {dates_select: valuation_date,
                                    'update_timestamp': last_timestamp}
                    report_data_temp = report_table.objects.filter(**temp_kwargs2)
                    if report_data is None:
                        report_data = report_data_temp
                    else:
                        report_data = report_data | report_data_temp

                report_submodule = getattr(
                    __import__(client.label.lower() + '.reports.reports').reports.reports,
                    report_model)
                report = report_submodule(report_data)
                report.generate(start_date, end_date, export_type)
                file_name = report.file_name_wb
                nrows = report_data.count()
            else:
                error_message = 'No data available.'
    except ObjectDoesNotExist as exception:
        print(traceback.format_exc())
    except Exception as exception:
        print(traceback.format_exc())
    finally:
        resp = {
            'file_name': file_name,
            'error_message': error_message,
            'nrows': nrows,
        }
        return JsonResponse(resp)


@login_required(login_url='login')
@user_is_allowed
def ajax_averagedweightnotional(request):
    if request.is_ajax() is False or request.method != 'POST':
        return HttpResponseBadRequest()

    fund = request.POST.get('fund')
    counterparty = request.POST.get('counterparty')
    client = request.POST.get('client')
    data_date = []
    data_notional_amt = []
    data_weight_strike = []
    res_data = {}
    avg_notion = None
    module = None
    try:
        client = VRMClient.objects.get(name=client)
        general_client = Clients.objects.get(client_name=client)
        general_fund = Funds.objects.get(fund_name=fund, client=general_client.pk)
        module = module_loader(client)
    except Exception as e:
        raise Exception('Unable to find the client to process!')
    if None in [module, fund, counterparty, client]:
        return HttpResponseBadRequest()

    try:
        if counterparty == 'all':
            DailyNotionalStrike = module.models.DailyNotionalStrike
            avg_notion = DailyNotionalStrike.objects.filter(fund_id=general_fund.id, cp_id=0).values('valuation_date',
                                                            'notional_amount',
                                                            'weighted_strike')
        else:
            DailyNotionalStrike = module.models.DailyNotionalStrike
            cp_id = Counterparties.objects.get(counterparty_name=counterparty)
            avg_notion = DailyNotionalStrike.objects.filter(fund_id=general_fund.id,cp_id=cp_id.id).values(
                                                            'valuation_date',
                                                            'notional_amount',
                                                            'weighted_strike')

        for item in avg_notion:
            data_date.append(str(item['valuation_date']))
            data_notional_amt.append([str(item['valuation_date']),item['notional_amount']])
            data_weight_strike.append([str(item['valuation_date']),item['weighted_strike']])
        res_data['start_date'] = data_date[0]
        res_data['end_date'] = data_date[-1]
        res_data['notional_amount'] = data_notional_amt
        res_data['weighted_strike'] = data_weight_strike
        return JsonResponse(res_data)
    except ObjectDoesNotExist as exception:
        print(traceback.format_exc())
        return JsonResponse({
            'start_date': str(datetime.date.today()),
            'end_date': str(datetime.date.today()),
            'weighted_strike': [],
            'notional_amount': [],
        })
    except Exception as exception:
        print(traceback.format_exc())
        return JsonResponse({
            'start_date': str(datetime.date.today()),
            'end_date': str(datetime.date.today()),
            'weighted_strike': [],
            'notional_amount': [],
        })


@login_required(login_url='login')
@user_is_allowed
def ajax_netexposure(request):
    if request.is_ajax() is False or request.method != 'POST':
        return HttpResponseBadRequest()

    counterparty = request.POST.get('counterparty')
    fund = request.POST.get('fund')
    client = request.POST.get('client')
    netexp_list = list()
    if None in [counterparty, client, fund]:
        return HttpResponseBadRequest()

    try:
        general_client = Clients.objects.get(client_name=client)
        general_fund = Funds.objects.get(fund_name=fund)

        currency = None
        if counterparty == 'all':
            netexp_list = list(
                    VrmDailyNetExposure.objects.filter(
                        client_id=general_client.pk,
                        fund_id=general_fund.pk,
                    ).exclude(cp_id=0).values('cp_id','net_exposure'))

        else:
            cp = Counterparties.objects.get(counterparty_name=counterparty, cancellation=0)
            netexp_list = list(
                VrmDailyNetExposure.objects.filter(
                    client_id=general_client.pk,
                    fund_id=general_fund.pk,
                    cp_id=cp.pk
                ).values('cp_id', 'net_exposure'))
        data = []
        counterparty = []
        individual_mtm = []
        threshold = []
        for item in netexp_list:
            data.append(item['net_exposure'])
            counterparty.append(Counterparties.objects.get(id=item['cp_id'], cancellation=0).counterparty_name)
            individual_mtm.append(
                round(VrmAggregateMtm.objects.filter(
                    client_id=general_client.pk,
                    fund_id=general_fund.pk,
                    cp_id=Counterparties.objects.get(id=item['cp_id'], cancellation=0).id
                ).latest('valuation_date').agg_mtm / 1000000, 2))
            #TODO incorrect need to change to LegalCounterparty
            # cp_obj = VrmCounterparty.objects.filter(**individual_set,
                                                                                # cp=Counterparties.
                                                                                # objects.get(
                                                                                #     id=item['cp_id'])
                                                                                # )
            cp_obj = VrmCounterparty.objects.filter(fund=general_fund.fund_name,
                                                    client=general_client.client_name,
                                                    counterparty=Counterparties.objects.get(id=item['cp_id'], cancellation=0).counterparty_name)
            if len(cp_obj) > 0:
                if cp_obj[0].threshold and cp_obj[0].mta:
                    threshold.append(round((cp_obj[0].threshold + cp_obj[0].mta) / 1000000, 2))
                else:
                    if cp_obj[0].threshold:
                        threshold.append(round(cp_obj[0].threshold / 1000000, 2))
                    elif cp_obj[0].mta:
                        threshold.append(round(cp_obj[0].mta / 1000000, 2))
                    else:
                        threshold.append(None)
            else:
                threshold.append(None)
        res_data = {
            'netexp_list': data,
            'counterparty': counterparty,
            'individual_mtm' : individual_mtm,
            'threshold':threshold
        }

    except Exception as exception:
        print(traceback.format_exc())
        res_data = {
            'netexp_list': [],
            'counterparty': [],
            'individual_mtm': [],
            'threshold': []
        }

    return JsonResponse(res_data, content_type='application/json')


@login_required(login_url='login')
@user_is_allowed
def ajax_realisedpnl(request):
    if request.is_ajax() is False or request.method != 'POST':
        return HttpResponseBadRequest()

    fund = request.POST.get('fund')
    client = request.POST.get('client')
    fund = request.POST.get('fund')
    try:
        user = User.objects.get(username=request.user.username)
        user.profile.recent_fund = fund
        user.save()
    except:
        raise Exception('Unable to find the fund to process!')
    try:
        client = VRMClient.objects.get(name=request.POST['client'])
    except VRMClient.DoesNotExist:
        raise Exception('Unable to find the client to process!')
    level = request.POST.get('level')
    level_value = request.POST.get('level_value')
    module = module_loader(client)
    if None in [module, level]:
        raise Exception('Unable to find the valuation level to process!')
    general_client = Clients.objects.get(client_name=client)
    general_fund = Funds.objects.get(fund_name=request.POST['fund'], client=general_client.pk)
    # NO need to worry about fund _id only cvc sis using this at the moment
    level_filter = {'fund_id': general_fund.id}
    if level and level != 'Fund' and level_value:
        dynamic_field = {level.lower() + '_name': level_value}
        try:
            table_access = getattr(module.models, level.title()).objects.get(**dynamic_field)
            level_filter[level.lower() + '_id'] = getattr(table_access, level.lower() + '_id')
        except ObjectDoesNotExist:
            print(traceback.format_exc())
    else:
        level_filter['entity_id__isnull'] = True
        level_filter['deal_id__isnull'] = True
    res_data = {}
    try:
        realisedpnl = module.models.Realisedpnl
        if realisedpnl.objects.all().count() > 0 :
            data_list = realisedpnl.objects.filter(**level_filter).order_by('value_date').values()
            data_realised_pnl = []
            weighted_strike = []
            ccy_amount = 0.0
            count = 0
            for item in data_list:
                current_date = str(item['value_date'])
                data_realised_pnl.append([str(item['value_date']), round(item['ccy_amount'] , 2)])
                ccy_amount = ccy_amount + item['ccy_amount']
                weighted_strike.append([str(item['value_date']), round(ccy_amount, 2)])
                if count == 0:
                    res_data['start_date'] = current_date
                    res_data['ccy'] = item['ccy']
                if count == len(data_list)-1:
                    res_data['end_date'] = current_date
                count = count +1

            if len(data_list) < 1:
                res_data['start_date'] = str(datetime.date.today())
                res_data['end_date'] = str(datetime.date.today())
                res_data['ccy'] = ''

            res_data['data_realised_pnl'] = data_realised_pnl
            res_data['weighted_strike'] = weighted_strike
    except Exception as exception:
        print(traceback.format_exc())
    finally:
        if len(res_data) > 1:
            return JsonResponse(res_data)
        else:
            return JsonResponse({
                'start_date': str(datetime.date.today()),
                'end_date': str(datetime.date.today()),
                 'data_realised_pnl': [],
                'weighted_strike': [],
                'ccy': ''
            })


class ClientEntityView(LoginRequiredMixin, View):
    @user_is_allowed
    def get(self, request, *args, **kwargs):
        try:
            client = VRMClient.objects.get(name=request.GET.get('client'))
        except ObjectDoesNotExist:
            return HttpResponseBadRequest()

        module = module_loader(client)

        try:
            Entity = getattr(module.models, 'Entity')
        except:
            return HttpResponseBadRequest()

        entities = Entity.objects.all()

        return JsonResponse({
            "entities": [{
                "id": entity.entity_id, # This is named randomly
                "entity_name": entity.entity_name,
                "cancellation": entity.cancellation
            } for entity in entities]
        })

    @user_is_allowed
    def post(self, request, *args, **kwargs):
        try:
            client = VRMClient.objects.get(name=request.POST.get("client"))
        except ObjectDoesNotExist as e:
            return HttpResponseBadRequest()

        module = module_loader(client)

        try:
            Entity = getattr(module.models, 'Entity')
        except:
            return HttpResponseBadRequest()

        id = request.POST.get("id")
        entity_name = request.POST.get("entity_name")
        cancellation = request.POST.get("cancellation")

        if id:
            # Updating
            try:
                entity = Entity.objects.get(entity_id=id)
            except ObjectDoesNotExist:
                HttpResponseBadRequest()

            entity.entity_name = entity_name
            entity.cancellation = cancellation

            try:
                # TODO: This does nothing
                entity.full_clean()
            except ValidationError as e:
                raise e

            entity.save()
        else:
            # Creating
            entity = Entity(
                entity_name=entity_name,
                cancellation=cancellation
            )

            try:
                # TODO: This does nothing
                entity.full_clean()
            except ValidationError as e:
                raise e

            entity.save()

        entities = Entity.objects.all()

        return JsonResponse({
            "entities": [{
                "id": entity.entity_id, # This is named randomly
                "entity_name": entity.entity_name,
                "cancellation": entity.cancellation
            } for entity in entities]
        })


class ClientCashflowsView(LoginRequiredMixin, View):
    @user_is_allowed
    def get(self, request, *args, **kwargs):
        try:
            client = VRMClient.objects.get(name=request.GET.get('client'))
        except ObjectDoesNotExist:
            return HttpResponseBadRequest()

        module = module_loader(client)

        try:
            Cashflows = getattr(module.models, 'Cashflows')
            Deal = getattr(module.models, 'Deal')
            Fund = getattr(module.models, 'Fund')
            FundAssociation = getattr(module.models, 'FundAssociation')
        except:
            return HttpResponseBadRequest()

        cashflows = Cashflows.objects.filter(fund_id=Fund.objects.get(fund_name=request.GET.get('fund')).fund_id, cancellation=0)
        deals = Deal.objects.filter(cancellation=0)
        funds = Fund.objects.all()
        fund_association = FundAssociation.objects.all()
        fund_items = {}
        for fund_deal in fund_association:
            if fund_deal.fund_id and fund_deal.deal_id:
                if Fund.objects.filter(pk=fund_deal.fund_id).exists():
                    if Fund.objects.get(pk=fund_deal.fund_id).fund_name in fund_items.keys():
                        fund_items[Fund.objects.get(pk=fund_deal.fund_id).fund_name].append({fund_deal.deal_id
                                                                                             :Deal.objects.get(pk=fund_deal.deal_id).deal_name})
                    else:
                        fund_items[Fund.objects.get(pk=fund_deal.fund_id).fund_name] = []
                        fund_items[Fund.objects.get(pk=fund_deal.fund_id).fund_name].append({fund_deal.deal_id:
                            Deal.objects.get(pk=fund_deal.deal_id).deal_name})


        return JsonResponse({
            "deal_name": [{
                index: deal
            } for index,deal in enumerate(deals.values_list('deal_name',flat=True))],
            "fund_name": [{
                index: deal
            } for index, deal in enumerate(funds.values_list('fund_name', flat=True))],
            "fund_association": [fund_items],
            "cashflows": [{
                "id": cf.transaction_id, # This is named randomly
                "deal_name": Deal.objects.get(pk=cf.deal_id).deal_name,
                "fund_name": Fund.objects.get(pk=cf.fund_id).fund_name,
                "value_date":str(cf.value_date),
                "cf_type":cf.cf_type,
                "cashflows":cf.cashflows or 0,
                "fx_rate":cf.fx_rate or 0,
                "reset_rate": cf.reset_rate or 0,
                "cancellation": int(cf.cancellation)
            } for cf in cashflows]
        })

    @user_is_allowed
    def post(self, request, *args, **kwargs):
        try:
            client = VRMClient.objects.get(name=request.POST.get("client"))
        except ObjectDoesNotExist as e:
            return HttpResponseBadRequest()

        module = module_loader(client)

        try:
            Cashflows = getattr(module.models, 'Cashflows')
            Deal = getattr(module.models, 'Deal')
            Fund = getattr(module.models, 'Fund')
            FundAssociation = getattr(module.models, 'FundAssociation')
        except:
            return HttpResponseBadRequest()

        id = request.POST.get("id")
        deal_name = request.POST.get("deal_name")
        fund_name = request.POST.get("fund_name")
        value_date = request.POST.get("value_date")
        cf_type = request.POST.get("cf_type")
        cashflows = request.POST.get("cashflows")
        fx_rate = request.POST.get("fx_rate")
        reset_rate = request.POST.get("reset_rate")
        cancellation = request.POST.get("cancellation")

        if id:
            # Updating
            try:
                cashflow = Cashflows.objects.get(transaction_id=id)
                old_value = list(Cashflows.objects.filter(transaction_id=id).values())
            except ObjectDoesNotExist:
                HttpResponseBadRequest()
            cashflow.deal = Deal.objects.get(deal_name=deal_name)
            cashflow.fund = Fund.objects.get(fund_name=fund_name)
            cashflow.value_date = datetime.date(int(value_date.split('-')[0]),
                                                int(value_date.split('-')[1]),
                                                int(value_date.split('-')[2]))
            cashflow.cf_type = cf_type
            cashflow.cashflows = float(cashflows)
            cashflow.fx_rate = float(fx_rate)
            cashflow.reset_rate = float(reset_rate)
            cashflow.cancellation = cancellation

            try:
                # TODO: This does nothing
                cashflow.full_clean()
            except ValidationError as e:
                raise e

            cashflow.save()
            sync_fundassociation(client)
            # update audit log.

            try:
                audit_update = LogCashflows.objects.create(user=request.user,
                                            action=Action.objects.get(id=2),
                                            client_name=client.name,
                                            cashflows=cashflow.pk,
                                            data=list(Cashflows.objects.filter(transaction_id=id).values()),
                                            old_data=old_value,
                                            comment="Update"
                                            )
                audit_update.save()
            except Exception as e:
                logger.exception("audit log failed {}".format(e))

        else:
            # Creating
            cashflow = Cashflows(
                deal = Deal.objects.get(deal_name=deal_name),
                fund = Fund.objects.get(fund_name=fund_name),
                value_date = datetime.date(int(value_date.split('-')[0]),
                                           int(value_date.split('-')[1]),
                                           int(value_date.split('-')[2])),
                cf_type = cf_type,
                cashflows = float(cashflows),
                fx_rate = float(fx_rate),
                reset_rate = float(reset_rate),
                cancellation = cancellation
            )

            try:
                # TODO: This does nothing
                cashflow.full_clean()
            except ValidationError as e:
                raise e

                cashflow.save()
                sync_fundassociation(client)

        cashflows = Cashflows.objects.filter(fund_id=Fund.objects.get(fund_name=request.GET.get('fund')).fund_id, cancellation=0)
        deals = Deal.objects.filter(cancellation=0)
        funds = Fund.objects.all()
        fund_association = FundAssociation.objects.all()
        fund_items = {}
        for fund_deal in fund_association:
            if fund_deal.fund_id and fund_deal.deal_id:
                if Fund.objects.filter(pk=fund_deal.fund_id).exists():
                    if Fund.objects.get(pk=fund_deal.fund_id).fund_name in fund_items.keys():
                        fund_items[Fund.objects.get(pk=fund_deal.fund_id).fund_name].append({fund_deal.deal_id
                                                                                             : Deal.objects.get(
                                pk=fund_deal.deal_id).deal_name})
                    else:
                        fund_items[Fund.objects.get(pk=fund_deal.fund_id).fund_name] = []
                        fund_items[Fund.objects.get(pk=fund_deal.fund_id).fund_name].append({fund_deal.deal_id:
                                                                                                 Deal.objects.get(
                                                                                                     pk=fund_deal.deal_id).deal_name})

        return JsonResponse({
            "deal_name": [{
                index: deal
            } for index, deal in enumerate(deals.values_list('deal_name', flat=True))],
            "fund_name": [{
                index: deal
            } for index, deal in enumerate(funds.values_list('fund_name', flat=True))],
            "fund_association": [fund_items],
            "cashflows": [{
                "id": cf.transaction_id,  # This is named randomly
                "deal_name": Deal.objects.get(pk=cf.deal_id).deal_name,
                "fund_name": Fund.objects.get(pk=cf.fund_id).fund_name,
                "value_date": str(cf.value_date),
                "cf_type": cf.cf_type,
                "cashflows": cf.cashflows or 0,
                "fx_rate": cf.fx_rate or 0,
                "reset_rate": cf.reset_rate or 0,
                "cancellation": int(cf.cancellation)
            } for cf in cashflows]
        })


class ClientDealValuationView(LoginRequiredMixin, View):
    @user_is_allowed
    def get(self, request, *args, **kwargs):
        try:
            client = VRMClient.objects.get(name=request.GET.get('client'))
        except ObjectDoesNotExist:
            return HttpResponseBadRequest()

        module = module_loader(client)

        try:
            DealValuation = getattr(module.models, 'DealValuation')
            Deal = getattr(module.models, 'Deal')
            Fund = getattr(module.models, 'Fund')
            FundAssociation = getattr(module.models, 'FundAssociation')
        except:
            return HttpResponseBadRequest()

        dealvaluations = DealValuation.objects.filter(fund_id=Fund.objects.get(fund_name=request.GET.get('fund')).fund_id)
        deals = Deal.objects.filter(cancellation=0)
        funds = Fund.objects.all()
        fund_association = FundAssociation.objects.all()
        fund_items = {}
        for fund_deal in fund_association:
            if fund_deal.fund_id and fund_deal.deal_id:
                if Fund.objects.filter(pk=fund_deal.fund_id).exists():
                    if Fund.objects.get(pk=fund_deal.fund_id).fund_name in fund_items.keys():
                        fund_items[Fund.objects.get(pk=fund_deal.fund_id).fund_name].append({fund_deal.deal_id
                                                                                             :Deal.objects.get(pk=fund_deal.deal_id).deal_name})
                    else:
                        fund_items[Fund.objects.get(pk=fund_deal.fund_id).fund_name] = []
                        fund_items[Fund.objects.get(pk=fund_deal.fund_id).fund_name].append({fund_deal.deal_id:
                            Deal.objects.get(pk=fund_deal.deal_id).deal_name})


        return JsonResponse({
            "deal_name": [{
                index: deal
            } for index,deal in enumerate(deals.values_list('deal_name',flat=True))],
            "fund_name": [{
                index: deal
            } for index, deal in enumerate(funds.values_list('fund_name', flat=True))],
            "fund_association": [fund_items],
            "dealvaluation": [{
                "id": dv.id,  # This is named randomly
                "deal_name": Deal.objects.get(pk=dv.deal_id).deal_name,
                "fund_name": Fund.objects.get(pk=dv.fund_id).fund_name,
                "valuation_date": str(dv.valuation_date),
                "unrealised_value": dv.unrealised_value or 0
            } for dv in dealvaluations]
        })

    @user_is_allowed
    def post(self, request, *args, **kwargs):
        try:
            client = VRMClient.objects.get(name=request.POST.get("client"))
        except ObjectDoesNotExist as e:
            return HttpResponseBadRequest()

        module = module_loader(client)

        try:
            DealValuation = getattr(module.models, 'DealValuation')
            Deal = getattr(module.models, 'Deal')
            Fund = getattr(module.models, 'Fund')
            FundAssociation = getattr(module.models, 'FundAssociation')
        except:
            return HttpResponseBadRequest()

        id = request.POST.get("id")
        deal_name = request.POST.get("dv_deal_name")
        fund_name = request.POST.get("dv_fund_name")
        valuation_date = request.POST.get("valuation_date")
        unrealised_value = request.POST.get("unrealised_value")

        if id:
            # Updating
            try:
                dealvaluation = DealValuation.objects.get(id=id)
                old_value = list(DealValuation.objects.filter(id=id).values())
            except ObjectDoesNotExist:
                HttpResponseBadRequest()
            dealvaluation.deal = Deal.objects.get(deal_name=deal_name)
            dealvaluation.fund = Fund.objects.get(fund_name=fund_name)
            dealvaluation.valuation_date = datetime.date(int(valuation_date.split('-')[0]),
                                                int(valuation_date.split('-')[1]),
                                                int(valuation_date.split('-')[2]))
            dealvaluation.unrealised_value = float(unrealised_value)

            try:
                # TODO: This does nothing
                dealvaluation.full_clean()
            except ValidationError as e:
                raise e

            dealvaluation.save()
            sync_fundassociation(client)

            try:
                audit_update = LogDealvaluation.objects.create(user=request.user,
                                          action=Action.objects.get(id=2),
                                          client_name=client.name,
                                          dealvaluation=dealvaluation.pk,
                                          data=list(DealValuation.objects.filter(id=id).values()),
                                          old_data=old_value,
                                          comment="Update"
                                )
                audit_update.save()
            except Exception as e:
                logger.exception("audit log failed {}" .format(e))
        else:
            # Creating
            dealvaluation = DealValuation(
                deal = Deal.objects.get(deal_name=deal_name),
                fund = Fund.objects.get(fund_name=fund_name),
                valuation_date = datetime.date(int(valuation_date.split('-')[0]),
                                           int(valuation_date.split('-')[1]),
                                           int(valuation_date.split('-')[2])),
                unrealised_value = float(unrealised_value)
            )

            try:
                # TODO: This does nothing
                dealvaluation.full_clean()
            except ValidationError as e:
                raise e

                dealvaluation.save()
                sync_fundassociation(client)

        dealvaluations = DealValuation.objects.filter(fund_id=Fund.objects.get(fund_name=request.GET.get('fund')).fund_id)
        deals = Deal.objects.filter(cancellation=0)
        funds = Fund.objects.all()
        fund_association = FundAssociation.objects.all()
        fund_items = {}
        for fund_deal in fund_association:
            if fund_deal.fund_id and fund_deal.deal_id:
                if Fund.objects.filter(pk=fund_deal.fund_id).exists():
                    if Fund.objects.get(pk=fund_deal.fund_id).fund_name in fund_items.keys():
                        fund_items[Fund.objects.get(pk=fund_deal.fund_id).fund_name].append({fund_deal.deal_id
                                                                                             : Deal.objects.get(
                                pk=fund_deal.deal_id).deal_name})
                    else:
                        fund_items[Fund.objects.get(pk=fund_deal.fund_id).fund_name] = []
                        fund_items[Fund.objects.get(pk=fund_deal.fund_id).fund_name].append({fund_deal.deal_id:
                                                                                                 Deal.objects.get(
                                                                                                     pk=fund_deal.deal_id).deal_name})

        return JsonResponse({
            "deal_name": [{
                index: deal
            } for index, deal in enumerate(deals.values_list('deal_name', flat=True))],
            "fund_name": [{
                index: deal
            } for index, deal in enumerate(funds.values_list('fund_name', flat=True))],
            "fund_association": [fund_items],
            "dealvaluation": [{
                "id": dv.id,  # This is named randomly
                "deal_name": Deal.objects.get(pk=dv.deal_id).deal_name,
                "fund_name": Fund.objects.get(pk=dv.fund_id).fund_name,
                "fund_name": Fund.objects.get(pk=dv.fund_id).fund_name,
                "valuation_date": str(dv.valuation_date),
                "unrealised_value": dv.unrealised_value or 0
            } for dv in dealvaluations]
        })


class ClientDealView(LoginRequiredMixin, View):
    @user_is_allowed
    def get(self, request, *args, **kwargs):
        try:
            client = VRMClient.objects.get(name=request.GET.get('client'))
        except ObjectDoesNotExist:
            return HttpResponseBadRequest()

        module = module_loader(client)

        try:
            Deal = getattr(module.models, 'Deal')
        except:
            return HttpResponseBadRequest()

        deals = Deal.objects.all()

        return JsonResponse({
            "deals": [{
                "deal_id": deal.deal_id,
                "deal_name": deal.deal_name,
                "cancellation": deal.cancellation
            } for deal in deals]
        })

    @user_is_allowed
    def post(self, request, *args, **kwargs):
        try:
            client = VRMClient.objects.get(name=request.POST.get('client'))
        except ObjectDoesNotExist:
            return HttpResponseBadRequest()

        module = module_loader(client)

        try:
            Deal = getattr(module.models, 'Deal')
        except:
            return HttpResponseBadRequest()

        deal_id = request.POST.get("deal_id")
        deal_name = request.POST.get("deal_name")
        cancellation = request.POST.get("cancellation")

        if deal_id:
            # Updating
            try:
                deal = Deal.objects.get(deal_id=deal_id)
            except ObjectDoesNotExist:
                HttpResponseBadRequest()

            deal.deal_name = deal_name
            deal.cancellation = cancellation

            try:
                # TODO: This does nothing
                deal.full_clean()
            except ValidationError as e:
                raise e

            deal.save()
        else:
            # Creating
            deal = Deal(
                deal_name=deal_name,
                cancellation=cancellation
            )

            try:
                # TODO: This does nothing
                deal.full_clean()
            except ValidationError as e:
                raise e

            deal.save()

        deals = Deal.objects.all()

        return JsonResponse({
            "deals": [{
                "deal_id": deal.deal_id,
                "deal_name": deal.deal_name,
                "cancellation": deal.cancellation
            } for deal in deals]
        })


class ClientFundView(LoginRequiredMixin, View):
    @user_is_allowed
    def get(self, request, *args, **kwargs):
        try:
            client = VRMClient.objects.get(name=request.GET.get('client'))
        except ObjectDoesNotExist as e:
            raise e

        funds = Funds.objects.filter(
            client__client_name=client.name
        )
        ccys = Ccys.objects.all()
        clients = Clients.objects.filter(
            client_name=client.name
        )

        return JsonResponse({
            "ccys": [{
                "id": ccy.id,
                "identifier": ccy.identifier,
                "name": ccy.name
            } for ccy in ccys],
            "funds": [{
                "id": fund.id,
                "fund_name": fund.fund_name,
                "ccy": fund.ccy.id if fund.ccy else fund.ccy,
                "client": fund.client.id if fund.client else fund.client
            } for fund in funds],
            "clients": [{
                "id": client.id,
                "name": client.client_name
            } for client in clients]
        })

    @user_is_allowed
    def post(self, request, *args, **kwargs):
        try:
            current_client = VRMClient.objects.get(name=request.POST.get('client'))
        except ObjectDoesNotExist as e:
            raise e

        id = request.POST.get("id")
        fund_name = request.POST.get("fund_name")
        ccy = request.POST.get("ccy")
        client = request.POST.get("client_id")

        if id:
            # Updating
            try:
                fund = Funds.objects.get(id=id)
            except ObjectDoesNotExist as e:
                raise e

            try:
                fund.fund_name = fund_name
                fund.ccy = Ccys.objects.get(id=ccy)
                fund.client = Clients.objects.get(id=client)
            except ObjectDoesNotExist as e:
                raise e

            try:
                # TODO: This does nothing
                fund.full_clean()
            except ValidationError as e:
                raise e

            fund.save()
        else:
            # Creating
            try:
                fund = Funds(
                    fund_name=fund_name,
                    ccy=Ccys.objects.get(id=ccy),
                    client=Clients.objects.get(id=client)
                )
            except ObjectDoesNotExist as e:
                raise e

            try:
                # TODO: This does nothing
                fund.full_clean()
            except ValidationError as e:
                raise e

            fund.save()

        funds = Funds.objects.filter(
            client__client_name=current_client.name
        )
        ccys = Ccys.objects.all()
        clients = Clients.objects.filter(
            client_name=current_client.name
        )

        return JsonResponse({
            "ccys": [{
                "id": ccy.id,
                "identifier": ccy.identifier,
                "name": ccy.name
            } for ccy in ccys],
            "funds": [{
                "id": fund.id,
                "fund_name": fund.fund_name,
                "ccy": fund.ccy.id if fund.ccy else fund.ccy,
                "client": fund.client.id if fund.client else fund.client
            } for fund in funds],
            "clients": [{
                "id": client.id,
                "name": client.client_name
            } for client in clients]
        })


@login_required(login_url="login")
@user_is_allowed
def ajax_get_entity_lar(request):
    try:
        if request.is_ajax() and request.method == "GET":

            try:
                client = request.GET["client"]
                fund = request.GET["fund"]
                counterparties = request.GET["counterparty"]
                include_expired_counterparties = request.POST.get(
                    'include_expired_counterparties',
                    'False'
                ).lower() == 'true'
            except KeyError:
                return HttpResponseBadRequest()

            data = []
            query_set = {'client': client}
            client = VRMClient.objects.get(name=client)
            general_client = Clients.objects.get(client_name=client.name)
            general_fund = Funds.objects.get(fund_name=fund, client=general_client.pk)
            if client is None:
                raise Exception('Unable to find the client to process!')

            module = module_loader(client)
            condition = {}
            if not include_expired_counterparties:
                condition["value_date__gte"] = date.today()
            fund_association = module.models.FundAssociation
            level_choice = fund_association.objects.filter(fund_id=general_fund.pk).exclude(
                entity_id__isnull=True).values('entity_id').order_by('entity_id')
            ccy = getattr(module.models, 'LegalCounterparty').\
                            objects.filter(fund=getattr(module.models, 'Fund').\
                            objects.get(fund_name=general_fund.fund_name)).first().base_currency
            lar_sum = 0
            if len(level_choice) > 0:
                for item in level_choice:
                    entity = getattr(module.models, 'Entity').objects.get(entity_id=item['entity_id'])
                    cp_list = []
                    if counterparties != 'all':
                        query_set = {'counterparty': counterparties}
                    if client.entity_summary:
                        if entity and entity != '':
                            query_set['legal_entity'] = entity.entity_name
                        else:
                            query_set['fund'] = general_fund.fund_name
                    else:
                        query_set['fund'] = general_fund.fund_name

                    cp_list = list(VrmHedge1CopyCopy.objects.filter(
                        **query_set,
                        dummy__iexact='FALSE',
                        cancellation='FALSE',
                        **condition,
                    ).values_list("counterparty", flat=True).distinct())

                    for legal_cp in cp_list:
                        cp = Counterparties.objects.get(counterparty_name=legal_cp, cancellation=0)
                        lar = getattr(module.models, 'Lar').objects.filter(fund=general_fund, entity=entity,
                                                                           counterparty_id=cp.pk,
                                                                           confidence_level__gt=0.9)

                        if len(lar) > 0:
                            lar = lar.latest('valuation_date')
                            data.append({'fund': lar.fund.fund_name if  lar.fund else '',
                                         'entity': lar.entity.entity_name  if lar.entity else '',
                                         'counterparty': lar.counterparty.counterparty_name if lar.counterparty else '',
                                         'lar': round(lar.lar,2)})
                            lar_sum += round(lar.lar)
                data.append({'section': 'quarters', 'fund': 'Total : ', 'lar': lar_sum})
    except ObjectDoesNotExist as exception:
        print(traceback.format_exc())
    return JsonResponse({'data': data, 'ccy': ccy})



@login_required(login_url="login")
@user_is_allowed
def ajax_download_deal_booking_template(request):
    try:
        msg = 'Unknown error'
        success = False
        folder_name = ''
    
        try:
            client = VRMClient.objects.get(name=request.GET["client"])
        except KeyError:
            response = HttpResponseBadRequest()
            return response

        try:
            module = __import__(client.name.lower())
        except Exception as e:
            try:
                module = __import__(client.label.lower())
            except Exception as e:
                raise Exception('Unable to find the client to process!')
        

        try:
            version = request['version']
        except:
            version = 'v'

        # Download template from Bitbucket
        bucket = 'riskview-booking-templates'
        s3_client = boto3.client('s3')
        keys = _get_s3_keys(s3_client, bucket, version)
        versions = [key for key in keys if version in key]
        versions.sort()
        last_version = versions[-1]

        timestamp = datetime.datetime.now()
        folder_name = str(request.user.first_name or "") + str(request.user.last_name or "") + \
                      str(timestamp.year) + str(timestamp.month) + str(timestamp.day) + \
                      str(timestamp.hour) + str(timestamp.minute) + str(timestamp.second) + \
                      str(timestamp.microsecond)

        path = os.path.join(settings.MEDIA_ROOT,
                            'deal-booking-templates',
                            folder_name)

        if not os.path.exists(path):
            os.mkdir(path)
        file_path = os.path.join(path, 'deal-booking.xlsm')
        with open(file_path, 'wb') as file:
            s3_client.download_fileobj(bucket, last_version, file)

        

        funds = module.models.Fund.objects.order_by('fund_name').values('fund_name', 'ccy')
        deals = module.models.Deal.objects.order_by('deal_name').values('deal_name')

        # Populate tabs in xlsm file
        wb = load_workbook(file_path, keep_vba=True, read_only=False)
        # specs_sheet = wb['specs']
        s3_client = boto3.client('s3')
        try:
            s3_response_object = s3_client.get_object(Bucket='databaseconf', Key='django_config.yml')
            configfile = yaml.safe_load(s3_response_object['Body'])
        except:
            print("Unable to load config. file")
            exit()

        ws_version = wb['specs']
        hash_key = ws_version['B1'].value
       # if hash_key != configfile['RVEXCEL'][0]['VERSION_HASH']:
       #     if hash_key != configfile['RVEXCEL'][1]['DEV_VERSION_HASH']:
       #         msg = 'Please use the latest Excel Template.'
       #         raise Http404

        _clear_cells(sheet=wb['Funds'], col=1, start_row=2)
        _clear_cells(sheet=wb['Funds'], col=2, start_row=2)
        for index, fund in enumerate(list(funds)):
            wb['Funds'].cell(column=1, row=index + 2).value = fund['fund_name']
            wb['Funds'].cell(column=2, row=index + 2).value = fund['ccy']

        _clear_cells(sheet=wb['Deals'], col=1, start_row=2)
        for index, deal in enumerate(list(deals)):
            wb['Deals'].cell(column=1, row=index + 2).value = deal['deal_name']

        wb.save(file_path)

        if os.path.exists(file_path):
            # with open(file_path, 'rb') as fh:
            #     response = HttpResponse(fh.read(), content_type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            #     response['Content-Disposition'] = 'inline; filename=' + os.path.basename(file_path)
            #     os.remove(file_path)
            #     os.rmdir(path)

            success = True
        else:
            msg = "File " + file_path + "does not exits"
            raise Http404

    except:
        print(msg)

    finally:
        resp = ({
            'success': success,
            'msg': msg,
            'file_name': folder_name
        })
        return JsonResponse(resp)


def _get_s3_keys(s3_client, bucket, start_after):
    """Get a list of keys in an S3 bucket.

        :param bucket: name of the S£ bucket
        :returns: list of keys
    """
    keys = []
    resp = s3_client.list_objects_v2(Bucket=bucket, StartAfter=start_after)
    for obj in resp['Contents']:
        keys.append(obj['Key'])
    return keys


def _clear_cells(sheet, cell_range=None, col=None, start_row=None, end_row=None, clear_until_last=True):
    if cell_range:
        for row in sheet[cell_range]:
            for cell in row:
                cell.value = None
    elif clear_until_last:
        row = start_row
        while sheet.cell(column=col, row=row).value:
            sheet.cell(column=col, row=row).value = None
            row += 1
    else:
        for row in range(start_row, end_row):
            sheet.cell(column=col, row=row).value = None


@login_required(login_url="login")
@user_is_allowed
@transaction.atomic
def ajax_upload_deal_booking_template(request):
    try:
        inner_msg = ""
        msg = 'Unknown error'
        success = False

        try:
            vrm_client = VRMClient.objects.get(name=request.POST["client"])
        except KeyError:
            return HttpResponseBadRequest()

        try:
            user = request.user
        except:
            return HttpResponseBadRuest()

        try:
            module = module_loader(vrm_client)
        except:
            raise Exception('Unable to find the client to process')

        try:
            file_obj = request.FILES["file"]
        except:
            raise Exception('No attached file')

        try:
            contents = file_obj.read()
            path = default_storage.save(
                file_obj.name,
                ContentFile(contents)
            )
            tmp_file = os.path.join(settings.MEDIA_ROOT, path)
        except:
            msg = "Error uploading the file. If this issue persists, contact the tech team."
            raise Exception(msg)

        try:
            if file_obj.name.endswith('xlsm'):
                wb = load_workbook(filename=tmp_file,
                                   read_only=True, data_only=True)
                s3_client = boto3.client('s3')
                try:
                    s3_response_object = s3_client.get_object(Bucket='databaseconf', Key='django_config.yml')
                    configfile = yaml.safe_load(s3_response_object['Body'])
                except:
                    inner_msg = "Unable to load config. file"
                    raise Exception(inner_msg)

                ws_version = wb['specs']
                hash_key = ws_version['B1'].value
                #if hash_key != configfile['RVEXCEL'][0]['VERSION_HASH']:
                #    if hash_key != configfile['RVEXCEL'][1]['DEV_VERSION_HASH']:
                #        inner_msg = 'Please use the latest Excel Template.'
                #        raise Exception


                try:
                    sid = transaction.savepoint()
                    # Cashflows tab:
                    parsed_cashflows, parsed_cashflows_dict = _parse_file(wb['Cashflows'],
                                                                          module.models.Cashflows,
                                                                          module)
                    if len(parsed_cashflows) > 0:
                        inserted_data = _save_records(parsed_cashflows, parsed_cashflows_dict, 'transaction_id')
                        
                        data = {'data': json.dumps(inserted_data)}
                        _log_insertion(
                            model=audit.models.LogCashflows,
                            user_id=user.id,
                            client_name=vrm_client.name,
                            **data,
                        )

                    parsed_deal_valuations, parsed_valuations_dict = _parse_file(wb['Deal Valuations'],
                                                                                 module.models.DealValuation,
                                                                                 module)
                    if len(parsed_deal_valuations) > 0:
                        inserted_data = _save_records(parsed_deal_valuations, parsed_valuations_dict)
                        data = {'data': json.dumps(inserted_data)}
                        _log_insertion(
                            model=audit.models.LogCashflows,
                            user_id=user.id,
                            client_name=vrm_client.name,
                            **data,
                        )
                    success = True
                except Exception as e:
                    transaction.savepoint_rollback(sid)
                    inner_msg = 'An error occurred when saving the data:' + str(e) + '. No data has been saved.'
                    raise Exception(inner_msg)

            else:
                inner_msg = 'File extension not supported. Only .xlsm files can be uploaded.'
                raise Exception(inner_msg)
        except:
            msg = "Error while parsing file: " + inner_msg
            raise Exception(msg)

    except:
        success = False
        print(msg)
    finally:
        if success:
            sync_fundassociation(vrm_client)
        resp = {
            'msg': msg,
            'success': success,
        }
        return JsonResponse(resp)


def _parse_file(worksheet, model=None, module=None):

    x_ing = {'row': 1,
             'col': 1}
    x_ing_found = False
    for row in worksheet['A1':'ZZ100']:
        for cell in row:
            if cell.value == 'x-ing':
                x_ing['row'] = cell.row
                x_ing['col'] = cell.column
                x_ing_found = True
                break
        if x_ing_found:
            break
    if not x_ing_found:
        msg = 'Wrong Excel Template uploaded: "x-ing" identifier not found.'
        raise Exception(msg)

    headings = [element.value.lower() for
            element in worksheet[(x_ing['row'] - 1)][0:x_ing['col'] - 1] if "__" not in element.value and element.value]
    start_col = len([element.value.lower() for
            element in worksheet[(x_ing['row'] - 1)][0:x_ing['col'] - 2] if "__" in element.value or element.value is None])
    
    parsed_data = []
    parsed_data_dict = []
    for row in worksheet[1:worksheet.max_row][x_ing['row']:]:
        row = row[start_col:x_ing['col']-1]
        if row[0].value is None:
            break

        row_data = {}
        row_data_dict = {}
        for cell in row:
            if 'EmptyCell' not in str(type(cell)):
                value = cell.value
                value_dict = value
                key = headings[cell.column - start_col - 1]
                key_dict = key
                if isinstance(value, datetime.datetime):
                    value = value.date()
                    value_dict = str(value)
                if module:
                    if headings[cell.column - start_col - 1] == 'fund':
                        value = list(module.models.Fund.objects.filter(fund_name=value))[0]
                        value_dict = value.pk
                        key_dict = 'fund_id'
                    elif headings[cell.column - start_col - 1] == 'deal':
                        value = list(module.models.Deal.objects.filter(deal_name=value))[0]
                        value_dict = value.pk
                        key_dict = 'deal_id'
                row_data[key] = value
                row_data_dict[key_dict] = value_dict

        parsed_data_dict += [row_data_dict]
        if model:
            parsed_data += [model(**row_data)]
        else:
            parsed_data += [row_data]

    return parsed_data, parsed_data_dict


def _log_insertion(model, user_id, client_name, **data):
    return model(
        timestamp=datetime.datetime.now(),
        user_id=user_id,
        action_id=3,
        client_name=client_name,
        **data
    ).save()


def _save_records(data, data_dict, pk_name='id'):
    for index, datum in enumerate(data):
        datum.save()
        data_dict[index][pk_name] = datum.pk
    return data_dict


class RoundTwo(Func):
    function = 'ROUND'
    arity = 2


@login_required(login_url="login")
@user_is_allowed
def agg_report_lexington(request):

    if request.is_ajax() is False or request.method != 'POST':
        return HttpResponseBadRequest()
    client = request.POST.get('client')
    resp_data = None
    if None in [client]:
        return HttpResponseBadRequest()

    vrm_client = VRMClient.objects.get(name=request.POST['client'])

    try:
        module = module_loader(vrm_client)
    except Exception as e:
        raise Exception('Unable to find the client to process!')
    psummary = getattr(module.models, "ParentSummaryDeal")
    extra_parent = getattr(module.models, "ExtraParent")
    try:
        resp_data = list(psummary.objects.values().annotate(
            initial_amounts=RoundTwo(Sum('initial_amounts'),2), initial_rate=Avg('initial_rate'),
            outstanding_amounts=RoundTwo(Sum('outstanding_amounts'),2), realised_pnl=RoundTwo(Sum('realised_pnl'),2),
            unrealised_pnl=RoundTwo(Sum('unrealised_pnl'),2)))

        extra_id_list = list(psummary.objects.filter(audited=1).values_list('id', flat=True))
        extra_parent_data = list(extra_parent.objects.filter(summary_id__in=extra_id_list).values())
        
        for data in extra_parent_data:
            for index, data2 in enumerate(resp_data,0):
                if data['summary_id'] == data2['id']:
                    resp_data[index].update(data)


    except Exception as e:
        print(traceback.format_exc())
    return JsonResponse({
        'data': resp_data,

    })

