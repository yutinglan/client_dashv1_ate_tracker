from rest_framework import serializers
from .common import module_loader
import json

class AteItemRelatedField(serializers.RelatedField):
    def to_representation(self, value):
        res = AteItemSerializer(value).data
        return res

class CsaConditionRelatedField(serializers.RelatedField):
    def to_representation(self, value):
        res = CsaConditionSerializer(value).data
        return res

class AteConditionRelatedField(serializers.RelatedField):
    def to_representation(self, value):
        print(value)
        res = AteConditionSerializer(value).data
        return res

class EntityRelatedField(serializers.RelatedField):
    def to_representation(self, value):
        name = value.entity_name
        return name

class AteItemSerializer(serializers.Serializer):
    id = serializers.IntegerField(required=False, read_only=True)
    item_name = serializers.CharField(max_length=255, required=True,)

class AteConditionSerializer(serializers.Serializer):
    id = serializers.IntegerField(required=False, read_only=True)
    csa_id = serializers.StringRelatedField()
    condition_name = serializers.CharField(required=True, max_length=255)
    active = serializers.BooleanField(required=True)
    threshold = serializers.FloatField(required=True)

class AteCalculationitemSerializer(serializers.Serializer):
    id = serializers.IntegerField(required=False, read_only=True)
    ate_item = AteItemRelatedField(read_only=True)
    ate_condition = AteConditionRelatedField(read_only=True)
    multiplier = serializers.IntegerField(required=True)
    numerator = serializers.IntegerField(required=True)

class AteSettingSerializer(serializers.Serializer):
    # id = serializers.IntegerField(required=False, read_only=True)
    ate_item = AteItemRelatedField(read_only=True)
    ate_condition = AteConditionRelatedField(read_only=True)
    multiplier = serializers.IntegerField(required=True)
    numerator = serializers.IntegerField(required=True)

class CsaConditionSerializer(serializers.Serializer):
    id = serializers.IntegerField(required=False, read_only=True)
    csa_id = serializers.StringRelatedField()
    condition_name = serializers.CharField(max_length=255)
    active = serializers.BooleanField(required=True)

class CsaCalculationitemSerializer(serializers.Serializer):
    id = serializers.IntegerField(required=False, read_only=True)
    ate_item = AteItemRelatedField(read_only=True)
    csa_condition = CsaConditionRelatedField(read_only=True)
    positive = serializers.BooleanField(required=True)

class CsaSettingSerializer(serializers.Serializer):
    id = serializers.IntegerField(required=False, read_only=True)
    ate_item = AteItemRelatedField(read_only=True)
    csa_condition = CsaConditionRelatedField(read_only=True)
    positive = serializers.BooleanField(required=True)

class CsaThresholdSerializer(serializers.Serializer):
    id = serializers.IntegerField(required=False, read_only=True)
    csa_condition = CsaConditionRelatedField(read_only=True)
    condition_threshold = serializers.IntegerField()
    csa_threshold = serializers.IntegerField()

class FinancialStatementSerializer(serializers.Serializer):
    id = serializers.IntegerField(required=False, read_only=True)
    entity = EntityRelatedField(read_only=True)
    ate_item = AteItemRelatedField(read_only=True)
    year = serializers.IntegerField()
    quarter = serializers.IntegerField()
    value = serializers.IntegerField()

class LegalCounterPartySerializer(serializers.Serializer):
    csa_id = serializers.IntegerField(required=False, read_only=True)
    entity = EntityRelatedField(read_only=True)
    cp = serializers.StringRelatedField(many=False)
