import logging
from django.core.exceptions import ObjectDoesNotExist
from django.core.exceptions import PermissionDenied

from client.models import VRMClient

logger = logging.getLogger(__name__)


def user_is_allowed(function):
    def wrapper(request, *args, **kwargs):
        try:
            try:
                user = request.user
                client_name = None
                client = None
                if len(request.POST) > 0:
                    client_name = request.POST.get('client')
                elif len(request.GET) > 0:
                    client_name = request.GET.get('client')
                else:
                    return PermissionDenied
            except Exception as e:
                # this is required for Class based view function
                user = request.request.user
                client_name = None
                client = None
                if len(request.request.POST) > 0:
                    client_name = request.request.POST.get('client')
                elif len(request.request.GET) > 0:
                    client_name = request.request.GET.get('client')
                else:
                    return PermissionDenied

            if client_name is None:
                return PermissionDenied
            try:
                client = VRMClient.objects.get(name=client_name)
            except ObjectDoesNotExist as exception:
                return PermissionDenied

            if user.is_staff and user.is_active:
                return function(request, *args, **kwargs)
            elif user.is_active and user.profile.clients.all().exists() and client_name in user.profile.clients.all().values_list('name', flat=True):
                return function(request, *args, **kwargs)
            else:
                return PermissionDenied
        except Exception as e:
            print(e)
        return PermissionDenied
    wrapper.__doc__ = function.__doc__
    wrapper.__name__ = function.__name__
    return wrapper


def module_loader(client):
    """
    TODO: Clean this up

    Client parameter must be a VRMClient object
    """
    try:
        module = __import__(client.name.lower())
    except:
        try:
            module = __import__(client.label.lower())
        except:
            module = None

    return module

def module_loader_by_client_name(client_name):
    """
    TODO: Clean this up

    Client parameter must be a VRMClient object
    """
    try:
        module = __import__(client_name.lower())
    except:
        try:
            module = __import__(client_name.lower())
        except:
            module = None

    return module


def update_user_fund_selection(request):
    try:
        fund = request.POST.get('fund', '')
        request.user.profile.recent_fund = fund
        request.user.save()
    except:
        logger.exception("Unable to associate the user fund details")


def unique(_list):
    unique_list = []
    for element in _list:
        if element not in unique_list:
            unique_list.append(element)
    return unique_list
