import datetime
import logging

import traceback
from datetime import date
import operator
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Avg, F, Func, Max, Q, Value as V, DurationField, ExpressionWrapper
from django.db.models.aggregates import Min, Sum
from django.db.models.functions import Coalesce
from django.http import HttpResponse, HttpResponseBadRequest, JsonResponse
from client.models import VRMClient
from database_schema.models import (VrmAggregateMtm, VrmCounterparty,
                                    VrmFund, VrmHedge1CopyCopy, VrmMtm1)
from dss_schema.models import LatestSpotRelatedData
from general.models import Counterparties, Funds, Styles, Clients, SchemaFundCp
from .common import user_is_allowed, module_loader, update_user_fund_selection
logger = logging.getLogger(__name__)

def _get_module_lar(request):
    try:
        vrm_client, module, LAR, general_client, \
        general_fund, general_entity, lar_queryset, individual_set, hedge_set = None, None, None, None, None, \
                                                                                None, {}, {}, {}
        if 'label' in request.POST:
            vrm_client = VRMClient.objects.get(label=request.POST['label'])
        else:
            vrm_client = VRMClient.objects.get(name=request.POST['client'])
        try:
            module = module_loader(vrm_client)
        except Exception as e:
            raise Exception('Unable to find the client to process!')
        # TODO remove this once what exactly is required to be processed for LAR level
        # Assume this will become the schema level model for timebeing switch between hedges Lar and schema Lar
        general_client = Clients.objects.get(client_name=vrm_client.name)
        general_fund = None
        if request.POST['fund'] != '':
            general_fund = Funds.objects.get(fund_name=request.POST['fund'], client=general_client.pk)
            lar_queryset = {'fund': general_fund}
            individual_set = {'fund': general_fund}
        if vrm_client.entity_summary:
            LAR = getattr(module.models, 'Lar')
            if request.POST.get('entity') and request.POST.get('entity') != '':
                general_entity = module.models.Entity.objects.get(entity_name=request.POST.get('entity'))
                lar_queryset['entity'] = general_entity
                individual_set = {'entity': general_entity}
                hedge_set = {'legal_entity': general_entity.entity_name}
        else:
            # already imported
            LAR = getattr(__import__('hedges').models, 'Lar')
            lar_queryset = {'fund': general_fund.pk, 'client': general_client.pk}
            hedge_set = {'fund': general_fund.fund_name}
    except ObjectDoesNotExist as exception:
        raise Exception('Unable to find the client to process!')
    except Exception as e:
        print(e)
    finally:
        return vrm_client, module, LAR, general_client, general_fund, general_entity, lar_queryset, individual_set, hedge_set


@login_required(login_url='login')
@user_is_allowed
def ajax_underlying_request(request):
    try:
        underlying_dict = dict()
        spot_rate = dict()
        additional_underlying = list()
        module = None
        if request.is_ajax() and request.method == 'POST':
            client_type = request.POST.get('client', '')
            fund = request.POST.get('fund', '')
            update_user_fund_selection(request)
            underlying = list(VrmHedge1CopyCopy.objects.filter(client=client_type,
                                                               fund=fund,
                                                               cancellation__iexact='FALSE',
                                                               asset_class__iexact='FX',
                                                               dummy__iexact='FALSE').values_list('underlying',
                                                                                                  flat=True).distinct())

            try:
                client = VRMClient.objects.get(name=client_type)
                module = module_loader(client)
            except Exception as e:
                    raise Exception('Unable to find the client to process!')

            if module:
                try:
                    additional_underlying_obj = getattr(module.models, 'AdditionalUnderlying')
                except Exception as e:
                    additional_underlying_obj = None

            if additional_underlying_obj:
                additional_underlying = list(
                    additional_underlying_obj.objects.all().exclude(
                        underlying__in=underlying
                    ).values_list(
                        'underlying',
                        flat=True
                    ).distinct()
                )
            # Second list,  we display additional underlying which is not part of the trade.
            full_underlying_list = underlying + additional_underlying

            spot_rate_pairs = list(
                LatestSpotRelatedData.objects.filter(underlying__in=full_underlying_list,
                                                     security="spot").values("underlying", "valuation_date").distinct())

            for pair in spot_rate_pairs:
                query = Q(underlying=pair['underlying'], valuation_date=pair['valuation_date'], security="spot")
                spot_rate[pair['underlying']] = LatestSpotRelatedData.objects.get(query).mid

    except Exception as e:
        logger.exception(traceback.format_exc())
        return HttpResponseBadRequest()
    finally:
        res_data = {
            'spot_rate': spot_rate
        }
        return JsonResponse(res_data, content_type='application/json')


@login_required(login_url='login')
@user_is_allowed
def liquidity_at_risk(request):
    if request.is_ajax() is False or request.method != 'POST':
        return HttpResponseBadRequest()

    client_label = request.POST.get('label')
    fund = request.POST.get('fund')
    entity_name = request.POST.get('entity')
    # TODO need to address this issue.
    user = User.objects.get(username=request.user.username)
    user.profile.recent_fund = fund
    user.save()
    if None in [client_label, fund]:
        response = {
            'total_lar': None,
            'total': None,
            'confidence_level': '95%',
            'data': [],
            'margin_txt': None
        }
        return JsonResponse(response)
    select_hedging_list = []
    value_hedging_list = []
    hedge_cost = None
    module = None
    lar_list = []
    data = []
    summary_list_option = []
    # Overall (44) and related fund (43) overall
    try:
        overall_cp = Counterparties.objects.get(pk=44)
    except ObjectDoesNotExist as exception:
        overall_cp = None
    try:
        related_cp = Counterparties.objects.get(pk=43)
    except ObjectDoesNotExist as exception:
        related_cp = None
    try:
        vrm_client, module, LAR, general_client, general_fund, general_entity, lar_queryset,individual_set, hedge_set = _get_module_lar(request)
        # time to set if any trade summary list option
        if vrm_client.label.lower() in ['cvc']:
            # This needs to be updated for the client in admin panel not to be hardcoded.
            summary_list_option = {1: 'Asset', 2: 'Subscription'}
        elif vrm_client.label.lower() in ['permira']:
            # This needs to be updated for the client in admin panel not to be hardcoded.
            summary_list_option = {1: 'Principal only', 2:'Interest Only'}
        # 43 and 44 are Overall and Related Fund Counterparties should be ignored
        counterparties = [Counterparties.objects.get(pk=item['counterparty_id']) for item in
                          LAR.objects.filter(
                **lar_queryset,
                confidence_level__gt=0.9,
            ).values('counterparty_id').exclude(counterparty__in=[overall_cp, related_cp]).annotate(Max('insert_timestamp'))]

        counterparties = list(sorted(counterparties, key=operator.attrgetter('counterparty_name')))
        # Now LAR is calculated at Fund level and Related Fund level as well. but below code exist to support
        # the previous flow.
        if LAR.objects.filter(**lar_queryset, counterparty=overall_cp,
                              confidence_level__gt=0.9).exists():

            # TODO need to  inform the clients before turning this feature , but database will be
            #  updated by daily script as this will affect all the clients.
            # try:
            #     total = Lar.objects.filter(fund=general_fund.pk, client=general_client.pk, counterparty=overall_cp,
            #                                confidence_level__gt=0.9).latest('valuation_date').lar
            #     total = "{0:.2f}".format(total/ 10e5)
            #     lar_list.append({'id': general_fund.fund_name, 'value': total})
            # except ObjectDoesNotExist as exception:
            #     total = 1

            # TODO Until client agrees need to display the sum of counterparties probablity.
            # expecting this might be added to visual after confirmation

            liquidity_all = LAR.objects.filter(**lar_queryset).exclude(counterparty__in=[overall_cp, related_cp])
            #  TODO: 404 is not an appropriate response code
            if liquidity_all.count() == 0:
                # return JsonResponse({}, status=404)
                response = {
                    'total_lar': None,
                    'total': None,
                    'confidence_level': '95%',
                    'data': [],
                    'margin_txt': None
                }
                return JsonResponse(response)

            liquidity_at_risk_insert_timestamp = liquidity_all.filter(
                insert_timestamp=liquidity_all.aggregate(
                    Max('insert_timestamp')
                )['insert_timestamp__max']
            )

            liquidity_at_risk_max_cl = liquidity_at_risk_insert_timestamp.filter(
                confidence_level=liquidity_at_risk_insert_timestamp.aggregate(
                    Max('confidence_level')
                )['confidence_level__max']
            )

            total = liquidity_at_risk_max_cl.aggregate(Sum('lar'))
            total = "{0:.2f}".format(total['lar__sum'] / 10e5)
            if general_client.client_name == 'CVC':
                lar_list.append({'id': general_fund.fund_name, 'value': total})
                try:
                    related_total = LAR.objects.filter(**lar_queryset,
                                                       counterparty=related_cp,
                                                       confidence_level__gt=0.9).latest('valuation_date').lar
                    related_total = "{0:.2f}".format(related_total / 10e5)
                    lar_list.append({'id': 'Overall', 'value': related_total})
                except ObjectDoesNotExist as exception:
                    related_total = 1
        else:
            liquidity_all = LAR.objects.filter(**lar_queryset).exclude(counterparty__in=[overall_cp, related_cp])
            #  TODO: 404 is not an appropriate response code
            if liquidity_all.count() == 0:
                # return JsonResponse({}, status=404)
                response = {
                    'total_lar': None,
                    'total': None,
                    'confidence_level': '95%',
                    'data': [],
                    'margin_txt': None,
                    'summary_list_option':summary_list_option,
                }
                return JsonResponse(response)

            liquidity_at_risk_insert_timestamp = liquidity_all.filter(
                insert_timestamp=liquidity_all.aggregate(
                    Max('insert_timestamp')
                )['insert_timestamp__max']
            )

            liquidity_at_risk_max_cl = liquidity_at_risk_insert_timestamp.filter(
                confidence_level=liquidity_at_risk_insert_timestamp.aggregate(
                    Max('confidence_level')
                )['confidence_level__max']
            )

            total = liquidity_at_risk_max_cl.aggregate(Sum('lar'))
            total = "{0:.2f}".format(total['lar__sum']/10e5)
            if general_client.client_name == 'CVC':
                lar_list.append({'id': general_fund.fund_name, 'value': total})

        # TOdo need to check everything works fine here.
        margin_txt = ''
        for item in counterparties:
            margin_call = LAR.objects.filter(
                **lar_queryset,
                counterparty=item.id,
                confidence_level__gt=0.9
            ).order_by('-insert_timestamp')[0]
            data.append({'counterparty':item.counterparty_name,
                'probability': str(round(margin_call.probability * 100, 2)) + '%'})
            if margin_call.probability > 0:
                margin_txt += '{} : {}% to  get a margin  call in the  next  6   months  from {}'.\
                                format(fund, margin_call.probability, item.counterparty_name)
        if not margin_txt:
            margin_txt = 'No margin call is expected in the next 6 months.'
        try:
            try:
                module = __import__(vrm_client.name.lower())
            except Exception as e:
                try:
                    module = __import__(vrm_client.label.lower())
                except Exception as e:
                    raise Exception('Unable to find the client to process!')
            SummaryTable = getattr(module.models, 'SummaryTable')
            if SummaryTable:
                if general_client.client_name == 'CVC':
                    # get all different type of underlying
                    hedge_list = ['Overall','Exposure']
                    underlying_list = VrmHedge1CopyCopy.objects.filter(client=vrm_client.name, fund=fund,
                                                                       cancellation__iexact='FALSE',
                                                                       dummy__iexact='FALSE',
                                                                       value_date__gte=date.today()). \
                        values( 'underlying').order_by('underlying').distinct()
                    for ul in underlying_list:
                        hedge_list.append(ul['underlying'])
                    # annulaised bps / monthly bps
                    value_hedging_list = [{'abps': [], 'mbps': []}]
                    count = 0
                    for actual_deal in hedge_list:
                        # 0 - Annualised bps
                        abps_val = 0
                        if SummaryTable.objects.filter(fund_id=general_fund.id, deal_name=actual_deal, bps=0).exists():
                             abps_val = SummaryTable.objects.filter(fund_id=general_fund.id, deal_name=actual_deal, bps=0). \
                                         order_by('-update_date').values('hedging_cost')[0]['hedging_cost'] or 0
                        value_hedging_list[0]['abps'].append({'id': count, 'val': abps_val})
                        # 0 - Monthly bps
                        mbps_val = 0
                        if SummaryTable.objects.filter(fund_id=general_fund.id, deal_name=actual_deal, bps=1).exists():
                            mbps_val = \
                            SummaryTable.objects.filter(fund_id=general_fund.id, deal_name=actual_deal, bps=1). \
                                order_by('-update_date').values('hedging_cost')[0]['hedging_cost'] or 0
                        value_hedging_list[0]['mbps'].append({'id': count, 'val': mbps_val})
                        count = count+1
                    count = 0
                    for hl_item in hedge_list:
                        select_hedging_list.append({'id': count, 'val': hl_item })
                        count = count + 1

                else:
                    #Todo need to validate if heding cost exist and we have started to use ID TODO
                    if SummaryTable.objects.filter(fund=general_fund.fund_name, deal_name='Overall').exists():
                        hedge_cost = SummaryTable.objects.filter(fund=general_fund.fund_name, deal_name='Overall').\
                                            order_by('-update_date').values('hedging_cost')[0]['hedging_cost'] or 0
                    else:
                        hedge_cost = 0

        except Exception as e:
            print(traceback.format_exc())

        response = {
            'total_lar': total,
            'total': float(total),
            'confidence_level': '95%',
            'data': data,
            'margin_txt': margin_txt,
            'hedge_cost': hedge_cost,
            'select_hedging_list': select_hedging_list,
            'value_hedging_list': value_hedging_list,
            'lar_list': lar_list,
            'summary_list_option': summary_list_option,

        }

        return JsonResponse(response)
    except ObjectDoesNotExist as exception:
        print(traceback.format_exc())
        response = {
            'total_lar': None,
            'total': None,
            'confidence_level': '95%',
            'data': [],
            'margin_txt': None,
            'select_hedging_list': [],
            'value_hedging_list': [],
            'lar_list': [],
            'summary_list_option': [],

        }
        return JsonResponse(response)
    except Exception as e:
        print(e)
        response = {
            'total_lar': None,
            'total': None,
            'confidence_level': '95%',
            'data': [],
            'margin_txt': None,
            'select_hedging_list': [],
            'value_hedging_list': [],
            'lar_list': [],
            'summary_list_option': [],
        }
        return JsonResponse(response)
    return HttpResponseBadRequest()


@login_required(login_url='login')
@user_is_allowed
def trade_mtm_table(request):
    """AJAX Trade Mark-to-Market Table

    Valid HTTP Methods: POST

    Returns the payload to render a 'Hedge Liquidity' column chart,
    and a table of all trades.

    The request has 4 valid parameters:
        - client
        - fund
        - include_expired_counterparties [true|false]

    :param object request: The request objects.
    :return: A JSON response object
    :rtype: object
    """

    if request.is_ajax() is False or request.method != 'POST':
        return HttpResponseBadRequest()
    resp_sum = 0
    fund = request.POST.get('fund')
    client = request.POST.get('client')
    entity = request.POST.get('entity') or ''
    if None in [fund, client]:
            return HttpResponseBadRequest()
    include_expired_counterparties = request.POST.get(
        'include_expired_counterparties',
        'False'
    ).lower() == 'true'

    trade_summary_options = 0
    summary_list_option = []
    if 'trade_summary_options' in request.POST:
        trade_summary_options = int(request.POST.get('trade_summary_options'))
    # Getting all relevant fields
    vrm_client, module, LAR, general_client, general_fund, general_entity, lar_queryset,\
        individual_set, hedge_set = _get_module_lar(request)

    # Overall (44) and related fund (43) overall must be excluded unless total is required
    try:
        overall_cp = Counterparties.objects.get(pk=44)
    except ObjectDoesNotExist as exception:
        overall_cp = None
    try:
        related_cp = Counterparties.objects.get(pk=43)
    except ObjectDoesNotExist as exception:
        related_cp = None

    query = {
        'trade__fund': fund,
        'trade__client': client
    }
    if entity and entity != '':
        query['trade__legal_entity'] = entity

    exclude_trade = {}
    if vrm_client.trade_summary_options:
        if trade_summary_options == 1:
            if client.lower() in ['cvc']:
                # Asset trade hedges
                pass
            elif client.lower() in ['permira']:
                # principal hedges
                # Expected to be inverse for value_date - tradedate > = 14 and value_date >=20
                exclude_trade['trade_id__in'] = [item['trade_id'] for item in
                                                 VrmHedge1CopyCopy.objects.annotate(diff=ExpressionWrapper(
                                                                                    F('value_date')-F('trade_date'),
                                                                                    output_field=DurationField())).
                                                 filter(client=client,
                                                        cancellation__iexact='FALSE',
                                                        dummy__iexact='FALSE',
                                                        diff__gte=datetime.timedelta(days=14),
                                                        value_date__day__gte=20).values('trade_id')]
        elif trade_summary_options == 2:
            if client.lower() in ['CVC']:
                # Subscription trade hedges
                pass
            elif client.lower() in ['permira']:
                # Interest hedges
                # Expected to value_date - tradedate > = 14 and value_date >=20
                query['trade_id__in'] = [item['trade_id'] for item in
                                         VrmHedge1CopyCopy.objects.annotate(diff=ExpressionWrapper(
                                             F('value_date')-F('trade_date'), output_field=DurationField())).filter(
                                             client=client,
                                             cancellation__iexact='FALSE',
                                             dummy__iexact='FALSE',
                                             diff__gte=datetime.timedelta(days=14),
                                             value_date__day__gte=20).values('trade_id')]

    mtm = VrmMtm1.objects.filter(**query)
    if len(exclude_trade) > 0:
        mtm = mtm.exclude(**exclude_trade)

    liquidity_all = LAR.objects.filter(**lar_queryset).exclude(
        counterparty__in=[overall_cp, related_cp])

    counterparties_query = VrmHedge1CopyCopy.objects.filter(
        client=client,
        **hedge_set,
        cancellation__iexact='FALSE',
        dummy__iexact='FALSE'
    )
    if include_expired_counterparties is False:
        counterparties_query = counterparties_query.filter(
            value_date__gte=date.today()
        )

    counterparties = [Counterparties.objects.get(counterparty_name=item['counterparty']) for item in
                        counterparties_query.values('counterparty').distinct()]
    counterparties = list(sorted(counterparties, key=operator.attrgetter('counterparty_name')))
    threshold = []
    total_lar = []
    individual_mtm = []

    if liquidity_all.count() != 0:
        for item in counterparties:
            try:
                if vrm_client.label in ['glendower_capital']:
                    cp_obj = getattr(module.models,'LegalCounterparty').objects.filter(**individual_set,
                                                                                       cp=Counterparties.
                                                                                       objects.get(
                                                                                           counterparty_name=
                                                                                           item.counterparty_name)
                                                                                       )
                else:
                    cp_obj = VrmCounterparty.objects.filter(fund=general_fund.fund_name,
                                                            client=general_client.client_name,
                                                            counterparty=item.counterparty_name)
                if len(cp_obj) > 0 and client != "UTAM":
                    if cp_obj[0].threshold and cp_obj[0].mta:
                        threshold.append(round((cp_obj[0].threshold-cp_obj[0].mta)/1000000, 2))
                    else:
                        if cp_obj[0].threshold:
                            threshold.append(round(cp_obj[0].threshold/1000000, 2))
                        elif cp_obj[0].mta:
                            threshold.append(round(cp_obj[0].mta/1000000, 2) * -1)
                        else:
                            threshold.append(None)
                else:
                    threshold.append(None)
                iliquidity_all = LAR.objects.filter(**lar_queryset,
                                                    counterparty=Counterparties.objects.get(
                                                                    counterparty_name=item.counterparty_name),
                                                    confidence_level__gt=0.9)
                iliquidity_at_risk_insert_timestamp = iliquidity_all.filter(
                    insert_timestamp=iliquidity_all.aggregate(
                        Max('insert_timestamp')
                    )['insert_timestamp__max']
                )

                iliquidity_at_risk_max_cl = iliquidity_at_risk_insert_timestamp.filter(
                    confidence_level=iliquidity_at_risk_insert_timestamp.aggregate(
                        Max('confidence_level')
                    )['confidence_level__max']
                )
                itotal = iliquidity_at_risk_max_cl.aggregate(lar__sum=Coalesce(Sum('lar'), V(0)))
                total_lar.append(round(itotal['lar__sum'] / 1000000, 2))

                try:
                    if general_entity:
                        agg_mtm = VrmAggregateMtm.objects.filter(
                            client_id=general_client.pk,
                            entity_id=general_entity.pk,
                            cp_id=Counterparties.objects.get(counterparty_name=item.counterparty_name).id
                        ).latest('valuation_date').agg_mtm
                    else:
                        agg_mtm = VrmAggregateMtm.objects.filter(
                            client_id=general_client.pk,
                            fund_id=general_fund.pk,
                            cp_id=Counterparties.objects.get(counterparty_name=item.counterparty_name).id
                        ).latest('valuation_date').agg_mtm
                except ObjectDoesNotExist:
                    agg_mtm = 0
                individual_mtm.append(
                    round(agg_mtm /1000000, 2))
            except Exception as e:
                print(traceback.format_exc())
                threshold = []
                total_lar = []
                individual_mtm = []

    try:
        mtm = mtm.filter(
            valuation_date=mtm.aggregate(Max('valuation_date'))['valuation_date__max'])
        mtm_summation = mtm.aggregate(Sum('mtm'))
        # mtm_sum = mtm_summation['mtm__sum']
        # resp_sum = mtm_sum / 10e5 if mtm_sum is not None else None
        mtm_values = mtm.values(
            'trade__trade_date',
            'trade__value_date',
            'trade__delivery_date',
            'trade__style',
            'trade__underlying',
            'trade__direction',
            'trade__notional_currency',
            'trade__notional_amounts',
            'trade__strike',
            'trade__premium',
            'trade__counterparty',
            'mtm',
            'net_mtm'
        )

        resp_data = list(mtm_values)

        # These extra checks are added to prevent an exception
        mtm_sum = mtm_summation['mtm__sum']
        resp_sum = mtm_sum/10e5 if mtm_sum is not None else None
        next_date = mtm.aggregate(Min('trade__value_date'))['trade__value_date__min']

    except Exception as e:
        print(traceback.format_exc())
        resp_data = []
        resp_sum = None
        next_date = None

    return JsonResponse({
        'data': resp_data,
        'next_date': next_date,
        'mtm_sum': resp_sum,
        'total_lar': total_lar,
        'threshold': threshold,
        'individual_mtm': individual_mtm,
        'summary_list_option': summary_list_option,
        'counterparties': [cp.counterparty_name for cp in counterparties]

    })


@login_required(login_url='login')
@user_is_allowed
def api_hedge_summary(request):
    if request.is_ajax() is False or request.method != 'POST':
        return HttpResponseBadRequest()
    resp_sum = 0
    fund = request.POST.get('fund')
    client = request.POST.get('client')

    if None in [fund, client]:
        return HttpResponseBadRequest()

    resp_data = list()
    value_date_list = VrmHedge1CopyCopy.objects.filter(client=client, fund=fund,
                                                       cancellation__iexact='FALSE', dummy__iexact='FALSE',
                                                       value_date__gte = date.today()).\
        values('value_date', 'underlying').order_by('underlying','value_date').distinct()
    resp_data1 = []
    total_summation = dict()
    try:
        for item in value_date_list:
            item_list = dict()
            item_list = item
            buy_amount = VrmHedge1CopyCopy.objects.filter(client=client, fund=fund, cancellation__iexact='FALSE',
                                                          dummy__iexact='FALSE', direction='Buy',
                                                          underlying=item['underlying'],
                                                          value_date=item['value_date']).\
                aggregate(Sum('notional_amounts'))
            sell_amount = VrmHedge1CopyCopy.objects.filter(client=client, fund=fund, cancellation__iexact='FALSE',
                                                           underlying=item['underlying'],
                                                           dummy__iexact='FALSE', direction='Sell',
                                                           value_date=item['value_date']).\
                aggregate(Sum('notional_amounts'))
            item_list['notional_amounts'] = (buy_amount['notional_amounts__sum'] or 0.0) - (
                    sell_amount['notional_amounts__sum'] or 0.0)
            buy_strike_sum = VrmHedge1CopyCopy.objects.filter(client=client,
                                                          fund=fund, cancellation__iexact='FALSE',
                                                          underlying=item['underlying'],
                                                          dummy__iexact='FALSE', direction='Buy',
                                                          value_date=item['value_date']).\
                aggregate(total=Sum(F('notional_amounts') / F('strike')))['total']
            sell_strike_sum = VrmHedge1CopyCopy.objects.filter(client=client,
                                                             fund=fund, cancellation__iexact='FALSE',
                                                             underlying=item['underlying'],
                                                             dummy__iexact='FALSE', direction='Sell',
                                                             value_date=item['value_date']). \
                aggregate(total=Sum(F('notional_amounts') / F('strike')))['total']
            strike_sum = (buy_strike_sum or 0.0) - (sell_strike_sum or 0.0)
            if item_list['notional_amounts'] == 0:
                strike_sum = 0.0
            else:
                strike_sum = abs(item_list['notional_amounts'] / strike_sum)
            item_list['strike'] = strike_sum
            if not item_list['notional_amounts'] == 0:
                if item['underlying'] in total_summation.keys():
                    total_summation[item['underlying']][0] += item_list['notional_amounts']
                    total_summation[item['underlying']][1] += item_list['notional_amounts']/item_list['strike']
                else:
                    total_summation[item['underlying']] = list()
                    total_summation[item['underlying']].append(item_list['notional_amounts'])
                    total_summation[item['underlying']].append(item_list['notional_amounts']/item_list['strike'])

                # item_list['settlement_limit'] = VrmCounterparty.objects.filter(client=client, fund=fund).\
                #     aggregate(Sum('settlement_line'))
                # if item_list['settlement_limit']['settlement_line__sum'] is None:
                #     item_list['settlement_limit'] = 0.0
                # else:
                #     ccy = VrmCounterparty.objects.filter(client=client, fund=fund).first().settlement_line_currency
                #
                #     notional_ccy = VrmHedge1CopyCopy.objects.filter(client=client, fund=fund, underlying=item['underlying']).\
                #         first().notional_currency
                    # if notional_ccy == ccy:
                    #     item_list['settlement_limit'] = item_list['settlement_limit']['settlement_line__sum']
                    # else:
                    #     if FixingdataTempBridgeTable.objects.filter(underlying=notional_ccy+ccy, security='Spot').\
                    #             exists():
                    #         fxtmp = FixingdataTempBridgeTable.objects.filter(underlying=notional_ccy+ccy, security='Spot').\
                    #             latest('valuation_date')
                    #         item_list['settlement_limit'] = item_list['settlement_limit']['settlement_line__sum']/fxtmp.mid
                    #     else:
                    #         fxtmp = FixingdataTempBridgeTable.objects.filter(underlying=ccy+notional_ccy,
                    #                                                          security='Spot').latest('valuation_date')
                    #         item_list['settlement_limit'] = item_list['settlement_limit']['settlement_line__sum']*fxtmp.mid

                resp_data1.append(item_list)
            total_summary_table = list()

        for keys, values in total_summation.items():
            resp_data.append({'section': 'big-title', 'underlying': keys})
            for item  in resp_data1:
                if item['underlying'] == keys:
                    resp_data.append(item)
            resp_data.append({'section': 'quarters', 'underlying': 'Total  in ' +
                                                                   VrmHedge1CopyCopy.objects.filter(client=client,
                                                                                                    fund=fund,
                                                                                                    underlying=keys).
                             first().notional_currency, 'notional_amounts': values[0], 'strike': values[0] / values[1]})
    except Exception as e:
        print(traceback.format_exc())


    return JsonResponse({
        'data': resp_data,

    })


@login_required(login_url='login')
@user_is_allowed
def api_settlement_summary(request):
    if request.is_ajax() is False or request.method != 'POST':
        return HttpResponseBadRequest()
    requestedfund = request.POST.get('fund')
    client = request.POST.get('client')
    fund = list()

    if None in [requestedfund, client]:
        return HttpResponseBadRequest()
    # add the requested fund
    fund.append(requestedfund)
    if request.POST['selectedFund_overall'] == '1':
        # we need to change the fund to list
        try:
            client = VRMClient.objects.get(name=request.POST['client'])
        except VRMClient.DoesNotExist:
            raise Exception('Unable to find the client to process!')
        try:
            module = __import__(client.name.lower())
        except Exception as e:
            try:
                module = __import__(client.label.lower())
            except Exception as e:
                raise Exception('Unable to find the client to process!')
        fund_association = getattr(module.models,'FundAssociation').objects.filter(fund=Funds.objects.get(fund_name =requestedfund)).\
            exclude(related_fund__isnull=True)
        for fund_item in fund_association:
            fund.append(Funds.objects.get(pk=fund_item.related_fund_id).fund_name)

    resp_data = []
    try:
        value_date_list = VrmHedge1CopyCopy.objects.filter(client=client, fund__in=fund,
                                                           cancellation__iexact='FALSE', dummy__iexact='FALSE',
                                                           value_date__gte = date.today()).\
            values('value_date', 'underlying','counterparty').order_by('value_date').distinct()
        # No need to worry here as hedge table has only single fund currency, but overall settlement
        # will have single currency


        value_notional_list = {}
        for item in value_date_list:
            item_list = item
            item_list['notional_amounts'] = 0.0
            for fund_item in fund:
                fund_currency = SchemaFundCp.objects.get(client=client, fund=fund_item,
                                                         counterparty=item['counterparty']).base_currency
                if item['underlying'][3:] == fund_currency:
                    buy_strike_sum = VrmHedge1CopyCopy.objects.filter(client=client,
                                                                      fund=fund_item, cancellation__iexact='FALSE',
                                                                      underlying=item['underlying'],
                                                                      counterparty=item['counterparty'],
                                                                      dummy__iexact='FALSE', direction='Buy',
                                                                      value_date=item['value_date']).\
                        aggregate(total=Sum(F('notional_amounts') * F('strike')))['total']
                    sell_strike_sum = VrmHedge1CopyCopy.objects.filter(client=client,
                                                                       fund=fund_item, cancellation__iexact='FALSE',
                                                                       underlying=item['underlying'],
                                                                       counterparty=item['counterparty'],
                                                                       dummy__iexact='FALSE', direction='Sell',
                                                                       value_date=item['value_date']). \
                        aggregate(total=Sum(F('notional_amounts') * F('strike')))['total']
                else:
                    buy_strike_sum = VrmHedge1CopyCopy.objects.filter(client=client,
                                                                      fund=fund_item, cancellation__iexact='FALSE',
                                                                      underlying=item['underlying'],
                                                                      counterparty=item['counterparty'],
                                                                      dummy__iexact='FALSE', direction='Buy',
                                                                      value_date=item['value_date']). \
                        aggregate(total=Sum(F('notional_amounts') / F('strike')))['total']
                    sell_strike_sum = VrmHedge1CopyCopy.objects.filter(client=client,
                                                                       fund=fund_item, cancellation__iexact='FALSE',
                                                                       underlying=item['underlying'],
                                                                       counterparty=item['counterparty'],
                                                                       dummy__iexact='FALSE', direction='Sell',
                                                                       value_date=item['value_date']). \
                        aggregate(total=Sum(F('notional_amounts') / F('strike')))['total']
                item_list['notional_amounts'] = item_list['notional_amounts'] + (buy_strike_sum or 0.0) - (sell_strike_sum or 0.0)
            if item['value_date'] in value_notional_list.keys():
                value_notional_list[item['value_date']].append(item_list)
            else:
                value_notional_list[item['value_date']] = [item_list]

        for value_date_key,item_list in value_notional_list.items():
            cp = dict()
            for value_list in item_list:
                if value_list['counterparty'] in cp.keys():
                    cp[value_list['counterparty']] += value_list['notional_amounts']
                else:
                    cp[value_list['counterparty']] = value_list['notional_amounts']

            for cp_key, value in cp.items():
                summation_data = dict()
                summation_data['value_date'] = value_date_key
                summation_data['counterparty'] = cp_key
                summation_data['notional_amounts'] = abs(value)
                summation_data['settlement_limit'] = VrmCounterparty.objects.filter(client=client, fund__in=fund). \
                    aggregate(Sum('settlement_line'))
                if summation_data['settlement_limit']['settlement_line__sum'] is None:
                    summation_data['settlement_limit'] = 0.0
                    summation_data['settlement_usage'] = 0.0
                else:
                    summation_data['settlement_limit'] = summation_data['settlement_limit']['settlement_line__sum']
                    if summation_data['settlement_limit'] == 0.0 or summation_data['notional_amounts'] == 0.0:
                        summation_data['settlement_usage'] = 0.0
                    else:
                        summation_data['settlement_usage'] = round(summation_data['notional_amounts']/summation_data['settlement_limit'],2) * 100
                resp_data.append(summation_data)

    except Exception as e:
        print(traceback.format_exc())
    return JsonResponse({
        'data': resp_data,

    })


@login_required(login_url='login')
@user_is_allowed
def api_settlement_summary(request):
    if request.is_ajax() is False or request.method != 'POST':
        return HttpResponseBadRequest()
    requestedfund = request.POST.get('fund')
    client = request.POST.get('client')
    fund = list()

    if None in [requestedfund, client]:
        return HttpResponseBadRequest()
    # add the requested fund
    fund.append(requestedfund)
    if request.POST['selectedFund_overall'] == '1':
        # we need to change the fund to list
        try:
            client = VRMClient.objects.get(name=request.POST['client'])
        except VRMClient.DoesNotExist:
            raise Exception('Unable to find the client to process!')
        try:
            module = __import__(client.name.lower())
        except Exception as e:
            try:
                module = __import__(client.label.lower())
            except Exception as e:
                raise Exception('Unable to find the client to process!')
        fund_association = getattr(module.models,'FundAssociation').objects.filter(fund=Funds.objects.get(fund_name =requestedfund)).\
            exclude(related_fund__isnull=True)
        for fund_item in fund_association:
            fund.append(Funds.objects.get(pk=fund_item.related_fund_id).fund_name)

    resp_data = []
    try:
        value_date_list = VrmHedge1CopyCopy.objects.filter(client=client, fund__in=fund,
                                                           cancellation__iexact='FALSE', dummy__iexact='FALSE',
                                                           value_date__gte = date.today()).\
            values('value_date', 'underlying','counterparty').order_by('value_date').distinct()
        # No need to worry here as hedge table has only single fund currency, but overall settlement
        # will have single currency


        value_notional_list = {}
        for item in value_date_list:
            item_list = item
            item_list['notional_amounts'] = 0.0
            for fund_item in fund:
                fund_currency = SchemaFundCp.objects.get(client=client, fund=fund_item,
                                                         counterparty=item['counterparty']).base_currency
                if item['underlying'][3:] == fund_currency:
                    buy_strike_sum = VrmHedge1CopyCopy.objects.filter(client=client,
                                                                      fund=fund_item, cancellation__iexact='FALSE',
                                                                      underlying=item['underlying'],
                                                                      counterparty=item['counterparty'],
                                                                      dummy__iexact='FALSE', direction='Buy',
                                                                      value_date=item['value_date']).\
                        aggregate(total=Sum(F('notional_amounts') * F('strike')))['total']
                    sell_strike_sum = VrmHedge1CopyCopy.objects.filter(client=client,
                                                                       fund=fund_item, cancellation__iexact='FALSE',
                                                                       underlying=item['underlying'],
                                                                       counterparty=item['counterparty'],
                                                                       dummy__iexact='FALSE', direction='Sell',
                                                                       value_date=item['value_date']). \
                        aggregate(total=Sum(F('notional_amounts') * F('strike')))['total']
                else:
                    buy_strike_sum = VrmHedge1CopyCopy.objects.filter(client=client,
                                                                      fund=fund_item, cancellation__iexact='FALSE',
                                                                      underlying=item['underlying'],
                                                                      counterparty=item['counterparty'],
                                                                      dummy__iexact='FALSE', direction='Buy',
                                                                      value_date=item['value_date']). \
                        aggregate(total=Sum(F('notional_amounts') / F('strike')))['total']
                    sell_strike_sum = VrmHedge1CopyCopy.objects.filter(client=client,
                                                                       fund=fund_item, cancellation__iexact='FALSE',
                                                                       underlying=item['underlying'],
                                                                       counterparty=item['counterparty'],
                                                                       dummy__iexact='FALSE', direction='Sell',
                                                                       value_date=item['value_date']). \
                        aggregate(total=Sum(F('notional_amounts') / F('strike')))['total']
                item_list['notional_amounts'] = item_list['notional_amounts'] + (buy_strike_sum or 0.0) - (sell_strike_sum or 0.0)
            if item['value_date'] in value_notional_list.keys():
                value_notional_list[item['value_date']].append(item_list)
            else:
                value_notional_list[item['value_date']] = [item_list]

        for value_date_key,item_list in value_notional_list.items():
            cp = dict()
            for value_list in item_list:
                if value_list['counterparty'] in cp.keys():
                    cp[value_list['counterparty']] += value_list['notional_amounts']
                else:
                    cp[value_list['counterparty']] = value_list['notional_amounts']

            for cp_key, value in cp.items():
                summation_data = dict()
                summation_data['value_date'] = value_date_key
                summation_data['counterparty'] = cp_key
                summation_data['notional_amounts'] = abs(value)
                summation_data['settlement_limit'] = 0
                summation_data['settlement_usage'] = 0
                temp_settlement_line = VrmCounterparty.objects.filter(client=client,
                                                                      fund__in=fund,
                                                                      counterparty=cp_key)
                if len(temp_settlement_line) > 0:
                    if (temp_settlement_line.first().settlement_line_currency and
                            (temp_settlement_line.first().base_currency !=
                             temp_settlement_line.first().settlement_line_currency)):
                        try:
                            temp_currency = temp_settlement_line.first().base_currency + temp_settlement_line.first().settlement_line_currency
                            if LatestSpotRelatedData.objects.filter(underlying=temp_currency, security='spot').exists():
                                temp_settlement_line.first().settlement_line /= LatestSpotRelatedData.objects.filter(underlying=temp_currency,
                                                                                                                   security='spot').latest('valuation_date').mid
                            else:
                                try:
                                    temp_currency = temp_settlement_line.first().settlement_line_currency +\
                                                    temp_settlement_line.first().base_currency
                                    temp_spot_rate = LatestSpotRelatedData.objects.filter(underlying=temp_currency,
                                                                                          security='spot').latest(
                                        'valuation_date').mid
                                    temp_settlement_line.first().settlement_line *= temp_spot_rate
                                except:
                                    logger.exception("Unable to match the underlying - {}, {}".format(client, fund))
                                    pass
                        except:
                            logger.exception("Unable to match the underlying {}, {}".format(client, fund))
                            pass

                    summation_data['settlement_limit'] = temp_settlement_line.first().settlement_line or 0
                    if summation_data['notional_amounts'] == 0 or summation_data['settlement_limit'] == 0:
                        summation_data['settlement_usage'] = 0.0
                    else:
                        summation_data['settlement_usage'] = round(
                            summation_data['notional_amounts'] / summation_data['settlement_limit'], 2) * 100
            resp_data.append(summation_data)
    except Exception as e:
        print(traceback.format_exc())
    return JsonResponse({
        'data': resp_data,

    })


@login_required(login_url='login')
@user_is_allowed
def ajax_deal_style_summary(request):
    if request.is_ajax() is False or request.method != 'POST':
        return HttpResponseBadRequest()

    requestedfund = request.POST.get('fund')
    client = request.POST.get('client')
    resp_data = None
    try:
        module = module_loader(VRMClient.objects.get(name=client))

        deal_style_summary = list(module.models.SummaryDealstyleFlexifield.objects.filter(
            summary_table__fund__fund_name=requestedfund
        ).values(
            'summary_table_id',
            'summary_table__fund__fund_name',
            'summary_table__deal__deal_name',
            'summary_table__duration',
            'summary_table__mtm',
            'style'
        ).distinct())

        flexifield_data = list(module.models.SummaryDealstyleFlexifield.objects.filter(
            summary_table__fund__fund_name=requestedfund
        ).values())

        deal_style_summary_data = list()
        for deal_style_row in deal_style_summary:
            new_row = {
                'fund_name': deal_style_row['summary_table__fund__fund_name'],
                'deal_name': deal_style_row['summary_table__deal__deal_name'],
                'duration': deal_style_row['summary_table__duration'],
                'mtm': deal_style_row['summary_table__mtm'],
                'style': Styles.objects.get(id=deal_style_row['style']).style,
            }
            for index, flexifield_row in enumerate(flexifield_data):
                if flexifield_row['summary_table_id'] == deal_style_row['summary_table_id'] and \
                 flexifield_row['style_id'] == deal_style_row['style']:
                    new_row[flexifield_row['field_name']] = flexifield_row['value']
            deal_style_summary_data.append(new_row)

        all_flex_fields = [item['field_name']
                           for item in list(module.models.SummaryDealstyleFlexifield.objects.filter(
                                summary_table__fund__fund_name=requestedfund,
                                value__isnull=False,
                            ).exclude(
                                value=0,
                            ).values('field_name').distinct())]

        resp_data = deal_style_summary_data
    except Exception as e:
        print(traceback.format_exc())
    finally:
        return JsonResponse({
            'data': resp_data,
            'flex_fields': all_flex_fields,
        })
