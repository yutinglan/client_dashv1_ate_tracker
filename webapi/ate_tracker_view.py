from rest_framework import serializers
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from .common import module_loader, module_loader_by_client_name
from client.models import VRMClient
from webapi.serializers import *
from webapi.utils import (Concat)
from itertools import groupby
from operator import itemgetter

class AteItemView(APIView):
    # permission_classes = (IsAuthenticated,)

    serializer_class = AteItemSerializer
    def get(self, request, *args, **kwargs):
        client_name = kwargs.get("client_name")
        module = module_loader_by_client_name(client_name)
        ate_item_table = getattr(module.models, 'AteItem')
        res = ate_item_table.objects.all()
        serializer = AteItemSerializer(res, many=True)
        return Response(serializer.data)

class LegalCounterPartyView(APIView):
    # permission_classes = (IsAuthenticated,)

    serializer_class = LegalCounterPartySerializer
    def get(self, request, *args, **kwargs):
        client_name = kwargs.get("client_name")
        module = module_loader_by_client_name(client_name)
        legal_counterparty_table = getattr(module.models, 'LegalCounterparty')
        res = legal_counterparty_table.objects.all()
        serializer = LegalCounterPartySerializer(res, many=True)
        return Response(serializer.data)


class AteConditionView(APIView):
    # permission_classes = (IsAuthenticated,)

    serializer_class = AteConditionSerializer
    def get(self, request, *args, **kwargs):
        client_name = kwargs.get("client_name")
        module = module_loader_by_client_name(client_name)
        ate_condition_table = getattr(module.models, 'AteCondition')
        res = ate_condition_table.objects.all()
        serializer = AteConditionSerializer(res, many=True)
        return Response(serializer.data)

class AteCalculationitemView(APIView):
    # permission_classes = (IsAuthenticated,)

    serializer_class = AteCalculationitemSerializer
    def get(self, request, *args, **kwargs):
        client_name = kwargs.get("client_name")
        csa_id = kwargs.get("csa_id")
        module = module_loader_by_client_name(client_name)
        ate_calculationitem_table = getattr(module.models, 'AteCalculationitem')
        res = ate_calculationitem_table.objects.all()
        serializer = AteCalculationitemSerializer(res, many=True)
        return Response(serializer.data)

class AteSettingView(APIView):
    # permission_classes = (IsAuthenticated,)

    serializer_class = AteSettingSerializer
    def get(self, request, *args, **kwargs):
        client_name = kwargs.get("client_name")
        csa_id = kwargs.get("csa_id")
        module = module_loader_by_client_name(client_name)
        ate_calculationitem_table = getattr(module.models, 'AteCalculationitem')
        res = ate_calculationitem_table.objects.all().filter(ate_condition_id__csa_id=csa_id)
        serializer = AteSettingSerializer(res, many=True)
        grouper = itemgetter("multiplier", "numerator", "ate_condition")
        result = []
        for key, grp in groupby(serializer.data, grouper):
            temp_dict = dict(zip(["multiplier", "numerator", "ate_condition"], key))
            temp_dict["ate_items"] = []
            for item in grp:
                temp_dict["ate_items"].append(item["ate_item"])
            result.append(temp_dict)
        return Response(result)
    

class CsaConditionView(APIView):
    # permission_classes = (IsAuthenticated,)

    serializer_class = CsaConditionSerializer
    def get(self, request, *args, **kwargs):
        client_name = kwargs.get("client_name")
        module = module_loader_by_client_name(client_name)
        csa_condition_table = getattr(module.models, 'CsaCondition')
        res = csa_condition_table.objects.all()
        serializer = CsaConditionSerializer(res, many=True)
        return Response(serializer.data)

class CsaCalculationitemView(APIView):
    # permission_classes = (IsAuthenticated,)

    serializer_class = CsaCalculationitemSerializer
    def get(self, request, *args, **kwargs):
        client_name = kwargs.get("client_name")
        module = module_loader_by_client_name(client_name)
        csa_calculationitem_table = getattr(module.models, 'CsaCalculationitem')
        res = csa_calculationitem_table.objects.all()
        serializer = CsaCalculationitemSerializer(res, many=True)
        return Response(serializer.data)

class CsaSettingView(APIView):
    # permission_classes = (IsAuthenticated,)

    serializer_class = CsaSettingSerializer
    def get(self, request, *args, **kwargs):
        client_name = kwargs.get("client_name")
        csa_id = kwargs.get("csa_id")
        module = module_loader_by_client_name(client_name)
        csa_calculationitem_table = getattr(module.models, 'CsaCalculationitem')
        res = csa_calculationitem_table.objects.all().filter(csa_condition__csa_id=csa_id)
        serializer = CsaSettingSerializer(res, many=True)
        grouper = itemgetter("positive", "csa_condition")
        result = []
        for key, grp in groupby(serializer.data, grouper):
            temp_dict = dict(zip(["positive", "csa_condition"], key))
            temp_dict["ate_items"] = []
            for item in grp:
                temp_dict["ate_items"].append(item["ate_item"])
            result.append(temp_dict)
        return Response(result)
        
        

class CsaThresholdView(APIView):
    # permission_classes = (IsAuthenticated,)

    serializer_class = CsaThresholdSerializer
    def get(self, request, *args, **kwargs):
        client_name = kwargs.get("client_name")
        module = module_loader_by_client_name(client_name)
        csa_threshold_table = getattr(module.models, 'CsaThreshold')
        res = csa_threshold_table.objects.all()
        serializer = CsaThresholdSerializer(res, many=True)
        return Response(serializer.data)


class FinancialStatementView(APIView):
    # permission_classes = (IsAuthenticated,)

    serializer_class = FinancialStatementSerializer
    def get(self, request, *args, **kwargs):
        client_name = kwargs.get("client_name")
        module = module_loader_by_client_name(client_name)
        financial_statement_table = getattr(module.models, 'FinancialStatement')
        res = financial_statement_table.objects.all()
        serializer = FinancialStatementSerializer(res, many=True)
        return Response(serializer.data)


    # def post(self, request, format=None):
    #     client_name = request.POST.get("client_name")
    #     client = VRMClient.objects.get(name=client_name)
    #     module = module_loader(client)
    #     ate_calculationitem_table = getattr(module.models, 'AteCalculationitem')
    #     res = ate_calculationitem_table.objects.all()
    #     serializer = AteConditionSerializer(res, many=True)
    #     return Response(serializer.data)

