from django.apps import AppConfig


class ClientContactConfig(AppConfig):
    name = 'client'
