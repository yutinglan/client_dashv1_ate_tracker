from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Q
from django.db.models.query import QuerySet
from django.views.generic import ListView
from django.contrib.auth.models import User, Group

from accounts.models import Profile
from client.dashboard.views import BaseDashboardView
from contacts.views import Counter
from .models import VRMClient
from django.shortcuts import get_object_or_404


class ClientContactPage(BaseDashboardView):
    template_name = 'client_contact/client_contact.html'

    def get_context_data(self, *, object_list=None, **kwargs):
        data = super(ClientContactPage, self).get_context_data(**kwargs)
        data['groups'] = Group.objects.all()
        c = Counter()
        data['counter'] = c
        data['client_data'] = VRMClient.objects.get(label=self.kwargs['label'])
        partner = Group.objects.get(name='Partner')
        data['partners'] = User.objects.filter(groups=partner,is_active=True)
        data['count'] = data['partners'].count()
        data['user_count'] = self.get_queryset().count()
        data['partner_instance'] = partner
        data['label'] = self.kwargs['label']
        partner = Group.objects.get(name='Partner')
        data['staff'] = User.objects.filter(~Q(groups=partner), is_staff=True,is_active=True)
        return data
#



