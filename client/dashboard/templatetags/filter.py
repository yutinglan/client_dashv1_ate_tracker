import itertools
from django import template
from database_schema.models import VrmHedge1CopyCopy
from datetime import date
import traceback
from client.models import VRMContentTab
from client.models import VRMClient

# Filter for if the user belongs to the group in question
register = template.Library()


@register.filter(name='has_group_check')
def has_group_check():

    return '''<li ><a href="{% url 'market-dashboard' label=label %}"><i class="fa fa-bar-chart-o"></i> <span class="nav-label">Market</span></a></li>
                    <!--<li><a data-toggle="tab" href="#dashboard5"><i class="fa fa-edit"></i> <span class="nav-label">Data Editor</span></a></li>-->
                    {% if user.is_active %}
                    <li ><a href="{% url 'client-contact-page' label=label %}"><i class="fa fa-phone-square"></i> <span class="nav-label">Contact</span></a></li>
                    {% endif %}'''


@register.simple_tag
def radiobutton(counterparty, client, user):


    condition = {}
    include_expired = VRMContentTab.objects.filter(
        label='include_expired_counterparties',
        client__name=client
    ).exists()

    if include_expired is False:
        condition['value_date__gte'] = date.today()

    ids = list(set(cp['counterparty'] for cp in VrmHedge1CopyCopy.objects.filter(
        client=client,
        fund=user.profile.recent_fund,
        dummy__iexact='FALSE',
        cancellation='FALSE',
        **condition,
    ).values('counterparty').distinct()))

    list_of_radio = """
        <div class="col-xs-3 col-sm-2 col-lg-1">
            <div>
                <input type="radio" name="optradio" id="all" value="all" checked>
                <div class="cp-sizer">
                    <img src="/static/img/counterparty/All.png" class="img-thumbnail">
                </div>
            </div>
        </div>
    """

    for item in ids:
        if isinstance(item, str):
            start_bit = '''
                <div class="col-xs-3 col-sm-2 col-lg-1">
                    <div>
                        <input type="radio" name="optradio" id="{}" value="{}">
                        <div class="cp-sizer">
                            <img src="/static/img/counterparty/{}.png" class="img-thumbnail">
                        </div>
                    </div>
                </div>
            '''.format(item,item, item)
            list_of_radio = list_of_radio + start_bit

    return list_of_radio


@register.simple_tag(name='tab_validation')
def tab_validation(permission_list, tab_name, user):
    condition = False

    found_permissions = [permission for permission in permission_list if tab_name in permission.name]
    if len(found_permissions) > 1:
        print('Warning: more than one permission found for ' + tab_name + '.')

    if len(found_permissions) != 0:
        permission = found_permissions[0]
        condition = permission.active or (permission.is_internal_editable and user.is_staff)

    return condition
