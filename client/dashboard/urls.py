"""
Urls for the dashboard template rendering
"""

from django.urls import path, re_path
from django.views.generic.base import RedirectView

from . import views

urlpatterns = [
    path('summary', views.SummaryDashboard.as_view(), name='summary-dashboard'),
    path('market', views.MarketDashboard.as_view(), name='market-dashboard'),
    path('counterparty', views.CounterpartyDashboard.as_view(), name='counterparty'),
    path('settlementrolling', views.SARDashboard.as_view(), name='settlementrolling'),
    path('risk_analytics', views.AnalyticsDashboard.as_view(), name='analytics'),
    path('mrisk_analytics', views.MultiLevelAnalyticsDashboard.as_view(), name='manalytics'),
    path('frisk_analytics', views.FundLevelAnalyticsDashboard.as_view(), name='fanalytics'),
    path('puc_analytics', views.PickUpCreditDashboard.as_view(), name='pucanalytics'),
    path('trade-db-info', views.TradeDatabaseInfoDashboard.as_view(), name='trade-database-info-dashboard'),
    path('hedge_ratio', views.HedgeDashboard.as_view(), name='hedge_ratio'),
    path('multi_hedge_ratio', views.MultiLevelHedgeDashboard.as_view(), name='multi_hedge_ratio'),
    path('fund_hedge_ratio', views.FundLevelHedgeDashboard.as_view(), name='fund_hedge_ratio'),
    path('data_editor', views.DataEditorDashboard.as_view(), name='data_editor'),
    path('gc_data_editor', views.GCDataEditorDashboard.as_view(), name='gc_data_editor'),
    path('portfolio', views.PortfolioDashboard.as_view(), name='portfolio'),
    path('futures_summary', views.FuturesSummaryDashboard.as_view(), name='futures_summary'),
    path('futures_counterparty', views.FutureCounterpartyDashboard.as_view(), name='future_counterparty'),
    path('future-trade-db-info', views.FutureTradeDatabaseInfoDashboard.as_view(),
                                                                name='future-trade-database-info-dashboard'),
    path('reporting', views.ReportingDashboard.as_view(), name='reporting'),
    path('forecast', views.ForecastDashboard.as_view(), name='forecast'),
    path('netexposure', views.ExposureDashboard.as_view(), name='netexposure'),
    path('agg_report', views.AggregateReportDashboard.as_view(), name='agg_report'),
    path('editor_tab', views.CostAnalyticsDashboard.as_view(),name='editor_tab'),
    path('ate_tracker', views.TBSSPA.as_view(), name='ate_tracker'),
    re_path(r'^.*$', RedirectView.as_view(url='summary', permanent=False), name='summary-redirect')
]
