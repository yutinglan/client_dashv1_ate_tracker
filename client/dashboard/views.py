import json
from database_schema.models import VrmHedge1CopyCopy, VrmFund, VrmMtm1
from general.models import Funds
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import DetailView, TemplateView
from client.models import VRMClient, VRMContentTab, VRMContentPermission,VRMDisplayTab
from accounts.views import ClientPermissionHandler
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, render


class BaseDashboardView(LoginRequiredMixin, ClientPermissionHandler, DetailView):
    context_object_name = 'client'
    model = VRMClient
    slug_field = 'label'
    slug_url_kwarg = 'label'
    login_url = 'login'

    def generate_client_configuration(self, client):
        """
        Here we can construct and output a configuration Javascript object.

        This can be used to add in Client configuration settings that the
        web client can feed into the API in a format that doesn't require
        Client conditional checks on the webapi.
        """
        include_expired_counterparties = VRMContentTab.objects.filter(
            label='include_expired_counterparties',
            client__name=client
        ).exists()

        obj = {
            "include_expired_counterparties": include_expired_counterparties
        }

        return json.dumps(obj)

    def get(self, request, *args, **kwargs):
        if self.request.user.is_active and not self.request.user.is_staff:
            # Condition to check whether the summary or any template doesnt exist but redirect them to only template
            # available for the client
            if VRMDisplayTab.objects.get(html_filename=self.template_name.split('/')[1])\
                    not in self.get_object().display_tab.all():
                if self.get_object().display_tab.all().count() > 0:
                    return HttpResponseRedirect('/' + self.get_object().label + '/' +
                                                self.get_object().display_tab.all()[0].label)
                else:
                    return HttpResponseRedirect('/' + self.get_object().label + '/summary')
        context = self.get_context_data()
        return self.render_to_response(context)

    def get_context_data(self, **kwargs):
        user = self.request.user
        obj = self.get_object()
        res_data = dict()
        res_data['fund'] = Funds.objects.filter(client__client_name=obj.name).exclude(dashboard_display=2).distinct().order_by('fund_name')
        res_data['client'] = obj
        res_data['label'] = obj.label
        res_data['pretty_name'] = obj.pretty_name
        res_data['allowed_tab_list'] = [tab.name for tab in obj.display_tab.all()]
        if not user.profile.recent_fund in [fund.fund_name for fund in res_data['fund']]:
            user.profile.recent_fund = res_data['fund'][0].fund_name
            user.save()
        res_data['selected_fund'] = user.profile.recent_fund

        content_tabs = VRMContentTab.objects.filter(client__name=obj.name)
        # TODO need to remove unnecessary logic need a revisit.
        if content_tabs:
            res_data['content_tab_list'] = [content.name for content in content_tabs]

        permissions = VRMContentPermission.objects.filter(client__name=obj)
        if permissions:
            res_data['permission_list'] = list(permissions)

        res_data['client_configuration'] = self.generate_client_configuration(
            res_data['client']
        )

        return res_data


class MarketDashboard(BaseDashboardView):
    template_name = 'dashboard/market_dashboard.html'


class SummaryDashboard(BaseDashboardView):
    template_name = 'dashboard/summary_dashboard.html'

    def get_context_data(self, **kwargs):
        context = super(SummaryDashboard, self).get_context_data()
        if self.get_object().entity_summary:
            self.template_name = 'dashboard/msummary_dashboard.html'
        return context


class HedgeDashboard(BaseDashboardView):
    template_name = 'dashboard/hedge_ratio.html'


class FundLevelHedgeDashboard(BaseDashboardView):
    template_name = 'dashboard/fundlevel_hedge_ratio.html'


class MultiLevelHedgeDashboard(BaseDashboardView):
    template_name = 'dashboard/multi_hedge_ratio.html'


class CounterpartyDashboard(BaseDashboardView):
    template_name = 'dashboard/counterparty.html'

    def get_context_data(self, **kwargs):
        context = super(CounterpartyDashboard, self).get_context_data()
        if self.get_object().entity_summary:
            self.template_name = 'dashboard/mcounterparty.html'
        if self.get_object().guarantor_summary:
            self.template_name = 'dashboard/gcounterparty.html'
        return context


class FutureCounterpartyDashboard(BaseDashboardView):
    template_name = 'dashboard/future_counterparty.html'

    def get_context_data(self, **kwargs):
        context = super(FutureCounterpartyDashboard, self).get_context_data()
        if self.get_object().entity_summary:
            self.template_name = 'dashboard/mcounterparty.html'
        if self.get_object().guarantor_summary:
            self.template_name = 'dashboard/gcounterparty.html'
        return context


class ForecastDashboard(BaseDashboardView):
    template_name = 'dashboard/forecast.html'

    def get_context_data(self, **kwargs):
        context = super(ForecastDashboard, self).get_context_data()
        context['counterparty'] = VrmHedge1CopyCopy.objects.filter(
            client=self.get_object().name, dummy__iexact='FALSE').values('counterparty')
        return context


class SARDashboard(BaseDashboardView):
    template_name = 'dashboard/sar_dashboard.html'

    def get_context_data(self, **kwargs):
        context = super(SARDashboard, self).get_context_data()
        context['counterparty'] = VrmHedge1CopyCopy.objects.filter(client=self.get_object().name,
                                                                   cancellation__iexact='FALSE',
                                                                   dummy__iexact='FALSE'
                                                                   ).values('counterparty').distinct()
        context['value_date'] = list(VrmHedge1CopyCopy.objects.filter(client=self.get_object().name,
                                                                      cancellation__iexact='FALSE',
                                                                      dummy__iexact='FALSE').
                                     values_list('value_date', flat=True).distinct().order_by('-value_date'))
        for item in range(len( context['value_date'])):
            context['value_date'][item] = context['value_date'][item].strftime("%Y-%m-%d")
        return context


class TradeDatabaseInfoDashboard(BaseDashboardView):
    template_name = 'dashboard/trade_database_info_dashboard.html'

    def get_context_data(self, **kwargs):
        context = super(TradeDatabaseInfoDashboard, self).get_context_data()

        context['value_date'] = VrmHedge1CopyCopy.objects.filter(fund=self.get_object().name,
                                                                 cancellation__iexact='FALSE',
                                                                 dummy__iexact='FALSE').values('value_date').\
                                                                 distinct().order_by('-value_date')
        return context


class FutureTradeDatabaseInfoDashboard(BaseDashboardView):
    template_name = 'dashboard/future_trade_database_info_dashboard.html'

    def get_context_data(self, **kwargs):
        context = super(FutureTradeDatabaseInfoDashboard, self).get_context_data()

        context['value_date'] = VrmHedge1CopyCopy.objects.filter(fund=self.get_object().name,
                                                                 cancellation__iexact='FALSE',
                                                                 dummy__iexact='FALSE').values('value_date').\
                                                                 distinct().order_by('-value_date')
        return context


class AnalyticsDashboard(BaseDashboardView):
    template_name = 'dashboard/analytics_dashboard.html'

    def get_context_data(self, **kwargs):
        context = super(AnalyticsDashboard, self).get_context_data()
        context['valuation_date'] = VrmMtm1.objects.filter(
                                                           trade__client=self.get_object().name,
                                                           trade__cancellation__iexact='FALSE',
                                                           trade__dummy__iexact='FALSE'
                                                           ).values('valuation_date').distinct().order_by('-valuation_date')
        return context


class MultiLevelAnalyticsDashboard(BaseDashboardView):
    template_name = 'dashboard/multi_analytics_dashboard.html'

    def get_context_data(self, **kwargs):
        context = super(MultiLevelAnalyticsDashboard, self).get_context_data()
        context['valuation_date'] = VrmMtm1.objects.filter(
                                                           trade__client=self.get_object().name,
                                                           trade__cancellation__iexact='FALSE',
                                                           trade__dummy__iexact='FALSE'
                                                           ).values('valuation_date').distinct().order_by('-valuation_date')
        return context


class FundLevelAnalyticsDashboard(BaseDashboardView):
    template_name = 'dashboard/fundlevel_analytics_dashboard.html'

    def get_context_data(self, **kwargs):
        context = super(FundLevelAnalyticsDashboard, self).get_context_data()
        context['valuation_date'] = VrmMtm1.objects.filter(
                                                           trade__client=self.get_object().name,
                                                           trade__cancellation__iexact='FALSE',
                                                           trade__dummy__iexact='FALSE'
                                                           ).values('valuation_date').distinct().order_by('-valuation_date')
        return context


class PickUpCreditDashboard(BaseDashboardView):
    template_name = 'dashboard/pickup_credit_dashboard.html'

    def get_context_data(self, **kwargs):
        context = super(PickUpCreditDashboard, self).get_context_data()
        return context


class DataEditorDashboard(BaseDashboardView):
    template_name = 'dashboard/data_editor.html'


class GCDataEditorDashboard(BaseDashboardView):
    template_name = 'dashboard/gc_data_editor.html'


class PortfolioDashboard(BaseDashboardView):
    template_name = 'dashboard/portfolio_dashboard.html'


class FuturesSummaryDashboard(BaseDashboardView):
    template_name = 'dashboard/futures_summary_dashboard.html'


class ReportingDashboard(BaseDashboardView):
    template_name = 'dashboard/reporting_dashboard.html'


class ExposureDashboard(BaseDashboardView):
    template_name = 'dashboard/netexposure.html'


class AggregateReportDashboard(BaseDashboardView):
    template_name = 'dashboard/aggregate_reports.html'

    def get_context_data(self, **kwargs):
        context = super(AggregateReportDashboard, self).get_context_data()
        context['valuation_date'] = VrmMtm1.objects.filter(
                                                           trade__client=self.get_object().name,
                                                           trade__cancellation__iexact='FALSE',
                                                           trade__dummy__iexact='FALSE'
                                                           ).values('valuation_date').distinct().order_by('-valuation_date')
        return context



class CostAnalyticsDashboard(BaseDashboardView):
    template_name = 'dashboard/cost_analytics_dashboard.html'

# class AteTracker(TemplateView):
#     template_name = 'dashboard/ate_tracker.html'


class TBSSPA(BaseDashboardView):
    template_name = 'dashboard/ate_tracker.html'
    
    def get_context_data(self, **kwargs):
        context = super(TBSSPA, self).get_context_data()
        return context

    

# def TBSSPA(request,label):
#     """
#     Renders the SPA
#     """
#     return render(request,  'dashboard/ate_tracker.html')
