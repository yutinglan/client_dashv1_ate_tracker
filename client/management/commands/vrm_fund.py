import os
import sys
from datetime import datetime
import boto3
import yaml
from client.models import  VRMClient
from general.models import Clients, Ccys, Funds, Fundtype
s3_client = boto3.client('s3')
try:
    s3_response_object = s3_client.get_object(Bucket='databaseconf', Key='django_config.yml')
    configfile = yaml.safe_load(s3_response_object['Body'])
except:
    print("Unable to rpcoess")
    configfile = []


print(configfile['DB'])

for item in configfile['DB']:
    try:
        client = VRMClient.objects.get(label=item['schemaname'])
    except VRMClient.DoesNotExist:
        print(item['schemaname'])
        continue

    try:
        module = __import__(client.name.lower())
    except Exception as e:
        try:
            module = __import__(client.label.lower())
        except Exception as e:
            raise Exception('Unable to find the client to process!')
    try:
        fund = getattr(module.models, 'Fund')
        db_client = Funds.objects.filter(client=Clients.objects.get(client_name=client.name)).values()
        for _db in db_client:
            try:
                fund_type = Fundtype.objects.get(id=_db['fundtype_id']).fund_type
            except Fundtype.DoesNotExist :
                fund_type = None
            try:
                obj, created = fund.objects.get_or_create(
                   fund_id=_db['id'],
                   fund_name=_db['fund_name'],
                   ccy=Ccys.objects.get(id=_db['ccy_id']),
                   fundtype=fund_type,
                   dashboard_display=_db['dashboard_display']
               )
            except Exception as e:
                print (e)
            fund.objects.filter(
                    fund_id=_db['id'],
                    fund_name=_db['fund_name'],
                    ccy=Ccys.objects.get(id=_db['ccy_id']),

                dashboard_display = _db['dashboard_display']
                ).update(fundtype=fund_type)
    except Exception  as e:
        print (e)
        print(client)

