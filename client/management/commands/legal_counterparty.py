from django.core.management.base import BaseCommand, CommandError
from database_schema.models import VrmHedge1CopyCopy , VrmMtm1, VrmFund, VrmAggregateMtm, VrmCounterparty
from general.models import Clients, Counterparties, Funds
from dateutil.relativedelta import relativedelta
from dss_schema.models import TempBridgeTable
import datetime
from client.models import VRMClient
import math
from django.db.models.aggregates import Sum
from django.db.models import F, Func
import traceback
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
import logging
logger = logging.getLogger(__name__)
from datetime import date, timedelta, timezone


class Command(BaseCommand):
    help = 'Associate the legal entity with counterparty for LAR'
    element = None

    def add_arguments(self, parser):

        parser.add_argument(
            '--client',
            dest='client',
            help='Choose the client',
        )
        # parser.add_argument('value_date today', nargs='+', type=int)

    def module_loader(self, client):
        """
        TODO: Clean this up

        Client parameter must be a VRMClient object
        """
        try:
            self.module = __import__(client.name.lower())
        except:
            try:
                self.module = __import__(client.label.lower())
            except:
                module = None

    def handle(self, *args, **options):
        self.client = options['client']
        client_list = VRMClient.objects.filter(name=self.client)
        for client in client_list:

            fund_list = SchemaFundCp.objects.filter(client=self.client)
            for fund in fund_list:
                print("Client: {}, Fund: {} ".format(client.name, fund.fund))
                counterparty = SchemaFundCp.objects.filter(client=client.name, fund=fund.fund)
                self.module_loader(client)
                general_client = Clients.objects.get(client_name=client.name)
                general_fund = Funds.objects.get(fund_name=fund.fund)
                for cp in counterparty:
                    general_cp = Counterparties.objects.get(counterparty_name=cp.counterparty)
                    lc_model = self.module.models.LegalCounterparty
                    entity_list = self.module.models.Entity.objects.all()
                    if len(entity_list) > 0:
                        for entity in entity_list:
                            try:
                                entityfund = self.module.models.Entityfund.objects.get(fund=general_fund, entity=entity)
                                if entityfund:
                                    print("Counterparty: {}, Entity: {} ".format(general_cp, entity))
                                    dt, created = lc_model.objects.get_or_create(
                                        fund=general_fund,
                                        cp=general_cp,
                                        entity=entity,
                                        short_code=fund.short_code,
                                        agreement_type=cp.agreement_type,
                                        margin_type=cp.margin_type,
                                        agreement_name=cp.agreement_name,
                                        base_currency=cp.base_currency,
                                        threshold=cp.threshold,
                                        counterparty_threshold=cp.counterparty_threshold,
                                        mta=cp.mta,
                                        csa_condition=cp.csa_condition,
                                        counterparty_mta=cp.counterparty_mta,
                                        settlement_line=cp.settlement_line,
                                        settlement_line_currency=cp.settlement_line_currency,
                                        rounding_amount=cp.rounding_amount,
                                        rounding_deliver_method=cp.rounding_deliver_method,
                                        rounding_return_method=cp.rounding_return_method,
                                        notification_time=cp.notification_time,
                                        notification_time_zone=cp.notification_time_zone,
                                        notification_calendars=cp.notification_calendars,
                                        eligible_collateral=cp.eligible_collateral,
                                        counterparty_short_code=cp.counterparty_short_code,
                                        settlement_date=cp.settlement_date,
                                        csa_link=cp.csa_link,
                                        cancellation= 0 if cp.cancellation=='FALSE' else 1,
                                        notional_limit=cp.notional_limit,
                                        notional_limit_currency=cp.notional_limit_currency or 0)
                            except ObjectDoesNotExist as exception:
                                pass
                    else:
                        dt, created = lc_model.objects.get_or_create(
                            fund=general_fund,
                            cp=general_cp,
                            short_code=fund.short_code,
                            agreement_type=cp.agreement_type,
                            margin_type=cp.margin_type,
                            agreement_name=cp.agreement_name,
                            base_currency=cp.base_currency,
                            threshold=cp.threshold,
                            counterparty_threshold=cp.counterparty_threshold,
                            mta=cp.mta,
                            csa_condition=cp.csa_condition,
                            counterparty_mta=cp.counterparty_mta,
                            settlement_line=cp.settlement_line,
                            settlement_line_currency=cp.settlement_line_currency,
                            rounding_amount=cp.rounding_amount,
                            rounding_deliver_method=cp.rounding_deliver_method,
                            rounding_return_method=cp.rounding_return_method,
                            notification_time=cp.notification_time,
                            notification_time_zone=cp.notification_time_zone,
                            notification_calendars=cp.notification_calendars,
                            eligible_collateral=cp.eligible_collateral,
                            counterparty_short_code=cp.counterparty_short_code,
                            settlement_date=cp.settlement_date,
                            csa_link=cp.csa_link,
                            cancellation=0 if cp.cancellation == 'FALSE' else 1,
                            notional_limit=cp.notional_limit,
                            notional_limit_currency=cp.notional_limit_currency or 0)


