import os
import sys
from datetime import datetime
import boto3
import yaml
import requests
from datetime import datetime
from dss_schema.models import LiveSpotRateFeed
from general.models import Underlyings
from django.core.management.base import BaseCommand



class Command(BaseCommand):
    help = 'Generate realised pnl for inidiviual client'

    def _process_feed(self):
        # trail license da95ed3888
        url2 = 'https://live-rates.com/rates?key=4e60e49c48'
        res = requests.get(url2, verify=False)
        if res.status_code == 200:
            fetched_list = []
            for item in res.json():
                fetched_list.append(item['currency'].replace('/',''))
                live_rate = LiveSpotRateFeed.objects.create(
                    currency=item['currency'],
                    rate=float(item['rate']) if item['rate'] != "n/a" else 0.0,
                    bid=float(item['bid']) if item['bid'] != "n/a" else 0.0,
                    ask = float(item['ask']) if item['ask'] != "n/a" else 0.0,
                    high = float(item['high']) if item['high'] != "n/a" else 0.0,
                    low = float(item['low']) if item['low'] != "n/a" else 0.0,
                    open = float(item['open']) if item['open'] != "n/a" else 0.0,
                    close = float(item['close']) if item['close'] != "n/a" else 0.0,
                    timestamp=datetime.fromtimestamp(int(item['timestamp'])/1000)
                )
                live_rate.save()
            remaining_list = Underlyings.objects.filter(asset_class='FX').exclude(underlying__in=fetched_list).values_list('underlying', flat=True)
            for item in remaining_list:
                # most of the current one has USD/XXX
                # https://github.com/Live-Rates/live-rates.com#cross-rates
                #
                split1 = item[0:3]
                split2 = item[3:]
                try:
                    split1_usd = LiveSpotRateFeed.objects.filter(currency='USD/' + item[0:3]).latest('timestamp')
                    split2_usd = LiveSpotRateFeed.objects.filter(currency='USD/' + item[3:]).latest('timestamp')
                    live_rate = LiveSpotRateFeed.objects.create(
                        currency=item[0:3]+'/'+item[3:],
                        rate=float(split2_usd.rate / split1_usd.rate ) or 0.0, bid=0.0, ask=0.0,high=0.0, low=0.0,
                        open=0.0,
                        close=0.0,
                        timestamp=datetime.now()
                    )
                    live_rate.save()
                except:
                    # Might be USD rate  will begin after 12 pm.
                    pass
    def handle(self, *args, **options):
        try:
            self._process_feed()
        except Exception as e:
            print(e)