import os
import sys
from datetime import datetime
import boto3
import yaml
from client.models import  VRMClient
from django.core.management.base import BaseCommand
from general.models import Clients, Ccys, Funds, Fundtype, SchemaFundCp, SchemaLegalCp


class Command(BaseCommand):
    help = 'Generate realised pnl for inidiviual client'

    def _process(self):
        s3_client = boto3.client('s3')
        try:
            s3_response_object = s3_client.get_object(Bucket='databaseconf', Key='django_config.yml')
            configfile = yaml.safe_load(s3_response_object['Body'])
        except:
            print("Unable to rpcoess")
            configfile = []
        print(configfile['DB'])
        legal_entity_rows = []
        counterparties = []
        for item in configfile['DB']:#['premierleague', 'standardbrands', 'hgcapital']:
            try:
                print("my clinet name {}".format(item['schemaname']))
                client = VRMClient.objects.get(label=str(item['schemaname']))
            except VRMClient.DoesNotExist:
                print("I am getting this error {}".format(item['schemaname']))
                continue


            try:
                module = __import__(client.label.lower())
            except Exception as e:
                continue
            try:
                legal_entities = []
                counterparties_fund = []
                if not module:
                    continue
                entity_all = getattr(module.models, 'Entity').objects.filter(cancellation=0)
                dict_obj = {}
                dict_obj_fund_cp = {}
                for entity_cp in (getattr(module.models,'LegalCounterparty').
                                  objects.filter(entity__in=entity_all.values_list('pk', flat=True), cancellation=0)):
                    for ecp in entity_cp.fund.all():
                        if entity_cp.entity.entity_name + '_' + ecp.fund_name not in dict_obj.keys():
                            dict_obj[entity_cp.entity.entity_name + '_' + ecp.fund_name] = 0
                            try:
                                legal_entities.append(
                                    {'client': client.name, 'legal_entity': entity_cp.entity.entity_name,
                                     'fund': ecp.fund_name, 'base_currency': str(entity_cp.base_currency)})
                            except:
                                pass
                        if client.name + '_' + ecp.fund_name + '_' + str(
                                entity_cp.cp) not in dict_obj_fund_cp.keys():
                            dict_obj_fund_cp[client.name + '_' + ecp.fund_name + '_' + str(entity_cp.cp)] = 0
                            try:
                                counterparties_fund.append(
                                    {'client': client.name, 'fund': ecp.fund_name, 'counterparty': str(entity_cp.cp),
                                     'base_currency': str(entity_cp.base_currency)})
                            except:
                                pass
                if len(legal_entities) > 0:
                    legal_entity_rows += legal_entities
                print("legal_entity_rows {}".format(len(legal_entity_rows)))

                if len(counterparties_fund) > 0:
                    counterparties += counterparties_fund
                print("legal_entity_rows {}".format(len(counterparties)))
            except Exception  as e:
                print (e)
                print(client)

        for item in legal_entity_rows:
            if SchemaLegalCp.objects.filter(client=item['client']).exists():
                SchemaLegalCp.objects.filter(client=item['client']).delete()
        for item in legal_entity_rows:
            obj,create = SchemaLegalCp.objects.get_or_create(
                client =item['client'],
                legal_entity=item['legal_entity'],
                base_currency=item['base_currency'],
                fund=item['fund']
            )
        for item in counterparties:
            if SchemaFundCp.objects.filter(client=item['client']).exists():
                SchemaFundCp.objects.filter(client=item['client']).delete()
        for item in counterparties:
            obj,create = SchemaFundCp.objects.get_or_create(
                client =item['client'],
                counterparty=item['counterparty'],
                base_currency=item['base_currency'],
                fund=item['fund']
            )

    def handle(self, *args, **options):
        try:
            self._process()
        except Exception as e:
            print(e)

