from django.core.management.base import BaseCommand, CommandError
from database_schema.models import VrmHedge1CopyCopy
from general.models import Clients, Counterparties, Funds
from dss_schema.models import TempBridgeTable2
from django.db.models.aggregates import Sum
import logging
from common import get_last_business_day
import pandas as pd
import datetime
import math
from datetime import date
logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = 'Generate realised pnl for inidiviual client'

    def add_arguments(self, parser):

        parser.add_argument('--all_date', action='store_true', dest="all_date", default=False)
        parser.add_argument(
            '--client',
                dest='client',
            help='Choose the client',
        )
        parser.add_argument(
            '--from_date',
            dest='from_date',
            default=None,
            help='Choose from date in YYYY-MM-DD format',
        )
        parser.add_argument(
            '--to_date',
            dest='to_date',
            default=None,
            help='Choose to date YYYY-MM-DD format',
        )
        parser.add_argument(
            '--lastday',
            dest='lastday',
            action='store_true',
            default=False,
            help='Choose to process last business day',
        )

    def getlast_business_date(self, date):
        ''' This funciton returns the last business day prior to the given date

            Input:
                year: an integer indicating year, the format should be xxxx
                month: an integer indicating month, if the month is Nov or Dec, its format should be 1x,
                        otherwise it should be x
                day: an integer indicating the day, if the day is less than 10, its format should be x,
                        otherwise it shoudl be 1x, 2x or 3x

            Ouput:
                LastBDate: the latest business day prior to the given date
        '''

        checkDate = date - datetime.timedelta(days=1)
        while not self.check_business_day(checkDate):
            checkDate = checkDate - datetime.timedelta(days=1)

        return checkDate

    def check_business_day(self, date):
        ''' This function aims to check whether a specific date is a business day

        '''
        return bool(len(pd.bdate_range(date, date)))


    def settlement_process(self, value_date):
        current_rollingvalues = self.list_rolling_period
        current_livespotrates =[]
        # fund_deselection = []
        # cp_deselection = []
        # f
        # fund_deselection = [fund.fund for fund in VrmFund.objects.filter(client=client.name).distinct()]
        # cp_deselection = [item['counterparty'] for item in VrmHedge1CopyCopy.objects.filter(client=client,
        #                                                                                     dummy__iexact='FALSE').values(
        #     'counterparty').distinct()]
        # value_date = None
        # value_date = VrmHedge1CopyCopy.objects.filter(client=client,
        #                                               cancellation__iexact='FALSE',
        #                                               dummy_iexact='FALSE').values('value_date').distinct()
        cash_settlement = list()
        cash_summation = list()
        for rolling_item in current_rollingvalues:
            rolling_table = dict(rolling_item)
            for trade_item in VrmHedge1CopyCopy.objects.filter(client=self.client,
                                                               cancellation__iexact='FALSE',
                                                               dummy__iexact='FALSE', fund=rolling_table['fund'],
                                                               notional_currency=rolling_table['notional_currency'],
                                                               underlying=rolling_table['underlying'],
                                                               concept=1,
                                                               counterparty=rolling_table['counterparty'],
                                                               value_date__in=[value_date]):
                    # exclude(fund__in=fund_deselection). \
                    # exclude(counterparty__in=cp_deselection):
                new_cash_settlment = dict()
                if trade_item.direction == 'Buy':
                    if trade_item.underlying[3:] == rolling_table['notional_currency']:
                        new_cash_settlment['notional_currency'] = trade_item.notional_currency
                        new_cash_settlment['notional_amounts'] = trade_item.notional_amounts
                        new_cash_settlment['in_notional_currency'] = trade_item.underlying[0:3]
                        # You are selling and the amount is different currency
                        new_cash_settlment['in_notional_amounts'] = -1 * trade_item.notional_amounts * (
                                1 / trade_item.strike)
                    else:
                        new_cash_settlment['notional_currency'] = trade_item.underlying[0:3]
                        new_cash_settlment['notional_amounts'] = trade_item.notional_amounts
                        new_cash_settlment['in_notional_currency'] = trade_item.underlying[3:]
                        # You are selling and the amount is different currency
                        new_cash_settlment[
                            'in_notional_amounts'] = -1 * trade_item.notional_amounts * trade_item.strike
                elif trade_item.direction == 'Sell':
                    if trade_item.underlying[3:] == rolling_table['notional_currency']:
                        new_cash_settlment['notional_currency'] = trade_item.notional_currency
                        new_cash_settlment['notional_amounts'] = trade_item.notional_amounts * (-1)
                        new_cash_settlment['in_notional_currency'] = trade_item.underlying[0:3]
                        # You are selling and the amount is different currency
                        new_cash_settlment['in_notional_amounts'] = trade_item.notional_amounts * (
                                1 / trade_item.strike)
                    else:
                        new_cash_settlment['notional_currency'] = trade_item.underlying[0:3]
                        new_cash_settlment['notional_amounts'] = trade_item.notional_amounts * (-1)
                        new_cash_settlment['in_notional_currency'] = trade_item.underlying[3:]
                        # You are selling and the amount is different currency
                        new_cash_settlment['in_notional_amounts'] = trade_item.notional_amounts * trade_item.strike
                new_cash_settlment['trade_id'] = trade_item.trade_id
                new_cash_settlment['underlying'] = trade_item.underlying
                new_cash_settlment['fund'] = trade_item.fund
                new_cash_settlment['counterparty'] = trade_item.counterparty
                cash_settlement.append(new_cash_settlment)
            new_cash_settlment = dict()
            # print(rolling_table['underlying'], value_date)
            print(rolling_table['underlying'],value_date)
            strike = float(TempBridgeTable2.objects.get(underlying=rolling_table['underlying'], security='Spot',
                                                        valuation_date=value_date, pricing_type='Mid').value)
            if rolling_table['direction'] == 'Sell':
                if rolling_table['underlying'][3:] == rolling_table['notional_currency']:
                    new_cash_settlment['notional_currency'] = rolling_table['underlying'][3:]
                    new_cash_settlment['notional_amounts'] = float(rolling_table['notional_amounts__newsum'])
                    new_cash_settlment['in_notional_currency'] = rolling_table['underlying'][0:3]
                    # You are selling and the amount is different currency
                    new_cash_settlment['in_notional_amounts'] = -1 * float(
                        rolling_table['notional_amounts__newsum']) * (1 / strike)
                else:
                    new_cash_settlment['notional_currency'] = rolling_table['underlying'][0:3]
                    new_cash_settlment['notional_amounts'] = float(rolling_table['notional_amounts__newsum'])
                    new_cash_settlment['in_notional_currency'] = rolling_table['underlying'][3:]
                    # You are selling and the amount is different currency
                    new_cash_settlment['in_notional_amounts'] = -1 * float(
                        rolling_table['notional_amounts__newsum']) * strike
            elif rolling_table['direction'] == 'Buy':
                if rolling_table['underlying'][3:] == rolling_table['notional_currency']:
                    new_cash_settlment['notional_currency'] = rolling_table['underlying'][3:]
                    new_cash_settlment['notional_amounts'] = float(rolling_table['notional_amounts__newsum']) * (-1)
                    new_cash_settlment['in_notional_currency'] = rolling_table['underlying'][0:3]
                    # You are selling and the amount is different currency
                    new_cash_settlment['in_notional_amounts'] = float(rolling_table['notional_amounts__newsum']) * (
                           1 / strike)
                else:
                    new_cash_settlment['notional_currency'] = rolling_table['underlying'][0:3]
                    new_cash_settlment['notional_amounts'] = float(rolling_table['notional_amounts__newsum']) * (-1)
                    new_cash_settlment['in_notional_currency'] = rolling_table['underlying'][3:]
                    # You are selling and the amount is different currency
                    new_cash_settlment['in_notional_amounts'] = float(
                        rolling_table['notional_amounts__newsum']) * strike
            new_cash_settlment['trade_id'] = "New Roll"
            new_cash_settlment['fund'] = rolling_table['fund']
            new_cash_settlment['underlying'] = rolling_table['underlying']
            new_cash_settlment['counterparty'] = rolling_table['counterparty']
            cash_settlement.append(new_cash_settlment)
        cash_sum = dict()
        for cash_in in cash_settlement:

            key_find_underlying = cash_in['fund'] + '_' + cash_in['counterparty'] + '_' + cash_in[
                'notional_currency']
            key_find_underlying2 = cash_in['fund'] + '_' + cash_in['counterparty'] + '_' + cash_in[
                'in_notional_currency']

            if key_find_underlying in cash_sum.keys():
                cash_sum[key_find_underlying]['ccy_amunt'] += cash_in['notional_amounts']
            else:
                cash_sum[key_find_underlying] = {}
                cash_sum[key_find_underlying]['fund'] = cash_in['fund']
                cash_sum[key_find_underlying]['counterparty'] = cash_in['counterparty']
                cash_sum[key_find_underlying]['ccy_amunt'] = cash_in['notional_amounts']
                cash_sum[key_find_underlying]['ccy'] = cash_in['notional_currency']
                cash_sum[key_find_underlying]['value_date'] = str(value_date)
            if key_find_underlying2 in cash_sum.keys():
                cash_sum[key_find_underlying2]['ccy_amunt'] += cash_in['in_notional_amounts']
            else:
                cash_sum[key_find_underlying2] = {}
                cash_sum[key_find_underlying2]['fund'] = cash_in['fund']
                cash_sum[key_find_underlying2]['counterparty'] = cash_in['counterparty']
                cash_sum[key_find_underlying2]['ccy_amunt'] = cash_in['in_notional_amounts']
                cash_sum[key_find_underlying2]['ccy'] = cash_in['in_notional_currency']
                cash_sum[key_find_underlying2]['value_date'] = value_date
        for key, value in cash_sum.items():
            if value['ccy_amunt'] != 0.0:
                cash_summation.append(value)

        for item in  cash_summation:
            try:
                module = __import__(self.client.lower())
            except Exception as e:
                 raise Exception('Unable to find the client to process!')
            try:
                realisednpnl = getattr(module.models, 'Realisedpnl')
                dt, created = realisednpnl.objects.get_or_create(fund_id=Funds.objects.get(fund_name=item['fund']).id,
                                                                 ccy=item['ccy'],
                                                                 ccy_amount=item['ccy_amunt'],
                                                                 value_date=item['value_date'],
                                                                 counterparty_id=Counterparties.
                                                                 objects.get(counterparty_name=item['counterparty']).id)

            except Exception as e:
                print(e)
        print(cash_summation)

    def trade_fetch(self):
        clients = [Clients.objects.get(client_name= self.client)]
        for client in clients:
            self.client = client.client_name
            if self.include_value_date:
                value_date_list = VrmHedge1CopyCopy.objects.filter(client=self.client ,
                                                                   cancellation__iexact='FALSE',
                                                                   dummy__iexact='FALSE',
                                                                   concept=1,
                                                                   value_date__lte=datetime.date.today()).values('value_date').distinct()
            elif self.from_date and self.to_date:
                value_date_list = VrmHedge1CopyCopy.objects.filter(client=self.client,
                                                                   cancellation__iexact='FALSE',
                                                                   dummy__iexact='FALSE',
                                                                   concept=1,
                                                                   value_date__gte=self.from_date,
                                                                   value_date__lte=self.to_date).values(
                                                                   'value_date').distinct()
            else:
                value_date_list = [{'value_date': get_last_business_day()}]
            # fund_deselection = [fund.fund_name for fund in Funds.objects.filter(client=client).distinct()]
            # cp_deselection = [item['counterparty'] for item in VrmHedge1CopyCopy.objects.filter(client=client.,
            #                                                                                     dummy__iexact='FALSE')
            #    .values('counterparty').distinct()]
            for value_date_item in value_date_list:
                value_date = value_date_item['value_date']
                trade_data = VrmHedge1CopyCopy.objects.filter(client=self.client ,
                                                              cancellation__iexact='FALSE', dummy__iexact='FALSE',
                                                              concept=1,
                                                              value_date__in=[value_date]). \
                    values('trade_id', 'client', 'fund', 'cp_ref', 'trade_date', 'value_date', \
                           'delivery_date', 'style', 'underlying', 'direction', \
                           'notional_currency', 'notional_amounts', 'strike', \
                           'premium', 'premium_payment_date', 'counterparty', \
                           'legal_entity', 'unwind', 'unwind_proceeds')#. \
                    # exclude(fund__in=fund_deselection). \
                    # exclude(counterparty__in=cp_deselection)
                rolling_period = VrmHedge1CopyCopy.objects.filter(client=self.client ,
                                                                  concept=1,
                                                                  cancellation__iexact='FALSE',
                                                                  dummy__iexact='FALSE',
                                                                  value_date__in=[value_date]). \
                    values('fund', 'underlying', 'counterparty', \
                           'notional_currency').distinct()#. \
                    # exclude(fund__in=fund_deselection). \
                    # exclude(counterparty__in=cp_deselection).distinct()
                self.list_rolling_period = list()
                if len(trade_data) > 0:
                    value_date = trade_data[0]['value_date']
                else :
                    raise ValueError("Unable to process trade data")
                for item in rolling_period:
                    rolling_table = dict(item)
                    rolling_table.update({'strike': 0.00, 'value_date': value_date})

                    buy_direction = VrmHedge1CopyCopy.objects.filter(client=self.client ,
                                                                     cancellation__iexact='FALSE',
                                                                     dummy__iexact='FALSE', fund=item['fund'],
                                                                     direction__iexact='Buy',
                                                                     concept=1,
                                                                     underlying=item['underlying'],
                                                                     notional_currency=item['notional_currency'],
                                                                     counterparty=item['counterparty'],
                                                                     value_date__in=[value_date]) \
                        .aggregate(Sum("notional_amounts"))
                    sell_direction = VrmHedge1CopyCopy.objects.filter(client=self.client ,
                                                                      cancellation__iexact='FALSE',
                                                                      dummy__iexact='FALSE', fund=item['fund'],
                                                                      notional_currency=item['notional_currency'],
                                                                      underlying=item['underlying'],
                                                                      direction='Sell',
                                                                      concept=1,
                                                                      counterparty=item['counterparty'],
                                                                      value_date__in=[value_date]) \
                        .aggregate(Sum("notional_amounts"))
                    rolling_amount = (buy_direction['notional_amounts__sum'] or 0.00) - (
                                sell_direction['notional_amounts__sum'] or 0.00)
                    if rolling_amount > 0.00:
                        rolling_table['direction'] = 'Buy'
                    else:
                        rolling_table['direction'] = 'Sell'
                    rolling_table['notional_amounts__sum'] = math.fabs(rolling_amount)
                    rolling_table['notional_amounts__newsum'] = math.fabs(rolling_amount)
                    self.list_rolling_period.append(rolling_table)
                try:
                    self.settlement_process(value_date)
                except:
                    print("I am trying to find the reason")

    def handle(self, *args, **options):
        try:
            self.client = options['client']
            self.include_value_date = options['all_date']
            self.from_date = options['from_date']
            self.to_date = options['to_date']
            if options['lastday']:
                self.lastday = self.getlast_business_date(date.today())
                self.from_date = self.lastday
                self.to_date = self.lastday
                print(options['lastday'],  self.lastday)
            self.trade_fetch()
        except Exception as e:
            print(e)


