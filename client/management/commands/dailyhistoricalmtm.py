from django.core.management.base import BaseCommand, CommandError
from database_schema.models import VrmHedge1CopyCopy , VrmMtm1, VrmFund, VrmAggregateMtm
from general.models import Clients, Counterparties, Funds
from dateutil.relativedelta import relativedelta
from dss_schema.models import TempBridgeTable
import datetime
from client.models import VRMClient
import math
from django.db.models.aggregates import Sum
from django.db.models import F, Func
import traceback
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
import logging
logger = logging.getLogger(__name__)
from datetime import date, timedelta, timezone


class Command(BaseCommand):
    help = 'Closes the specified poll for voting'
    element = None

    def add_arguments(self, parser):
        pass
        # parser.add_argument('value_date today', nargs='+', type=int)

    def mtm_process(self, client, fund, counterparty=None):
        res_data = {}
        data_list = []
        target_dt = date.today() + relativedelta(months=-6)

        res_data['start_month'] = target_dt
        res_data['end_month'] = date.today()

        today = date.today()
        if counterparty == None:
            counterparty= 0
        else:
            counterparty = Counterparties.objects.get(counterparty_name=counterparty).id
        while target_dt < today:
            target_mtm = VrmMtm1.objects.filter(
                trade__in=self.elements,
                valuation_date=target_dt
            )

            if target_mtm.exists() and target_mtm.aggregate(Sum('mtm'))["mtm__sum"]:
                data_list.append([
                    target_dt,
                    int(round(target_mtm.aggregate(Sum('mtm'))["mtm__sum"]))
                ])
                p, created = VrmAggregateMtm.objects.get_or_create(
                    client_id=client.id,
                    fund_id=fund.id,
                    cp_id=counterparty,
                    valuation_date=target_dt,
                    agg_mtm=int(round(target_mtm.aggregate(Sum('mtm'))["mtm__sum"]))
                )

            target_dt = target_dt + timedelta(days=1)

        res_data['hmtm'] = data_list

    def handle(self, *args, **options):
        client_list = Clients.objects.all()
        for client in client_list:

            fund_list = Funds.objects.filter(client=client.id)
            for fund in fund_list:
                counterparties_list = list(
                    set(cp['counterparty'] for cp in
                        VrmHedge1CopyCopy.objects.filter(
                            client=client.client_name,
                            fund=fund.fund_name,
                            dummy__iexact='FALSE',
                            value_date__gte=date.today()
                    ).values('counterparty').distinct()))
                for counterparty in counterparties_list:
                    try:
                        self.elements = list(set(
                                cp['trade_id'] for cp in VrmHedge1CopyCopy.objects.filter(
                                    client=client.client_name,
                                    counterparty=counterparty,
                                    fund=fund.fund_name
                                ).values('trade_id')
                            ))
                        self.mtm_process(client, fund, counterparty)
                    except ObjectDoesNotExist as exception:
                        print(traceback.format_exc())

                self.elements = list(set(
                    cp['trade_id'] for cp in VrmHedge1CopyCopy.objects.filter(
                        client=client.client_name,
                        fund=fund.fund_name
                    ).values('trade_id')
                ))
                self.mtm_process(client, fund)
