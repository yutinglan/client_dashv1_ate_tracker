from django.core.management.base import BaseCommand, CommandError
from database_schema.models import VrmHedge1CopyCopy
# from general.models import Clients, Counterparties, Funds
# from dateutil.relativedelta import relativedelta
# from django.db.models import F, Func
# import traceback
# from django.core.exceptions import ObjectDoesNotExist
import logging
logger = logging.getLogger(__name__)
# from datetime import date, timedelta, timezone


class Command(BaseCommand):
    help = 'No help required run by daily job checking few condition in DB'
    element = None

    def add_arguments(self, parser):
        pass
        # parser.add_argument('value_date today', nargs='+', type=int)

    def handle(self, *args, **options):
        trade_data = VrmHedge1CopyCopy.objects.filter(dummy__iexact='FALSE',cancellation__iexact='FALSE')

        for trade_d in trade_data:
            trade = VrmHedge1CopyCopy.objects.get(trade_id=trade_d.trade_id)
            if trade.style == 'Call' or trade.style =='Put':
                if trade.option_exercise_type is None:
                    logger.debug("trade_id option {} ".format(trade.trade_id))
                    print ("trade_id option : {} ".format(trade.trade_id))
                    trade.option_exercise_type = 'European'
            if trade.unwind == 'TRUE':
                if trade.npv_date is None:
                    logger.debug("trade_id delivery_date: {}".format(trade.trade_id))
                    print("trade_id delivery_date: {}".format(trade.trade_id))
                    trade.npv_date = trade.delivery_date
            if trade.legal_entity is None:
                logger.debug("trade_id legal entity: {}".format(trade.trade_id))
                print("trade_id legal entity: {}".format(trade.trade_id))
                trade.legal_entity = trade.fund

