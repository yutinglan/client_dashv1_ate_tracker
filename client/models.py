from django.db import models
from django.utils.translation import ugettext_lazy as _
from jsonfield import JSONField


class VRMDisplayTab(models.Model):
    name = models.CharField(_("display_tab_name"), unique=True, max_length=120)
    label = models.CharField(_("display_tab_label"), max_length=30, unique=True, null=True)
    html_filename = models.CharField(_("display_tab_filename"), max_length=120, unique=True, null=True)

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'links_vrmdisplaytab'


class VRMClient(models.Model):
    name = models.CharField(_("client_name"),  unique=True, max_length=120)
    label = models.CharField(max_length=120, unique=True, null=True)
    pretty_name = models.CharField(max_length=100, null=True, blank=True)
    display_tab = models.ManyToManyField(VRMDisplayTab,  verbose_name=_('display_list'), blank=True,)
    hedging_view = models.BooleanField(default=False,
                                       help_text="Setting to display Hedging value in Summary Dashboard")
    hedging_multilevel_view = models.BooleanField(default=False,
                                                  help_text="Setting to display that requires in \
                                                  Summary Dashboard to have multilevel Hedging Level display")
    lar_view = models.BooleanField(default=True, help_text="Setting to display LAR value in Summary Dashboard page")
    lar_multilevel_view = models.BooleanField(default=False,
                                              help_text="Setting to display multilevel LAR display in the  \
                                              Summary Dashboard page ")
    hedge_settlement_summary = models.BooleanField(default=False,
                                                   help_text="Setting to display Hedge and \
                                                   Settlement Summary in Summary Dashboard page ")
    avg_notional_amount = models.BooleanField(default=False,
                                              help_text="Setting to display Average Notional Amounts\
                                              in Counterparty page ")
    display_mtm_only = models.BooleanField(default=False,
                                           help_text="Setting to display only MTM Calculation in Analytical page")

    moic = models.BooleanField(default=False,
                               help_text="Setting to display MOIC in Analytical page")
    realisedpnl = models.BooleanField(default=False,
                                      help_text="Setting to display MOIC in Analytical page")
    investor = models.BooleanField(default=False,
                                   help_text="Setting to display MOIC in Analytical page")
    trade_summary_options = models.BooleanField(default=False,
                                                help_text="Setting to display different options for trade display")
    in_the_money_probability = models.BooleanField(default=True,
                                                   help_text="Setting to display in the money probability")
    entity_summary = models.BooleanField(default=False,
                                         help_text="Setting to Entity driven summary / Counterparty dashboard")
    interest_curve = models.BooleanField(default=False,
                                         help_text="Setting to Interest Curve in Market dashboard")
    process_pnl = models.BooleanField(default=False,
                                      help_text="Setting to process the pnl for the client once the new \
                                                   trade has been booked")
    guarantor_summary = models.BooleanField(default=False,
                                            help_text="Setting to Guarantor driven summary / Counterparty dashboard")

    portfolio = models.BooleanField(default=False,
                                    help_text="Setting to  display Portfolio Analysis in Risk analytics")
    irr = models.BooleanField(default=True,
                                    help_text="Setting to  display IRR Analysis in Risk analytics")
    outstanding_report = models.BooleanField(default=True,
                                    help_text="Setting to  display outstanding Report in Aggregate Reports")

    parents_summary_report = models.BooleanField(default=False,
                                             help_text="Setting to  display Parents Summary Report in Aggregate Reports")


    def __str__(self):
        return self.name

    class Meta:
        db_table = 'links_vrmclient'


class VRMCommonSettings(models.Model):
    hedge_value_dropdown = JSONField()


class VRMContentTab(models.Model):
    name = models.CharField(_("display_tab_name"), unique=True, max_length=120)
    label = models.CharField(_("display_tab_label"), max_length=30, unique=True, null=True)
    client = models.ManyToManyField(VRMClient, verbose_name=_('contenttab_clientlist'), blank=True, default=None,)

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'links_vrmcontenttab'


class VRMContentPermission(models.Model):
    name = models.CharField(_("permission_tab_name"), unique=True, max_length=60)
    active = models.BooleanField(_('active'), default=False,
                                help_text=_('decide whether or not this item gets pasted onto the website'))
    is_internal_editable = models.BooleanField(_('internal editable'), default=False,
                                               help_text=_(
                                                   'decide whether or not this item gets pasted onto the website'))

    client = models.ForeignKey(VRMClient, verbose_name=_('permission_clientlist'), blank=True, null=True, default=None,
                               on_delete = models.SET_NULL )

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'links_vrmcontentpermission'


class VRMConfig(models.Model):
    name = models.CharField(_("name"), unique=True, max_length=60)
    active = models.BooleanField(_('active'), default=False,
                                help_text=_('decide whether or not this item gets pasted onto the website'))

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'links_vrmconfig'