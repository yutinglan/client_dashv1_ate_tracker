from django.urls import path, re_path, include

from client import views

urlpatterns = [
    re_path(r'^(?P<label>\D+)/contacts/', views.ClientContactPage.as_view(), name='client-contact-page'),
    re_path(r'^(?P<label>\D+)/', include('client.dashboard.urls'))
]