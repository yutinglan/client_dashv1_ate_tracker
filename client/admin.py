from django.contrib import admin
from .models import VRMClient, VRMDisplayTab, VRMContentTab, VRMContentPermission, VRMCommonSettings, VRMConfig

# Register your models here.
@admin.register(VRMClient)
class VRMClient(admin.ModelAdmin):
    list_display = (
        'name',
        'label'
   )
    search_fields = ('name',)
    ordering = ('name',)
    filter_horizontal = ('display_tab',)


@admin.register(VRMDisplayTab)
class VRMDisplayTab(admin.ModelAdmin):
    list_display = (
        'name',
        'label',
        'html_filename'
   )
    search_fields = ('name','html_filename')
    ordering = ('name',)


@admin.register(VRMContentTab)
class VRMContentTab(admin.ModelAdmin):
    list_display = (
        'name',
        'label',

   )
    search_fields = ('name','label')
    ordering = ('name',)

@admin.register(VRMCommonSettings)
class VRMCommonSettings(admin.ModelAdmin):
    list_display = (
        'hedge_value_dropdown',
   )
    #search_fields = ('name','label')
    ordering = ('hedge_value_dropdown',)


@admin.register(VRMContentPermission)
class VRMContentPermissionTab(admin.ModelAdmin):
    list_display = (
        'name',
        'active',
        'is_internal_editable',
   )
    ordering = ('active','name')

@admin.register(VRMConfig)
class VRMConfigTab(admin.ModelAdmin):
    list_display = (
        'name',
        'active',
   )
    ordering = ('active','name')