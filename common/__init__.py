from pandas.tseries.holiday import (
    AbstractHolidayCalendar, DateOffset, EasterMonday,
    GoodFriday, Holiday, MO,
    next_monday, next_monday_or_tuesday)
from datetime import date
from pandas.tseries.offsets import CDay


class EnglandAndWalesHolidayCalendar(AbstractHolidayCalendar):
   rules = [
       Holiday('New Years Day', month=1, day=1, observance=next_monday),
       GoodFriday,
       EasterMonday,
       Holiday('Early May bank holiday',
               month=5, day=1, offset=DateOffset(weekday=MO(1))),
       Holiday('Spring bank holiday',
               month=5, day=31, offset=DateOffset(weekday=MO(-1))),
       Holiday('Summer bank holiday',
               month=8, day=31, offset=DateOffset(weekday=MO(-1))),
       Holiday('Christmas Day', month=12, day=25, observance=next_monday),
       Holiday('Boxing Day',
               month=12, day=26, observance=next_monday_or_tuesday)
   ]


def get_last_business_day():
    business = CDay(calendar=EnglandAndWalesHolidayCalendar())
    #return (date.today() - 0*business).date()
    return (date.today() - 1*business).date()
