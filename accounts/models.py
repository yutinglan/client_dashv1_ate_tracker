from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils.translation import ugettext_lazy as _
from django.utils.timezone import now

from client.models import VRMClient


class Profile(models.Model):
    REG_TYPE={('NIDN','NIDN'),
              ('CCPT','CCPT'),
              ('CONCAT', 'CONCAT')}
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    email_confirmed = models.BooleanField(default=False)
    trade_name = models.CharField(max_length=100, unique=True, null=True, blank=True)
    image_url = models.CharField(max_length=256, null=True, blank=True)
    # image = models.ImageField(upload_to=)
    clients = models.ManyToManyField(VRMClient, blank=True)
    recent_fund = models.CharField(max_length=100, unique=True, null=True, blank=True)
    trade_id_number = models.PositiveIntegerField(unique=True, null=True, blank=True)
    regulation_id = models.CharField(max_length=256, null=True, blank=True)
    regulation_id_type =  models.CharField(max_length=12,  choices=REG_TYPE, default='NIDN')
    last_password_change =  models.DateTimeField(_('last password change'), auto_now_add=True)
    two_factor_value = models.CharField(max_length=12, null=False, blank=False, default='99999999')


    def __str__(self):
        if not self.user.first_name == '' and not self.user.last_name == '':
            return self.user.first_name + ' ' + self.user.last_name
        elif not self.user.first_name == '':
            return self.user.first_name
        else:
            return self.user.username

@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()
