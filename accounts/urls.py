from django.urls import path, include
from .views import account_activation_sent, activate, SignUp, CustomLoginView, ClientPasswordView
from django.contrib.auth import views


urlpatterns = [
    path('signup/', SignUp.as_view(), name='signup'),
    path('account_activation_sent/', account_activation_sent, name='account_activation_sent'),
    path('activate/<uidb64>/<token>/', activate, name='activate'),
    path('login/', CustomLoginView.as_view(), name='login'),
    path('logout/', views.LogoutView.as_view(extra_context={'title': ''}), name='logout'),

    path('password_change/', views.PasswordChangeView.as_view(), name='password_change'),
    path('password_change/done/', ClientPasswordView.as_view(), name='password_change_done'),
    path('password_logout/', views.LogoutView.as_view( extra_context={'title': 'Password reset successfully. Please login again'}), name='password_logout'),

    path('password_reset/', views.PasswordResetView.as_view(), name='password_reset'),
    path('password_reset/done/', views.PasswordResetDoneView.as_view(), name='password_reset_done'),
    path('reset/<uidb64>/<token>/', views.PasswordResetConfirmView.as_view(), name='password_reset_confirm'),
    path('reset/done/', views.PasswordResetCompleteView.as_view(), name='password_reset_complete'),

]


