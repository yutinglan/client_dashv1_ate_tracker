from django.contrib.auth import login
from django.contrib.sites.shortcuts import get_current_site
from django.core.mail import EmailMessage
from django.shortcuts import render, redirect
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.template.loader import render_to_string
from django.http import Http404
from accounts.models import Profile
from .forms import SignUpForm, CustomAuthenticationForm
from .tokens import account_activation_token
from django.views import generic
from django.contrib.auth.views import LoginView
from client.models import VRMClient, VRMDisplayTab
from django.contrib.auth.mixins import AccessMixin
from database_schema.models import VrmFund
from general.models import Funds
from django.contrib.auth.models import User
from django.contrib.auth.views import PasswordContextMixin
from django.contrib.auth.decorators import login_required
from django.views.generic.base import TemplateView
from django.utils.timezone import now
from django.utils.translation import gettext_lazy as _
from django.utils.decorators import method_decorator


class SignUp(generic.CreateView):
    form_class = SignUpForm
    template_name = 'signup.html'

    def form_valid(self, form):
        if not (self.request.user.is_staff and self.request.user.is_active):
            error = True
            self.form_class.add_error(form, 'first_name',
                                      'Please contact Validus admin team')
            return super(SignUp, self).form_invalid(form)
        else:
            user = form.save(commit=False)
            trade_name = form.cleaned_data['trade_name']
            error = False
            if Profile.objects.filter(trade_name=trade_name).count() > 0:
                error = True
        if not error:
            # the first save is here to create the profile for the user/
            user.save()
            user.profile.trade_name = trade_name
            user.is_active = False
            user.save()
            current_site = get_current_site(self.request)
            message = render_to_string('acc_active_email.html', {
                'user': user,
                'domain': current_site.domain,
                'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                'token': account_activation_token.make_token(user),
            })
            mail_subject = 'Confirm Your Email Address'
            to_email = 'techteam@validusrm.com'  # TODO set the email that should receive these activation emails
            email = EmailMessage(mail_subject, message, to=[to_email])
            email.send()
            return render(self.request, 'SignUpActivationNotification.html')
        else:
            self.form_class.add_error(form, 'trade_name', 'Trade name already in use, please try again with a new trade name')
            return super(SignUp, self).form_invalid(form)


def account_activation_sent(request):
    return render(request, 'account_activation_sent.html')


def activate(request, uidb64, token, backend='django.contrib.auth.backends.ModelBackend'):
    try:
        # uid = force_text(urlsafe_base64_decode(uidb64))
        uid = urlsafe_base64_decode(uidb64)
        user = User.objects.get(pk=uid)
    except (TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None

    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.profile.email_confirmed = True
        user.save()
        login(request, user, backend)
        return redirect('logout')
    else:
        return render(request, 'account_activation_invalid.html')


class CustomLoginView(LoginView):
    redirect_authenticated_user = True
    form_class = CustomAuthenticationForm

    def get_success_url(self):
        """Return the user-originating redirect URL if it's safe."""
        redirect_string = None
        if (now().today().date() -self.request.user.profile.last_password_change.date()).days > 180 :
            redirect_string = '/accounts/password_change/'
            return redirect_string
        if self.request.user.is_staff:
            for item in self.request.body.decode("utf-8").split('&'):
                check_next = item.split('=')
                if check_next[0] == 'next':
                    redirect_string = check_next[1].replace('%2F', '/')
                    break
            if redirect_string is None or redirect_string == '':
                if self.request.user.profile.clients.all().exists():
                    if  redirect_string is None:
                        redirect_string = '/' + self.request.user.profile.clients.all()[0].label + '/summary'
                else:
                    redirect_string = '/' + VRMClient.objects.all().first().label + '/summary'
        else:
            if self.request.user.profile.clients.all().exists():
                summary_tab = VRMDisplayTab.objects.get(name='Summary')
                if self.request.user.profile.clients.all()[0].display_tab.all().count() > 0:
                    if summary_tab not in self.request.user.profile.clients.all()[0].display_tab.all():
                        redirect_string = '/' + self.request.user.profile.clients.all()[0].label + '/' + \
                                     self.request.user.profile.clients.all()[0].display_tab.all()[0].label
                    else:
                        redirect_string = '/' + self.request.user.profile.clients.all()[0].label + '/summary'
                else:
                    raise Http404
            else:
                raise Http404

        return redirect_string

class ClientPasswordView(PasswordContextMixin, TemplateView):
    template_name = 'registration/password_change_done.html'
    title = _('Password change successful')

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        user = self.request.user
        user.profile.last_password_change = now()
        user.profile.save()
        return redirect('password_logout')

class ClientPermissionHandler(AccessMixin):
    """
    Deny a request with a permission error if the client_permission_validator() method returns
    False.
    """

    def client_permission_validator(self):
        try:
            user = self.request.user
            request_label = self.request.path.split('/')[1]

            try:
                client = VRMClient.objects.get(label=request_label)
            except Exception:
                client = None

            if user.is_active and user.is_staff:
                if not user.profile.recent_fund:
                    fund = Funds.objects.filter(
                        client__client_name=client.name
                    ).distinct().order_by(
                        'fund_name'
                    ).first().fund_name
                    user.profile.recent_fund = fund
                    user.profile.save()
                else:
                    try:
                        Funds.objects.get(fund_name=user.profile.recent_fund, client__client_name=client.name)
                    except Exception as e:
                        fund = \
                            Funds.objects.filter(
                                client__client_name=client.name
                            ).distinct().order_by(
                                'fund_name'
                            ).first().fund_name
                        user.profile.recent_fund = fund
                        user.profile.save()
                return user
            elif user.is_active and user.profile.clients.all().exists() and self.kwargs['label'] in user.profile.clients.all().values_list('label', flat=True):
                if not user.profile.recent_fund:
                    fund = Funds.objects.filter(
                        client__client_name=client.name
                    ).distinct().order_by(
                        'fund_name'
                    ).first().fund_name
                    user.profile.recent_fund = fund
                    user.profile.save()

                display_tab = VRMDisplayTab.objects.get(html_filename=self.template_name.split('/')[1])
                if self.template_name and display_tab:
                    if display_tab in user.profile.clients.all().first().display_tab.all():
                        return user

                return None
            else:
                return None
        except Exception as e:
            print(e)
        return None

    # Dont remove the test_func as it is depend on  UserPassesTestMixin for authentication for all the pages.
    def dispatch(self, request, *args, **kwargs):
        if request.user.username == '':
            return self.handle_no_permission()
        if (now().today().date() - request.user.profile.last_password_change.date()).days > 180 :
            return redirect('password_change')
        if not self.client_permission_validator():
            raise Http404
        return super().dispatch(request, *args, **kwargs)
