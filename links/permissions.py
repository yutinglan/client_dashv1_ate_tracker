from django.contrib.auth.models import User, Group, Permission


def get_user_group(user, groupName):
    UserGroup = Group.objects.filter(user=user)
    Group = Group.objects.get(name=groupName)
    if Group in UserGroup:
        return True
    else:
        return False

