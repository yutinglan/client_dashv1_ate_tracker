import inspect
import logging

from django.utils.functional import wraps
from .models import Action
logger = logging.getLogger(__name__)


def verify_binding(func, permission):
    try:
        action = None
        module = None
        try:
            action = Action.objects.get(name=func.func_name)
        except:
            '''
            Verify if the pre-check exists:
            '''
            module_name = None
            pre_check_name = None
            try:
                pre_check = func.func_name + '_pre_check'
                module = inspect.getmodule(func)
                module_name = module.__name__
                for member in inspect.getmembers(module):
                    if inspect.isfunction(member[1]):
                        if member[0] == pre_check:
                            pre_check_name = pre_check
                            break
            except:
                pass
            if module:
                logger.info("Auto registration of the python action [{}]. Please update it in the admin panel if need "
                            "be!".format(func.func_name))
                action = Action(name=func.func_name, module=module_name, async_mode=False, is_ownership_driven=False, \
                                pre_check=pre_check_name)
                action.save()

        if permission is not None and action and action.permission is None:
            if permission:
                logger.info("Applying: {} to {} ".format(permission.codename, func.func_name))
            action.permission = permission
            action.save()

    except:
        logger.exception("Exception")


def bind_action_to_permission(permission=None):
    def inner_bind_action_to_permission(func, permission=None):
        verify_binding(func, permission)

        def _bind(*args, **kw):
            return func(*args, **kw)

        return wraps(func)(_bind)

    return lambda func: inner_bind_action_to_permission(func, permission)