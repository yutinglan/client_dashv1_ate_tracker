from django.db import models
import datetime
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone
from django.contrib.auth.models import User, Permission, Group
from .managers import TransitionManager


class VRMSitesdetail(models.Model):
    ACTIVE_CHOICES = (
        (1, _("Yes")),
        (0, _("No")),
    )
    CATEGORY_CHOICES = (
        (1, _("Client Dashboard")),
        (2, _("Internal Dashboard")),
        (3, _("Dev Dashboard")),
    )
    name = models.CharField( max_length=80)
    url = models.CharField(unique=True, max_length=250)
    category = models.IntegerField(choices=CATEGORY_CHOICES, default=3)
    active = models.IntegerField(choices=ACTIVE_CHOICES, default=1)
    comments = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.name


class VRMDailyscript(models.Model):
    user = models.ForeignKey(User, related_name='daily_user', on_delete=models.PROTECT)
    crashNumber = models.IntegerField( default=0)
    isCrashed = models.IntegerField(default=0)
    success_days = models.IntegerField( default=0)
    date = models.DateField(default=timezone.now)

