from __future__ import unicode_literals

from django.db import models, IntegrityError
from django.contrib.auth.models import User
from django.db.models import Q
from django.db.models.query import QuerySet

class ConcurrencyManager(models.Manager):
    """
    A class designed to work around database concurrency issues.
    """
    def get_or_create(self, **kwargs):
        """
        A wrapper around get_or_create that makes a final attempt to get
        the object if the creation fails.
        This helps with race conditions in the database where, between the
        original get() and the create(), another process created the object,
        causing us to fail. We'll then execute a get().
        This is still prone to race conditions, but they're even more rare.
        A delete() would have to happen before the unexpected create() but
        before the get().
        """
        try:
            return super(ConcurrencyManager, self).get_or_create(**kwargs)
        except IntegrityError:
            kwargs.pop('defaults', None)
            return self.get(**kwargs)


class TransitionManager(ConcurrencyManager):
    def _query(self, user=None, with_counts=False, extra_query=None):
        query = Q()

        if extra_query:
            query = query & extra_query

        query = self.filter(query).distinct()

        if with_counts:
            # noinspection PyUnresolvedReferences
            query = query.with_counts(user)

        return query

    def from_request(self, request):
        query = Q(current_state=request.status_id)
        return self._query(extra_query=query)

    def from_request_type(self, request, request_type):
        if request and request_type:
            query = Q(current_state=request.status_id, type=request_type)
        else:
            query = Q(type=request_type)
        return self._query(extra_query=query)

    # noinspection PyUnusedLocal
    def from_state(self, instance, status_id):
        query = Q(current_state=status_id)
        return self._query(extra_query=query)

    # noinspection PyUnusedLocal
    def from_request_as_user(self, request, user):
        query = Q(current_state=request.status_id)
        return self._query(extra_query=query)