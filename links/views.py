# from datetime import timedelta, datetime
from django.contrib.auth.mixins import LoginRequiredMixin
from .forms import DailyScriptForm
from .models import VRMSitesdetail, VRMDailyscript
from django.views.generic import FormView
import datetime as dt
import json


# **************************CLASS BASED INDEX**********************************************

class Index(LoginRequiredMixin, FormView):
    form_class = DailyScriptForm
    login_url = 'login'
    template_name = 'index.html'
    success_url = '/links'

    def get_context_data(self, **kwargs):
        data = super(Index, self).get_context_data(**kwargs)
        # for better performance, consider filtering the data to less number of days
        # using timedelta - look at commented code as an example
        # VRMDailyscript.objects.filter(date__gte=dt.today() - timedelta(days=30))
        scripts = VRMDailyscript.objects.all()
        successful_run = []
        is_crashed = []
        crash_number = []
        date_label = []
        for script in scripts:
            successful_run += [script.success_days]
            is_crashed += [script.isCrashed]
            crash_number += [script.crashNumber]
            date_label += [script.date.strftime("%d/%m/%Y")]
        num_books = VRMSitesdetail.objects.all().order_by('name')
        num_instances = VRMSitesdetail.objects.all().count()
        last_script = VRMDailyscript.objects.last()
        last_update = last_script.date - dt.datetime.today().date() != dt.timedelta(days=0)
        data['num_books'] = num_books
        data['num_instances'] = num_instances
        data['successful_run'] = successful_run
        data['is_crashed'] = is_crashed
        data['crash_number'] = crash_number
        data['last_update'] = last_update
        data['date_label'] = json.dumps(date_label)
        return data

    def form_valid(self, form):
        form.save(self.request.user)
        return super(Index, self).form_valid(form)


