from django.contrib import admin

# Register your models here.
from .models import *


@admin.register(VRMSitesdetail)
class VRMSitesAdmin(admin.ModelAdmin):
    list_display = (
        'name',
        'url',
        'category',
        'active',
   )

@admin.register(VRMDailyscript)
class VRMDailyscripts(admin.ModelAdmin):
    list_display = (
        'user',
        'crashNumber',
        'isCrashed',
        'success_days',
        'date'
   )




