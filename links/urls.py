from django.urls import path
from django.views.generic.base import RedirectView
from . import views

urlpatterns = [
    path('index/', views.Index.as_view(), name='index'),
   # path('', RedirectView.as_view(pattern_name='tbs', permanent=False), name='index'),
]
