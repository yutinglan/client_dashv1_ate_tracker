from datetime import datetime, timedelta
import numpy as np
from django.contrib.auth.models import User
from django.core.management.base import BaseCommand, CommandError
from links.models import VRMDailyscript
from links.holidays import get_last_business_day

class Command(BaseCommand):
    help = 'This is to be set in a cron to run \
    automatically after 12 to update the \
    VRMDailyscripts table.'

    def handle(self, *args, **options):
        try:
            if not VRMDailyscript.objects.filter(date=get_last_business_day()).exists():
                user = User.objects.get(username='auto')
                last_script = VRMDailyscript.objects.last()
                script = VRMDailyscript.objects.create(user=user)
                script.crashNumber = 0
                script.isCrashed = 0
                script.success_days = last_script.success_days + 1
                script.save()
        except Exception as e:
            print(e)
