from django.forms import ModelForm, NumberInput, CheckboxInput

from .models import VRMDailyscript
from django import forms
from django.contrib.admin import widgets
from django.forms.widgets import SelectDateWidget
import datetime


class DailyScriptForm(forms.ModelForm):
    is_crashed = forms.BooleanField(required=False, label='Did the scripts crash today?', widget=CheckboxInput(attrs={
        'id': 'CrashCheckbox',
    }))
    crashNumber = forms.IntegerField(initial=0, label='Number of Crashes')

    class Meta:
        model = VRMDailyscript
        exclude = {
            'user',
            'crashNumber',
            'isCrashed',
            'success_days',
            'date'
        }

    def save(self, user):
        # script = super(DailyScriptForm, self).save(commit=False)
        script, created = VRMDailyscript.objects.get_or_create(date=datetime.datetime.today(), user=user)
        if created:
            script.user = user

            # script.user = self
            is_crashed = self.cleaned_data['is_crashed']
            crashNumber = self.cleaned_data['crashNumber']

            if is_crashed:
                script.isCrashed = 1
                if crashNumber == 0:
                    script.crashNumber = 1
                else:
                    script.crashNumber = crashNumber
                script.success_days = 0
            else:
                script.isCrashed = 0
                script.crashNumber = 0
                yesterdays_script = VRMDailyscript.objects.get(date=datetime.datetime.today() - datetime.timedelta(days=1))
                script.success_days = yesterdays_script.success_days + 1
            script.save()
        return script
