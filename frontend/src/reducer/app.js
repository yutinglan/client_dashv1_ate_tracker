import { handleActions } from 'redux-actions'
import * as types from '../constants'

export const initialState = {
  ateBasicInfo: [],
  selectList: [],
  results: [],
  ateConditions: [],
  csaConditions: [],
  csaThresholds: [],
  financialDatas: [],
  client:''
}

const app = handleActions({
  [types.FETCH_ATEBASICINFO_SUCCESS]: (state, action) => ({ ...state, ateBasicInfo:[...state.ateBasicInfo, ...action.payload ] }),
  [types.FETCH_ATECONDITION_SUCCESS]: (state, action) => ({ ...state, ateConditions:[...action.payload ]}),
  [types.FETCH_ATEITEM_SUCCESS]: (state, action) => ({ ...state, selectList: [...action.payload ]}),
  [types.FETCH_CSACONDITION_SUCCESS]: (state, action) => ({ ...state, csaConditions: [...action.payload ]}),
  [types.FETCH_CSATHRESHOLD_SUCCESS]: (state, action) => ({ ...state, csaThresholds: [...action.payload ]}),
  // [types.FETCH_INVESTMENT_SUCCESS]: (state, action) => ({ ...state, investments:[...state.invetments, action.payload ]}),
  // [types.FETCH_INVESTMENT_FAILURE]: (state, action) => ({ ...state, failure: true }),
  // [types.FETCH_ALL_INVESTMENT_SUCCESS]: (state, action) => ({ ...state, investments: action.payload }),

  // [types.CREATE_COMMITMENT_SUCCESS]: (state, action) => ({ ...state, commitments:[...state.commitments, action.payload ] }),
  // [types.UPDATE_COMMITMENT_SUCCESS]: (state, action) => ({ ...state, commitments:[...state.commitments, action.payload ]}),
  // [types.FETCH_COMMITMENT_SUCCESS]: (state, action) => ({ ...state, commitments:[...state.commitments, action.payload ]}),
  // [types.FETCH_COMMITMENT_FAILURE]: (state, action) => ({ ...state, failure: true }),
  // [types.FETCH_ALL_COMMITMENT_SUCCESS]: (state, action) => ({ ...state, commitments: action.payload }),
  
  
  // [types.CREATE_CALL_SUCCESS]: (state, action) => ({ ...state, calls:[...state.calls, action.payload ] }),
  // [types.UPDATE_CALL_SUCCESS]: (state, action) => ({ ...state, calls:[...state.calls, action.payload ]}),
  // [types.FETCH_CALL_SUCCESS]: (state, action) => ({ ...state, calls:[...state.calls, action.payload ]}),
  // [types.FETCH_CALL_FAILURE]: (state, action) => ({ ...state, failure: true }),
  // [types.FETCH_ALL_CALL_SUCCESS]: (state, action) => ({ ...state, calls: action.payload }),

}, initialState)
export default app
