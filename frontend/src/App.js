import React, { Component } from "react";
import { AteTracker } from './containers'

import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

class App extends Component{
  
  render(){
    console.log('reach app');
    // return (
    //   // <Layout>
    //   <button>fe</button>
    //   // </Layout>
    // )
    return(
      <Router>
        <Switch>
          <Route path="" exact component={AteTracker} />
        </Switch>        
      </Router>
    );
  }
}

export default App;
