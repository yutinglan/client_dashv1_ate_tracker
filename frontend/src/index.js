import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from "react-redux";
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import configureStore from './store/store'
import {LicenseManager} from "ag-grid-enterprise";
LicenseManager.setLicenseKey("Validus_Risk_Management_Ltd___RiskView_1Devs7_January_2020__MTU3ODM1NTIwMDAwMA==63eb9e59e0e6f9653240a1a826efa0d2");

const store = configureStore()

ReactDOM.render(
  <Provider store={store}>
    <App/>  
  </Provider>
  
  ,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
