import { callApi, getCookie } from '.'
import config from '../config'



export default {
  fetch: ({clientName, csaId}) => callApi(`${config.validus.api.publicUrl}/ate_item/${clientName}`, {
    method: 'GET',
    headers : { 
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'X-CSRFToken': getCookie('csrftoken'),
    },
    // data: {'csrfmiddlewaretoken': getCookie('csrftoken'),}
    // body: JSON.stringify(query)
  }),
  update: (clientName) => callApi(`${config.validus.api.publicUrl}/ate_item/${clientName}`, {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    body: JSON.stringify(clientName)
  }),

}