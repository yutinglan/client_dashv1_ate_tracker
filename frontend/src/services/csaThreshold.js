import { callApi, getCookie } from '.'
import config from '../config'

export default {
  fetch: ({clientName, csaId}) => callApi(`${config.validus.api.publicUrl}/csa_threshold/${clientName}/${csaId}`, {
    method: 'GET',
    headers : { 
      'Content-Type': 'application/json',
      'Accept': 'application/json',
    //   'csrfmiddlewaretoken': getCookie('csrftoken'),
    }
    // body: JSON.stringify(query)
  }),
  create: (csaThreshold, clientName) => callApi(`${config.validus.api.publicUrl}/csa_condition/${clientName}`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(csaThreshold)
  }),
  update: (csaThreshold, clientName) => callApi(`${config.validus.api.publicUrl}/csa_condition/${clientName}`, {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(csaThreshold)
  }),
  delete: (id) => callApi(`${config.validus.api.publicUrl}/csa_condition/${id}`, {
    method: 'DELETE',
    headers: {
      'Content-Type': 'application/json',
    }
  })
}