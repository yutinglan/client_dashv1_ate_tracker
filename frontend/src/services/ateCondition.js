import { callApi, getCookie } from '.'
import config from '../config'



export default {
  fetch: ({clientName, csaId}) => callApi(`${config.validus.api.publicUrl}/ate_setting/${clientName}/${csaId}`, {
    method: 'GET',
    headers : { 
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'X-CSRFToken': getCookie('csrftoken'),
    },
    // data: {'csrfmiddlewaretoken': getCookie('csrftoken'),}
    // body: JSON.stringify(query)
  }),
  update: (ateCondition, clientName) => callApi(`${config.validus.api.publicUrl}/ate_setting/${clientName}`, {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(ateCondition)
  }),

}