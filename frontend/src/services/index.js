function checkHttpStatus (response) {
  if (response.status >= 200 && response.status < 300) {
    return response
  } else {
    var error = new Error(response.statusText)
    error.response = response
    throw error
  }
}

function parseJSON (response) {
  return response.json()
}

export const getCookie = (name) => {
  var cookieValue = null;
  if (document.cookie && document.cookie !== '') {
      var cookies = document.cookie.split(';');
      for (var i = 0; i < cookies.length; i++) {
          var cookie = cookies[i].trim();
          // Does this cookie string begin with the name we want?
          if (cookie.substring(0, name.length + 1) === (name + '=')) {
              cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
              break;
          }
      }
  }
  return cookieValue;
}

export const callApi = (url, options) => {
  return fetch(url, options)
    .then(checkHttpStatus)
    .then(parseJSON)
}

export { default as ateBasicInfo } from './ateBasicInfo';
// export { default as conditionNames } from './conditionNames
export { default as result } from './result';
export { default as ateCondition } from './ateCondition';
export { default as csaCondition } from './csaCondition';
export { default as csaThreshold } from './csaThreshold';
export { default as financialData } from './financialData';
export { default as ateItem } from './ateItem';
