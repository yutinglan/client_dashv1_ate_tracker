import { callApi, getCookie } from '.'
import config from '../config'

export default {
  fetch: ({clientName}) => callApi(`${config.validus.api.publicUrl}/financial_data/${clientName}`, {
    method: 'GET',
    headers : { 
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'X-CSRFToken': getCookie('csrftoken'),
    }
    // body: JSON.stringify(query)
  }),
  create: (financialData, clientName) => callApi(`${config.validus.api.publicUrl}/financial_data/${clientName}`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(financialData)
  }),
  update: (financialData, clientName) => callApi(`${config.validus.api.publicUrl}/financial_data/${clientName}`, {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    body: JSON.stringify(financialData)
  }),
  delete: (id) => callApi(`${config.validus.api.publicUrl}/financial_data/${id}`, {
    method: 'DELETE',
    headers: {
      'Content-Type': 'application/json',
    }
  })

}