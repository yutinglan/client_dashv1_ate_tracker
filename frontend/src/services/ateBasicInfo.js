import { callApi, getCookie } from '.'
import config from '../config'


export default {
  fetch: ({clientName}) => callApi(`${config.validus.api.publicUrl}/legal_counterparty/${clientName}`, {
    method: 'GET',
    headers : { 
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'X-CSRFToken': getCookie('csrftoken'),
    }
    // body: JSON.stringify(query)
  }),
  update: (clientName) => callApi(`${config.validus.api.publicUrl}/legal_counterparty/${clientName}`, {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    body: JSON.stringify(clientName)
  }),

}
