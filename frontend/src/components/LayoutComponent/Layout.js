import React from 'react'

import './Layout.css';
const Layout = (props) => {
  
  return (
    <div className="h-100"> 
      {props.children}
    </div>
     
);}

export default Layout;
