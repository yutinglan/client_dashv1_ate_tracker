import React, { useState } from 'react'
import { Nav, Navbar, Image  } from 'react-bootstrap'
import './navbar.css';

const CustomiseNavbar = (props) => {
  var [clienName, setClientName] = useState(props.client['client'])  
  return (
    <Navbar bg="dark" expand='lg' variant='dark' style={{'height':'100%'}}>
      
      <Nav className="flex-column" style={{'height':'100%'}}>
              <Nav.Item className="mt-5">      
                  <img className="image-container" src="/static/img/Validus-on-black-CMYK.svg"></img>
              </Nav.Item>
              <Nav.Item className="mt-2">
                <div className="client-name">{clienName.toUpperCase()}</div>
              </Nav.Item>
              <Nav.Link href={"../"+clienName+"/summary"}>Summary Dashboard</Nav.Link>
              <Nav.Link href={"../"+clienName+"/counterparty"}>Counterparty</Nav.Link>
              <Nav.Link href={"../"+clienName+"/market"}>Market</Nav.Link>
              <Nav.Link href={"../"+clienName+"/settlementrolling"}>Settlement and Rolling</Nav.Link>
              <Nav.Link href={"../"+clienName+"/fund_hedge_ratio"}>Hedge Analytics</Nav.Link>
              <Nav.Link href={"../"+clienName+"/frisk_analytics"}>Risk Analytics</Nav.Link>
              <Nav.Link href={"../"+clienName+"/agg_report"}>Aggregate Report</Nav.Link>
              <Nav.Link href={"../"+clienName+"/contacts"}>Contact Report</Nav.Link>
        </Nav>
    </Navbar>
    
);}

export default CustomiseNavbar;
