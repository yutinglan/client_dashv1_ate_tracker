
import React, { Component } from 'react'
// import { Container, Tabs, Tab, } from 'react-bootstrap'
import { AgGridReact } from 'ag-grid-react'
import { Table } from 'react-bootstrap'
import './AteTracker.css'
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-balham.css';

class ConditionsActiveTable extends Component {
    constructor(props) {
      super(props);
      
      this.state = {
        gridOptions:{
          columnDefs: [
            { headerName: "", field: "rowHeader"}, 
            { headerName: props.headerName, 
              field: props.field,
              cellRenderer: this.checkboxCellRenderer
            },
          ],
          rowData: [{
            rowHeader: "Active" , NAV_Decline: "checkbox"
          }, {
            rowHeader: "Threshold", NAV_Decline: "Mondeo"
          },],
          defaultColDef: {
            editable: true,
            resizable: true
          },
          rowSelection: "single",
        }
      }

      
    }

    checkboxCellRenderer = (params) =>{
      console.log(params)
      if(params.value=="checkbox"){
        var input = document.createElement("input")
        input.type = "checkbox";
        input.checked = params.value
        return input
      }else{
        return params.value
      }
      
    }

    onGridReady = params => {
      this.gridApi = params.api;
      this.gridColumnApi = params.columnApi;
  
      params.api.sizeColumnsToFit();
      window.addEventListener("resize", function() {
        setTimeout(function() {
          params.api.sizeColumnsToFit();
        });
      });
  
      params.api.sizeColumnsToFit();
    };
  
    render() {
      return (
        <div className="ag-theme-balham" 
        style={{ 
          height: '200px', 
          width: '100%'
        }}>
          
          <AgGridReact
            gridOptions={this.state.gridOptions}
          />
        
        </div>
      );
    }
  }
  


export default ConditionsActiveTable