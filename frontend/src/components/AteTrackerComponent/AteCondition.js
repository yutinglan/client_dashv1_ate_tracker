import React, { useState, useEffect } from 'react'
import MaterialTable from 'material-table';
import Select from "react-select";
import { updateAteBasicInfo } from '../../sagas/api';

const AteCondition = (props) => {
  const { selectList } = props

  const handleChange = (e) => {
    return e.target.value

  }

  const editComponent = prev => {
    console.log(prev.value)
    if(prev.value!=undefined){
      return <Select
        name='123'
        options={props.selectList}
        value={prev.value.split(',').map(function(elem){
          return(elem==""? "": {value:elem, label:elem})
        })}
        isMulti
        onChange={e => prev.onChange(e.map(function(elem){return elem.value}).join(','))}
      />
    }else{
      return <Select
        name='123'
        options={props.selectList}
        isMulti
        onChange={e => prev.onChange(e.map(function(elem){return elem.value}).join(','))}
      />
    }
  }

  const columns = (() => {
    return [
    { title: 'Condition Name', field: 'conditionName', type: 'string' },
    { title: 'Active', field: 'active', type: 'boolean' },
    { title: 'Threshold', field: 'threshold', type: 'numeric' },
    { title: 'Positive Numerator', field: 'positiveNumerator' , editComponent: editComponent},
    { title: 'Negative Numerator', field: 'negativeNumerator' , editComponent: editComponent},
    { title: 'Positive Denominator', field: 'positiveDenominator' , editComponent: editComponent},
    { title: 'Negative Denominator', field: 'negativeDenominator' , editComponent: editComponent}
    ]
  }) ()
  
  const [dataSet, setDataset] = useState(props.data)

  useEffect(() => {
    if(dataSet !== props.data) {
      console.log(props.data)
      setDataset(props.data);
    }
  }, [props.data, props.selectList]);

  return (
    <MaterialTable
      title=""
      columns={columns}
      data={dataSet}
      editable={{
        onRowUpdate: (newData, oldData) =>
          new Promise(resolve => {
            setTimeout(() => {
              resolve();
              console.log(newData)
              // if (oldData) {
              //   setDataset(prevState => {
              //     const data = [...prevState];
                  // data[data.indexOf(oldData)] = newData;
              //     return [...data];
              //   });
              // }
              const tempDataSet = [...dataSet]
              tempDataSet[tempDataSet.indexOf(oldData)] = newData;
              props.updateAteCondition(tempDataSet)
            }, 600);
          }),
      }}
    />
  );
}

export default AteCondition





