
import React, { Component } from 'react'
// import { Container, Tabs, Tab, } from 'react-bootstrap'
import { AgGridReact } from 'ag-grid-react'
import './AteTracker.css'
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-balham.css';

class ResultPassTable extends Component {
    constructor(props) {
      super(props);
      this.state = {
        columnDefs: [
          {headerName: '',
            children: [
              {headerName: 'Legal Entity', field: 'legalEntity'},
            ]
          },
          {headerName: 'CP',
            children: 
              props.entities
          }
        ],
        defaultColDef: {
          editable: false,
          resizable: true
        },
      }
    }

    onGridReady = params => {
      this.gridApi = params.api;
      this.gridColumnApi = params.columnApi;
  
      params.api.sizeColumnsToFit();
      window.addEventListener("resize", function() {
        setTimeout(function() {
          params.api.sizeColumnsToFit();
        });
      });
  
      params.api.sizeColumnsToFit();
    };

  
    render() {
      return (
        <div 
          className="ag-theme-balham" 
          style={{ 
            height: '300px', 
            width: 'auto'
          }}
        >
          <AgGridReact
            defaultColDef={this.state.defaultColDef}
            columnDefs={this.state.columnDefs}
            rowData={this.props.data}
            onGridReady={this.onGridReady}
          />
        </div>
      );
    }
  }
  


export default ResultPassTable