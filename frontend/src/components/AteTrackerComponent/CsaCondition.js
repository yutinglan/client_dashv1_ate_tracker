import React, { useState, useEffect } from 'react'
import MaterialTable from 'material-table';
import { Button, Dialog, DialogTitle, DialogActions, DialogContent } from '@material-ui/core';
import { ThresholdTable } from '.';
import Select from "react-select";

const CsaCondition = (props) => {

  // const [state, setState] = useState({
  //   data: [
  //     {
  //       csaName: 'Nav',
  //       positive: true,
  //       negative: 'false',
  //       thresholdSetting: 'Nav',
  //       csaThreshold: 1987,
  //       conditionThreshold: 63,
  //     },
  //     {
  //       csaName: 'Nav2',
  //       positive: true,
  //       negative: 'false',
  //       thresholdSetting: 'Nav2',
  //       csaThreshold: 1988,
  //       conditionThreshold: 63,
  //     },
  //   ],
  // });
  
  const [open, setOpen] = useState(false)
  const [thresholdName, setThresholdName] = useState()
  const handleThresholdSettingClick = (e, target) => {
    setOpen(true)
    console.log(target)
    setThresholdName(target)
  }

  const editComponent = prev => {
    if(prev.value!=undefined){
      return <Select
        name='123'
        options={props.selectList}
        value={prev.value.split(',').map(function(elem){
          return(elem==""? "": {value:elem, label:elem})
        })}
        isMulti
        onChange={e => prev.onChange(e.map(function(elem){return elem.value}).join(','))}
      />
    }else{
      return <Select
        name='123'
        options={props.selectList}
        isMulti
        onChange={e => prev.onChange(e.map(function(elem){return elem.value}).join(','))}
      />
    }
  }

  const columns = (() => {
    return [
      { title: 'CSA Name', field: 'csaName', type: 'string', editable: 'never'},
      { title: 'Active', field: 'active', type: 'boolean'},
      { title: 'Positive', field: 'positive', editComponent: editComponent },
      { title: 'Negative', field: 'negative', editComponent: editComponent },
      { title: 'Threshold Setting', field: 'thresholdSetting', editable: 'never', render: rowData => 
        <Button name={rowData.thresholdSetting} value={rowData.thresholdSetting} variant="text" onClick={e => handleThresholdSettingClick(e, rowData.csaName)}>Threshold Setting</Button>
      },
    ]
  }) ()
  
  const [dataset, setDataset] = useState([])
  const [csaThresholds, setCsaThresholds] = useState([])
  useEffect(() => {
    if(dataset !== props.data) {
      console.log(props.data)
      setDataset(props.data);
      setCsaThresholds(props.csaThresholds)
    }
  }, [props.data, props.selectList, props.csaThresholds]);
  

  const handleClose = () => {
    setOpen(false);
  };

  console.log(csaThresholds)
  return (
    <div>
    <MaterialTable
      title=""
      data={dataset}
      editable={{
        onRowAdd: newData =>
          new Promise(resolve => {
            setTimeout(() => {
              resolve();
              setDataset(prevState => {
                const data = [...prevState];
                data.push(newData);
                console.log(data)
                return [...data];
              });
            }, 600);
          }),
          onRowUpdate: (newData, oldData) =>
          new Promise(resolve => {
            setTimeout(() => {
              resolve();
              console.log(newData)
              if (oldData) {
                setDataset(prevState => {
                  const data = [...prevState];
                  data[data.indexOf(oldData)] = newData;
                  return [...data];
                });
              }
            }, 600);
          }),
      }}
      columns={columns}
      options={{
        selection: true
      }}
    />

    <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-slide-title"
        aria-describedby="alert-dialog-slide-description"
      >
        <DialogTitle id="alert-dialog-slide-title">CsaCondition</DialogTitle>
        <DialogContent>
          <ThresholdTable
            csaThreshold={csaThresholds[thresholdName]}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>
            Disagree
          </Button>
          <Button onClick={handleClose}>
            Agree
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}

export default CsaCondition

