import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Container, Row, Col, } from 'react-bootstrap'
import { Layout, CustomiseNavbar } from '../LayoutComponent';
import { ResultThresholdTable , ResultPassTable, ConditionsActiveTable, ConditionsFormulaTable, AteBasicInfoTable, FinancialDataTable, AteCondition, CsaCondition } from '.'
import { Tab, Tabs, AppBar, Typography, Box, Card, CardContent } from '@material-ui/core'
import './AteTracker.css'
import { ateCondition } from '../../services';

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <Typography
      component="div"
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      <Box>{children}</Box>
    </Typography>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function Panel(props) {
  const { children, name, ...other } = props;

  return (
    <Typography
      component="div"
      role="tabpanel"
      id={`simple-tabpanel-${name}`}
      aria-labelledby={`simple-tab-${name}`}
      {...other}
    >
      <Box>{children}</Box>
    </Typography>
  );
}

Panel.propTypes = {
  children: PropTypes.node,
  name: PropTypes.string
};


class AteTracker extends Component {
  
  constructor(props) {
    super(props);
    this.state = {
      tabValue: 0
    }
  }

  static propTypes = {
    ateConditions: PropTypes.array.isRequired,
    loadAteTracker: PropTypes.func.isRequired,
    fetchAteData: PropTypes.func.isRequired,
    selectList: PropTypes.array.isRequired,
    updateAteCondition: PropTypes.func.isRequired,
    updateCsaCondition: PropTypes.func.isRequired,
    createCsaThreshold: PropTypes.func.isRequired,
    updateCsaThreshold: PropTypes.func.isRequired,
    deleteCsaThreshold: PropTypes.func.isRequired,
    createFinancialData: PropTypes.func.isRequired,
    updateFinancialData: PropTypes.func.isRequired,
    deleteFinancialData: PropTypes.func.isRequired,
  }

  componentDidMount () {
    console.log('reach didmount')
    this.props.loadAteTracker('cvc')
  }
  
  handleChange = (event, newValue) => {
    this.setState(
      {tabValue: newValue}
    )
  }

  render (){
    const { ateBasicInfo, legalEntities, selectList, ateConditions, csaConditions, csaThresholds, client } = this.props
    const { tabValue } = this.state
    
    return (
      <Row>

          <Col md={2}><CustomiseNavbar client={client}/></Col>
          <Col md={8}>
          <AppBar position="static" style={{height:'48px'}}>
          <Tabs value={0}>
            <Tab label="Basic Info" id="0"/>
          </Tabs>
        </AppBar>
        <Panel name='BasicInfo'>
          <AteBasicInfoTable
            legalEntities = {legalEntities}
            fetchAteData = {this.props.fetchAteData}
          />
        </Panel>
        
        <AppBar position="static">
          <Tabs value={tabValue} onChange={this.handleChange} aria-label="simple tabs example">
            <Tab label="Result" id="0"/>
            <Tab label="ATE Condition" id="1"/>
            <Tab label="CSA" id="2"/>
          </Tabs>
        </AppBar>
        <TabPanel value={tabValue} index={0}>
          <Row>
            <Col>
              <Card>
                <CardContent>
                  <Typography variant="h6" component="h2">
                    Test Period
                  </Typography>
                </CardContent>
              </Card>
            </Col>
          </Row>
          <Row>
            <Col>
              <ResultPassTable
                entities={[
                  {headerName: 'Barclay', field: 'CounterParty'},
                  {headerName: 'Sander', field: 'CounterParty'},
                  {headerName: 'Llyods', field: 'CounterParty'},
                  {headerName: 'HSBC', field: 'CounterParty'},
                ]}
                data={[
                  {legalEntity: 'A', CounterParty: "pass"}, 
                  {legalEntity: 'B', CounterParty: "pass"}, 
                  {legalEntity: 'C', CounterParty: "pass"}, 
                  {legalEntity: 'D', CounterParty: "pass"}, 
                ]}
              />
            </Col>
          </Row>
          <Row>
            <Col>
              <ResultThresholdTable
                data={[
                  {threshold: -5000000, navDecline: 'Pass', navFloor: "Failed", assetCoverageRatio: "Failed", uncalledLPCover: "Pass", liquidityFloor: "Pass"},
                  {threshold: -2500000, navDecline: 'Pass', navFloor: "Pass", assetCoverageRatio: "Failed", uncalledLPCover: "Pass", liquidityFloor: "Pass"},
                  {threshold: -5000000, navDecline: 'Pass', navFloor: "Pass", assetCoverageRatio: "Pass", uncalledLPCover: "Pass", liquidityFloor: "Pass"},
                  {threshold: 0, navDecline: 'Pass', navFloor: "Pass", assetCoverageRatio: "Pass", uncalledLPCover: "Pass", liquidityFloor: "Pass"},
                ]}
              />
            </Col>
          </Row>
        </TabPanel>
        <TabPanel value={tabValue} index={1}>
          <AteCondition
            selectList = {selectList}
            data = {ateConditions}
            updateAteCondition = {this.props.updateAteCondition}
          />
        </TabPanel>
        <TabPanel value={tabValue} index={2}>
          <CsaCondition
            selectList = {selectList}
            data = {csaConditions}
            csaThresholds = {csaThresholds}
          />
        </TabPanel>
        
        <AppBar position="static"  style={{height:'48px'}}>
          <Tabs value={0}>
            <Tab label="Financial Data" id="0"/>
          </Tabs>
        </AppBar>
        <Panel name='FinancialData'>
          <FinancialDataTable/>
        </Panel>
        </Col>
         
      </Row>
        

        
    )
  }
}

export default AteTracker
