import React, { useState } from 'react'
import MaterialTable from 'material-table';

const FinancialDataTable = (props) => {
  const [state, setState] = useState({
    columns: [
      { title: 'Entity Id', field: 'entityId' },
      { title: 'Valuation Date', field: 'valuationDate', type: 'date'},
      { title: 'Year', field: 'year', type: 'numeric' },
      { title: 'Quarter', field: 'quarter', type: 'numeric' },
      { title: 'Nav', field: 'nav' },
      { title: 'UCC', field: 'ucc'},
      { title: 'Recallable', field: 'recallable'},
      { title: 'Distribution', field: 'distribution'},
      { title: 'Indebtedness', field: 'indebtedness' },
      { title: 'Liabilities', field: 'liabilities'},
      { title: 'Assets', field: 'assets', type: 'numeric' },
      { title: 'Cash', field: 'cash', type: 'numeric' },
      { title: 'Cash Equivalents', field: 'cashEquivalents', type: 'numeric' },

    ],
    data: [
      { entityId: 'Mehmet', valuationDate: '', year: 1987, quarter: 63, nav: 'test', ucc: 'ucc', recallable: 'test', distribution: 'test', indebtedness: 'test', assets: 1, cash: 2, cashEquivalents:3},
    ],
  });

  return (
    <MaterialTable
      title=""
      columns={state.columns}
      data={state.data}
      editable={{
        onRowAdd: newData =>
          new Promise(resolve => {
            setTimeout(() => {
              resolve();
              setState(prevState => {
                const data = [...prevState.data];
                data.push(newData);
                return { ...prevState, data };
              });
            }, 600);
          }),
        onRowUpdate: (newData, oldData) =>
          new Promise(resolve => {
            setTimeout(() => {
              resolve();
              if (oldData) {
                setState(prevState => {
                  const data = [...prevState.data];
                  data[data.indexOf(oldData)] = newData;
                  return { ...prevState, data };
                });
              }
            }, 600);
          }),
        onRowDelete: oldData =>
          new Promise(resolve => {
            setTimeout(() => {
              resolve();
              setState(prevState => {
                const data = [...prevState.data];
                data.splice(data.indexOf(oldData), 1);
                return { ...prevState, data };
              });
            }, 600);
          }),
      }}
    />
  );
}

export default FinancialDataTable