
import React, { Component } from 'react'
// import { Container, Tabs, Tab, } from 'react-bootstrap'
import { AgGridReact } from 'ag-grid-react'
import './AteTracker.css'
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-balham.css';

class ResultThresholdTable extends Component {
    constructor(props) {
      super(props);
      this.state = {
        columnDefs: [{
          headerName: "Threshold", field: "threshold"
        }, {
          headerName: "NAV Decline: (Previous quarter NAV - NAV) / Previous quarter NAV", field: "navDecline"
        }, {
          headerName: "NAV Floor: NAV", field: "navFloor"
        }, {
          headerName: "Asset Coverage Ratio: NAV / Assets", field: "assetCoverageRatio"
        }, {
          headerName: "Uncalled LP Cover: UCC / Liabilities", field: "uncalledLPCover"
        }, {
          headerName: "Liquidity Floor: Cash / NAV", field: "liquidityFloor"
        },],
        defaultColDef: {
          editable: true,
          resizable: true
        },
      }
    }

    onGridReady = params => {
      this.gridApi = params.api;
      this.gridColumnApi = params.columnApi;
  
      params.api.sizeColumnsToFit();
      window.addEventListener("resize", function() {
        setTimeout(function() {
          params.api.sizeColumnsToFit();
        });
      });
  
      params.api.sizeColumnsToFit();
    };
  
    render() {
      return (
        <div 
          className="ag-theme-balham" 
          style={{ 
            height: '300px', 
            width: 'auto'
          }}
        >
          <AgGridReact
            defaultColDef={this.state.defaultColDef}
            columnDefs={this.state.columnDefs}
            rowData={this.props.data}
            onGridReady={this.onGridReady}
          />
        </div>
      );
    }
  }
  


export default ResultThresholdTable