
import React, { Component } from 'react'
import { Table } from 'react-bootstrap'
import Select from "react-select";
import './AteTracker.css'

class ConditionsFormulaTable extends Component {
    constructor(props) {
      super(props);
      this.state = {
        items: [
          {value: '1', label: 'UCC'},
          {value: '2', label: 'Recallable'},
          {value: '3', label: 'Distributions'},
          {value: '4', label: 'Indebtedness'},
          {value: '5', label: 'Liabilites'},
          {value: '6', label: 'Assets'},
          {value: '7', label: 'Cash'},
          {value: '8', label: 'CashEquivalents'},
        ],
        selectedObject: {},
      }
    }
    
    handleChange = (event, actionMeta) => {
      let { selectedObject } = this.state;
      
      if(event !== undefined) {
        if(actionMeta === undefined) {
            selectedObject[event.target.name] = event.target.value;
        }
        else{
            if(event.value  !== undefined)
                selectedObject[actionMeta.name] = event.value;
            else
                selectedObject[actionMeta.name] = event
        }
        this.setState({
            selectedObject: Object.assign({}, selectedObject)
        });
        console.log(selectedObject)
      }
    }
    
  
    render() {
      const generateDropDownList = (name) =>{
        return <Select name={name} options={this.state.items} value={this.state.selectedObject[{name}]} defaultValue={this.state.selectedObject[{name}]} isMulti={true} onChange={this.handleChange} />
      }
      return (
        <Table striped bordered hover size="sm" class="condition-table">
          <tbody>
            <tr>
              <td></td> 
              <td>Postive</td> 
              <td>Negative</td>
            </tr>
            <tr>
              <td>Numerator</td> 
              <td>{generateDropDownList('np')}</td> 
              <td>{generateDropDownList('nn')}</td>
            </tr>
            <tr>
              <td>Denominator</td> 
              <td>{generateDropDownList('dp')}</td> 
              <td>{generateDropDownList('dn')}</td>
            </tr>
          </tbody>
        </Table>
      );
    }
  }
  


export default ConditionsFormulaTable