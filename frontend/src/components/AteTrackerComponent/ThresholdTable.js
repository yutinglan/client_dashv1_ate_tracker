import React, { useState, useEffect } from 'react'
import MaterialTable from 'material-table';

const ThresholdTable = (props) => {
  const [state, setState] = useState({
    columns: [
      { title: 'CSA Threshold', field: 'csaThreshold', type: 'numeric'  },
      { title: 'Condition Threshold', field: 'conditionThreshold', type: 'numeric'  },
    ]
  });
  const [dataset, setDataset] = useState([])
  console.log(props.csaThreshold)

  useEffect(() => {
    if(dataset !== props.csaThreshold) {
      setDataset(props.csaThreshold);
    }
  }, [props.csaThreshold]);

  return (
    <div>
    <MaterialTable
      title=""
      columns={state.columns}
      data={dataset}
      editable={{
        onRowAdd: newData =>
          new Promise(resolve => {
            setTimeout(() => {
              resolve();
              setDataset(prevState => {
                const data = [...prevState];
                data.push(newData);
                console.log(data)
                return [...data];
              });
            }, 600);
          }),
        onRowUpdate: (newData, oldData) =>
        new Promise(resolve => {
          setTimeout(() => {
            resolve();
            if (oldData) {
              setDataset(prevState => {
                const data = [...prevState];
                data[data.indexOf(oldData)] = newData;
                return [...data];
              });
            }
          }, 600);
        }),
        onRowDelete: oldData =>
          new Promise(resolve => {
            setTimeout(() => {
              resolve();
              setDataset(prevState => {
                const data = [...prevState];
                console.log(data)
                data.splice(data.indexOf(oldData), 1);
                console.log(data)
                return [...data];
              });
            }, 600);
          }),
      }}
    />
    </div>
  );
}

export default ThresholdTable