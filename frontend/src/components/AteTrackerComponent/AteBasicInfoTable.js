
import React, { useState, useEffect } from 'react'
// import { Table } from 'react-bootstrap'
import { Card, Typography, CardContent, MenuItem, FormControl, Select} from '@material-ui/core'
import { Row, Col, } from 'react-bootstrap'

const AteBasicInfoTable = (props) => {

  const [legalEntities, setLegalEntities] = useState(Object.keys(props.legalEntities))
  const [legalEntity, setLegalEntity] = useState("")
  const [counterparties, setCounterparties] = useState([])
  const [counterparty, setCounterparty] = useState()
  const [testPeriod, setTestPeriod] = useState(props.testPeriod)

  const handleChange = event => {
    setLegalEntity(event.target.value);
    setCounterparties(props.legalEntities[event.target.value])
  };
  const handleCounterPartyChange = event => {
    let selectedValue = 0
    counterparties.forEach(function(ele,idx){
      if(Object.keys(ele)[0]==event.target.value[0]){
        selectedValue = Object.values(ele)[0]
      }
    })
    setCounterparty(selectedValue);
    const query = { clientName: 'cvc', csaId: event.target.value[0] }
    props.fetchAteData(query)
  };

  useEffect(() => {
    // Update the document title using the browser API
    setLegalEntities(Object.keys(props.legalEntities))
  },[props.legalEntities]);

  const selectCp =  
  <Select value={counterparty} onChange={handleCounterPartyChange}>
    {counterparties.map((ele, idx) => (
      <MenuItem key={idx} value={Object.keys(ele)[0]}>{Object.values(ele)[0]}</MenuItem>
    ))}
  </Select>

  return (
    <div>
   
    <Row>
      <Col>
        <Card>
          <CardContent>
            <Typography variant="h6" component="h2">
              Legal Entity
            </Typography>
            <FormControl>
              <Select
                labelId="demo-simple-select-label"
                name ="legalEntitySelect"
                id="legalEntitySelect"
                value={legalEntity}
                onChange={handleChange}
              >
                {legalEntities.map((name, idx) => (
                  <MenuItem key={idx} value={name}>{name}</MenuItem>
                ))}
              </Select>
            </FormControl>
          </CardContent>
        </Card>
      </Col>
      <Col>
        <Card>
          <CardContent>
            <Typography variant="h6" component="h2">
              Counterparty
            </Typography>
            <FormControl>
              {selectCp}
            </FormControl>
          </CardContent>
        </Card>
      </Col>
    </Row>
    </div>
  );
}


export default AteBasicInfoTable