import { take, fork, put, call, select } from 'redux-saga/effects'
import { showLoading, hideLoading } from 'react-redux-loading-bar'
import { actions as toastrActions } from 'react-redux-toastr'
import * as actions from '../actions'
import * as api from '../services'
import { ComponentSource } from 'ag-grid-community/dist/lib/components/framework/userComponentFactory'

export function * callApi (apiAction, apiFn, options) {
  const { request, success, failure } = apiAction
  const { meta } = options
  try {
    yield put(request(meta))
    const response = yield call(apiFn, options)
    yield put(success(meta, response))
  } catch (error) {
    console.log(error)
    yield put(failure(meta, error))
  }
}

export const fetchAteBasicInfo = callApi.bind(null, actions.ateBasicInfo.fetch, api.ateBasicInfo.fetch)
// export const updateAteBasicInfo = callApi.bind(null, actions.ateBasicInfo.update, api.ateBasicInfo.update)

export const fetchResult = callApi.bind(null, actions.result.fetch, api.result.fetch)
export const fetchAteItem = callApi.bind(null, actions.ateItem.fetch, api.ateItem.fetch)

export const fetchAteCondition = callApi.bind(null, actions.ateCondition.fetch, api.ateCondition.fetch)
export const updateAteCondition = callApi.bind(null, actions.ateCondition.update, api.ateCondition.update)

export const fetchCsaCondition = callApi.bind(null, actions.csaCondition.fetch, api.csaCondition.fetch)
export const updateCsaCondition = callApi.bind(null, actions.csaCondition.update, api.csaCondition.update)

export const fetchCsaThreshold = callApi.bind(null, actions.csaThreshold.fetch, api.csaThreshold.fetch)
export const createCsaThreshold = callApi.bind(null, actions.csaThreshold.create, api.csaThreshold.create)
export const updateCsaThreshold = callApi.bind(null, actions.csaThreshold.update, api.csaThreshold.update)
export const deleteCsaThreshold = callApi.bind(null, actions.csaThreshold.delete, api.csaThreshold.delete)

export const fetchFinancialData = callApi.bind(null, actions.financialData.fetch, api.financialData.fetch)
export const createFinancialData = callApi.bind(null, actions.financialData.create, api.financialData.create)
export const updateFinancialData = callApi.bind(null, actions.financialData.update, api.financialData.update)
export const deleteFinancialData = callApi.bind(null, actions.financialData.delete, api.financialData.delete)

function * watchCallApi () {
  while (true) {
    const action = yield take(action => ~action.type.indexOf('api/'))
    if (action.error && action.payload.response && action.payload.response.status === 401) {
      // const lastURL = yield select(selectors.getPathname)
      // const lastURLOptions = { path: '/' }
      // return yield [call( 'last_url', lastURL, lastURLOptions)]
    }

    if (~action.type.indexOf('REQUEST')) yield put(showLoading())
    if (~action.type.indexOf('SUCCESS')) yield put(hideLoading())
    if (~action.type.indexOf('FAILURE')) yield [put(hideLoading()) , put(errorToastr(action.type, action.payload))]
    
    // if (action.error && action.payload.message === 'Unauthorized') yield put(login())
  }
}

function errorToastr (type, payload) {
  const { add } = toastrActions
  return add({
    type: 'error',
    title: 'Something was wrong!',
    position: 'top-right',
    message: `#(${type}) ${payload}`,
    options: {
      removeOnHover: true
    }

  })
}

export default function * () {
  yield [
    fork(watchCallApi)
  ]
}
