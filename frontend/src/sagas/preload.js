import { take, fork, put, all, call } from 'redux-saga/effects'
import * as types from '../constants/'
// import * as actions from '../actions'
import * as api from './api'
// import * as selectors from '../reducers/selectors'

export function * watchLoadAteTracker () {
  console.log("checking here");
  while (true) {
    
    const { payload: clientName } =  yield take(types.LOAD_ATE_TRACKER)
    yield call(api.fetchAteBasicInfo, { clientName })
    // yield call(api.fetchAteConditionNames)
  }
}


export default function * () {
  console.log("checking here");
  yield all([
    fork(watchLoadAteTracker),
  ])
}
