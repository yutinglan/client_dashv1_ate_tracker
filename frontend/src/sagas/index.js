import { fork, all } from 'redux-saga/effects'
import app from './app'
// import api from './api'
import preload from './preload'


export default function * rootSaga () {
  console.log("checking here");
  yield all([
    fork(app),
    fork(preload),
  ])
}
