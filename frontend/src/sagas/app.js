import { take, fork, call, all } from 'redux-saga/effects'
import * as api from './api'
import * as types from '../constants'

export function * watchFetchAteData() {
  while (true) {
    const { payload: query } = yield take(types.FETCH_ATE_DATA)
    yield call(api.fetchResult, {query})
    yield call(api.fetchAteCondition, {...query})
    yield call(api.fetchCsaCondition, {...query})
    yield call(api.fetchFinancialData, {...query})
    yield call(api.fetchAteItem, {...query})
    yield call(api.fetchCsaThreshold, {...query})
  }
}

export function * watchUpdateAteCondition() {
  while (true) {
    const { payload: ateCondition } = yield take(types.UPDATE_ATE_CONDITION)
    yield call(api.updateAteCondition, {ateCondition})
  }
}

export function * watchUpdateCsaCondition() {
  while (true) {
    const { payload: query } = yield take(types.UPDATE_CSA_CONDITION)
    yield call(api.updateCsaCondition, {query})
  }
}

export function * watchCreateCsaThreshold() {
  while (true) {
    const { payload: query } = yield take(types.CREATE_CSA_THRESHOLD)
    yield call(api.createCsaThreshold, {query})
  }
}

export function * watchUpdateCsaThreshold() {
  while (true) {
    const { payload: query } = yield take(types.UPDATE_CSA_THRESHOLD)
    yield call(api.updateCsaThreshold, {query})
  }
}

export function * watchDeleteCsaThreshold() {
  while (true) {
    const { payload: query } = yield take(types.DELETE_CSA_THRESHOLD)
    yield call(api.deleteCsaThreshold, {query})
  }
}

export function * watchCreateFinancialData() {
  while (true) {
    const { payload: query } = yield take(types.CREATE_FINANCIAL_DATA)
    yield call(api.createFinancialData, {query})
  }
}

export function * watchUpdateFinancialData() {
  while (true) {
    const { payload: query } = yield take(types.UPDATE_FINANCIAL_DATA)
    yield call(api.updateFinancialData, {query})
  }
}

export function * watchDeleteFinancialData() {
  while (true) {
    const { payload: query } = yield take(types.DELETE_FINANCIAL_DATA)
    yield call(api.deleteFinancialData, {query})
  }
}


export default function * () {
  console.log("checking here");
  yield all([
    fork(watchFetchAteData),

    fork(watchUpdateAteCondition),

    fork(watchUpdateCsaCondition),

    fork(watchCreateCsaThreshold),
    fork(watchUpdateCsaThreshold),
    fork(watchDeleteCsaThreshold),

    fork(watchCreateFinancialData),
    fork(watchUpdateFinancialData),
    fork(watchDeleteFinancialData),

  ])
}
