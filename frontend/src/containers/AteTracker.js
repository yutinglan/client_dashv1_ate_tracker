import React from 'react'
import { connect } from 'react-redux'
import { AteTracker } from '../components/AteTrackerComponent'
import { Layout } from '../components/LayoutComponent'
import * as actions from '../actions'
import { ateCondition, csaThreshold } from '../services'
function getClientName(url){
  var re =/\/(.*)\//;
  var client = re.exec(url)[1];
  return client
}
const mapStateToProps = (state, ownProps) => {
  var client = {'client':getClientName(ownProps.location.pathname)}
  console.log(ownProps.location)

  // const ateBasicinfo =  state.app.ateBasicinfo
  // const ateConditionNames = state.app.ateConditionNames
  console.log('reach container')
  let legalEntities = {}
  state.app.ateBasicInfo
  ? state.app.ateBasicInfo
    .forEach(function(ele, idx){
      const obj = {}
      obj[ele.csa_id] = ele.cp
      if(ele.entity in legalEntities){
        legalEntities[ele.entity].push(obj)
      }else{
        legalEntities[ele.entity] = [obj]
      }
    })
  : {}

  let ateConditions = [] 
  state.app.ateConditions
  ? state.app.ateConditions
    .forEach(function(ele, idx){
      let tempObj = {}
      tempObj['conditionName'] = ele.ate_condition.condition_name
      tempObj['active'] = ele.ate_condition.active
      tempObj['threshold'] = ele.ate_condition.threshold
      let item_names = ele.ate_items.map(ele2 => ele2.item_name)
      if(ele.multiplier>0 && ele.numerator >0){
        tempObj['positiveNumerator'] = item_names.join(',')
      }else if(ele.multiplier>0 && ele.numerator <0){
        tempObj['positiveDenominator'] = item_names.join(',')
      }else if(ele.multiplier<0 && ele.numerator>0){
        tempObj['negativeNumerator'] = item_names.join(',')
      }else{
        tempObj['negativeDenominator'] = item_names.join(',')
      }
      ateConditions.push(tempObj)
    })
  : []

  let csaConditions = [] 
  state.app.csaConditions
  ? state.app.csaConditions
    .forEach(function(ele, idx){
      let tempObj = {}
      tempObj['csaName'] = ele.csa_condition.condition_name
      tempObj['active'] = ele.csa_condition.active
      let item_names = ele.ate_items.map(ele2 => ele2.item_name)
      if(ele.positive==true){
        tempObj['positive'] = item_names.join(',')
      }else{
        tempObj['negative'] = item_names.join(',')
      }
      csaConditions.push(tempObj)
    })
  : []

  let csaThresholds = {}
  state.app.csaThresholds
  ? state.app.csaThresholds
    .forEach(function(ele, idx){
      let tempObj = {}
      if(csaThresholds[ele.csa_condition.condition_name] === undefined){
        csaThresholds[ele.csa_condition.condition_name] = []
      }
      tempObj['csaThreshold'] = ele.csa_threshold
      tempObj['conditionThreshold'] = ele.condition_threshold
      csaThresholds[ele.csa_condition.condition_name].push(tempObj)
    })
  : []

  let selectList = []
  state.app.selectList
  ? state.app.selectList
    .forEach(function(ele, idx){
      let tempObj = {}
      tempObj['value'] = ele.item_name
      tempObj['label'] = ele.item_name
      selectList.push(tempObj)
    })
  : {}
  return { legalEntities, ateConditions, selectList, csaConditions, csaThresholds, client }
}

const mapDispatchToProps = (dispatch) => ({
  loadAteTracker: (clientName) => dispatch(actions.loadAteTracker(clientName)),

  fetchAteData: (query) => dispatch(actions.fetchAteData(query)),
  
  updateAteCondition: (query) => dispatch(actions.updateAteCondition(query)),

  updateCsaCondition: (query) => dispatch(actions.updateCsaCondition(query)),

  createCsaThreshold: (query) => dispatch(actions.createCsaThreshold(query)),
  updateCsaThreshold: (query) => dispatch(actions.updateCsaThreshold(query)),
  deleteCsaThreshold: (query) => dispatch(actions.deleteCsaThreshold(query)),

  createFinancialData: (query) => dispatch(actions.createFinancialData(query)),
  updateFinancialData: (query) => dispatch(actions.updateFinancialData(query)),
  deleteFinancialData: (query) => dispatch(actions.deleteFinancialData(query)),
})
const AteTrackerPage = connect(mapStateToProps, mapDispatchToProps)(AteTracker)
export default AteTrackerPage
