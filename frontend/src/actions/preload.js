import { createAction } from 'redux-actions'
import * as types from '../constants'

// Frontend Action
export const loadAteTracker = createAction(types.LOAD_ATE_TRACKER)