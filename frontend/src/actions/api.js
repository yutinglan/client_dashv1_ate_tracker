import { createAction } from 'redux-actions'
import * as types from '../constants'

// Actions Called by Backend
export const ateBasicInfo = {
  fetch: {
    request: createAction(types.FETCH_ATEBASICINFO_REQUEST, meta => undefined, meta => meta),
    success: createAction(types.FETCH_ATEBASICINFO_SUCCESS, (meta, response) => response, meta => meta),
    failure: createAction(types.FETCH_ATEBASICINFO_FAILURE, (meta, error) => error, meta => meta)
  }
}

export const result = {
  fetch: {
    request: createAction(types.FETCH_RESULT_REQUEST, meta => undefined, meta => meta),
    success: createAction(types.FETCH_RESULT_SUCCESS, (meta, response) => response, meta => meta),
    failure: createAction(types.FETCH_RESULT_FAILURE, (meta, error) => error, meta => meta)
  }
}

export const ateItem = {
  fetch: {
    request: createAction(types.FETCH_ATEITEM_REQUEST, meta => undefined, meta => meta),
    success: createAction(types.FETCH_ATEITEM_SUCCESS, (meta, response) => response, meta => meta),
    failure: createAction(types.FETCH_ATEITEM_FAILURE, (meta, error) => error, meta => meta)
  }
}

export const ateCondition = {
  fetch: {
    request: createAction(types.FETCH_ATECONDITION_REQUEST, meta => undefined, meta => meta),
    success: createAction(types.FETCH_ATECONDITION_SUCCESS, (meta, response) => response, meta => meta),
    failure: createAction(types.FETCH_ATECONDITION_FAILURE, (meta, error) => error, meta => meta)
  },
  update: {
    request: createAction(types.UPDATE_ATECONDITION_REQUEST, meta => undefined, meta => meta),
    success: createAction(types.UPDATE_ATECONDITION_SUCCESS, (meta, response) => response, meta => meta),
    failure: createAction(types.UPDATE_ATECONDITION_FAILURE, (meta, error) => error, meta => meta)
  },
}

export const csaCondition = {
  fetch: {
    request: createAction(types.FETCH_CSACONDITION_REQUEST, meta => undefined, meta => meta),
    success: createAction(types.FETCH_CSACONDITION_SUCCESS, (meta, response) => response, meta => meta),
    failure: createAction(types.FETCH_CSACONDITION_FAILURE, (meta, error) => error, meta => meta)
  },
  update: {
    request: createAction(types.UPDATE_CSACONDITION_REQUEST, meta => undefined, meta => meta),
    success: createAction(types.UPDATE_CSACONDITION_SUCCESS, (meta, response) => response, meta => meta),
    failure: createAction(types.UPDATE_CSACONDITION_FAILURE, (meta, error) => error, meta => meta)
  },
}

export const csaThreshold = {
  create: {
    request: createAction(types.CREATE_CSATHRESHOLD_REQUEST, meta => undefined, meta => meta),
    success: createAction(types.CREATE_CSATHRESHOLD_SUCCESS, (meta, response) => response, meta => meta),
    failure: createAction(types.CREATE_CSATHRESHOLD_FAILURE, (meta, error) => error, meta => meta)
  },
  fetch: {
    request: createAction(types.FETCH_CSATHRESHOLD_REQUEST, meta => undefined, meta => meta),
    success: createAction(types.FETCH_CSATHRESHOLD_SUCCESS, (meta, response) => response, meta => meta),
    failure: createAction(types.FETCH_CSATHRESHOLD_FAILURE, (meta, error) => error, meta => meta)
  },
  update: {
    request: createAction(types.UPDATE_CSATHRESHOLD_REQUEST, meta => undefined, meta => meta),
    success: createAction(types.UPDATE_CSATHRESHOLD_SUCCESS, (meta, response) => response, meta => meta),
    failure: createAction(types.UPDATE_CSATHRESHOLD_FAILURE, (meta, error) => error, meta => meta)
  },
  delete: {
    request: createAction(types.DELETE_CSATHRESHOLD_REQUEST, meta => undefined, meta => meta),
    success: createAction(types.DELETE_CSATHRESHOLD_SUCCESS, (meta, response) => response, meta => meta),
    failure: createAction(types.DELETE_CSATHRESHOLD_FAILURE, (meta, error) => error, meta => meta)
  },
}

export const financialData = {
  create: {
    request: createAction(types.CREATE_FINANCIALDATA_REQUEST, meta => undefined, meta => meta),
    success: createAction(types.CREATE_FINANCIALDATA_SUCCESS, (meta, response) => response, meta => meta),
    failure: createAction(types.CREATE_FINANCIALDATA_FAILURE, (meta, error) => error, meta => meta)
  },
  fetch: {
    request: createAction(types.FETCH_FINANCIALDATA_REQUEST, meta => undefined, meta => meta),
    success: createAction(types.FETCH_FINANCIALDATA_SUCCESS, (meta, response) => response, meta => meta),
    failure: createAction(types.FETCH_FINANCIALDATA_FAILURE, (meta, error) => error, meta => meta)
  },
  update: {
    request: createAction(types.UPDATE_FINANCIALDATA_REQUEST, meta => undefined, meta => meta),
    success: createAction(types.UPDATE_FINANCIALDATA_SUCCESS, (meta, response) => response, meta => meta),
    failure: createAction(types.UPDATE_FINANCIALDATA_FAILURE, (meta, error) => error, meta => meta)
  },
  delete: {
    request: createAction(types.DELETE_FINANCIALDATA_REQUEST, meta => undefined, meta => meta),
    success: createAction(types.DELETE_FINANCIALDATA_SUCCESS, (meta, response) => response, meta => meta),
    failure: createAction(types.DELETE_FINANCIALDATA_FAILURE, (meta, error) => error, meta => meta)
  },
}
