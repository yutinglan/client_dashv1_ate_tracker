import { createAction } from 'redux-actions'
import * as types from '../constants'
import { ateCondition, csaThreshold, financialData } from '../services'

// Frontend Action
export const fetchAteData = createAction(types.FETCH_ATE_DATA)

export const updateAteCondition = createAction(types.UPDATE_ATE_CONDITION, ateCondition => ateCondition)

export const updateCsaCondition = createAction(types.UPDATE_CSA_CONDITION, csaCondition => csaCondition)

export const createCsaThreshold = createAction(types.CREATE_CSA_THRESHOLD, csaThreshold => csaThreshold)
export const updateCsaThreshold = createAction(types.UPDATE_CSA_THRESHOLD, csaThreshold => csaThreshold)
export const deleteCsaThreshold = createAction(types.DELETE_CSA_THRESHOLD, id => id)

export const createFinancialData = createAction(types.CREATE_FINANCIAL_DATA, financialData => financialData)
export const updateFinancialData = createAction(types.UPDATE_FINANCIAL_DATA, financialData => financialData)
export const deleteFinancialData = createAction(types.DELETE_FINANCIAL_DATA, id => id)