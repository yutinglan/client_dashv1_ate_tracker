(function($){

    var params = fundsEditorJsParams;

    var fundsDiv = document.querySelector('#fundsGrid');
    var fundsGridOptions = null;
    var fundsColumnDefs = [
        {headerName: 'ID', field: 'fund_id', width: 70  },
        {headerName: 'Fund', field: 'fund'  },
        {headerName: 'Fund Currency', field: 'fund_currency'  },
        {headerName: 'Fund Type', field: 'fund_type'},
        {headerName: 'Short Code', field: 'fund_code'  },
    ];

    // Format Functions:

    function MySimpleCellEditor() {}

    MySimpleCellEditor.prototype.init = function(params) {
        this.gui = document.createElement('input');
        this.gui.type = 'text';
        this.gui.classList.add('my-simple-editor');

        this.params = params;

        var startValue;

        let keyPressBackspaceOrDelete =
            params.keyPress === KEY_BACKSPACE
            || params.keyPress === KEY_DELETE;

        if (keyPressBackspaceOrDelete) {
            startValue = '';
        } else if (params.charPress) {
            startValue = params.charPress;
        } else {
            startValue = params.value;
            if (params.keyPress !== KEY_F2) {
                this.highlightAllOnFocus = true;
            }
        }

        if (startValue!==null && startValue!==undefined) {
            this.gui.value = startValue;
        }
    };

    MySimpleCellEditor.prototype.getGui = function() {
        return this.gui;
    };

    MySimpleCellEditor.prototype.getValue = function() {
        return this.gui.value;
    };

    MySimpleCellEditor.prototype.afterGuiAttached = function() {
        this.gui.focus();
    };

    MySimpleCellEditor.prototype.myCustomFunction = function() {
        return {
            rowIndex: this.params.rowIndex,
            colId: this.params.column.getId()
        };
    };
    function commaFormat(params){
        return (Math.round(parseFloat(params.value))).toLocaleString();
    }


    // Main Functions:

    function createGridOptions (data, colDefs) {
        gridOptions = {
                columnDefs: colDefs,
                rowData: data,
                gridAutoHeight : true,
                enableFilter: true,
                headerHeight: 45,
                enableSorting: true,
                pagination: true,
                paginationPageSize: 15,
                rowSelection: 'single',
                onSelectionChanged: onFundSelectionChanged,
                components:{
                    mySimpleCellEditor: MySimpleCellEditor
                },
                defaultColDef: {
                // set every column width
                    // width: 150,
                    suppressMovable: true,
                    suppressFilter: true,
                    suppressMenu : true,
                    editable: false,
                    onGridReady: function (params) {
                        // params.columnApi.autoSizeAllColumns();
                        params.api.sizeColumnsToFit();
                        window.addEventListener('resize', function () {
                            setTimeout(function () {
                                params.columnApi.autoSizeAllColumns();
                                //params.api.sizeColumnsToFit();
                            })
                        })
                    }

                    // make every column use 'text' filter by default
                    <!--cellStyle: {'textAlign': 'center'},-->
                    //filter: 'agTextColumnFilter'
                },
            };
        return gridOptions
    }

    function generateFundTable (data) {

       if (fundsGridOptions != null){
             fundsGridOptions.api.setRowData(data.funds_data);
       } else {
            fundsGridOptions = createGridOptions(data.funds_data, fundsColumnDefs);
            fundsGrid =  new agGrid.Grid(fundsDiv, fundsGridOptions);
       }
    }

    function fetchFundData () {
       $.ajax({
            type: 'POST',
            url: params.fetch_funds_data,
            data: {
                'csrfmiddlewaretoken': params.csrf_token,
                'client': params.client,
                'label': params.label,
            },
            success: function(data) {
                generateFundTable(data);
            }
        });
    }

    function onFundSelectionChanged() {
        var selectedRows = fundsGridOptions.api.getSelectedRows();
        var selectedFundId = null;
        var selectedRowsString = 'Selected Row: ';
        if(selectedRows.length == 1){
            selectedRows.forEach( function(row, index) {
                selectedFundId = row.fund_id;
                selectedRowsString += selectedFundId;
            });
            document.querySelector('#selectedFunds').innerHTML = selectedRowsString;
        } else {
            console.log('Unexpected error: more than one row were selected')
        }
    }

    $('#resetFunds').click(function() {
        fetchFundData();
    });

    $(document).ready(function () {
        fetchFundData();
    });

})(jQuery);