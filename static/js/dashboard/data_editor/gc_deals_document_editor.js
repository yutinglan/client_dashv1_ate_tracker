(function($){


    var params = dealsHedgesEditorJsParams;
    var selectedDealId = null;
    var selectedRowsString = '';
    var dealDiv =document.querySelector('#dealgrid');
    var dealOptions = null;

    var indirectDiv =document.querySelector('#indirectgrid');
    var indirectOptions = null;

    var backwardCashflowsDiv =document.querySelector('#backwardcashflowgrid');
    var backwardCashflowsOptions = null;

    var forwardCashflowsDiv = document.querySelector('#forwardcashflowgrid');
    var forwardCashflowsOptions = null;

    var documentCashflowsDiv = document.querySelector('#documentcashflowgrid');
    var documentCashflowsGridOptions = null;

    var genColumnDefs = null;
    var documentCashflowsColumnDefs = [
         {headerName: 'Vehicle', field: 'vehicle',width:110},
         {headerName: 'Deal', field: 'deal',width:110},
         {headerName: 'Type', field: 'type',width:110},
         {headerName: 'Amount', field: 'amount',width:110, valueFormatter:commaFormat,},
         {headerName: 'Currency', field: 'currency', width:120 , cellStyle: { 'text-align': "center" }},
         {headerName: 'Date', field: 'date', width:120 , cellStyle: { 'text-align': "right" }},
         {headerName: 'Current Date', field: 'current_date',width:110, cellStyle: { 'text-align': "center" }},
     ];

       var dealsColumnDefs = [
         {headerName: 'Deal Name', field: 'deal__deal_name'},
         {headerName: 'Direct Currency', field: 'direct_local_currency'},
         {headerName: 'Entity', field: 'entity__entity_name'},
     ];

     var indirectdealsColumnDefs = null;

    // Format Functions:

    function MySimpleCellEditor() {}

    MySimpleCellEditor.prototype.init = function(params) {
        this.gui = document.createElement('input');
        this.gui.type = 'text';
        this.gui.classList.add('my-simple-editor');

        this.params = params;

        var startValue;

        let keyPressBackspaceOrDelete =
            params.keyPress === KEY_BACKSPACE
            || params.keyPress === KEY_DELETE;

        if (keyPressBackspaceOrDelete) {
            startValue = '';
        } else if (params.charPress) {
            startValue = params.charPress;
        } else {
            startValue = params.value;
            if (params.keyPress !== KEY_F2) {
                this.highlightAllOnFocus = true;
            }
        }

        if (startValue!==null && startValue!==undefined) {
            this.gui.value = startValue;
        }
    };

    MySimpleCellEditor.prototype.getGui = function() {
        return this.gui;
    };

    MySimpleCellEditor.prototype.getValue = function() {
        return this.gui.value;
    };

    MySimpleCellEditor.prototype.afterGuiAttached = function() {
        this.gui.focus();
    };

    MySimpleCellEditor.prototype.myCustomFunction = function() {
        return {
            rowIndex: this.params.rowIndex,
            colId: this.params.column.getId()
        };
    };
    function commaFormat(params) {
        return (Math.round(params.value)).toLocaleString()+'.00';
    }

       // converting cell value to percentage
    function percentageFormatter(params) {
        var vaule_percentage = params.value ;
        return vaule_percentage + '%';
    }

    function getDatePicker() {
        // function to act as a class
        function Datepicker() {}

        // gets called once before the renderer is used
        Datepicker.prototype.init = function(params) {
            // create the cell
            this.eInput = document.createElement('input');
            this.eInput.value = params.value;

            // https://jqueryui.com/datepicker/
            $(this.eInput).datepicker({
                dateFormat: 'yy-mm-dd'
            });
        };

        // gets called once when grid ready to insert the element
        Datepicker.prototype.getGui = function() {
            return this.eInput;
        };

        // focus and select can be done after the gui is attached
        Datepicker.prototype.afterGuiAttached = function() {
            this.eInput.focus();
            this.eInput.select();
        };

        // returns the new value after editing
        Datepicker.prototype.getValue = function() {
            return this.eInput.value;
        };

        // any cleanup we need to be done here
        Datepicker.prototype.destroy = function() {
            // but this example is simple, no cleanup, we could
            // even leave this method out as it's optional
        };

        // if true, then this editor will appear in a popup
        Datepicker.prototype.isPopup = function() {
            // and we could leave this method out also, false is the default
            return false;
        };

        return Datepicker;
    }

    // Main Functions:

    function createGridOptions (data, colDefs) {
        gridOptions = {
                columnDefs: colDefs,
                rowData: data,
                enableFilter: true,
                headerHeight: 45,
                enableSorting: true,
                pagination: false,
                paginationPageSize: 15,
                components:{
                    mySimpleCellEditor: MySimpleCellEditor,
                    datePicker: getDatePicker()
                },
                paginationPageSize: 15,
                rowSelection: 'single',
                domLayout: 'autoHeight',
                suppressHorizontalScroll: false,
                defaultColDef: {
                // set every column width
                    suppressMovable: true,
                    suppressFilter: true,
                    suppressMenu : true,
                    editable: false,
                    onGridReady: function (params) {
                         params.columnApi.autoSizeAllColumns();
//                        params.api.sizeColumnsToFit();
                        window.addEventListener('resize', function () {
                            setTimeout(function () {
                                params.columnApi.autoSizeAllColumns();
//                                params.api.sizeColumnsToFit();
                            })
                        })
                    }

                    // make every column use 'text' filter by default
                    <!--cellStyle: {'textAlign': 'center'},-->
                    //filter: 'agTextColumnFilter'
                },
            };
        return gridOptions
    }



        function creategrideditableGridOptions (data, colDefs) {
        gridOptions = {
                columnDefs: colDefs,
                rowData: data,
                enableFilter: true,
                headerHeight: 45,
                enableSorting: true,
                pagination: false,
                suppressHorizontalScroll: false,
                paginationPageSize: 15,
//                components:{
//                    mySimpleCellEditor: MySimpleCellEditor
//                },
                components:{
                    datePicker: getDatePicker()
                },
                paginationPageSize: 15,
                rowSelection: 'single',
                enableColResize: true,
                domLayout: 'autoHeight',
                defaultColDef: {
                // set every column width
                    suppressMovable: true,
                    suppressFilter: true,
                    suppressMenu : true,
                    editable: true,
                    onGridReady: function (params) {
                         params.columnApi.autoSizeAllColumns();
//                        params.api.sizeColumnsToFit();
                        window.addEventListener('resize', function () {
                            setTimeout(function () {
                                params.columnApi.autoSizeAllColumns();
//                                params.api.sizeColumnsToFit();
                            })
                        })
                    }

                    // make every column use 'text' filter by default
                    <!--cellStyle: {'textAlign': 'center'},-->
                    //filter: 'agTextColumnFilter'
                },
            };
        return gridOptions
    }


    function indirect_grid(data){
              if (indirectOptions != null) {
                indirectOptions.api.setRowData(data.indirect_deals);
                indirectOptions.api.sizeColumnsToFit();

           } else {


             indirectOptions = {
                    columnDefs: [

                        {headerName: 'ID ',  hide: true, field: 'id', editable:false},
                        {headerName: 'Deal Name', field: 'deal__deal_name', editable:false},
                        {headerName: 'Indirect Currency', field: 'indirect_local_currency__identifier',
                            cellEditor: 'agRichSelectCellEditor',
                                cellEditorParams: {
                                    values: data.currency_list
                                }},
                        { headerName: 'Percentage',
                            field: 'percentage',
                            valueFormatter: percentageFormatter,
                            cellClass: 'number-cell',
                            suppressFilter: true},
                    ],
                    rowData: data.indirect_deals,
                    domLayout:'autoHeight',
                    enableFilter: true,
                    enableSorting: true,
                    headerHeight: 45,
                    pagination: false,
                    suppressHorizontalScroll: false,
                    paginationPageSize: 15,
    //                components:{
    //                    mySimpleCellEditor: MySimpleCellEditor
    //                },
                    enableColResize: true,

                    defaultColDef: {
                    // set every column width
                        suppressMovable: true,
                        suppressMenu : true,
                        editable: true,

                        onGridReady: function (params) {
                            // params.columnApi.autoSizeAllColumns();
                             params.api.sizeColumnsToFit();
                            window.addEventListener('resize', function () {
                                setTimeout(function () {
                                   // params.columnApi.autoSizeAllColumns();
                                    params.api.sizeColumnsToFit();
                                })
                            })
                        }
                    },
                };
                documentindirectGrid =  new agGrid.Grid(indirectDiv, indirectOptions);
                indirectOptions.api.sizeColumnsToFit();
           }
    }


    function indirect_deals(deal_name=null){
      if (deal_name==null){
            var selectedRows = dealOptions.api.getFirstDisplayedRow();
            if (selectedRows == 0)  {
                selectedRows = dealOptions.api.getDisplayedRowAtIndex(0);
                deal_name = selectedRows.data.deal__deal_name;
                selectedDealId = deal_name;
                selectedRowsString += selectedDealId;
                document.querySelector('#selectedDeals').innerHTML = 'Selected Row: ' +selectedDealId;
            }
        }
       if (deal_name != null) {
           $.ajax({
                type: 'POST',
                url: params.indirect_specific_tables_url,
                data: {
                    'csrfmiddlewaretoken': params.csrf_token,
                    'client': params.client,
                    'label': params.label,
                    'deal_name': deal_name,
                },
                success: function(data) {
                    indirect_grid(data);

                }
            });

        }
    }
     function onHedgeSelectionChanged() {
            var selectedRows = dealOptions.api.getSelectedRows();
            if(selectedRows.length == 1){
                selectedRows.forEach( function(row, index) {
                    selectedDealId = row.deal__deal_name;
                });
                document.querySelector('#selectedDeals').innerHTML = 'Selected Row: ' +selectedDealId;
                indirect_deals(deal_name = selectedDealId);
            } else {

                console.log('Unexpected error: more than one row were selected')
            }
        }


    function forwardgrid(data)
    {
         if (forwardCashflowsOptions != null) {
            forwardCashflowsOptions.api.setRowData(data.forward_cashflow);
            forwardCashflowsOptions.api.sizeColumnsToFit();
       } else {
         genColumnDefs = [
                {headerName: 'Deal Name', field: 'deal__deal_name',  editable: true,cellEditor: 'agRichSelectCellEditor',
                                        cellEditorParams: {
                                            values: data.deal_list
                                        }},
                {headerName: 'Cashflows', field: 'cashflows', valueFormatter:commaFormat, editable: true},
                {headerName: 'Value Date', field: 'value_date', editable: true, cellEditor: 'datePicker'},
                {headerName: 'Cashflow Type', field: 'cf_type',     editable: true,cellEditor: 'agRichSelectCellEditor',
                                        cellEditorParams: {
                                            values: ['Purchase','Contribution']
                                        }},
            ];
            forwardCashflowsOptions = createGridOptions(data.forward_cashflow, genColumnDefs);
            forwardCashflowGrid =  new agGrid.Grid(forwardCashflowsDiv, forwardCashflowsOptions);

       }
    }
    function generateCashflowTables (data) {
//
//       if (cashflowsOptions != null){
//             cashflowsOptions.api.setRowData(data.cashflow);
//       } else {
//            cashflowsOptions = createGridOptions(data.cashflow, genColumnDefs);
//            cashflowGrid =  new agGrid.Grid(cashflowsDiv, cashflowsOptions);
//       }

       if (backwardCashflowsOptions != null) {
            backwardCashflowsOptions.api.setRowData(data.backward_cashflow);

       } else {
              genColumnDefs = [
                {headerName: 'Deal Name', field: 'deal__deal_name',  editable: false,cellEditor: 'agRichSelectCellEditor',
                                        cellEditorParams: {
                                            values: data.deal_list
                                        }},
                {headerName: 'Cashflows', field: 'cashflows', valueFormatter:commaFormat, editable: false},
                {headerName: 'Value Date', field: 'value_date', editable: false, cellEditor: 'datePicker'},
                {headerName: 'Cashflow Type', field: 'cf_type',     cellEditor: 'agRichSelectCellEditor',
                                        cellEditorParams: {
                                            values: ['Purchase','Contribution']
                                        }},
            ];
            backwardCashflowsOptions = createGridOptions(data.backward_cashflow, genColumnDefs);
            backwardCashflowGrid =  new agGrid.Grid(backwardCashflowsDiv, backwardCashflowsOptions);
             backwardCashflowsOptions.api.sizeColumnsToFit();
       }

      forwardgrid(data);

//       if (documentCashflowsGridOptions != null) {
//            documentCashflowsGridOptions.api.setRowData(data.document_cashflow);
//       } else {
//            documentCashflowsGridOptions = createGridOptions(data.document_cashflow, documentCashflowsColumnDefs);
//            documentCashflowGrid =  new agGrid.Grid(documentCashflowsDiv, documentCashflowsGridOptions);
//       }

       if (dealOptions != null) {
            dealOptions.api.setRowData(data.deals);
       } else {
            dealOptions = creategrideditableGridOptions(data.deals, dealsColumnDefs);

            dealOptions['onSelectionChanged']=onHedgeSelectionChanged;
            documenDealsGrid =  new agGrid.Grid(dealDiv, dealOptions);
       }



    }
    function closeSnoAlertBox(msg="Error Undefined"){
        $('#snoAlertBox').text(msg);
            $('#snoAlertBox').addClass("alert alert-danger");
        window.setTimeout(function () {
          $("#snoAlertBox").fadeOut(300)
                    }, 3000);
    }

    function closeSuccesslertBox(msg="Error Undefined"){
        $('#successBox').text(msg);
        $('#successBox').addClass("alert alert-success");
        window.setTimeout(function () {
          $("#successBox").fadeOut(300)
                    }, 3000);
    }
    function fetchCashflowsData () {
       $.ajax({
            type: 'POST',
            url: params.cashflow_specific_tables_url,
            data: {
                'csrfmiddlewaretoken': params.csrf_token,
                'client': params.client,
            },
            success: function(data) {
                generateCashflowTables(data);
               if (selectedDealId== null){
                    if (data.deals.length > 0)
                    {
                      selectedDealId = data.deals[0]['deal__deal_name'];
                      document.querySelector('#selectedDeals').innerHTML = 'Selected Row: ' +selectedDealId;
                    }
                }
                indirect_deals(deal_name = selectedDealId);

            }
        });
    }

    function printResult(res) {
        console.log('---------------------------------------')
        if (res.add) {

//            res.add.forEach( function(rowNode) {
//                console.log('Added Row Node', rowNode);
//            });
//            $("#snoAlertBox").fadeIn();
//            closeSnoAlertBox("New Deal id added",0);
        }
        if (res.remove) {
            res.remove.forEach( function(rowNode) {
                console.log('Removed Row Node', rowNode);
            });
        }
        if (res.update) {
            res.update.forEach( function(rowNode) {
                console.log('Updated Row Node', rowNode);
            });
        }
    }
    $("#addIDDeals").button().click(function(){
        var selectedRows = dealOptions.api.getSelectedRows();
        if (selectedRows.length > 0){
             var res = indirectOptions.api.updateRowData({add: [{id:indirectOptions.rowData.length+1,
                                        deal__deal_name:selectedRows[0].deal__deal_name,
                                        percentage:100}],   addIndex: 0});
             printResult(res);
        }
        else{
            $("#snoAlertBox").fadeIn();
            closeSnoAlertBox("Error no Deal name selected");
        }
    });

     $("#addFWDdeal").button().click(function(){
       var res = forwardCashflowsOptions.api.updateRowData({add: [{'cashflows':0.0,'value_date':'yyyy-mm-dd'}],   addIndex: 0});
        printResult(res);
//        }
//        else{
//            $("#snoAlertBox").fadeIn();
//            closeSnoAlertBox("Error no Deal name selected");
//        }
    });

     $("#saveIDDeals").button().click(function(){
        var current_rollingvalues = {};
        indirectOptions.api.forEachNode( function(rowNode, index) {
            current_rollingvalues[index] =rowNode.data;
        });
         $.ajax({
            type: 'POST',
            url: params.save_indirect_deal_data_url,
            data: {
               'csrfmiddlewaretoken': params.csrf_token,
                'client': params.client,
                'label': params.label,
                'current_dealvalues':JSON.stringify(current_rollingvalues),
            },
            success: function (data) {
                 if (data.errorcode == 1){
                     $("#successBox").fadeIn();
                      closeSuccesslertBox(data.msg + 'Indirect Deal');
                      indirect_deals(selectedDealId);
                      }
                      else{
                           $("#snoAlertBox").fadeIn();
                          closeSnoAlertBox(data.msg + 'Indirect Deal');
                      }
            }
        });


    });

      $("#saveFWDdeal").button().click(function(){
        var current_rollingvalues = {};
        forwardCashflowsOptions.api.forEachNode( function(rowNode, index) {
            current_rollingvalues[index] =rowNode.data;
        });
         $.ajax({
            type: 'POST',
            url: params.save_fwd_deal_data_url,
            data: {
               'csrfmiddlewaretoken': params.csrf_token,
                'client': params.client,
                'label': params.label,
                'current_dealvalues':JSON.stringify(current_rollingvalues),
            },
            success: function (data) {
                 if (data.errorcode == 1){
                     $("#successBox").fadeIn();
                      closeSuccesslertBox(data.msg + 'Forward Cash Flow ');
                      forwardgrid(data);
                      }
                      else{
                           $("#snoAlertBox").fadeIn();
                          closeSnoAlertBox(data.msg + 'Forward Cash Flow ');
                      }
            }
        });


    });
    $('#resetIDDeals').click(function() {
        indirect_deals(selectedDealId);

    });
     $('#resetFWDdeal').click(function() {
        fetchCashflowsData();

    });
$("#cashflow-editor-tab").click(function() {
  backwardCashflowsOptions.api.sizeColumnsToFit();
});
$("#assign-hedge-tab").click(function() {
  forwardCashflowsOptions.api.sizeColumnsToFit();
});

    $(document).ready(function () {
        fetchCashflowsData();
    });

})(jQuery);