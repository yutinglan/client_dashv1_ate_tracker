"use strict";

$(function() {
    const params = dealsHedgesEditorJsParams;

    /**
    DEALS GO HERE
    */

    // Deal CRUD Variables
    const $dealGrid = $('#table-deal');
    const $dealCRUD = $('.CRUD-deal');
    const $dealViewing = $dealCRUD.find('.viewing').eq(0);
    const $dealAddEdit = $dealCRUD.find('.add-edit').eq(0);
    const $dealForm = $('.CRUD-deal form').eq(0);
    const $dealNew = $dealCRUD.find('.new').eq(0);
    const $dealCancel = $dealForm.find('.cancel').eq(0);
    const $dealSubmit = $dealForm.find('.submit').eq(0);
    let isDealLoading = false;

    function populateClientDealForm(grid) {
        $dealViewing.addClass('hidden');
        $dealAddEdit.removeClass('hidden');

        const selectedRow = grid.api.getSelectedRows()[0];

        const $deal_id = $dealForm.find('.deal_id').eq(0);
        const $deal_name = $dealForm.find('.deal_name').eq(0);
        const $cancellation = $dealForm.find('.cancellation').eq(0);

        $deal_id.val(selectedRow.deal_id);
        $deal_name.val(selectedRow.deal_name);
        $cancellation.val(selectedRow.cancellation);
    }

    function toggleClientDealFormButtons(isLoading) {
        isDealLoading = isLoading;
        $dealCancel.prop('disabled', isLoading);
        $dealSubmit.prop('disabled', isLoading);
    }

    function submitClientDealForm(e) {
        if (isDealLoading) { e.preventDefault(); return false; }

        const payload = {}
        const serializeArray = $dealForm.serializeArray();

        toggleClientDealFormButtons(true);

        serializeArray.forEach(function(obj) {
            payload[obj.name] = obj.value;
        })

        $.ajax({
            type: 'POST',
            url: params.ajax_client_deal,
            data: Object.assign({
                'csrfmiddlewaretoken': params.csrf_token,
                'client': params.client
            }, payload),
            success: function (data) {
                populateClientDealGrid(data.deals);
                cancelClientDealForm();
            },
            error: function() {
                alert('There was an error adding your deal. Contact the tech team if this persists.');
            },
            complete: function() {
                toggleClientDealFormButtons(false);
            }
        });

        e.preventDefault();
        return false;
    }

    function clearClientDealForm() {
        const $deal_id = $dealForm.find('.deal_id').eq(0);
        const $deal_name = $dealForm.find('.deal_name').eq(0);
        const $cancellation = $dealForm.find('.cancellation').eq(0);

        $deal_id.val(null);
        $deal_name.val(null);
        $cancellation.val(null);
    }

    function addNewClientDealForm() {
        clearClientDealForm();

        $dealViewing.addClass('hidden');
        $dealAddEdit.removeClass('hidden');
    }

    function cancelClientDealForm() {
        clearClientDealForm();

        $dealViewing.removeClass('hidden');
        $dealAddEdit.addClass('hidden');
    }

    const clientDealGridOptions = {
        columnDefs: [
            { headerName: 'Deal ID', field: 'deal_id' },
            { headerName: 'Deal Name', field: 'deal_name' },
            { headerName: 'Cancellation', field: 'cancellation' }
        ],
        domLayout: 'autoHeight',
        enableFilter: true,
        enableSorting: true,
        headerHeight: 45,
        pagination: true,
        paginationPageSize: 15,
        defaultColDef: {
            suppressMovable: true,
            suppressMenu: true
        },
        rowSelection: 'single',
        onSelectionChanged: populateClientDealForm,
        onGridReady: function (params) {
            params.api.sizeColumnsToFit();
        }
    }

    // Initializing the grid
    new agGrid.Grid($dealGrid[0], clientDealGridOptions);

    function populateClientDealGrid(deals) {
        clientDealGridOptions.api.setRowData(deals)
    }

    // Event Bindings

    // Fetch data on navigation
    $('a[href="#data_editor_tab"]').on('click', function() {
        populateClientDealGrid();

        $.ajax({
            type: 'GET',
            url: params.ajax_client_deal,
            data: {
                'csrfmiddlewaretoken': params.csrf_token,
                'client': params.client
            },
            success: function (data) {
                populateClientDealGrid(data.deals);
            }
        })
    })

    $dealForm.on('submit', function(e) {
        submitClientDealForm(e);
    });

    $dealCancel.on('click', function(e) {
        cancelClientDealForm(e);
    });

    $dealNew.on('click', function(e) {
        addNewClientDealForm(e);
    });

    /**
    ENTITIES GO HERE
    */

    // Entity CRUD Variables
    const $entityGrid = $('#table-entity');
    const $entityCRUD = $('.CRUD-entity');
    const $entityViewing = $entityCRUD.find('.viewing').eq(0);
    const $entityAddEdit = $entityCRUD.find('.add-edit').eq(0);
    const $entityForm = $('.CRUD-entity form').eq(0);
    const $entityNew = $entityCRUD.find('.new').eq(0);
    const $entityCancel = $entityForm.find('.cancel').eq(0);
    const $entitySubmit = $entityForm.find('.submit').eq(0);
    let isEntityLoading = false;

    function populateClientEntityForm(grid) {
        $entityViewing.addClass('hidden');
        $entityAddEdit.removeClass('hidden');

        const selectedRow = grid.api.getSelectedRows()[0];

        const $id = $entityForm.find('.id').eq(0);
        const $entity_name = $entityForm.find('.entity_name').eq(0);
        const $cancellation = $entityForm.find('.cancellation').eq(0);

        $id.val(selectedRow.id);
        $entity_name.val(selectedRow.entity_name);
        $cancellation.val(selectedRow.cancellation);
    }

    function toggleClientEntityFormButtons(isLoading) {
        isEntityLoading = isLoading;
        $entityCancel.prop('disabled', isLoading);
        $entitySubmit.prop('disabled', isLoading);
    }

    function submitClientEntityForm(e) {
        if (isEntityLoading) { e.preventDefault(); return false; }

        const payload = {}
        const serializeArray = $entityForm.serializeArray();

        toggleClientEntityFormButtons(true);

        serializeArray.forEach(function(obj) {
            payload[obj.name] = obj.value;
        })

        $.ajax({
            type: 'POST',
            url: params.ajax_client_entity,
            data: Object.assign({
                'csrfmiddlewaretoken': params.csrf_token,
                'client': params.client
            }, payload),
            success: function (data) {
                populateClientEntityGrid(data.entities);
                cancelClientEntityForm();
            },
            error: function() {
                alert('There was an error adding your entity. Contact the tech team if this persists.');
            },
            complete: function() {
                toggleClientEntityFormButtons(false);
            }
        });

        e.preventDefault();
        return false;
    }

    function clearClientEntityForm() {
        const $id = $entityForm.find('.id').eq(0);
        const $entity_name = $entityForm.find('.entity_name').eq(0);
        const $cancellation = $entityForm.find('.cancellation').eq(0);

        $id.val(null);
        $entity_name.val(null);
        $cancellation.val(null);
    }

    function addNewClientEntityForm() {
        clearClientEntityForm();

        $entityViewing.addClass('hidden');
        $entityAddEdit.removeClass('hidden');
    }

    function cancelClientEntityForm() {
        clearClientEntityForm();

        $entityViewing.removeClass('hidden');
        $entityAddEdit.addClass('hidden');
    }

    const clientEntityGridOptions = {
        columnDefs: [
            { headerName: 'ID', field: 'id' },
            { headerName: 'Entity Name', field: 'entity_name' },
            { headerName: 'Cancellation', field: 'cancellation' }
        ],
        domLayout: 'autoHeight',
        enableFilter: true,
        enableSorting: true,
        headerHeight: 45,
        pagination: true,
        paginationPageSize: 15,
        defaultColDef: {
            suppressMovable: true,
            suppressMenu: true
        },
        rowSelection: 'single',
        onSelectionChanged: populateClientEntityForm,
        onGridReady: function (params) {
            params.api.sizeColumnsToFit();
        }
    }

    // Initializing the grid
    new agGrid.Grid($entityGrid[0], clientEntityGridOptions);

    function populateClientEntityGrid(entities) {
        clientEntityGridOptions.api.setRowData(entities)
    }

    // Event Bindings

    // Fetch data on navigation
    $('a[href="#data_editor_tab"]').on('click', function() {
        populateClientEntityGrid();

        $.ajax({
            type: 'GET',
            url: params.ajax_client_entity,
            data: {
                'csrfmiddlewaretoken': params.csrf_token,
                'client': params.client
            },
            success: function (data) {
                populateClientEntityGrid(data.entities);
            }
        });
    })

    $entityForm.on('submit', function(e) {
        submitClientEntityForm(e);
    });

    $entityCancel.on('click', function(e) {
        cancelClientEntityForm(e);
    });

    $entityNew.on('click', function(e) {
        addNewClientEntityForm(e);
    });

    /**
    Funds Go Here
    */

    // Deal CRUD Variables
    const $fundGrid = $('#table-fund');
    const $fundCRUD = $('.CRUD-fund');
    const $fundViewing = $fundCRUD.find('.viewing').eq(0);
    const $fundAddEdit = $fundCRUD.find('.add-edit').eq(0);
    const $fundForm = $fundCRUD.find('form').eq(0);
    const $fundNew = $fundCRUD.find('.new').eq(0);
    const $fundCancel = $fundForm.find('.cancel').eq(0);
    const $fundSubmit = $fundForm.find('.submit').eq(0);
    let isFundLoading = false;

    function populateClientFundForm(grid) {
        $fundViewing.addClass('hidden');
        $fundAddEdit.removeClass('hidden');

        const selectedRow = grid.api.getSelectedRows()[0];

        const $id = $fundForm.find('.id').eq(0);
        const $fundName = $fundForm.find('.fund_name').eq(0);
        const $ccy = $fundForm.find('.ccy').eq(0);
        const $client = $fundForm.find('.client_id').eq(0);

        $id.val(selectedRow.id);
        $fundName.val(selectedRow.fund_name);
        $ccy.val(selectedRow.ccy);
        $client.val(selectedRow.client);
    }

    function toggleClientFundFormButtons(isLoading) {
        isFundLoading = isLoading;
        $fundCancel.prop('disabled', isLoading);
        $fundSubmit.prop('disabled', isLoading);
    }

    function submitClientFundForm(e) {
        if (isFundLoading) { e.preventDefault(); return false; }

        const payload = {}
        const serializeArray = $fundForm.serializeArray();

        toggleClientFundFormButtons(true);

        serializeArray.forEach(function(obj) {
            payload[obj.name] = obj.value;
        })

        $.ajax({
            type: 'POST',
            url: params.ajax_client_fund,
            data: Object.assign({
                'csrfmiddlewaretoken': params.csrf_token,
                'client': params.client
            }, payload),
            success: function (data) {
                populateClientFundGrid(data.funds, data.ccys, data.clients);
                cancelClientFundForm();
            },
            error: function() {
                alert('There was an error adding your fund. Contact the tech team if this persists.');
            },
            complete: function() {
                toggleClientFundFormButtons(false);
            }
        });

        e.preventDefault();
        return false;
    }

    function clearClientFundForm() {
        const $id = $fundForm.find('.id').eq(0);
        const $fundName = $fundForm.find('.fund_name').eq(0);
        const $ccy = $fundForm.find('.ccy').eq(0);
        const $client = $fundForm.find('.client_id').eq(0);

        $id.val(null);
        $fundName.val(null);
        $ccy.val(null);
        $client.val(null);
    }

    function addNewClientFundForm() {
        clearClientDealForm();

        $fundViewing.addClass('hidden');
        $fundAddEdit.removeClass('hidden');
    }

    function cancelClientFundForm() {
        clearClientFundForm();

        $fundViewing.removeClass('hidden');
        $fundAddEdit.addClass('hidden');
    }

    const ccys = [];
    const clients = [];
    const clientFundGridOptions = {
        columnDefs: [
            { headerName: 'ID', field: 'id' },
            { headerName: 'Fund Name', field: 'fund_name' },
            {
                headerName: 'CCY',
                field: 'ccy',
                valueFormatter: function(params) {
                    const found = ccys.find(function(val) {
                        return params.value === val.id
                    })

                    console.log(found);
                    return (found) ? found.identifier : '';
                }
            }
        ],
        domLayout: 'autoHeight',
        enableFilter: true,
        enableSorting: true,
        headerHeight: 45,
        pagination: true,
        paginationPageSize: 15,
        defaultColDef: {
            suppressMovable: true,
            suppressMenu: true
        },
        rowSelection: 'single',
        onSelectionChanged: populateClientFundForm,
        onGridReady: function (params) {
            params.api.sizeColumnsToFit();
        }
    }

    // Initializing the grid
    new agGrid.Grid($fundGrid[0], clientFundGridOptions);

    function populateClientFundGrid(funds, _ccys, _clients) {
        while (ccys.length) {
            ccys.pop();
        }

        if (_ccys) {
            for (let i = 0; i < _ccys.length; i++) {
                ccys.push(_ccys[i]);
            }
        }

        while (clients.length) {
            clients.pop();
        }

        if (_clients) {
            for (let i = 0; i < _clients.length; i++) {
                clients.push(_clients[i]);
            }
        }

        const $ccy = $fundForm.find('.ccy').eq(0);
        const $client = $fundForm.find('.client_id').eq(0);

        $ccy.empty();
        ccys.forEach(function(ccy) {
            $ccy.append('<option value="'+ccy.id+'">'+ccy.identifier+' - ' + ccy.name + '</option>');
        });

        $client.empty();
        clients.forEach(function(client) {
            $client.append('<option value="'+client.id+'">'+client.name+'</option>');
        });

        clientFundGridOptions.api.setRowData(funds)
    }

    // Event Bindings

    // Fetch data on navigation
    $('a[href="#data_editor_tab"]').on('click', function() {
        populateClientFundGrid();

        $.ajax({
            type: 'GET',
            url: params.ajax_client_fund,
            data: {
                'csrfmiddlewaretoken': params.csrf_token,
                'client': params.client
            },
            success: function (data) {
                populateClientFundGrid(data.funds, data.ccys, data.clients);
            }
        })
    })

    $fundForm.on('submit', function(e) {
        submitClientFundForm(e);
    });

    $fundCancel.on('click', function(e) {
        cancelClientFundForm(e);
    });

    $fundNew.on('click', function(e) {
        addNewClientFundForm(e);
    });

});