"use strict";

$(function() {
    const params = dealsHedgesEditorJsParams;

    /**
    DEALS GO HERE
    */

    // Deal CRUD Variables
    const $dealGrid = $('#table-deal');
    const $dealCRUD = $('.CRUD-deal');
    const $dealViewing = $dealCRUD.find('.viewing').eq(0);
    const $dealAddEdit = $dealCRUD.find('.add-edit').eq(0);
    const $dealForm = $('.CRUD-deal form').eq(0);
    const $dealNew = $dealCRUD.find('.new').eq(0);
    const $dealCancel = $dealForm.find('.cancel').eq(0);
    const $dealSubmit = $dealForm.find('.submit').eq(0);
    const $valDate= $('#value_date');
    let isDealLoading = false;
    var fund_association_list;
    var deal_list;
    var fund_list;


    function populateClientDealForm(grid) {
        $dealViewing.addClass('hidden');
        $dealAddEdit.removeClass('hidden');

        const selectedRow = grid.api.getSelectedRows()[0];


        const $id = $dealForm.find('.id' ).eq(0);
        const $deal_name = $dealForm.find('.deal_name').eq(0);
        const $fund_name = $dealForm.find('.fund_name').eq(0);
        const $value_date = $dealForm.find('.value_date').eq(0);
        const $fx_rate = $dealForm.find('.fx_rate').eq(0);
        const $reset_rate = $dealForm.find('.reset_rate').eq(0);
        const $cancellation = $dealForm.find('.cancellation').eq(0);


        $id.val(selectedRow.id);
        document.getElementById("fund_name").options.length = 0;
        $.each(fund_list, function () {
           var options = ""
           let id = Object.keys(this)[0];
           let val = Object.values(this)[0];
           if (selectedRow.fund_name == val){
               options = "<option " + "value='" + val + "' selected>" + val + "</option>";
            }else{
               options = "<option " + "value='" + val + "'>" + val + "</option>";
            }
             $("#fund_name").append(options);
        });
        document.getElementById("deal_name").options.length = 0;
        $.each(fund_association_list[0][selectedRow.fund_name], function () {
           var options = ""
           let id = Object.keys(this)[0];
           let val = Object.values(this)[0];
           if (selectedRow.deal_name == val){
               options = "<option " + "value='" +val+ "' selected>" + val + "</option>";
            }else{
               options = "<option " + "value='" + val + "'>" + val + "</option>";
            }
            $("#deal_name").append(options);
        });

        $deal_name.val(document.getElementById("deal_name").value);
        $fund_name.val(document.getElementById("fund_name").value);
        var targetDate = new Date();
        targetDate = moment(selectedRow.value_date, 'YYYY-MM-DD').format('YYYY-MM-DD');
        $valDate.datepicker('setDate', targetDate);
        $valDate.datepicker("option", "dateFormat", "yy-mm-dd");
        $value_date.val($valDate.val());
        $cf_type.val(selectedRow.cf_type);
        $cashflows.val(selectedRow.cashflows);
        $fx_rate.val(selectedRow.fx_rate);
        $reset_rate.val(selectedRow.reset_rate);
        $cancellation.val(selectedRow.cancellation);
    }

    function toggleClientDealFormButtons(isLoading) {
        isDealLoading = isLoading;
        $dealCancel.prop('disabled', isLoading);
        $dealSubmit.prop('disabled', isLoading);
    }

    function submitClientDealForm(e) {
        if (isDealLoading) { e.preventDefault(); return false; }

        const payload = {}
        const serializeArray = $dealForm.serializeArray();

        toggleClientDealFormButtons(true);

        serializeArray.forEach(function(obj) {
            payload[obj.name] = obj.value;
        })
        payload['fund_name'] = document.getElementById("fund_name").value;
        payload['deal_name'] = document.getElementById("deal_name").value;

        $.ajax({
            type: 'POST',
            url: params.ajax_client_cashflow,
            data: Object.assign({
                'csrfmiddlewaretoken': params.csrf_token,
                'client': params.client
            }, payload),
            success: function (data) {
                populateClientDealGrid(data.cashflows);
                cancelClientDealForm();
            },
            error: function() {
                alert('There was an error adding your cashflow. Contact the tech team if this persists.');
            },
            complete: function() {
                toggleClientDealFormButtons(false);
            }
        });

        e.preventDefault();
        return false;
    }

    function clearClientDealForm() {
        const $id = $dealForm.find('.id' ).eq(0);
        const $deal_name = $dealForm.find('.deal_name').eq(0);
        const $fund_name = $dealForm.find('.fund_name').eq(0);
        const $value_date = $dealForm.find('.value_date').eq(0);
        const $cf_type = $dealForm.find('.cf_type').eq(0);
        const $cashflows = $dealForm.find('.cashflows').eq(0);
        const $fx_rate = $dealForm.find('.fx_rate').eq(0);
        const $reset_rate = $dealForm.find('.reset_rate').eq(0);
        const $cancellation = $dealForm.find('.cancellation').eq(0);

        $id.val(null);
        document.getElementById("fund_name").options.length = 0;
        document.getElementById("deal_name").options.length = 0;

        $fund_name.val(null);
        $deal_name.val(null);
        $value_date.val(null);
        $cf_type.val(null);
        $cashflows.val(null);
        $fx_rate.val(null);
        $reset_rate.val(null);
        $cancellation.val(null);
    }

    function addNewClientDealForm() {
        clearClientDealForm();

        $dealViewing.addClass('hidden');
        $dealAddEdit.removeClass('hidden');
    }

    function cancelClientDealForm() {
        clearClientDealForm();

        $dealViewing.removeClass('hidden');
        $dealAddEdit.addClass('hidden');
    }

    const clientDealGridOptions = {
        columnDefs: [
            { headerName: 'Transaction ID', field: 'id' },
            { headerName: 'Fund Name', field: 'fund_name' },
            { headerName: 'Deal Name', field: 'deal_name' },
            { headerName: 'Value Date', field: 'value_date' },
            { headerName: 'Cashflow Type', field: 'cf_type' },
            { headerName: 'Cashflows', field: 'cashflows' },
            { headerName: 'FX Rate', field: 'fx_rate' },
            { headerName: 'Reset Rate', field: 'reset_rate' },
            { headerName: 'Cancellation', field: 'cancellation' }
        ],
        domLayout: 'autoHeight',
        enableFilter: true,
        enableSorting: true,
        headerHeight: 45,
        pagination: true,
        paginationPageSize: 15,
        defaultColDef: {
            suppressMovable: true,
            suppressMenu: true
        },
        rowSelection: 'single',
        onSelectionChanged: populateClientDealForm,
        onGridReady: function (params) {
            params.api.sizeColumnsToFit();
        }
    }

    // Initializing the grid
    new agGrid.Grid($dealGrid[0], clientDealGridOptions);

    function populateClientDealGrid(deals) {
        clientDealGridOptions.api.setRowData(deals)
    }

    // Event Bindings

    // Fetch data on navigation
    function fetchDeal() {
        populateClientDealGrid();

        $.ajax({
            type: 'GET',
            url: params.ajax_client_cashflow,
            data: {
                'csrfmiddlewaretoken': params.csrf_token,
                'client': params.client,
                'fund':$('#fund-select').val(),
            },
            success: function (data) {
                deal_list = data.deal_name;
                fund_list = data.fund_name;
                fund_association_list =  JSON.parse(JSON.stringify(data.fund_association));
                populateClientDealGrid(data.cashflows);
            }
        })
    }

    $('#fund_name').change(function () {
        let count = 0;
        const $deal_name = $dealForm.find('.deal_name').eq(0);
        const $fund_name = $dealForm.find('.fund_name').eq(0);
        document.getElementById("deal_name").options.length = 0;
        $.each(fund_association_list[0][document.getElementById("fund_name").value], function () {
           var options = ""
           let id = Object.keys(this)[0];
           let val = Object.values(this)[0];
           if (count ==0 ){
               options = "<option " + "value='" +val+ "' selected>" + val + "</option>";
            }else{
               options = "<option " + "value='" + val + "'>" + val + "</option>";
            }
            count ++;
            $("#deal_name").append(options);
        });
        $deal_name.val(document.getElementById("deal_name").value);
        $fund_name.val(document.getElementById("fund_name").value);
    });

    $dealForm.on('submit', function(e) {
        submitClientDealForm(e);
    });

    $dealCancel.on('click', function(e) {
        cancelClientDealForm(e);
    });

    $dealNew.on('click', function(e) {
        addNewClientDealForm(e);
    });
    // jQuery UI Init
    $('#value_date').datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "yy-mm-dd"
    });

    $('#value_date').change(function () {
        const $value_date = $dealForm.find('.value_date').eq(0);
        var targetDate = new Date();
        targetDate = moment($valDate.val(), 'YYYY-MM-DD').format('YYYY-MM-DD');
        $valDate.datepicker('setDate', targetDate);
        $valDate.datepicker("option", "dateFormat", "yy-mm-dd");

    });

    /**
    ENTITIES GO HERE
    */

    // Entity CRUD Variables
    const $entityGrid = $('#table-entity');
    const $entityCRUD = $('.CRUD-entity');
    const $entityViewing = $entityCRUD.find('.viewing').eq(0);
    const $entityAddEdit = $entityCRUD.find('.add-edit').eq(0);
    const $entityForm = $('.CRUD-entity form').eq(0);
    const $entityNew = $entityCRUD.find('.new').eq(0);
    const $entityCancel = $entityForm.find('.cancel').eq(0);
    const $entitySubmit = $entityForm.find('.submit').eq(0);
    let isEntityLoading = false;
    const $valuationDate= $('#valuation_date');
    var dv_fund_association_list;
    var dv_deal_list;
    var dv_fund_list;

    function populateClientEntityForm(grid) {
        $entityViewing.addClass('hidden');
        $entityAddEdit.removeClass('hidden');

        const selectedRow = grid.api.getSelectedRows()[0];

        const $id = $entityForm.find('.id').eq(0);
        const $dv_deal_name = $dealForm.find('.dv_deal_name').eq(0);
        const $dv_fund_name = $dealForm.find('.dv_fund_name').eq(0);
        const $val_date = $dealForm.find('.valuation_date').eq(0);
        const $unrealised_value = $entityForm.find('.unrealised_value').eq(0);


        $id.val(selectedRow.id);
         document.getElementById("dv_fund_name").options.length = 0;
        $.each(dv_fund_list, function () {
           var options = ""
           let id = Object.keys(this)[0];
           let val = Object.values(this)[0];
           if (selectedRow.fund_name == val){
               options = "<option " + "value='" + val + "' selected>" + val + "</option>";
            }else{
               options = "<option " + "value='" + val + "'>" + val + "</option>";
            }
             $("#dv_fund_name").append(options);
        });
        document.getElementById("dv_deal_name").options.length = 0;
        $.each(dv_fund_association_list[0][selectedRow.fund_name], function () {
           var options = ""
           let id = Object.keys(this)[0];
           let val = Object.values(this)[0];
           if (selectedRow.deal_name == val){
               options = "<option " + "value='" +val+ "' selected>" + val + "</option>";
            }else{
               options = "<option " + "value='" + val + "'>" + val + "</option>";
            }
            $("#dv_deal_name").append(options);
        });

        $dv_deal_name.val(document.getElementById("dv_deal_name").value);
        $dv_fund_name.val(document.getElementById("dv_fund_name").value);
        var targetDate = new Date();
        targetDate = moment(selectedRow.valuation_date, 'YYYY-MM-DD').format('YYYY-MM-DD');
        $valuationDate.datepicker('setDate', targetDate);
        $valuationDate.datepicker("option", "dateFormat", "yy-mm-dd");
        $val_date.val($valuationDate.val());
        $unrealised_value.val(selectedRow.unrealised_value);
    }

    function toggleClientEntityFormButtons(isLoading) {
        isEntityLoading = isLoading;
        $entityCancel.prop('disabled', isLoading);
        $entitySubmit.prop('disabled', isLoading);
    }

    function submitClientEntityForm(e) {
        if (isEntityLoading) { e.preventDefault(); return false; }

        const payload = {}
        const serializeArray = $entityForm.serializeArray();

        toggleClientEntityFormButtons(true);

        serializeArray.forEach(function(obj) {
            payload[obj.name] = obj.value;
        })
        payload['dv_fund_name'] = document.getElementById("dv_fund_name").value;
        payload['dv_deal_name'] = document.getElementById("dv_deal_name").value;

        $.ajax({
            type: 'POST',
            url: params.ajax_client_dealvaluation,
            data: Object.assign({
                'csrfmiddlewaretoken': params.csrf_token,
                'client': params.client
            }, payload),
            success: function (data) {
                populateClientEntityGrid(data.dealvaluation);
                cancelClientEntityForm();
            },
            error: function() {
                alert('There was an error adding your entity. Contact the tech team if this persists.');
            },
            complete: function() {
                toggleClientEntityFormButtons(false);
            }
        });

        e.preventDefault();
        return false;
    }

    function clearClientEntityForm() {
        const $id = $entityForm.find('.id').eq(0);
        const $dv_fund_name = $dealForm.find('.dv_fund_name').eq(0);
        const $dv_deal_name = $dealForm.find('.dv_deal_name').eq(0);
        const $valuation_date = $dealForm.find('.valuation_date').eq(0);
        const $unrealised_value = $dealForm.find('.unrealised_value').eq(0);

        $id.val(null);
        document.getElementById("dv_fund_name").options.length = 0;
        document.getElementById("dv_deal_name").options.length = 0;

        $dv_fund_name.val(null);
        $dv_deal_name.val(null);
        $valuation_date.val(null);
        $unrealised_value.val(null);
    }

    function addNewClientEntityForm() {
        clearClientEntityForm();

        $entityViewing.addClass('hidden');
        $entityAddEdit.removeClass('hidden');
    }

    function cancelClientEntityForm() {
        clearClientEntityForm();

        $entityViewing.removeClass('hidden');
        $entityAddEdit.addClass('hidden');
    }

    const clientEntityGridOptions = {
        columnDefs: [
            { headerName: 'ID', field: 'id' },
            { headerName: 'Fund Name', field: 'fund_name' },
            { headerName: 'Deal Name', field: 'deal_name' },
            { headerName: 'Valuation Date', field: 'valuation_date' },
            { headerName: 'Unrealised Value', field: 'unrealised_value' },
        ],
        domLayout: 'autoHeight',
        enableFilter: true,
        enableSorting: true,
        headerHeight: 45,
        pagination: true,
        paginationPageSize: 15,
        defaultColDef: {
            suppressMovable: true,
            suppressMenu: true
        },
        rowSelection: 'single',
        onSelectionChanged: populateClientEntityForm,
        onGridReady: function (params) {
            params.api.sizeColumnsToFit();
        }
    }

    // Initializing the grid
    new agGrid.Grid($entityGrid[0], clientEntityGridOptions);

    function populateClientEntityGrid(entities) {
        clientEntityGridOptions.api.setRowData(entities)
    }

    // Event Bindings

    // Fetch data on navigation
    function fetchEntity(){
        populateClientEntityGrid();

        $.ajax({
            type: 'GET',
            url: params.ajax_client_dealvaluation,
            data: {
                'csrfmiddlewaretoken': params.csrf_token,
                'client': params.client,
                'fund':$('#fund-select').val(),
            },
            success: function (data) {
                dv_deal_list = data.deal_name;
                dv_fund_list = data.fund_name;
                dv_fund_association_list =  JSON.parse(JSON.stringify(data.fund_association));
                populateClientEntityGrid(data.dealvaluation);
            }
        });
    }

    function download_booking_document() {
        $.ajax({
            type: "GET",
            url: params.ajax_download_deal_booking_template,
            data:  {
                'csrfmiddlewaretoken': params.csrf_token,
                'client': params.client
            },
            success: function(response) {

                if (response.success) {
                    var url = params.download_file_data_url + response.file_name;
                    if (urlExists(url)) {
                        window.location.assign(url);
                    } else {
                        console.log('Error trying to download ' + response.file_name);
                    }
                }

            }
        });

    }

    function urlExists(url) {
        var http = $.ajax({
            type:"HEAD",
            url: url,
            async: false
        });
        var status = http.status;
        var exists = false;
        if (status == 200) {
            exists = true
        }
        return exists
    }

    function upload_booking_document(formData){
        $.ajax({
            type: "POST",
            url: params.ajax_upload_deal_booking_template,
            data: formData,
            contentType: false,
            processData: false,
            success: function(response) {
                if (response.success) {
                    location.reload();
                } else {
                    alert("Error encounter when saving data. Please verify the file format.")
                }
            }
        });
    }

    $entityForm.on('submit', function(e) {
        submitClientEntityForm(e);
    });

    $entityCancel.on('click', function(e) {
        cancelClientEntityForm(e);
    });

    $entityNew.on('click', function(e) {
        addNewClientEntityForm(e);
    });

    $('#dv_fund_name').change(function () {
        let count = 0;
        const $dv_deal_name = $entityForm.find('.deal_name').eq(0);
        const $dv_fund_name = $entityForm.find('.fund_name').eq(0);
        document.getElementById("dv_deal_name").options.length = 0;
        $.each(fund_association_list[0][document.getElementById("dv_fund_name").value], function () {
           var options = ""
           let id = Object.keys(this)[0];
           let val = Object.values(this)[0];
           if (count ==0 ){
               options = "<option " + "value='" +val+ "' selected>" + val + "</option>";
            }else{
               options = "<option " + "value='" + val + "'>" + val + "</option>";
            }
            count ++;
            $("#dv_deal_name").append(options);
        });
        $dv_deal_name.val(document.getElementById("dv_deal_name").value);
        $dv_fund_name.val(document.getElementById("dv_fund_name").value);
    });

        // jQuery UI Init
    $('#valuation_date').datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "yy-mm-dd"
    });

    $('#valuation_date').change(function () {
        const $valuation_date = $entityForm.find('.valuation_date').eq(0);
        var targetDate = new Date();
        targetDate = moment($valuationDate.val(), 'YYYY-MM-DD').format('YYYY-MM-DD');
        $valuationDate.datepicker('setDate', targetDate);
        $valuationDate.datepicker("option", "dateFormat", "yy-mm-dd");

    });

    $('#cashflow_download').on('click', function() {
        download_booking_document();
    })

    $('#cashflow_upload').on('click', function() {
        var formData = new FormData();
        var file =  $('#file_uploader')[0].files[0];
        formData.append('file', file);
        formData.append('csrfmiddlewaretoken', params.csrf_token);
        formData.append('client', params.client);

        upload_booking_document(formData);
    });

    $(document).ready(function () {
        fetchDeal();
        fetchEntity();

    });
    $('#fund-select').change( function() {
        fetchDeal();
        fetchEntity();
    });
    /**
    Funds Go Here
    */

    // Deal CRUD Variables
//    const $fundGrid = $('#table-fund');
//    const $fundCRUD = $('.CRUD-fund');
//    const $fundViewing = $fundCRUD.find('.viewing').eq(0);
//    const $fundAddEdit = $fundCRUD.find('.add-edit').eq(0);
//    const $fundForm = $fundCRUD.find('form').eq(0);
//    const $fundNew = $fundCRUD.find('.new').eq(0);
//    const $fundCancel = $fundForm.find('.cancel').eq(0);
//    const $fundSubmit = $fundForm.find('.submit').eq(0);
//    let isFundLoading = false;
//
//    function populateClientFundForm(grid) {
//        $fundViewing.addClass('hidden');
//        $fundAddEdit.removeClass('hidden');
//
//        const selectedRow = grid.api.getSelectedRows()[0];
//
//        const $id = $fundForm.find('.id').eq(0);
//        const $fundName = $fundForm.find('.fund_name').eq(0);
//        const $ccy = $fundForm.find('.ccy').eq(0);
//        const $client = $fundForm.find('.client_id').eq(0);
//
//        $id.val(selectedRow.id);
//        $fundName.val(selectedRow.fund_name);
//        $ccy.val(selectedRow.ccy);
//        $client.val(selectedRow.client);
//    }
//
//    function toggleClientFundFormButtons(isLoading) {
//        isFundLoading = isLoading;
//        $fundCancel.prop('disabled', isLoading);
//        $fundSubmit.prop('disabled', isLoading);
//    }
//
//    function submitClientFundForm(e) {
//        if (isFundLoading) { e.preventDefault(); return false; }
//
//        const payload = {}
//        const serializeArray = $fundForm.serializeArray();
//
//        toggleClientFundFormButtons(true);
//
//        serializeArray.forEach(function(obj) {
//            payload[obj.name] = obj.value;
//        })
//
//        $.ajax({
//            type: 'POST',
//            url: params.ajax_client_fund,
//            data: Object.assign({
//                'csrfmiddlewaretoken': params.csrf_token,
//                'client': params.client
//            }, payload),
//            success: function (data) {
//                populateClientFundGrid(data.funds, data.ccys, data.clients);
//                cancelClientFundForm();
//            },
//            error: function() {
//                alert('There was an error adding your fund. Contact the tech team if this persists.');
//            },
//            complete: function() {
//                toggleClientFundFormButtons(false);
//            }
//        });
//
//        e.preventDefault();
//        return false;
//    }
//
//    function clearClientFundForm() {
//        const $id = $fundForm.find('.id').eq(0);
//        const $fundName = $fundForm.find('.fund_name').eq(0);
//        const $ccy = $fundForm.find('.ccy').eq(0);
//        const $client = $fundForm.find('.client_id').eq(0);
//
//        $id.val(null);
//        $fundName.val(null);
//        $ccy.val(null);
//        $client.val(null);
//    }
//
//    function addNewClientFundForm() {
//        clearClientDealForm();
//
//        $fundViewing.addClass('hidden');
//        $fundAddEdit.removeClass('hidden');
//    }
//
//    function cancelClientFundForm() {
//        clearClientFundForm();
//
//        $fundViewing.removeClass('hidden');
//        $fundAddEdit.addClass('hidden');
//    }
//
//    const ccys = [];
//    const clients = [];
//    const clientFundGridOptions = {
//        columnDefs: [
//            { headerName: 'ID', field: 'id' },
//            { headerName: 'Fund Name', field: 'fund_name' },
//            {
//                headerName: 'CCY',
//                field: 'ccy',
//                valueFormatter: function(params) {
//                    const found = ccys.find(function(val) {
//                        return params.value === val.id
//                    })
//
//                    console.log(found);
//                    return (found) ? found.identifier : '';
//                }
//            }
//        ],
//        domLayout: 'autoHeight',
//        enableFilter: true,
//        enableSorting: true,
//        headerHeight: 45,
//        pagination: true,
//        paginationPageSize: 15,
//        defaultColDef: {
//            suppressMovable: true,
//            suppressMenu: true
//        },
//        rowSelection: 'single',
//        onSelectionChanged: populateClientFundForm,
//        onGridReady: function (params) {
//            params.api.sizeColumnsToFit();
//        }
//    }
//
//    // Initializing the grid
//    new agGrid.Grid($fundGrid[0], clientFundGridOptions);
//
//    function populateClientFundGrid(funds, _ccys, _clients) {
//        while (ccys.length) {
//            ccys.pop();
//        }
//
//        if (_ccys) {
//            for (let i = 0; i < _ccys.length; i++) {
//                ccys.push(_ccys[i]);
//            }
//        }
//
//        while (clients.length) {
//            clients.pop();
//        }
//
//        if (_clients) {
//            for (let i = 0; i < _clients.length; i++) {
//                clients.push(_clients[i]);
//            }
//        }
//
//        const $ccy = $fundForm.find('.ccy').eq(0);
//        const $client = $fundForm.find('.client_id').eq(0);
//
//        $ccy.empty();
//        ccys.forEach(function(ccy) {
//            $ccy.append('<option value="'+ccy.id+'">'+ccy.identifier+' - ' + ccy.name + '</option>');
//        });
//
//        $client.empty();
//        clients.forEach(function(client) {
//            $client.append('<option value="'+client.id+'">'+client.name+'</option>');
//        });
//
//        clientFundGridOptions.api.setRowData(funds)
//    }

    // Event Bindings

    // Fetch data on navigation
//    $('a[href="#data_editor_tab"]').on('click', function() {
//        populateClientFundGrid();
//
//        $.ajax({
//            type: 'GET',
//            url: params.ajax_client_fund,
//            data: {
//                'csrfmiddlewaretoken': params.csrf_token,
//                'client': params.client
//            },
//            success: function (data) {
//                populateClientFundGrid(data.funds, data.ccys, data.clients);
//            }
//        })
//    })
//
//    $fundForm.on('submit', function(e) {
//        submitClientFundForm(e);
//    });
//
//    $fundCancel.on('click', function(e) {
//        cancelClientFundForm(e);
//    });
//
//    $fundNew.on('click', function(e) {
//        addNewClientFundForm(e);
//    });

});
