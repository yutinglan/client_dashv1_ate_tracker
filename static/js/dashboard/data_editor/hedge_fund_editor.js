$(function() {
    var params = dealsHedgesEditorJsParams;
    var selectedTradeId = null;

    var hedgesDiv = document.querySelector('#selectHedgesFundTable')
    var fundHedgesGridOptions = null;

    var dealHedgeDiv = document.querySelector('#inputFundHedgeTable');
    var dealHedgeGridOptions = null;

    function MySimpleCellEditor() { }

    MySimpleCellEditor.prototype.init = function (params) {
        this.gui = document.createElement('input');
        this.gui.type = 'text';
        this.gui.classList.add('my-simple-editor');

        this.params = params;

        var startValue;

        let keyPressBackspaceOrDelete =
            params.keyPress === KEY_BACKSPACE
            || params.keyPress === KEY_DELETE;

        if (keyPressBackspaceOrDelete) {
            startValue = '';
        } else if (params.charPress) {
            startValue = params.charPress;
        } else {
            startValue = params.value;
            if (params.keyPress !== KEY_F2) {
                this.highlightAllOnFocus = true;
            }
        }

        if (startValue !== null && startValue !== undefined) {
            this.gui.value = startValue;
        }
    };

    MySimpleCellEditor.prototype.getGui = function () {
        return this.gui;
    };

    MySimpleCellEditor.prototype.getValue = function () {
        return this.gui.value;
    };

    MySimpleCellEditor.prototype.afterGuiAttached = function () {
        this.gui.focus();
    };

    MySimpleCellEditor.prototype.myCustomFunction = function () {
        return {
            rowIndex: this.params.rowIndex,
            colId: this.params.column.getId()
        };
    };
    function commaFormat(params) {
        if (typeof params.value != 'undefined') {
            return (Math.round(params.value)).toLocaleString() + '.00';
        }
    }

    function toFormatFour(params) {
        if (typeof params.value != 'undefined' & params.value != null) {
            return parseFloat(params.value).toFixed(4);
        }
    }

    function percentageFormatter(params) {
        var vaule_percentage = params.value;
        return vaule_percentage + '%';
    }

    // Main Functions:
    function generateDealHedgeTable(data) {
        if (dealHedgeGridOptions != null) {
            dealHedgeGridOptions.api.setRowData(data.fund_hedge);
        } else {
            dealHedgeGridOptions = {
                columnDefs: [
                    //{headerName: 'Deal ID', field: 'deal_id', width: 70  },
                    {
                        headerName: 'Trade ID',
                        field: 'trade__trade_id',
                        editable: false
                    },
                    {
                        headerName: 'Fund Name',
                        field: 'fund__fund_name',
                        cellEditor: 'agRichSelectCellEditor',
                        cellEditorParams: {
                            values: data.fund
                        }
                    },
                    {
                        headerName: 'Percentage',
                        field: 'percentage',
                        valueFormatter: percentageFormatter,
                        cellClass: 'number-cell',
                        suppressFilter: true
                    },
                ],
                rowData: data.fund_hedge,
                domLayout: 'autoHeight',
                enableFilter: true,
                enableSorting: true,
                headerHeight: 45,
                pagination: true,
                paginationPageSize: 15,
                enableColResize: true,
                defaultColDef: {
                    suppressMovable: true,
                    suppressMenu: true,
                    editable: true,
                },
                onGridReady: function (params) {
                    params.columnApi.autoSizeAllColumns();

                    window.addEventListener('resize', function () {
                        setTimeout(function () {
                            params.columnApi.autoSizeAllColumns();
                        })
                    })
                }
            };
            dealsGrid = new agGrid.Grid(dealHedgeDiv, dealHedgeGridOptions);
        }
    }

    function fetchDealHedgeData(trade_id = null) {
        if (trade_id == null) {
            var selectedRows = fundHedgesGridOptions.api.getFirstDisplayedRow();
            if (selectedRows == 0) {
                selectedRows = fundHedgesGridOptions.api.getDisplayedRowAtIndex(0);
                trade_id = selectedRows.data.trade_id;
                selectedTradeId = trade_id;
                selectedRowsString += selectedTradeId;
                document.querySelector('#selectedHedgesFund').innerHTML = 'Selected Row: ' + selectedTradeId;
            }
        }
        if (trade_id != null) {
            $.ajax({
                type: 'POST',
                url: params.fetch_deal_hedge_data_url,
                data: {
                    'csrfmiddlewaretoken': params.csrf_token,
                    'client': params.client,
                    'label': params.label,
                    'fund': $('#fund-select').val(),
                    'trade_id': trade_id,
                },
                success: function (data) {
                    generateDealHedgeTable(data);
//                    generateTradeAllocationTables(data);
                }
            });
        }
    }

    function generateHedgeTable(data) {
        if (fundHedgesGridOptions != null) {
            fundHedgesGridOptions.api.setRowData(data.data);
        } else {
            fundHedgesGridOptions = {
                columnDefs: [
                    { headerName: 'Trade ID', field: 'trade_id', width: 70 },
                    { headerName: 'Fund', field: 'fund', width: 100 },
                    { headerName: 'CP Ref', field: 'cp_ref', width: 80 },
                    { headerName: 'Trade date', field: 'trade_date', width: 80 },
                    { headerName: 'Style', field: 'style', width: 80 },
                    { headerName: 'Counterparty', field: 'counterparty', width: 120, cellStyle: { 'text-align': "left" } },
                    { headerName: 'Underlying', field: 'underlying', width: 80 },
                    { headerName: 'Direction', field: 'direction', width: 70 },
                    { headerName: 'Notional Currency', field: 'notional_currency', width: 60 },
                    { headerName: 'Notional Amounts', field: 'notional_amounts', width: 100, valueFormatter: window.formatters.VrmHedge.notional, cellClass: 'text-right' },
                    { headerName: 'Strike', field: 'strike', width: 100, valueFormatter: window.formatters.VrmHedge.strike, cellClass: 'text-right' },
                ],
                rowData: data.data,
                domLayout: 'autoHeight',
                enableFilter: true,
                enableSorting: true,
                headerHeight: 45,
                pagination: true,
                paginationPageSize: 15,
                rowSelection: 'single',
                onSelectionChanged: onHedgeSelectionChanged,
                components: {
                    mySimpleCellEditor: MySimpleCellEditor
                },
                defaultColDef: {
                    suppressMovable: true,
                    suppressMenu: true,
                    editable: false,
                },
                onGridReady: function (params) {
                    params.columnApi.autoSizeAllColumns();

                    window.addEventListener('resize', function () {
                        setTimeout(function () {
                            params.columnApi.autoSizeAllColumns();
                        })
                    })
                }
            };
            hedgesDiv = new agGrid.Grid(hedgesDiv, fundHedgesGridOptions);
        }
    }

    function generateDownloadable() {

        var current_hedges = {};
        if (fundHedgesGridOptions != null) {
            fundHedgesGridOptions.api.forEachNode(function (rowNode, index) {
                current_hedges[index] = rowNode.data;
            });
        }

        var current_deal_hedges = {};
        if (dealHedgeGridOptions != null) {
            dealHedgeGridOptions.api.forEachNode(function (rowNode, index) {
                current_deal_hedges[index] = rowNode.data;
            });
        }
        //
        //         var current_deal_hedges = {};
        ////        dhGridOptions.api.forEachNode( function(rowNode, index) {
        ////            current_deal_hedges[index] = rowNode.data;
        ////        });
        ////
        ////         var current_deal_hedges = {};
        ////        dhGridOptions.api.forEachNode( function(rowNode, index) {
        ////            current_deal_hedges[index] = rowNode.data;
        ////        });
        ////
        ////         var current_deal_hedges = {};
        ////        entityFundGridOptions.api.forEachNode( function(rowNode, index) {
        ////            current_deal_hedges[index] = rowNode.data;
        ////        });
        //
        //
        $.ajax({
            type: 'POST',
            url: params.prepare_trade_info_data_url,
            data: {
                'csrfmiddlewaretoken': params.csrf_token,
                'client': params.client,
                'hedge_data': JSON.stringify(current_hedges),
                'deal_hedge_data': JSON.stringify(current_deal_hedges)
            },
            success: function (data) {
                if (typeof data != 'undefined') {
                    var url = params.download_file_data_url + data.file_name;
                    window.location.assign(url);
                }
            }
        });
    }


    function fetchHedgeData() {
        $.ajax({
            type: 'POST',
            url: params.api_database_hedge_data_table_url,
            data: {
                'csrfmiddlewaretoken': params.csrf_token,
                'client': params.client,
                'label': params.label,
                // 'fund': $('#fund-select').val(),
                'fund': null,
            },
            success: function (data) {
                generateHedgeTable(data);
                if (selectedTradeId == null) {
                    if (data.data.length > 0) {
                        selectedTradeId = data.data[0]['trade_id'];
                        document.querySelector('#selectedHedgesFund').innerHTML = 'Selected Row: ' + selectedTradeId;
                    }
                }
                fetchDealHedgeData(trade_id = selectedTradeId);
            }
        });
    }

    function onHedgeSelectionChanged() {
        var selectedRows = fundHedgesGridOptions.api.getSelectedRows();
        if (selectedRows.length == 1) {
            selectedRows.forEach(function (row, index) {
                selectedTradeId = row.trade_id;
            });
            document.querySelector('#selectedHedgesFund').innerHTML = 'Selected Row: ' + selectedTradeId;
            fetchDealHedgeData(trade_id = selectedTradeId);
        } else {

            console.log('Unexpected error: more than one row were selected')
        }
    }

    $('#resetHedgeFund').click(function () {
        fetchHedgeData();

    });



    $('#fund-select').change(function () {
        fetchHedgeData();

    });

    $(document).ready(function () {
        $('#fund-select').selectpicker('val', params.selected_fund);
        fetchHedgeData();
    });

    $('#fund-select').prop('disabled', true);

//    $('#downloadAllHedge').click(function (e) {
//        generateDownloadable();
//
//        e.preventDefault();
//        return false;
//    });

    function printResult(res) {
        console.log('---------------------------------------')
        if (res.add) {

        }
        if (res.remove) {
            res.remove.forEach(function (rowNode) {
                console.log('Removed Row Node', rowNode);
            });
        }
        if (res.update) {
            res.update.forEach(function (rowNode) {
                console.log('Updated Row Node', rowNode);
            });
        }
    }
    function closeSnoAlertBox(msg = "Error Undefined") {
        $('#snoAlertBox').text(msg);
        $('#snoAlertBox').addClass("alert alert-danger");
        window.setTimeout(function () {
            $("#snoAlertBox").fadeOut(300)
        }, 3000);
    }

    function closeSuccesslertBox(msg = "Error Undefined") {
        $('#successBox').text(msg);
        $('#successBox').addClass("alert alert-success");
        window.setTimeout(function () {
            $("#successBox").fadeOut(300)
        }, 3000);
    }

    $("#addHedgeFund").button().click(function () {
        var selectedRows = fundHedgesGridOptions.api.getSelectedRows();
        if (selectedRows.length > 0) {
            var res = dealHedgeGridOptions.api.updateRowData({ add: [{ trade__trade_id: selectedRows[0].trade_id }], addIndex: 0 });
            printResult(res);
        }
        else {
            $("#snoAlertBox").fadeIn();
            closeSnoAlertBox("Error no tradeid selected");
        }
    });



    $("#saveHedgeFund").button().click(function () {
        var current_fundvalues = {};
        dealHedgeGridOptions.api.forEachNode(function (rowNode, index) {
            current_fundvalues[index] = rowNode.data;
        });
        $.ajax({
            type: 'POST',
            url: params.save_fund_hedge_data_url,
            data: {
                'csrfmiddlewaretoken': params.csrf_token,
                'client': params.client,
                'label': params.label,
                'current_fundvalues': JSON.stringify(current_fundvalues),
            },
            success: function (data) {
                if (data.errorcode == 1) {
                    $("#successBox").fadeIn();
                    closeSuccesslertBox(data.msg + 'Fund');
                    fetchDealHedgeData(selectedTradeId);
                }
                else {
                    $("#snoAlertBox").fadeIn();
                    closeSnoAlertBox(data.msg + 'Fund');
                }
            }
        });
    });


    function printResult(res) {
        console.log('---------------------------------------')
        if (res.add) {

            //            res.add.forEach( function(rowNode) {
            //                console.log('Added Row Node', rowNode);
            //            });
            //            $("#snoAlertBox").fadeIn();
            //            closeSnoAlertBox("New Deal id added",0);
        }
        if (res.remove) {
            res.remove.forEach(function (rowNode) {
                console.log('Removed Row Node', rowNode);
            });
        }
        if (res.update) {
            res.update.forEach(function (rowNode) {
                console.log('Updated Row Node', rowNode);
            });
        }
    }


    var params = dealsHedgesEditorJsParams;

//    var dealEntityGridDiv = document.querySelector('#data-deal-entity-all');
//    var dealEntityGridOptions = null;
//
//    var entityFundGridDiv = document.querySelector('#data-entity-fund-all');
//    var entityFundGridOptions = null;
//
//    var entityHedgeGridDiv = document.querySelector('#data-entity-hedge-all');
//    var entityHedgeGridOptions = null;

    // Main Functions:

    function createGridOptions(data, colDefs) {
        gridOptions = {
            columnDefs: colDefs,
            rowData: data,
            enableFilter: true,
            headerHeight: 45,
            enableSorting: true,
            pagination: true,
            paginationPageSize: 15,
            components: {
                mySimpleCellEditor: MySimpleCellEditor
            },
            domLayout: 'autoHeight',
            defaultColDef: {
                editable: true,
            },
            onGridReady: function (params) {
                params.api.sizeColumnsToFit();

                window.addEventListener('resize', function () {
                    setTimeout(function () {
                        params.api.sizeColumnsToFit();
                    })
                })

                // Redundent binding but doesn't require a refactor
                $('.nav-tabs > li > a').on('click', function() {
                    params.api.sizeColumnsToFit();
                })
            }
        };

        return gridOptions
    }


//    function generateTradeAllocationTables(data) {
//
//        if (entityHedgeGridOptions != null) {
//            entityHedgeGridOptions.api.setRowData(data.entity_trade_data);
//        } else {
//            var entityHedgeColumnDefs = [
//                { headerName: 'Trade ID', field: 'trade_id', width: 110, cellEditor: 'agRichSelectCellEditor', cellEditorParams: { values: data.trade_id_list }, },
//                { headerName: 'Entity Name', field: 'entity__entity_name', width: 160, cellEditor: 'agRichSelectCellEditor', cellEditorParams: { values: data.entity }, },
//                { headerName: 'Percentage', field: 'percentage', width: 110, valueFormatter: percentageFormatter, },
//            ];
//            entityHedgeGridOptions = createGridOptions(data.entity_trade_data, entityHedgeColumnDefs);
//            new agGrid.Grid(entityHedgeGridDiv, entityHedgeGridOptions);
//        }
//
//        if (entityFundGridOptions != null) {
//            entityFundGridOptions.api.setRowData(data.entity_fund_data);
//        } else {
//            var entityFundColumnDefs = [
//                { headerName: 'Entity Name', field: 'entity__entity_name', width: 160, cellEditor: 'agRichSelectCellEditor', cellEditorParams: { values: data.entity }, },
//                { headerName: 'Fund Name', field: 'fund_name', width: 200, cellEditor: 'agRichSelectCellEditor', cellEditorParams: { values: data.fund }, },
//                { headerName: 'Percentage', field: 'percentage', width: 110, valueFormatter: percentageFormatter, },
//            ];
//            entityFundGridOptions = createGridOptions(data.entity_fund_data, entityFundColumnDefs);
//            new agGrid.Grid(entityFundGridDiv, entityFundGridOptions);
//        }
//
//        if (dealEntityGridOptions != null) {
//            dealEntityGridOptions.api.setRowData(data.deal_entity_data);
//        } else {
//            var dealEntityColumnDefs = [
//                { headerName: 'Deal Name', field: 'deal__deal_name', width: 110, cellEditor: 'agRichSelectCellEditor', cellEditorParams: { values: data.deal }, },
//                { headerName: 'Entity Name', field: 'entity__entity_name', width: 160, cellEditor: 'agRichSelectCellEditor', cellEditorParams: { values: data.entity }, },
//                { headerName: 'Percentage', field: 'percentage', width: 110, valueFormatter: percentageFormatter, },
//            ];
//            dealEntityGridOptions = createGridOptions(data.deal_entity_data, dealEntityColumnDefs);
//            new agGrid.Grid(dealEntityGridDiv, dealEntityGridOptions);
//        }
//    }


//    function resetDealEntity() {
//        $.ajax({
//            type: 'POST',
//            url: params.fetch_deal_hedge_data_url,
//            data: {
//                'csrfmiddlewaretoken': params.csrf_token,
//                'client': params.client,
//                'label': params.label,
//                'fund': $('#fund-select').val(),
//                'trade_id': trade_id,
//            },
//            success: function (data) {
//                if (dealEntityGridOptions != null) {
//                    dealEntityGridOptions.api.setRowData(data.deal_entity_data);
//                } else {
//                    dealEntityGridOptions = createGridOptions(data.deal_entity_data, dealEntityColumnDefs);
//                    new agGrid.Grid(dealEntityGridDiv, dealEntityGridOptions);
//                }
//            }
//        });
//    }
//
//    function resetEntityFund(data) {
//
//        $.ajax({
//            type: 'POST',
//            url: params.fetch_deal_hedge_data_url,
//            data: {
//                'csrfmiddlewaretoken': params.csrf_token,
//                'client': params.client,
//                'label': params.label,
//                'fund': $('#fund-select').val(),
//                'trade_id': trade_id,
//            },
//            success: function (data) {
//
//                if (entityFundGridOptions != null) {
//                    entityFundGridOptions.api.setRowData(data.entity_fund_data);
//                } else {
//                    entityFundGridOptions = createGridOptions(data.entity_fund_data, entityFundColumnDefs);
//                    new agGrid.Grid(entityFundGridDiv, entityFundGridOptions);
//                }
//            }
//        });
//
//    }
//
//    function resetEntityHedge() {
//        $.ajax({
//            type: 'POST',
//            url: params.fetch_deal_hedge_data_url,
//            data: {
//                'csrfmiddlewaretoken': params.csrf_token,
//                'client': params.client,
//                'label': params.label,
//                'fund': $('#fund-select').val(),
//                'trade_id': trade_id,
//            },
//            success: function (data) {
//
//                if (entityHedgeGridOptions != null) {
//                    entityHedgeGridOptions.api.setRowData(data.entity_trade_data);
//                } else {
//                    entityHedgeGridOptions = createGridOptions(data.deal_entity_data, entityHedgeColumnDefs);
//                    new agGrid.Grid(entityHedgeGridDiv, entityHedgeGridOptions);
//                }
//            }
//        });
//    }
});