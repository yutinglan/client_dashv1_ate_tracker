(function($){

    var params = dealsDocumentEditorJsParams;

//    var cashflowsDiv = document.querySelector('#cashflowgrid');
//    var cashflowsOptions = null;

    var backwardCashflowsDiv =document.querySelector('#backwardcashflowgrid');
    var backwardCashflowsOptions = null;

    var forwardCashflowsDiv = document.querySelector('#forwardcashflowgrid');
    var forwardCashflowsOptions = null;

    var documentCashflowsDiv = document.querySelector('#documentcashflowgrid');
    var documentCashflowsGridOptions = null;

    var genColumnDefs = [
        {headerName: 'Transaction ID', field: 'transaction_id',width:110},
        {headerName: 'Deal ID', field: 'deal_id',width:110},
        {headerName: 'Cashflows', field: 'cashflows',width:110, valueFormatter:commaFormat},
        {headerName: 'Value Date', field: 'value_date',width:110},
        {headerName: 'CF Type', field: 'cf_type',  width:120 , cellStyle:  { 'text-align': "center" }},
        {headerName: 'Cancellation', field: 'cancellation', width:120 , cellStyle:  { 'text-align': "right" }},
        {headerName: 'FX Rate', field: 'fx_rate',width:110, cellStyle:  { 'text-align': "center" }},
    ];
    var documentCashflowsColumnDefs = [
         {headerName: 'Vehicle', field: 'vehicle',width:110},
         {headerName: 'Deal', field: 'deal',width:110},
         {headerName: 'Type', field: 'type',width:110},
         {headerName: 'Amount', field: 'amount',width:110, valueFormatter:commaFormat,},
         {headerName: 'Currency', field: 'currency', width:120 , cellStyle: { 'text-align': "center" }},
         {headerName: 'Date', field: 'date', width:120 , cellStyle: { 'text-align': "right" }},
         {headerName: 'Current Date', field: 'current_date',width:110, cellStyle: { 'text-align': "center" }},
     ];

    // Format Functions:

    function MySimpleCellEditor() {}

    MySimpleCellEditor.prototype.init = function(params) {
        this.gui = document.createElement('input');
        this.gui.type = 'text';
        this.gui.classList.add('my-simple-editor');

        this.params = params;

        var startValue;

        let keyPressBackspaceOrDelete =
            params.keyPress === KEY_BACKSPACE
            || params.keyPress === KEY_DELETE;

        if (keyPressBackspaceOrDelete) {
            startValue = '';
        } else if (params.charPress) {
            startValue = params.charPress;
        } else {
            startValue = params.value;
            if (params.keyPress !== KEY_F2) {
                this.highlightAllOnFocus = true;
            }
        }

        if (startValue!==null && startValue!==undefined) {
            this.gui.value = startValue;
        }
    };

    MySimpleCellEditor.prototype.getGui = function() {
        return this.gui;
    };

    MySimpleCellEditor.prototype.getValue = function() {
        return this.gui.value;
    };

    MySimpleCellEditor.prototype.afterGuiAttached = function() {
        this.gui.focus();
    };

    MySimpleCellEditor.prototype.myCustomFunction = function() {
        return {
            rowIndex: this.params.rowIndex,
            colId: this.params.column.getId()
        };
    };
    function commaFormat(params) {
        return (Math.round(params.value)).toLocaleString()+'.00';
    }

    // Main Functions:

    function createGridOptions (data, colDefs) {
        gridOptions = {
                columnDefs: colDefs,
                rowData: data,
                gridAutoHeight : true,
                enableFilter: true,
                headerHeight: 45,
                enableSorting: true,
                pagination: true,
                paginationPageSize: 15,
                components:{
                    mySimpleCellEditor: MySimpleCellEditor
                },
                domLayout: 'autoHeight',
                defaultColDef: {
                // set every column width
                    width: 110,
                    suppressMovable: true,
                    suppressFilter: true,
                    suppressMenu : true,
                    editable: false,
                    onGridReady: function (params) {
                        // params.columnApi.autoSizeAllColumns();
                        //params.api.sizeColumnsToFit();
                        window.addEventListener('resize', function () {
                            setTimeout(function () {
                                params.columnApi.autoSizeAllColumns();
                                //params.api.sizeColumnsToFit();
                            })
                        })
                    }

                    // make every column use 'text' filter by default
                    <!--cellStyle: {'textAlign': 'center'},-->
                    //filter: 'agTextColumnFilter'
                },
            };
        return gridOptions
    }

    function generateCashflowTables (data) {
//
//       if (cashflowsOptions != null){
//             cashflowsOptions.api.setRowData(data.cashflow);
//       } else {
//            cashflowsOptions = createGridOptions(data.cashflow, genColumnDefs);
//            cashflowGrid =  new agGrid.Grid(cashflowsDiv, cashflowsOptions);
//       }

       if (backwardCashflowsOptions != null) {
            backwardCashflowsOptions.api.setRowData(data.backward_cashflow);
       } else {
            backwardCashflowsOptions = createGridOptions(data.backward_cashflow, genColumnDefs);
            backwardCashflowGrid =  new agGrid.Grid(backwardCashflowsDiv, backwardCashflowsOptions);
       }

       if (forwardCashflowsOptions != null) {
            forwardCashflowsOptions.api.setRowData(data.forward_cashflow);
       } else {
            forwardCashflowsOptions = createGridOptions(data.forward_cashflow, genColumnDefs);
            forwardCashflowGrid =  new agGrid.Grid(forwardCashflowsDiv, forwardCashflowsOptions);
       }

       if (documentCashflowsGridOptions != null) {
            documentCashflowsGridOptions.api.setRowData(data.document_cashflow);
       } else {
            documentCashflowsGridOptions = createGridOptions(data.document_cashflow, documentCashflowsColumnDefs);
            documentCashflowGrid =  new agGrid.Grid(documentCashflowsDiv, documentCashflowsGridOptions);
       }

    }

    function fetchCashflowsData () {
       $.ajax({
            type: 'POST',
            url: params.cashflow_specific_tables_url,
            data: {
                'csrfmiddlewaretoken': params.csrf_token,
                'client': params.client,
            },
            success: function(data) {
                generateCashflowTables(data);
            }
        });
    }

    $(document).ready(function () {
        fetchCashflowsData();
    });

})(jQuery);