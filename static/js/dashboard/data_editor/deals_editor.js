(function($){

    var params = dealsEditorJsParams;

    var dealsDiv = document.querySelector('#dealsGrid');
    var dealsGridOptions = null;
    var dealsColumnDefs = [
        {headerName: 'Deal ID', field: 'deal_id'  },
        {headerName: 'Deal Name', field: 'deal_name', width: 250 },
        {headerName: 'Local Currency', field: 'local_currency'  },
        {headerName: 'Fund', field: 'fund', width: 250},
        //{headerName: 'Cancellation', field: 'cancellation'  },
    ];

    // Format Functions:

    function MySimpleCellEditor() {}

    MySimpleCellEditor.prototype.init = function(params) {
        this.gui = document.createElement('input');
        this.gui.type = 'text';
        this.gui.classList.add('my-simple-editor');

        this.params = params;

        var startValue;

        let keyPressBackspaceOrDelete =
            params.keyPress === KEY_BACKSPACE
            || params.keyPress === KEY_DELETE;

        if (keyPressBackspaceOrDelete) {
            startValue = '';
        } else if (params.charPress) {
            startValue = params.charPress;
        } else {
            startValue = params.value;
            if (params.keyPress !== KEY_F2) {
                this.highlightAllOnFocus = true;
            }
        }

        if (startValue!==null && startValue!==undefined) {
            this.gui.value = startValue;
        }
    };

    MySimpleCellEditor.prototype.getGui = function() {
        return this.gui;
    };

    MySimpleCellEditor.prototype.getValue = function() {
        return this.gui.value;
    };

    MySimpleCellEditor.prototype.afterGuiAttached = function() {
        this.gui.focus();
    };

    MySimpleCellEditor.prototype.myCustomFunction = function() {
        return {
            rowIndex: this.params.rowIndex,
            colId: this.params.column.getId()
        };
    };
    function commaFormat(params){
        return (Math.round(parseFloat(params.value))).toLocaleString();
    }


    // Main Functions:

    function createGridOptions (data, colDefs) {
        gridOptions = {
                columnDefs: colDefs,
                rowData: data,
                gridAutoHeight : true,
                enableFilter: true,
                headerHeight: 45,
                enableSorting: true,
                pagination: true,
                paginationPageSize: 15,
                rowSelection: 'single',
                onSelectionChanged: onDealSelectionChanged,
                components:{
                    mySimpleCellEditor: MySimpleCellEditor
                },
                defaultColDef: {
                // set every column width
                    // width: 150,
                    suppressMovable: true,
                    suppressFilter: true,
                    suppressMenu : true,
                    editable: false,
                    onGridReady: function (params) {
                        // params.columnApi.autoSizeAllColumns();
                        params.api.sizeColumnsToFit();
                        window.addEventListener('resize', function () {
                            setTimeout(function () {
                                params.columnApi.autoSizeAllColumns();
                                //params.api.sizeColumnsToFit();
                            })
                        })
                    }

                    // make every column use 'text' filter by default
                    <!--cellStyle: {'textAlign': 'center'},-->
                    //filter: 'agTextColumnFilter'
                },
            };
        return gridOptions
    }

    function generateDealTable (data) {

       if (dealsGridOptions != null){
             dealsGridOptions.api.setRowData(data.deals);
       } else {
            dealsGridOptions = createGridOptions(data.deals, dealsColumnDefs);
            dealsGrid =  new agGrid.Grid(dealsDiv, dealsGridOptions);
       }
    }

    function fetchDealData (trade_id = null) {
       $.ajax({
            type: 'POST',
            url: params.fetch_deals_data_url,
            data: {
                'csrfmiddlewaretoken': params.csrf_token,
                'client': params.client,
                'label': params.label,
                'fund': $('#fund-select').val(),
                'trade_id': trade_id,
            },
            success: function(data) {
                generateDealTable(data);
            }
        });
    }

    function onDealSelectionChanged() {
        var selectedRows = dealsGridOptions.api.getSelectedRows();
        var selectedDealId = null;
        var selectedRowsString = 'Selected Row: ';
        if(selectedRows.length == 1){
            selectedRows.forEach( function(row, index) {
                selectedDealId = row.deal_id;
                selectedRowsString += selectedDealId;
            });
            document.querySelector('#selectedDeals').innerHTML = selectedRowsString;
        } else {
            console.log('Unexpected error: more than one row were selected')
        }
    }

    $('#resetDeals').click(function() {
        fetchDealData();
    });

    $('#fund-select').change( function () {
        fetchDealData();
    });

    $(document).ready(function () {
        fetchDealData();
    });

})(jQuery);