"use strict";

$(function () {
    let gridOptions = null;
    let mtmgridOptions = null;
    let summaryGridOptions = null;


    function mtmgrid(data) {
        if (mtmgridOptions != null) {
            mtmgridOptions.api.setRowData(data.data);
            mtmgridOptions.columnApi.autoSizeAllColumns();
        } else {
            mtmgridOptions = {
                columnDefs: [
//                    { headerName: 'Position ID', field: 'position_id' },
                    { headerName: 'Fund', field: 'fund' ,headerClass: 'text-center' },
                    { headerName: 'Counterparty', field: 'counterparty' , headerClass: 'text-center' },
                    { headerName: 'Future Code', field: 'futurecode' ,headerClass: 'text-center'  },
                    { headerName: 'Chain Code', field: 'chain_code', headerClass: 'text-center'  },
//                    { headerName: 'Last Trade Date', field: 'lasttradedate' },
                    { headerName: 'Trade Date', field: 'trade_date', sort: 'asc', headerClass: 'text-center'  },
//                    { headerName: 'Strike', field: 'strike' ,valueFormatter: window.formatters.futureSpotRef.strike, cellStyle: { 'text-align': "right" }},
                    { headerName: 'Description', field: 'description',headerClass: 'text-center'  },
                    { headerName: 'Contract Currency', field: 'contract_currency', headerClass: 'text-center' ,  cellClass: 'text-left'  },
                    { headerName: 'Value of 1 Point', field: 'value_of_1_point', filter: 'agNumberColumnFilter', valueFormatter:commaUnDecimalFormat, cellStyle: { 'text-align': "right" } ,headerClass: 'text-center'  },
                    { headerName: 'Contract Size', field: 'contract_size',  valueFormatter:commaUnDecimalFormat, cellStyle: { 'text-align': "right" } ,headerClass: 'text-center'  },
                    { headerName: 'Direction', field: 'direction',headerClass: 'text-center' , cellStyle: { 'text-align': "left" } },
                    { headerName: 'Notional <br/>(Contract Currency)', field: 'notional_contract_ccy', valueFormatter: window.formatters.futureSpotRef.notional,cellStyle: { 'text-align': "right" },headerClass: 'text-center'   },
                    { headerName: 'Position', field: 'contracts' ,headerClass: 'text-center', cellClass: 'text-right' },
                    { headerName: 'Closing Rate', field: 'closing_rate', valueFormatter: window.formatters.futureSpotRef.spotRef,  cellStyle: { 'text-align': "right" }, headerClass: 'text-center' },
                    { headerName: 'Valuation Date', field: 'spot_ref_date',  cellClass: 'text-center',headerClass: 'text-center'   },
                    { headerName: 'MTM <br/>(Contract Currency)', field: 'gross_mtm_contract_ccy' , valueFormatter: commaFormat, filter: 'agNumberColumnFilter', cellStyle: { 'text-align': "right" },headerClass: 'text-center'   },
                    { headerName: 'MTM <br/>(CAD)', field: 'gross_mtm', valueFormatter: commaFormat, filter: 'agNumberColumnFilter', cellStyle: { 'text-align': "right" },headerClass: 'text-center'   },

                ],
                rowData: data.data,
                domLayout: 'autoHeight',
                headerHeight: 48,
                enableFilter: true,
                enableSorting: true,
                pagination: true,
                paginationPageSize: 15,
                defaultColDef: {
                    suppressMovable: true,
                    suppressMenu: true,
                    onGridReady: function (params) {
                        params.columnApi.autoSizeAllColumns();
                        window.addEventListener('resize', function () {
                            setTimeout(function () {
                                params.columnApi.autoSizeAllColumns();
                            })
                        })
                    }
                },
                rowSelection: 'multiple',
                suppressColumnVirtualisation: true,
                onGridReady: function (gridOptions) {
                    gridOptions.columnApi.autoSizeAllColumns();
                    window.addEventListener('resize', function () {
                            setTimeout(function () {
                                gridOptions.columnApi.autoSizeAllColumns();
                            })
                        })
                },
            };
            var eMtmGridDiv = document.querySelector('#future-trade-mtm-grid');
            new agGrid.Grid(eMtmGridDiv, mtmgridOptions);
        }

    }

    function summaryGrid(data) {
        if (summaryGridOptions != null) {
            summaryGridOptions.api.setRowData(data.summary_data);
            summaryGridOptions.columnApi.autoSizeAllColumns();
        } else {
            summaryGridOptions = {
                columnDefs: [
//                    { headerName: 'Position ID', field: 'position_id' },
                    { headerName: 'Fund', field: 'fund' ,headerClass: 'text-center'  },
                    { headerName: 'Counterparty', field: 'counterparty' ,headerClass: 'text-center'  },
                    { headerName: 'Chain Code', field: 'chain_code' ,headerClass: 'text-center'  },
                    { headerName: 'Last Trade Date', field: 'last_trade_date' ,headerClass: 'text-center'  },
                    { headerName: 'Description', field: 'description' ,headerClass: 'text-center'  },
                    { headerName: 'Contract Currency', field: 'contract_currency' ,headerClass: 'text-center' , cellStyle: { 'text-align': "left" } },
                    { headerName: 'Value of 1 Point', field: 'value_point', valueFormatter:commaUnDecimalFormat, cellStyle: { 'text-align': "right" } ,headerClass: 'text-center'  },
                    { headerName: 'Contract Size', field: 'contract_size', valueFormatter:commaUnDecimalFormat, cellStyle: { 'text-align': "right" },headerClass: 'text-center'   },
                    { headerName: 'Direction', field: 'direction',headerClass: 'text-center' ,cellStyle: { 'text-align': "left" } },
                    { headerName: 'Notional <br/>(Contract Currency)', field: 'notional_amounts',  valueFormatter: window.formatters.futureSpotRef.notional,cellStyle: { 'text-align': "right" } ,headerClass: 'text-center'   },
                    { headerName: 'Position', field: 'contracts', cellStyle: { 'text-align': "right" } ,headerClass: 'text-center'  },
                    { headerName: 'Total Commissions <br/>(Contract Currency)', field: 'total_commissions',   valueFormatter: commaFormat, filter: 'agNumberColumnFilter', cellStyle: { 'text-align': "right" },headerClass: 'text-center'  },
                    { headerName: 'Closing Rate', field: 'spot_ref', valueFormatter: window.formatters.futureSpotRef.spotRef,  cellClass: 'text-right' ,headerClass: 'text-center'  },
                    { headerName: 'Valuation Date', field: 'valuation_date',  cellClass: 'text-center',headerClass: 'text-center'   },
                    { headerName: 'Realized Cashflow <br/> (Contract Currency)', field: 'realized_cashflow', valueFormatter: commaFormat, filter: 'agNumberColumnFilter',cellStyle: { 'text-align': "right" } ,headerClass: 'text-center'  },
                    { headerName: 'MTM <br/>(Contract Currency)', field: 'mtm_contract_ccy', valueFormatter: commaFormat, headerClass: 'text-center', cellStyle: { 'text-align': "right" }, },
                    { headerName: 'MTM  <br/>(CAD)', field: 'mtm', valueFormatter: commaFormat, filter: 'agNumberColumnFilter', cellStyle: { 'text-align': "right" }, headerClass: 'text-center'      },

                ],
                rowData: data.summary_data,
                domLayout: 'autoHeight',
                headerHeight: 48,
                enableFilter: true,
                enableSorting: true,
                pagination: true,
                paginationPageSize: 15,
                defaultColDef: {
                    suppressMovable: true,
                    suppressMenu: true,
                    onGridReady: function (params) {
                        params.columnApi.autoSizeAllColumns();
                        window.addEventListener('resize', function () {
                            setTimeout(function () {
                                params.columnApi.autoSizeAllColumns();
                            })
                        })
                    }
                },
                rowSelection: 'multiple',
                suppressColumnVirtualisation: true,
                onGridReady: function (gridOptions) {
                    gridOptions.columnApi.autoSizeAllColumns();
                    window.addEventListener('resize', function () {
                            setTimeout(function () {
                                gridOptions.columnApi.autoSizeAllColumns();
                            })
                        })
                },
            };
            var eMtmGridDiv = document.querySelector('#summary-trade-mtm-grid');
            new agGrid.Grid(eMtmGridDiv, summaryGridOptions);
        }

    }

    function grid(rowData, columnDefs) {
        if (gridOptions != null) {
            gridOptions.api.setRowData(rowData);
        } else {
            gridOptions = {
                columnDefs: columnDefs,
                rowData: rowData,
                domLayout: 'autoHeight',
                headerHeight: 35,
                enableColResize: false,
                defaultColDef: {
                    suppressMovable: true,
                    suppressFilter: true,
                    suppressMenu: true,
                    onGridReady: function (params) {
                        params.columnApi.autoSizeAllColumns();
                        window.addEventListener('resize', function () {
                            setTimeout(function () {
                                params.columnApi.autoSizeAllColumns();
                            })
                        })
                    }
                },
                onGridReady: function (gridOptions) {
                    gridOptions.columnApi.autoSizeAllColumns();
                }
            };

            var eGridDiv = document.querySelector('#myGrid');
            new agGrid.Grid(eGridDiv, gridOptions);
        }
    }

    // converting cell value to two decimal places
    function commaFormat(params) {
       if (params.value === undefined|| params.value == null) {
                return '';
        }
        return (accounting.formatMoney(params.value,"", 2, ",", "."));

    }
    function commaUnDecimalFormat(params) {
       if (params.value === undefined|| params.value == null) {
                return '';
        }
        return (accounting.formatMoney(params.value,"", 0, ",", "."));

    }


    function setup_cpsummation(data) {
//        Highcharts.chart('container', {
//            chart: {
//                type: 'heatmap',
//                marginTop: 40,
//                marginBottom: 80,
//                plotBorderWidth: 1
//            },
//            title: {
//                text: 'Liquidity at Risk (95% CL), in ' + data.currency,
//                style: [{
//                    fontSize: "12px"
//                }]
//            },
//            xAxis: {
//                categories: [$('#fund-select').val()],
//
//            },
//            yAxis: {
//                categories: data['counterparty'],
//                title: null,
//
//            },
//            colorAxis: {
//                min: data['color'],
//                max: 0,
//                minColor: '#dd4b39',
//                maxColor: '#ffffff'
//            },
//            legend: {
//                enabled: false
//            },
//            tooltip: {
//                formatter: function () {
//                    return '<b>' + this.series.xAxis.categories[this.point.x] + '</b>  95% CL <br><b>' +
//                        (Math.round(this.point.value)).toLocaleString() + '</b> ' + data.currency + '<br><b>' + this.series.yAxis.categories[this.point.y] + '</b>';
//                }
//            },
//            series: [{
//                name: 'with 95% Confidence Level',
//                borderWidth: 1,
//                // The heatmap isn't 2D so index is horizontal index is always 0
//                data: data['cp'].map(function(val, index) { return [0, index, val]; }),
//                dataLabels: {
//                    enabled: true,
//                    color: '#000000',
//                    style: [{
//                        fontSize: "14px",
//                        textOutline: false
//                    }],
//                    formatter: function () {
//                        return this.point.value.toLocaleString();
//                    },
//                }
//            }],
//            credits: {
//                enabled: false
//            },
//        });
    }

    function setup_historicalmtm(data) {
        var arrayDate = [];
        for (let i = 0; i < data.hmtm.length; i++) {
            if (data.hmtm[i][0]['date'] !== undefined) {
                arrayDate.push([Date.parse(new Date(data.hmtm[i][0]['date'])), data.hmtm[i][1]['sum']]);
            }
            else {
                arrayDate.push([Date.parse(new Date(data.hmtm[i][0])), data.hmtm[i][1]]);
            }
        }

        if (arrayDate.length > 0) {
            var start_utc = arrayDate[0][0];
            var end_utc = arrayDate[arrayDate.length - 1][0];
        } else {
            var start_date = new Date(data.start_month);
            var start_utc = Date.UTC(start_date.getUTCFullYear(), start_date.getUTCMonth(), start_date.getUTCDate());
            var end_date = new Date(data.end_month);
            var end_utc = Date.UTC(end_date.getUTCFullYear(), end_date.getUTCMonth(), end_date.getUTCDate());
        }

        Highcharts.chart('historicalmtm', {
            chart: {
                zoomType: 'x',
                backgroundColor: "#ffffff"
            },
            title: {
                text: 'Historical MTM',
                style: [{
                    fontSize: "12px"
                }]
            },
            subtitle: {
                text: document.ontouchstart === undefined ?
                    'Click and drag in the plot area to zoom in' :
                    'Pinch the chart to zoom in'
            },
            xAxis: {
                min: start_utc,
                max: end_utc,
                type: 'datetime',
                tickInterval: 24 * 3600 * 1000 * 30, // 30 Days
                labels: {
                    rotation: 0
                },
            },
            yAxis: {
                title: {
                    text: data['currency']
                }
            },
            legend: {
                enabled: false
            },
            plotOptions: {
                area: {
                    fillColor: {
                        linearGradient: {
                            x1: 0,
                            y1: 0,
                            x2: 0,
                            y2: 1
                        },
                        stops: [
                            [0, Highcharts.getOptions().colors[0]],
                            [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                        ]
                    },
                }
            },
            series: [{
                type: 'area',
                name: 'Mark-to-Market',
                data: arrayDate
            }],
            credits: {
                enabled: false
            },
        });
    }

    function notional_strike(data) {
        var notionalamount = [];
        var weightedstrike = [];

        for (let i = 0; i < data.notional_amount.length; i++) {
            notionalamount.push([Date.parse(new Date(data.notional_amount[i][0])), data.notional_amount[i][1]]);
        }

        for (let i = 0; i < data.weighted_strike.length; i++) {
            weightedstrike.push([Date.parse(new Date(data.weighted_strike[i][0])), data.weighted_strike[i][1]]);
        }

        var start_date = new Date(data['start_date']);
        var start_utc = Date.UTC(start_date.getUTCFullYear(), start_date.getUTCMonth(), start_date.getUTCDate());
        var end_date = new Date(data['end_date']);
        var end_utc = Date.UTC(end_date.getUTCFullYear(), end_date.getUTCMonth(), end_date.getUTCDate());
        Highcharts.chart('notional_container', {
            chart: {
                zoomType: 'x',
                type: 'area',
            },
            title: {
                text: 'Daily Notional Amount and  Weighted Avg. Strike'
            },

            legend: {

                enabled: false
            },

            xAxis: {

                min: start_utc,
                max: end_utc,
                type: 'datetime',
                tickInterval: 24 * 3600 * 1000 * 30, // 30 Days
                labels: {
                    rotation: 0
                },
            },
            yAxis: [{
                lineWidth: 1,
                title: {
                    text: 'Notional Amounts'
                }
            }, {
                min: 1.1,
                max: 1.5,
                lineWidth: 1,
                opposite: true,
                title: {
                    text: 'Weighted Avg. Strike'
                }
            }],
            tooltip: {
                shared: true,
            },
            credits: {
                enabled: false
            },
            plotOptions: {
                area: {
                    fillOpacity: 0.1
                }
            },
            series: [{
                name: 'Notional Amount',

                data: notionalamount
            }, {
                name: 'Weighted Avg. Strike',

                data: weightedstrike,
                yAxis: 1
            }]
        });
    }

    function fetchCounterparties() {
        const promises = [];

        // Saving inputs
        const csrfmiddlewaretoken = window.counterpartyJsParams.csrf_token;
        const client = window.counterpartyJsParams.client;
        const fund = $('#fund-select').val();

        promises.push(new Promise(function(resolve, reject) {
            $.ajax({
                type: 'POST',
                url: window.counterpartyJsParams.apiUserRecentFund,
                data: {
                    'csrfmiddlewaretoken': csrfmiddlewaretoken,
                    'client':client,
                    'fund': fund
                },
                success: function() { resolve(function() {}); },
                error: function() { reject(); }
            });
        }));

        const counterpartiesResolve = function(data) {
            const $tray = $('.counterparty-tray');
            $tray.empty();

            const template =
                '<div class="col-xs-3 col-sm-2 col-lg-1">' +
                '    <div>' +
                '        <input type="radio" name="optradio" id="{cp}" value="{cp}" {checked}>' +
                '        <div class="cp-sizer">' +
                '            <img src="/static/img/counterparty/{img}.png" class="img-thumbnail">' +
                '        </div>' +
                '    </div>' +
                '</div>';

            // All has some special conditions
            const premadeContents = ["all"].map(function(cp) {
                let str = template;
                str = str.replace(/\{cp\}/g, cp);
                str = str.replace(/\{img\}/g, 'All');
                str = str.replace(/\{checked\}/g, 'checked');
                return str;
            });

            const contents = data.counterparties.map(function(cp) {
                let str = template;
                str = str.replace(/\{cp\}/g, cp);
                str = str.replace(/\{img\}/g, cp);
                str = str.replace(/\{checked\}/g, '');
                return str;
            });

            $tray.html(premadeContents.concat(contents).join(''));
        }
        promises.push(new Promise(function(resolve, reject) {
            $.ajax({
                type: 'GET',
                url: window.counterpartyJsParams.apiCounterparties,
                data: {
                    'csrfmiddlewaretoken': csrfmiddlewaretoken,
                    'client': client,
                    'fund': fund,
                    'include_expired_counterparties': clientConfig.include_expired_counterparties
                },
                success: function(data) {
                    resolve(function() {
                        counterpartiesResolve(data)
                    });
                },
                error: function(error) {
                    reject(error);
                }
            });
        }));

        return Promise.all(promises);
    }

    /**
     * Fetch the data for everything besides the counterparty tray.
     *
     * Pass in true if you are switching funds and want to use 'all' for the counterparty.
     *
     * @param {boolean} defaultCounterparty
     */
    function fetchData(defaultCounterparty) {
        const promises = [];

        // The default value of 'all'
        const client = window.counterpartyJsParams.client;
        const csrfmiddlewaretoken = window.counterpartyJsParams.csrf_token;
        const counterparty = (defaultCounterparty)
            ? 'all'
            : $('input[name=optradio]:checked', '#mainForm').val();
        const fund = $('#fund-select').val();

        // Saving Queries
        const $valuationDate = $('#ValuationDate_select');

        /*
        // Counter Party Monitor has been disabled
        // Data improvements must be made before we worry about re-adding
        // this table back into the project.

        promises.push(new Promise(function(resolve, reject) {
            $.ajax({
                type: 'POST',
                url: window.counterpartyJsParams.apiCpMonitor,
                data: {
                    'csrfmiddlewaretoken': window.counterpartyJsParams.csrf_token,
                    'counterparty': $('input[name=optradio]:checked', '#mainForm').val(),
                    'client': window.counterpartyJsParams.client,
                    'fund': $('#fund-select').val()
                },
                success: function(data) {
                    resolve(data);
                },
                error: function(error) {
                    reject(error);
                }
            });
        }));
        resolutions.push(function(data) {
            grid(data['row'], data['columns']);
        });
        */

        promises.push(new Promise(function(resolve, reject) {
            $.ajax({
                type: 'POST',
                url: window.counterpartyJsParams.apiHistoricalMtM,
                data: {
                    'csrfmiddlewaretoken': csrfmiddlewaretoken,
                    'counterparty': counterparty,
                    'client': client,
                    'fund': fund
                },
                success: function(data) {
                    resolve(function() {
                        setup_historicalmtm(data);
                    });
                },
                error: function(error) {
                    reject(error);
                }
            });
        }));

//        promises.push(new Promise(function(resolve, reject) {
//            $.ajax({
//                type: 'POST',
//                url: window.counterpartyJsParams.apiCpSummation,
//                data: {
//                    'csrfmiddlewaretoken': csrfmiddlewaretoken,
//                    'counterparty': counterparty,
//                    'client': client,
//                    'fund': fund,
//                    'include_expired_counterparties': clientConfig.include_expired_counterparties
//                },
//                success: function(data) {
//                    resolve(function() {
//                        setup_cpsummation(data)
//                    });
//                },
//                error: function(error) {
//                    reject(error);
//                }
//            });
//        }));

        promises.push(new Promise(function(resolve, reject) {
            $.ajax({
                type: 'POST',
                url: window.counterpartyJsParams.apiAnalyticsMtMTable,
                data: {
                    'csrfmiddlewaretoken': csrfmiddlewaretoken,
                    'client': client,
                    'fund': fund,
                    'valuation_date': $valuationDate.val(),
                    'counterparty': counterparty
                },
                success: function(analyticsMtMData) {
                    // Analytics MtM
                    var dateList = analyticsMtMData.date_list || [];
                    var targetDate = new Date();
                    if (analyticsMtMData.valuation_date) {
                        targetDate = new Date(moment(analyticsMtMData.valuation_date, 'YYYY-MM-DD').format())
                    }

                    $valuationDate.datepicker('setDate', targetDate);
                    $valuationDate.datepicker("option", "dateFormat", "yy-mm-dd");
                    $valuationDate.datepicker("option", "beforeShowDay", function(date) {
                        return [dateList.find(function(val) { return val === moment(date).format('YYYY-MM-DD'); }), '', ''];
                    });
                    summaryGrid(analyticsMtMData);
                    mtmgrid(analyticsMtMData);
                    resolve(function() {

                    });

                },
                error: function(error) {
                    reject(error);
                }
            });
        }));

        // TODO: This should be replaced by a general permission system in the
        // future.
        if (window.counterpartyJsParams.avg_notional_amount) {
            promises.push(new Promise(function(resolve, reject) {
                $.ajax({
                    type: 'POST',
                    url: window.counterpartyJsParams.apiAveragedWeightNotional,
                    data: {
                        csrfmiddlewaretoken: csrfmiddlewaretoken,
                        counterparty: counterparty,
                        client: client,
                        fund: fund
                    },
                    success: function (data) {
                        resolve(function() {
                            notional_strike(data);
                        });
                    },
                    error: function(error) {
                        reject(error);
                    }
                });
            }));
        }

        return Promise.all(promises);
    }

    // jQuery UI Init
    $('#ValuationDate_select').datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "yy-mm-dd"
    });

    $('#fund-select').selectpicker('val', window.counterpartyJsParams.defaultSelectedFund);

    // Event Bindings
    $('#fund-select').change(function () {
        fetchCounterparties().then(function(cpResolves) {
            fetchData(true).then(function(fetchResolves) {
                cpResolves.forEach(function(def) { def(); });
                fetchResolves.forEach(function(def) { def(); });
            });
        })
    });

    $('#mainForm').on('change', 'input', function () {
        fetchData().then(function(fetchResolves) {
            fetchResolves.forEach(function(def) { def(); });
        });
    });

    $('#ValuationDate_select').change(function () {
        fetchData().then(function(fetchResolves) {
            fetchResolves.forEach(function(def) { def(); });
        });
    });

    $('#downloadSpotData').on('click', function () {
        var params = {
            fileName: 'Positions_Report',
            sheetName: window.counterpartyJsParams.client,
            exportMode: 'csv'
        };
        params.processHeaderCallback = function(params) {
            return params.column.getColDef().headerName.replace('<br/>','');
        };
        params.processCellCallback = function(params) {
            if(params.column.colId == 'mtm'){
                if (params.value === undefined|| params.value == null) {
                        return '';
                }
                return (accounting.formatMoney(params.value,"", 2, ",", "."));
            }else{
                return params.value;
            }
        };
        summaryGridOptions.api.exportDataAsCsv(params);
    });
    $('#downloadTReportsData').on('click', function () {
        var params = {
            fileName: 'Transactions_Report',
            sheetName: window.counterpartyJsParams.client,
            exportMode: 'csv'
        };
        params.processHeaderCallback = function(params) {
            return params.column.getColDef().headerName.replace('<br/>','');
        };
        params.processCellCallback = function(params) {
            if(params.column.colId == 'gross_mtm' || params.column.colId== 'gross_mtm_contract_ccy'){
                if (params.value === undefined|| params.value == null) {
                        return '';
                }
                return (accounting.formatMoney(params.value,"", 2, ",", "."));
            }else{
                return params.value;
            }
        };
        mtmgridOptions.api.exportDataAsCsv(params);
    });

    // Intial Load
    fetchCounterparties().then(function(cpResolves) {
        fetchData(true).then(function(fetchResolves) {
            cpResolves.forEach(function(def) { def(); });
            fetchResolves.forEach(function(def) { def(); });
        });
    })
});