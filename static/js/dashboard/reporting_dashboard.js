(function($) {
    var params = reportingDashboardJsParams;

    var reportsDropDown = $('#report_select');
    var exportsDropDown = $('#export_select');

    // Main functions
    function closeSnoAlertBox(msg="Error Undefined"){
        $('#snoAlertBox').text(msg);
            $('#snoAlertBox').addClass("alert alert-danger");
        window.setTimeout(function () {
          $("#snoAlertBox").fadeOut(300)
                    }, 3000);
    }

    function closeSuccesslertBox(msg="Error Undefined"){
        $('#successBox').text(msg);
        $('#successBox').addClass("alert alert-success");
        window.setTimeout(function () {
          $("#successBox").fadeOut(300)
                    }, 3000);
    }
    function fetchReportsList() {
        $.ajax({
                type: 'POST',
                url: params.fetch_reports_list_url,
                data: {
                    'csrfmiddlewaretoken': params.csrf_token,
                    'client': params.client,
                    'is_staff': params.is_staff,
                },
                success: function(data) {
                    populateReportsControl(data);
                }
        });
    }

    function populateReportsControl(data) {
        reportsDropDown.empty();
        reportsDropDown.prop('selectedIndex', 0);

        data.reports.forEach(function (element) {
            reportsDropDown.append($('<option value="' + element.id + '">' + element.report_label + '</option>'));
        });
        exportsDropDown.empty();
        exportsDropDown.prop('selectedIndex', 0);

        exportsDropDown.append($('<option value="excel">Excel Format</option>'));
        exportsDropDown.append($('<option value="csv">Csv Format</option>'));

    }

    function downloadSelectedReport() {
       var alert_required = true;
       if ($('#report_select option:selected').text().indexOf("Position") != -1 || $('#report_select option:selected').text().indexOf("Performance") != -1 ){
          if ($('#selectValuationDate').val() == undefined || $('#selectValuationDate').val() == ''  )
           {
            alert_required = false;
            $("#snoAlertBox").fadeIn();
            closeSnoAlertBox("Please provide valuation date");
           }
       }
       else if ($('#report_select option:selected').text().indexOf("Position") == -1 || $('#report_select option:selected').text().indexOf("Performance") == -1 ){

            if ($('#selectStartDate').val() == '' || $('#selectEndDate').val() == '' )
           {
            alert_required = false;
            $("#snoAlertBox").fadeIn();

             closeSnoAlertBox("Please provide  start or end date");
           }
       }

       if (alert_required){
            $.ajax({
                type: "POST",
                url: params.prepare_report_url,
                data: {
                    'csrfmiddlewaretoken': params.csrf_token,
                    'client': params.client,
                    'report_id': $('#report_select').val(),
                    'report_name': $('#report_select option:selected').text(),
                    'start_date': $('#selectStartDate').val(),
                    'end_date': $('#selectEndDate').val(),
                    'valuation_date': $('#selectValuationDate').val(),
                    'export_format': $('#export_select option:selected').val(),
                },
                success: function(data){
                        if (data.nrows == 0){
                            $("#snoAlertBox").fadeIn();
                            closeSnoAlertBox(data.error_message);
                            console.log(data.error_message);
                        }
                        else{
                            var url = params.download_file_data_url + data.file_name;
                            if (urlExists(url)) {
                               $("#successBox").fadeIn();
                                closeSuccesslertBox('Successfully generated the file');
                                window.location.assign(url);
                            } else {
                                 $("#snoAlertBox").fadeIn();
                                 closeSnoAlertBox('Error: Not possible to download the file.');
                                 console.log('Error: Not possible to download the file.');
                            }
                        }
                }
            });
        }
    }

    function urlExists(url) {
        var http = $.ajax({
            type:"HEAD",
            url: url,
            async: false
        });
        var status = http.status;
        var exists = false;
        if (status == 200) {
            exists = true
        }
        return exists
    }

    // Triggers

    $('#report_select').change(function() {
        if($('#report_select option:selected').text().toLowerCase().includes('position') || $('#report_select option:selected').text().toLowerCase().includes('performance')){
            $('#startDateControl').hide()
            $('#endDateControl').hide()
            $('#valuationDateControl').show()
        } else {
            $('#startDateControl').show()
            $('#endDateControl').show()
            $('#valuationDateControl').hide()
        }
    });

    $('#reportingDownloadButton').click(function() {
        downloadSelectedReport();
    });

    $(document).ready(function() {
        fetchReportsList();
    });
    $('#fund-select').prop('disabled', true);

})(jQuery);