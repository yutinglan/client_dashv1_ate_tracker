$(function() {
    var params = window.multiAnalyticsJsParams;
    var analysisLevelDropDown = $('#analysis_level_select');
    var mtmgridOptions = null;
    var eMtmGridDiv = document.querySelector('#fund-mtm-grid-analytics');
    var cpmtmgridOptions = null;
    var eCPMtmGridDiv = document.querySelector('#cp-mtm-grid-analytics');
    var tradegridOptions = null;
    var eTradeMtmGridDiv = document.querySelector('#trade-grid-analytics');
    var spotRefGridOptions = null;
    var eSpotRefGridDiv = document.querySelector('#spot-rate-table-analytics');
    var parentSummaryGridOptions = null;

    // converting cell value to two decimal places
    function commaFormat(params) {
        if (typeof params.value != 'undefined') {
            return (accounting.formatMoney(params.value,"", 2, ",", "."));
        }
    }

    // converting cell value to two decimal places
    function toFormatFour(params) {
        if (typeof params.value != 'undefined' & params.value != null) {
            return parseFloat(params.value).toFixed(4);
        }
    }

    /// Main functions
    function fetchLevelDropDownOptions(level = 'fund') {
        level = level.toLowerCase();
        $.ajax({
            type: 'POST',
            url: params.fetch_valuation_level_options_url,
            data: {
                'csrfmiddlewaretoken': params.csrf_token,
                'client': params.client,
                'level': level,
            },
            success: function (data) {
                if (data != null) {
                    populateLevelDropDown(data, level);
                }
            }
        });
    }

     function trade_mtmgrid(data) {
        if (tradegridOptions != null) {
            tradegridOptions.api.setRowData(data.data);
        } else {
            tradegridOptions = {
                columnDefs: [
                    { headerName: 'Trade ID', field: 'trade__trade_id' },
                    { headerName: 'Fund', field: 'trade__fund' },
                    { headerName: 'Trade Date', field: 'trade__trade_date' },
                    { headerName: 'Value Date', field: 'trade__value_date', sort: 'asc' },
                    { headerName: 'Delivery Date', field: 'trade__delivery_date' },
                    { headerName: 'Style', field: 'trade__style' },
                    { headerName: 'Underlying', field: 'trade__underlying' },
                    { headerName: 'Direction', field: 'trade__direction' },
                    { headerName: 'Notional Currency', field: 'trade__notional_currency' },
                    { headerName: 'Notional Amounts', field: 'trade__notional_amounts', valueFormatter: window.formatters.VrmMtm.notional, filter: 'agNumberColumnFilter', cellStyle: { 'text-align': "right" } },
                    { headerName: 'Strike', field: 'trade__strike', valueFormatter: window.formatters.VrmMtm.strike, cellStyle: { 'text-align': "right" } },
                    { headerName: 'Premium', field: 'trade__premium', valueFormatter: window.formatters.VrmMtm.premium, cellClass: 'text-right' },
                    { headerName: 'Premium Payment Date', field: 'trade__premium_payment_date' },
                    { headerName: 'Counterparty', field: 'trade__counterparty' },
                    { headerName: 'Valuation Date', field: 'valuation_date' },
                    { headerName: 'Spot Ref', field: 'spot_ref', valueFormatter: window.formatters.VrmMtm.spotRef, cellStyle: { 'text-align': "right" } },
                    { headerName: 'Outright Forward', field: 'outright_forward', valueFormatter: window.formatters.VrmMtm.outrightForward, cellStyle: { 'text-align': "right" } },
                    { headerName: 'MTM Currency', field: 'mtm_currency' },
                    { headerName: 'MTM', field: 'mtm', valueFormatter: commaFormat, filter: 'agNumberColumnFilter', cellStyle: { 'text-align': "right" } },
                    { headerName: 'Net MTM', field: 'net_mtm', valueFormatter: commaFormat, cellStyle: { 'text-align': "right" } },
                ],
                rowData: data.data,
                domLayout: 'autoHeight',
                headerHeight: 35,
                enableFilter: true,
                enableSorting: true,
                pagination: true,
                paginationPageSize: 30,
                defaultColDef: {
                    suppressMovable: true,
                    suppressMenu: true,
                },
                suppressColumnVirtualisation: true,
                onGridReady: function (gridOptions) {
                    gridOptions.columnApi.autoSizeAllColumns();
                },
            };

            mtmGrid_obj =  new agGrid.Grid(eTradeMtmGridDiv, tradegridOptions);
        }
    }



    function mtmGrid(data) {
        if (mtmgridOptions != null) {
            mtmgridOptions.api.setRowData(data.data);
        } else {
            mtmgridOptions = {
                columnDefs: [
                    { headerName: 'Fund', field: 'trade__fund' },
                    { headerName: 'MTM Currency', field: 'mtm_currency' ,  cellStyle: { 'text-align': "center" } },
                    { headerName: 'MTM', field: 'mtm_sum', valueFormatter: commaFormat, filter: 'agNumberColumnFilter', cellStyle: { 'text-align': "right" } },
                    { headerName: 'Net MTM', field: 'net_mtm_sum', valueFormatter: commaFormat, filter: 'agNumberColumnFilter', cellStyle: { 'text-align': "right" } },
                ],
                rowData: data.data,
                domLayout: 'autoHeight',
                enableFilter: true,
                headerHeight: 35,
                enableSorting: true,
                pagination: true,
                paginationPageSize: 20,
                enableColResize: true,
                rowSelection: 'single',
                onSelectionChanged: onSelectionChanged,
                defaultColDef: {
                    suppressMovable: true,
                    suppressFilter: true,
                    suppressMenu: true,
                },
                suppressColumnVirtualisation: true,
                    onGridReady: function (gridOptions) {
                    gridOptions.columnApi.autoSizeAllColumns();

                    var debouncedResize = _.debounce(function () {
                        gridOptions.columnApi.autoSizeAllColumns();
                    }, 300);

                    window.addEventListener('resize', function () {
                        debouncedResize();
                    })
                }
            };
            mtmGrid_obj = new agGrid.Grid(eMtmGridDiv, mtmgridOptions);
        }
    }
    function onSelectionChanged() {
        var selectedRows = mtmgridOptions.api.getSelectedRows();
        var selectedRowsString = '';
        selectedRows.forEach( function(selectedRow, index) {
            if (index!==0) {
                selectedRowsString += ', ';
            }
            selectedRowsString += selectedRow.trade__fund;
            $.ajax({
                type: 'POST',
                url: params.ajax_client_fund_cp_mtm,
                data: {
                    'csrfmiddlewaretoken': params.csrf_token,
                    'client': params.client,
                    'fund':selectedRow.trade__fund,
                    'valuation_date': $('#SpotRate_valuationDate_select').val(),
                },
                success: function (data) {
                     mtmCounterpatyGrid(data.data);
                }
            });
        });
//        document.querySelector('#selectedRows').innerHTML = selectedRowsString;
    }
    function mtmCounterpatyGrid(data) {
        if (cpmtmgridOptions != null) {
            if (data.length > 0)
            {
               cpmtmgridOptions.api.hideOverlay();
            }
            cpmtmgridOptions.api.setRowData(data);
            cpmtmgridOptions.api.sizeColumnsToFit();
        } else {

            cpmtmgridOptions = {
                columnDefs: [
                    { headerName: 'Counterparty', field: 'trade__counterparty' ,  width:100, cellRenderer: CounterpartyCellRenderer},
                    { headerName: 'MTM Currency', field: 'mtm_currency',cellStyle: { 'text-align': "center" },},
                    { headerName: 'MTM', field: 'mtm_sum', valueFormatter: commaFormat, filter: 'agNumberColumnFilter', cellStyle: { 'text-align': "right" } },
                    { headerName: 'Net MTM', field: 'net_mtm_sum', valueFormatter: commaFormat, filter: 'agNumberColumnFilter', cellStyle: { 'text-align': "right" } },
                ],
                rowData: data,
                domLayout: 'autoHeight',
                headerHeight: 35,
                pagination: true,
                paginationPageSize: 20,
                enableColResize: true,
                defaultColDef: {
                    suppressMovable: true,
                    suppressFilter: true,
                    suppressMenu: true,
                },
                onGridReady: function (gridOptions) {
                    gridOptions.columnApi.autoSizeAllColumns();
                    window.addEventListener('resize', function () {
                        gridOptions.columnApi.autoSizeAllColumns();
                    })
                },
                getRowHeight: function(params) {
                    return 60;
                },

                overlayNoRowsTemplate: '<span style="padding: 10px; border: 2px solid #444; background: lightgoldenrodyellow;">Please select the fund</span>'
            };
            mtmCPGrid_obj = new agGrid.Grid(eCPMtmGridDiv, cpmtmgridOptions);
            if (data.length > 0)
            {
                cpmtmgridOptions.api.hideOverlay();
            }
            else
            {
                cpmtmgridOptions.api.showNoRowsOverlay();
            }
        }
    }
    function CounterpartyCellRenderer() {
    }

    CounterpartyCellRenderer.prototype.init = function (params) {
        this.eGui = document.createElement('span');
        if (params.value !== "" || params.value !== undefined || params.value !== null) {
            var cp = '<img  src="/static/img/counterparty/'+ params.value + '.png" class="img-thumbnail">';
            this.eGui.innerHTML = cp;
        }
    };

    CounterpartyCellRenderer.prototype.getGui = function () {
        return this.eGui;
    };
    function spotRefGrid(spot_data) {
        if (spotRefGridOptions != null) {
            spotRefGridOptions.api.setRowData(spot_data.spot_rate);
              spotRefGridOptions.api.sizeColumnsToFit();
        } else {
            spotRefGridOptions = {
                columnDefs: [
                    {
                        headerName: 'Underlying',
                        field: 'underlying'
                    },
                    {
                        headerName: 'Spot Ref',
                        field: 'mid',
                        editable: true,
                        headerClass: 'text-right',
                        cellClass: 'text-right',
                        valueFormatter: window.formatters.SpotRatesTable.spotRef
                    },
                ],
                rowData: spot_data.spot_rate,
                domLayout: 'autoHeight',
                headerHeight: 35,
                defaultColDef: {
                    suppressMovable: true,
                    suppressFilter: true,
                    suppressMenu: true,
                },
                onGridReady: function (gridOptions) {
                    gridOptions.api.sizeColumnsToFit();

                    var debouncedResize = _.debounce(function () {
                        gridOptions.api.sizeColumnsToFit();
                    }, 300);

                    window.addEventListener('resize', function () {
                        debouncedResize();
                    })
                }
            };
            spotRefGrid_obj = new agGrid.Grid(eSpotRefGridDiv, spotRefGridOptions);
             spotRefGridOptions.api.sizeColumnsToFit();
        }
    }


    function fetch_mtm_data(select=null) {
        const promises = [];

        // The default value of 'all'
        const client = window.multiAnalyticsJsParams.client;
        const csrfmiddlewaretoken = window.multiAnalyticsJsParams.csrf_token;

        // Saving Queries
        const $valuationDate = $('#SpotRate_valuationDate_select');

        promises.push(new Promise(function(resolve, reject) {
            $.ajax({
                type: 'POST',
                url: params.ajax_client_fund_mtm,
                data: {
                    'csrfmiddlewaretoken': params.csrf_token,
                    'client': params.client,
                    'valuation_date': select,
                },
                success: function (data) {
                    var dateList = data.date_list || [];
                    var targetDate = new Date();
                    if (data.valuation_date) {
                        targetDate = new Date(moment(data.valuation_date, 'YYYY-MM-DD').format())
                    }

                    $('#SpotRate_valuationDate_select').datepicker('setDate', targetDate);
                    $('#SpotRate_valuationDate_select').datepicker('option', 'dateFormat', 'yy-mm-dd');
                    $('#SpotRate_valuationDate_select').datepicker('option', 'beforeShowDay', function(date) {
                        return [dateList.find(function(val) { return val === moment(date).format('YYYY-MM-DD'); }), '', ''];
                    });
                    mtmGrid(data);
                    mtmCounterpatyGrid([]);
                    spot_related();
                    trade_data();
                    resolve(function() {

                     });


                },
                error: function(error) {
                    reject(error);
                }
            });

        }));
    }

    function spot_related(){
            $.ajax({
                type: 'POST',
                url: params.multi_spot_ref_url,
                data: {
                    'csrfmiddlewaretoken': params.csrf_token,
                    'client': params.client,
                    'valuation_date': $('#SpotRate_valuationDate_select').val(),
                },
                success: function (spot_data) {
                    spotRefGrid(spot_data);
                }
            });
    }

    function trade_data(){
            $.ajax({
                type: 'POST',
                url: params.ajax_client_trade_mtm,
                data: {
                    'csrfmiddlewaretoken': params.csrf_token,
                    'client': params.client,
                    'valuation_date': $('#SpotRate_valuationDate_select').val(),
                },
                success: function (data) {
                    trade_mtmgrid(data);
                }
            });
    }

     function showParentSummary(ParentData) {
        if (parentSummaryGridOptions != null) {

            parentSummaryGridOptions.api.setRowData(ParentData.data);
            parentSummaryGridOptions.api.sizeColumnsToFit();

        } else {

            parentSummaryGridOptions = {
                columnDefs: [
                    { headerName: 'Initial Date', field: 'initial_date', suppressFilter: true, minWidth:100,cellStyle: { 'text-align': "center" } },
                    { headerName: 'Deal Id', field: 'deal_id', suppressFilter: true, minWidth:100,cellStyle: { 'text-align': "center" } },
                    { headerName: 'Deal Name', field: 'deal_name', suppressFilter: true, minWidth:100,cellStyle: { 'text-align': "center" } },
                    { headerName: 'Fiscal Year', field: 'fiscal_year', suppressFilter: true, minWidth:100,cellStyle: { 'text-align': "center" } },
                    { headerName: 'Quarter', field: 'quarter', suppressFilter: true, minWidth:100,cellStyle: { 'text-align': "center" } },
                    { headerName: 'Unfixed Mark To Market', field: 'unfixed_mark_to_market', suppressFilter: true, minWidth:100,cellStyle: { 'text-align': "center" } },
                    { headerName: 'Update Timestamp', field: 'update_timestamp', suppressFilter: true, minWidth:100,cellStyle: { 'text-align': "center" } },
                    { headerName: 'Valuation Date', field: 'valuation_date', suppressFilter: true, minWidth:100,cellStyle: { 'text-align': "center" } },
                    { headerName: 'Audited', field: 'audited', suppressFilter: true, minWidth:100,cellStyle: { 'text-align': "center" } },
                    { headerName: 'Balance Year Prior', field: 'balance_year_prior', suppressFilter: true, minWidth:100,cellStyle: { 'text-align': "center" } },
                    { headerName: 'Realised In Quarter', field: 'realised_in_quarter', suppressFilter: true, minWidth:100,cellStyle: { 'text-align': "center" } },
                    { headerName: 'Realised In Year To Date', field: 'realised_in_year_to_date', suppressFilter: true, minWidth:100,cellStyle: { 'text-align': "center" } },
                    { headerName: 'Settlement Of Contract', field: 'settlement_of_contract', suppressFilter: true, minWidth:100,cellStyle: { 'text-align': "center" } },
                    { headerName: 'Balance Year End', field: 'balance_year_end', suppressFilter: true, minWidth:100,cellStyle: { 'text-align': "center" } },
                    { headerName: 'Initial Date', field: 'initial_date', suppressFilter: true, minWidth:100,cellStyle: { 'text-align': "center" } },
                    { headerName: 'Value Date', field: 'value_date', suppressFilter: true, minWidth:100,cellStyle: { 'text-align': "center" }},
                    { headerName: 'Underlying', field: 'underlying', suppressFilter: true, minWidth:110,cellStyle: { 'text-align': "center" }},
                    { headerName: 'Notional Currency', field: 'notional_currency', suppressFilter: true,minWidth:140, cellStyle: { 'text-align': "center" }},
                    { headerName: 'Initial Amounts', field: 'initial_amounts', valueFormatter: commaFormat,minWidth:130, suppressFilter: true, cellStyle: { 'text-align': "right" } },
                    { headerName: 'Initial Rate', field: 'initial_rate', valueFormatter: commaFormat, suppressFilter: true, minWidth:100, cellStyle: { 'text-align': "center" } },
                    { headerName: 'Outstanding Amounts', field: 'outstanding_amounts', minWidth:170,valueFormatter: commaFormat, suppressFilter: true, cellStyle: { 'text-align': "right" } },
                    { headerName: 'Realised PnL', field: 'realised_pnl', valueFormatter: commaFormat,minWidth:120, suppressFilter: true, cellStyle: { 'text-align': "right" } },
                    { headerName: 'Unrealised PnL', field: 'unrealised_pnl', valueFormatter: commaFormat, minWidth:130,suppressFilter: true, cellStyle: { 'text-align': "right" } },
                ],
                rowData: ParentData.data,
                domLayout: 'autoHeight',
                enableFilter: false,
                headerHeight: 35,
                enableSorting: true,
                pagination: true,
                paginationPageSize: 30,
                defaultColDef: {
                    // set every column width
                    suppressMovable: true,
                    suppressMenu: true,

                },
            };
            var parentSummaryGridDiv = document.querySelector('#parent_summary_grid_analytics');
            new agGrid.Grid(parentSummaryGridDiv, parentSummaryGridOptions);
            parentSummaryGridOptions.api.sizeColumnsToFit();
        }
    }
    function parent_summary() {
        const promises = [];

        // The default value of 'all'
        const client = window.multiAnalyticsJsParams.client;
        const csrfmiddlewaretoken = window.multiAnalyticsJsParams.csrf_token;

       promises.push(new Promise(function(resolve, reject) {
            $.ajax({
                type: 'POST',
                url: params.agg_report_lexington,
                data: {
                    'csrfmiddlewaretoken': params.csrf_token,
                    'client': params.client,
                },
                success: function (data) {
                    showParentSummary(data);
                    resolve(function() {

                     });


                },
                error: function(error) {
                    reject(error);
                }
            });

        }));
    }
    function fetchMetrics() {
        if (params.outstanding_report){
            fetch_mtm_data();
        }

        if (params.parents_summary_report){
            parent_summary();
        }
    };
    fetchMetrics();
    // Event Bindings
    $('#downloadSpotData-analytics').on('click', function () {

        var fileparams = {
            fileName: 'Aggregate Reports',
            sheetName: params.client,
            exportMode: 'csv'
         };
        tradegridOptions.api.exportDataAsCsv(fileparams);
    });

    $('#SpotRate_valuationDate_select').change(function () {
        fetch_mtm_data($('#SpotRate_valuationDate_select').val());
    });

    // if funds changes we need to perform the action
    $('#fund-select').prop('disabled', true);

    // Initializing Date picker
    $('#SpotRate_valuationDate_select').datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "yy-mm-dd"
    });
});