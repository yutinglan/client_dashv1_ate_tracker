"use strict";

$(function () {
    let gridOptions = null;
    let mtmgridOptions = null;
    let dropdown_data = null;
    let fund_analytics_option = $('#fund_analytics_option');
    let analysisLevelDropDown = $('#fund_analysis_level_select');
    var fundLarGridOptions = null;
    var cellClassRules = {
        'header-cell': 'data.section === "big-title"',
        'quarters-cell': 'data.section === "quarters"',

    };
    function numberToColor(val) {
        if (val>100) {
            return '#CD5C5C';
        } else if (val >= 80) {
            return '#ffae42';
        } else {
            return '#aaffaa';
        }
     }
    var cellCurrentClassRules = {
        'current-cell': 'data.section === "quarters"',

    };
    function mtmgrid(data) {
        if (mtmgridOptions != null) {
            mtmgridOptions.api.setRowData(data.data);
        } else {
            mtmgridOptions = {
                columnDefs: [
                    { headerName: 'Trade ID', field: 'trade__trade_id' },
                    { headerName: 'Fund', field: 'trade__fund' },
                    { headerName: 'Trade Date', field: 'trade__trade_date' },
                    { headerName: 'Value Date', field: 'trade__value_date', sort: 'asc' },
                    { headerName: 'Delivery Date', field: 'trade__delivery_date' },
                    { headerName: 'Style', field: 'trade__style' },
                    { headerName: 'Underlying', field: 'trade__underlying' },
                    { headerName: 'Direction', field: 'trade__direction' },
                    { headerName: 'Notional Currency', field: 'trade__notional_currency' },
                    { headerName: 'Notional Amounts', field: 'trade__notional_amounts', valueFormatter: window.formatters.VrmMtm.notional, filter: 'agNumberColumnFilter', cellStyle: { 'text-align': "right" } },
                    { headerName: 'Strike', field: 'trade__strike', valueFormatter: window.formatters.VrmMtm.strike, cellStyle: { 'text-align': "right" } },
                    { headerName: 'Premium', field: 'trade__premium', valueFormatter: window.formatters.VrmMtm.premium, cellClass: 'text-right' },
                    { headerName: 'Premium Payment Date', field: 'trade__premium_payment_date' },
                    { headerName: 'Counterparty', field: 'trade__counterparty' },
                    { headerName: 'Valuation Date', field: 'valuation_date' },
                    { headerName: 'Spot Ref', field: 'spot_ref', valueFormatter: window.formatters.VrmMtm.spotRef, cellStyle: { 'text-align': "right" } },
                    { headerName: 'Outright Forward', field: 'outright_forward', valueFormatter: window.formatters.VrmMtm.outrightForward, cellStyle: { 'text-align': "right" } },
                    { headerName: 'MTM Currency', field: 'mtm_currency' },
                    { headerName: 'MTM', field: 'mtm', valueFormatter: commaFormat, filter: 'agNumberColumnFilter', cellStyle: { 'text-align': "right" } },
                    { headerName: 'Net MTM', field: 'net_mtm', valueFormatter: commaFormat, cellStyle: { 'text-align': "right" } },
                ],
                rowData: data.data,
                domLayout: 'autoHeight',
                headerHeight: 35,
                enableFilter: true,
                enableSorting: true,
                pagination: true,
                paginationPageSize: 15,
                defaultColDef: {
                    suppressMovable: true,
                    suppressMenu: true,
                },
                rowSelection: 'multiple',
                suppressColumnVirtualisation: true,
                onGridReady: function (gridOptions) {
                    gridOptions.columnApi.autoSizeAllColumns();
                },
            };
            var eMtmGridDiv = document.querySelector('#trade-mtm-grid');
            new agGrid.Grid(eMtmGridDiv, mtmgridOptions);
        }

        if (data.mtm_sum != null) {
            if (data.currency != null){
                $('#CPMTM').text(data.currency.toUpperCase() + ' : ' + (Math.round(data.mtm_sum)).toLocaleString());
            }else{
                $('#CPMTM').text('');
            }
        } else {
            $('#CPMTM').text('');
        }
    }

    function grid(rowData, columnDefs) {
        if (gridOptions != null) {
            gridOptions.api.setRowData(rowData);
        } else {
            gridOptions = {
                columnDefs: columnDefs,
                rowData: rowData,
                domLayout: 'autoHeight',
                headerHeight: 35,
                enableColResize: false,
                defaultColDef: {
                    suppressMovable: true,
                    suppressFilter: true,
                    suppressMenu: true,
                },
                onGridReady: function (gridOptions) {
                    gridOptions.columnApi.autoSizeAllColumns();
                }
            };

            var eGridDiv = document.querySelector('#myGrid');
            new agGrid.Grid(eGridDiv, gridOptions);
        }
    }

    // converting cell value to two decimal places
    function commaFormat(params) {
       if (params.value === undefined|| params.value == null) {
                return '';
        }
        return (accounting.formatMoney(params.value,"", 2, ",", "."));

    }


    function setup_cpsummation(data) {
        Highcharts.chart('container', {
            chart: {
                type: 'heatmap',
                marginTop: 40,
                marginBottom: 80,
                plotBorderWidth: 1
            },
            title: {
                text: 'Liquidity at Risk (95% CL), in ' + data.currency,
                style: [{
                    fontSize: "12px"
                }]
            },
            xAxis: {
                categories: [analysisLevelDropDown.val()],

            },
            yAxis: {
                categories: data['counterparty'],
                title: null,

            },
            colorAxis: {
                min: data['color'],
                max: 0,
                minColor: '#dd4b39',
                maxColor: '#ffffff'
            },
            legend: {
                enabled: false
            },
            tooltip: {
                formatter: function () {
                    return '<b>' + this.series.xAxis.categories[this.point.x] + '</b>  95% CL <br><b>' +
                        (Math.round(this.point.value)).toLocaleString() + '</b> ' + data.currency + '<br><b>' + this.series.yAxis.categories[this.point.y] + '</b>';
                }
            },
            series: [{
                name: 'with 95% Confidence Level',
                borderWidth: 1,
                // The heatmap isn't 2D so index is horizontal index is always 0
                data: data['cp'].map(function(val, index) { return [0, index, val]; }),
                dataLabels: {
                    enabled: true,
                    color: '#000000',
                    style: [{
                        fontSize: "14px",
                        textOutline: false
                    }],
                    formatter: function () {
                        return this.point.value.toLocaleString();
                    },
                }
            }],
            credits: {
                enabled: false
            },
        });
    }

    function isHeaderRow(params) {
        return params.data.section === 'big-title';
    }

    function isQuarterRow(params) {
        return params.data.section === 'quarters';
    }
    function initFundLarDisplay(data, ccy){
         if (fundLarGridOptions != null) {
            fundLarGridOptions.api.setRowData(data);

        } else {
            fundLarGridOptions = {
                columnDefs: [
                    { headerName: 'Fund', field: 'fund' ,
                        colSpan: function (params) {
                                    if (isHeaderRow(params)) {
                                        return 4;
                                    } else if (isQuarterRow(params)) {
                                        return 3;
                                    } else {
                                        return 1;
                                    }
                                },
                                cellClassRules: cellClassRules},
                    { headerName: 'Legal Entity', field: 'entity', },
                    { headerName: 'Counterparty', field: 'counterparty', },
                    { headerName: 'Liquidity At Risk (95% CL) in '+ ccy, valueFormatter: commaFormat, field: 'lar',cellStyle: { 'text-align': "center" }, colSpan: function (params) {
                            if (isQuarterRow(params)) {
                                return 1;
                            } else {
                                return 1;
                            }
                        }, cellClassRules: cellCurrentClassRules
                    }
                ],
                rowData: data,
                domLayout: 'autoHeight',
                enableFilter: true,
                headerHeight: 35,
                enableSorting: true,
                pagination: true,
                paginationPageSize: 15,
                defaultColDef: {
                    suppressMovable: true,
                    suppressMenu: true,
                    resizable: true
                },
                onFirstDataRendered: onFirstDataRendered,
            };
           var fundLarDiv = document.querySelector('#fundLarGridOptions_table');
           new agGrid.Grid(fundLarDiv, fundLarGridOptions);

        }

   }
   function onFirstDataRendered(params) {
        params.api.sizeColumnsToFit();
    }

   function setup_historicalmtm(data) {
        var arrayDate = [];
        for (let i = 0; i < data.hmtm.length; i++) {
            if (data.hmtm[i][0]['date'] !== undefined) {
                arrayDate.push([Date.parse(new Date(data.hmtm[i][0]['date'])), data.hmtm[i][1]['sum']]);
            }
            else {
                arrayDate.push([Date.parse(new Date(data.hmtm[i][0])), data.hmtm[i][1]]);
            }
        }

        if (arrayDate.length > 0) {
            var start_utc = arrayDate[0][0];
            var end_utc = arrayDate[arrayDate.length - 1][0];
        } else {
            var start_date = new Date(data.start_month);
            var start_utc = Date.UTC(start_date.getUTCFullYear(), start_date.getUTCMonth(), start_date.getUTCDate());
            var end_date = new Date(data.end_month);
            var end_utc = Date.UTC(end_date.getUTCFullYear(), end_date.getUTCMonth(), end_date.getUTCDate());
        }
        Highcharts.chart(fund_analytics_option.val() == 'Entity' ? 'historicalmtm' : 'entity_historicalmtm', {
            chart: {
                zoomType: 'x',
                backgroundColor: "#ffffff"
            },
            title: {
                text: 'Historical MTM',
                style: [{
                    fontSize: "12px"
                }]
            },
            subtitle: {
                text: document.ontouchstart === undefined ?
                    'Click and drag in the plot area to zoom in' :
                    'Pinch the chart to zoom in'
            },
            xAxis: {
                min: start_utc,
                max: end_utc,
                type: 'datetime',
                tickInterval: 24 * 3600 * 1000 * 30, // 30 Days
                labels: {
                    rotation: 0
                },
            },
            yAxis: {
                title: {
                    text: data['currency']
                }
            },
            legend: {
                enabled: false
            },
            plotOptions: {
                area: {
                    fillColor: {
                        linearGradient: {
                            x1: 0,
                            y1: 0,
                            x2: 0,
                            y2: 1
                        },
                        stops: [
                            [0, Highcharts.getOptions().colors[0]],
                            [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                        ]
                    },
                }
            },
            series: [{
                type: 'area',
                name: 'Mark-to-Market',
                data: arrayDate
            }],
            credits: {
                enabled: false
            },
        });
    }

    function notional_strike(data) {
        var notionalamount = [];
        var weightedstrike = [];

        for (let i = 0; i < data.notional_amount.length; i++) {
            notionalamount.push([Date.parse(new Date(data.notional_amount[i][0])), data.notional_amount[i][1]]);
        }

        for (let i = 0; i < data.weighted_strike.length; i++) {
            weightedstrike.push([Date.parse(new Date(data.weighted_strike[i][0])), data.weighted_strike[i][1]]);
        }

        var start_date = new Date(data['start_date']);
        var start_utc = Date.UTC(start_date.getUTCFullYear(), start_date.getUTCMonth(), start_date.getUTCDate());
        var end_date = new Date(data['end_date']);
        var end_utc = Date.UTC(end_date.getUTCFullYear(), end_date.getUTCMonth(), end_date.getUTCDate());
        Highcharts.chart('notional_container', {
            chart: {
                zoomType: 'x',
                type: 'area',
            },
            title: {
                text: 'Daily Notional Amount and  Weighted Avg. Strike'
            },

            legend: {

                enabled: false
            },

            xAxis: {

                min: start_utc,
                max: end_utc,
                type: 'datetime',
                tickInterval: 24 * 3600 * 1000 * 30, // 30 Days
                labels: {
                    rotation: 0
                },
            },
            yAxis: [{
                lineWidth: 1,
                title: {
                    text: 'Notional Amounts'
                }
            }, {
                min: 1.1,
                max: 1.5,
                lineWidth: 1,
                opposite: true,
                title: {
                    text: 'Weighted Avg. Strike'
                }
            }],
            tooltip: {
                shared: true,
            },
            credits: {
                enabled: false
            },
            plotOptions: {
                area: {
                    fillOpacity: 0.1
                }
            },
            series: [{
                name: 'Notional Amount',

                data: notionalamount
            }, {
                name: 'Weighted Avg. Strike',

                data: weightedstrike,
                yAxis: 1
            }]
        });
    }

    function fetchCounterparties() {
        const promises = [];

        // Saving inputs
        const csrfmiddlewaretoken = window.counterpartyJsParams.csrf_token;
        const client = window.counterpartyJsParams.client;
        const fund =  $('#fund-select').val() ;
        const entity = fund_analytics_option.val() == 'Entity' ? analysisLevelDropDown.val() : '';

        promises.push(new Promise(function(resolve, reject) {
            $.ajax({
                type: 'POST',
                url: window.counterpartyJsParams.apiUserRecentFund,
                data: {
                    'csrfmiddlewaretoken': csrfmiddlewaretoken,
                    'client':client,
                    'fund': fund,
                    'entity': entity,
                },
                success: function() { resolve(function() {}); },
                error: function() { reject(); }
            });
        }));

        const counterpartiesResolve = function(data) {
            const $tray = $('.counterparty-tray');
            $tray.empty();

            const template =
                '<div class="col-xs-3 col-sm-2 col-lg-1">' +
                '    <div>' +
                '        <input type="radio" name="optradio" id="{cp}" value="{cp}" {checked}>' +
                '        <div class="cp-sizer">' +
                '            <img src="/static/img/counterparty/{img}.png" class="img-thumbnail">' +
                '        </div>' +
                '    </div>' +
                '</div>';

            // All has some special conditions
            const premadeContents = ["all"].map(function(cp) {
                let str = template;
                str = str.replace(/\{cp\}/g, cp);
                str = str.replace(/\{img\}/g, 'All');
                str = str.replace(/\{checked\}/g, 'checked');
                return str;
            });

            const contents = data.counterparties.map(function(cp) {
                let str = template;
                str = str.replace(/\{cp\}/g, cp);
                str = str.replace(/\{img\}/g, cp);
                str = str.replace(/\{checked\}/g, '');
                return str;
            });

            $tray.html(premadeContents.concat(contents).join(''));
        }
        promises.push(new Promise(function(resolve, reject) {
            $.ajax({
                type: 'GET',
                url: window.counterpartyJsParams.apiCounterparties,
                data: {
                    'csrfmiddlewaretoken': csrfmiddlewaretoken,
                    'client': client,
                    'fund': fund,
                    'entity': entity,
                    'include_expired_counterparties': clientConfig.include_expired_counterparties
                },
                success: function(data) {
                    resolve(function() {
                        counterpartiesResolve(data)
                    });
                },
                error: function(error) {
                    reject(error);
                }
            });
        }));

        return Promise.all(promises);
    }

    /**
     * Fetch the data for everything besides the counterparty tray.
     *
     * Pass in true if you are switching funds and want to use 'all' for the counterparty.
     *
     * @param {boolean} defaultCounterparty
     */
    function fetchData(defaultCounterparty) {
        const promises = [];

        // The default value of 'all'
        const client = window.counterpartyJsParams.client;
        const csrfmiddlewaretoken = window.counterpartyJsParams.csrf_token;
        const counterparty = (defaultCounterparty)
            ? 'all'
            : $('input[name=optradio]:checked', '#mainForm').val();
        const fund =  $('#fund-select').val() ;
        const entity = fund_analytics_option.val() == 'Entity' ? analysisLevelDropDown.val() : '';

        // Saving Queries
        const $valuationDate = $('#ValuationDate_select');

        promises.push(new Promise(function(resolve, reject) {
            $.ajax({
                type: 'POST',
                url: window.counterpartyJsParams.apiHistoricalMtM,
                data: {
                    'csrfmiddlewaretoken': csrfmiddlewaretoken,
                    'counterparty': counterparty,
                    'client': client,
                    'fund': $('#fund-select').val(),
                    'entity': entity,
                },
                success: function(data) {
                    resolve(function() {
                        setup_historicalmtm(data);
                    });
                },
                error: function(error) {
                    reject(error);
                }
            });
        }));

        if (entity == ''){
            promises.push(new Promise(function(resolve, reject) {

                $.ajax({
                    type: 'GET',
                    url: window.counterpartyJsParams.ajaxEntityLar,
                    data: {
                        'csrfmiddlewaretoken': csrfmiddlewaretoken,
                        'counterparty': counterparty,
                        'client': client,
                        'fund': $('#fund-select').val(),
                        'include_expired_counterparties': clientConfig.include_expired_counterparties
                    },
                    success: function(data) {
                        resolve(function() {
                            initFundLarDisplay(data.data, data.ccy);
                        });
                    },
                    error: function(error) {
                        reject(error);
                    }
                });
            }));
        }

        promises.push(new Promise(function(resolve, reject) {
            $.ajax({
                type: 'POST',
                url: window.counterpartyJsParams.apiCpSummation,
                data: {
                    'csrfmiddlewaretoken': csrfmiddlewaretoken,
                    'counterparty': counterparty,
                    'client': client,
                    'fund': $('#fund-select').val(),
                    'entity': entity,
                    'include_expired_counterparties': clientConfig.include_expired_counterparties
                },
                success: function(data) {
                    resolve(function() {
                        setup_cpsummation(data);

                    });
                },
                error: function(error) {
                    reject(error);
                }
            });
        }));


        promises.push(new Promise(function(resolve, reject) {
            $.ajax({
                type: 'POST',
                url: window.counterpartyJsParams.apiAnalyticsMtMTable,
                data: {
                    'csrfmiddlewaretoken': csrfmiddlewaretoken,
                    'client': client,
                    'fund': $('#fund-select').val(),
                    'entity': entity,
                    'valuation_date': $valuationDate.val(),
                    'counterparty': counterparty
                },
                success: function(analyticsMtMData) {
                    // Analytics MtM
                    var dateList = analyticsMtMData.date_list || [];
                    var targetDate = new Date();
                    if (analyticsMtMData.valuation_date) {
                        targetDate = new Date(moment(analyticsMtMData.valuation_date, 'YYYY-MM-DD').format())
                    }

                    $valuationDate.datepicker('setDate', targetDate);
                    $valuationDate.datepicker("option", "dateFormat", "yy-mm-dd");
                    $valuationDate.datepicker("option", "beforeShowDay", function(date) {
                        return [dateList.find(function(val) { return val === moment(date).format('YYYY-MM-DD'); }), '', ''];
                    });
                    resolve(function() {
                        mtmgrid(analyticsMtMData);
                    });
//                    $.ajax({
//                        type: 'POST',
//                        url: window.counterpartyJsParams.apiTradeMtMCounterparty,
//                        data: {
//                            'csrfmiddlewaretoken': csrfmiddlewaretoken,
//                            'counterparty': counterparty,
//                            'client': client,
//                            'fund': $('#fund-select').val(),
//                            'entity': entity,
//                            'valuation_date': $valuationDate.val()
//                        },
//                        success: function(tradeMtMCounterpartyData) {
//                            resolve(function() {
//                                mtmgrid(tradeMtMCounterpartyData);
//                            });
//                        },
//                        error: function(error) {
//                            reject(error);
//                        }
//                    });
                },
                error: function(error) {
                    reject(error);
                }
            });
        }));

        // TODO: This should be replaced by a general permission system in the
        // future.
        if (window.counterpartyJsParams.avg_notional_amount) {
            promises.push(new Promise(function(resolve, reject) {
                $.ajax({
                    type: 'POST',
                    url: window.counterpartyJsParams.apiAveragedWeightNotional,
                    data: {
                        csrfmiddlewaretoken: csrfmiddlewaretoken,
                        counterparty: counterparty,
                        client: client,
                        fund: $('#fund-select').val(),
                        entity: entity,
                    },
                    success: function (data) {
                        resolve(function() {
                            notional_strike(data);
                        });
                    },
                    error: function(error) {
                        reject(error);
                    }
                });
            }));
        }

        return Promise.all(promises);
    }

    // jQuery UI Init
    $('#ValuationDate_select').datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "yy-mm-dd"
    });



    $('#mainForm').on('change', 'input', function () {
        fetchData().then(function(fetchResolves) {
            fetchResolves.forEach(function(def) { def(); });
        });
    });

    $('#ValuationDate_select').change(function () {
        fetchData().then(function(fetchResolves) {
            fetchResolves.forEach(function(def) { def(); });
        });
    });


    $('#downloadSpotData').on('click', function () {
        var params = {
            fileName: 'Counterparty-level_Valuation',
            fileName: 'Counterparty-level_Valuation',
            sheetName: window.counterpartyJsParams.client,
            exportMode: 'csv'
        };
        mtmgridOptions.api.exportDataAsCsv(params);
    });
    /**
    *
    * Custom JS For the glendower summary page has been added here.
    *
    */

    /// Main functions
//    var $analysisLevelDropDown = $('#analysis_level_select');
   function fetchLevelDropDownOptions(){
         $.ajax({
            type: 'POST',
            url: window.counterpartyJsParams.fetch_hedge_valuation_level_options,
            data: {
              'csrfmiddlewaretoken': window.counterpartyJsParams.csrf_token,
              'client': window.counterpartyJsParams.client,
              'fund':$('#fund-select').val(),
            },
            success: function (data) {
                if (data.data != null || data.data.length > 0) {
                    dropdown_data = data.data;
                    fund_analytics_option.empty();
                    fund_analytics_option.prop('selectedIndex', 0);
                    var count = 1 ;
//                    document.getElementById("level_data_para").innerHTML = "";
                    data.data.forEach(function (element) {
                        if ( element.id  != 'Deal'){
                            fund_analytics_option.append($('<option value="' + element.id + '">' + element.id + '</option>'));
    //                        $('#level_data_para').append('<p>'+count+'. '+element.id+' Level Analysis.</p>')
                            count++;
                        }
                    });
                    populateLevelDropDown();

                }
                else{
                    fund_analytics_option.empty();
                    analysisLevelDropDown.empty();
                }
            }
        });
   }

   function populateLevelDropDown() {
        analysisLevelDropDown.empty();
        analysisLevelDropDown.prop('selectedIndex', 0);
        if (fundLarGridOptions!= null){
                fundLarGridOptions.api.destroy();
                fundLarGridOptions = null;
        }
        dropdown_data.forEach(function (element) {
            if (element.id == fund_analytics_option.val()){
                for (var value in element.value){
                    analysisLevelDropDown.append($('<option value="' +element.value[ value]+ '">' +element.value[ value]+ '</option>'));
                }
                if (element.id == 'Fund'){
                    analysisLevelDropDown.prop('disabled', true);
                      $('#fund_lar_display').show();
                      $('#lar_container_display').hide();
                      $('#fund_container_display').show();
                 }
                else
                {
                    analysisLevelDropDown.prop('disabled', false);
                      $('#fund_lar_display').hide();
                      $('#lar_container_display').show();
                      $('#fund_container_display').hide();
                }
            }
        });
        window.setTimeout(function() {
            // Intial Load
            fetchCounterparties().then(function(cpResolves) {
                fetchData(true).then(function(fetchResolves) {
                    cpResolves.forEach(function(def) { def(); });
                    fetchResolves.forEach(function(def) { def(); });
                });
            })
        })
    }

    // if funds changes we need to perform the action
    $('#fund-select').selectpicker('val', window.counterpartyJsParams.selected_fund);

    // Event Bindings
    $('#fund-select').change( function() {
       fetchLevelDropDownOptions();
    });
   fund_analytics_option.change( function() {
        populateLevelDropDown();
    });

    fetchLevelDropDownOptions();

    analysisLevelDropDown.change(function () {
         fetchCounterparties().then(function(cpResolves) {
                fetchData(true).then(function(fetchResolves) {
                    cpResolves.forEach(function(def) { def(); });
                    fetchResolves.forEach(function(def) { def(); });
                });
            })
     });

    /**
    *
    * Custom JS For the glendower summary page ends here.
    *
    */
});