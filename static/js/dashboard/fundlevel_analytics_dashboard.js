$(function() {

    var params = fundLevelAnalyticsJsParams;
    var analysisLevelDropDown = $('#fund_analysis_level_select');
    var fund_analytics_option = $('#fund_analytics_option');
    var dropdown_data = null;
    var investorGridOptions = null;
    var investoranalysiswaterfall = null;
    var mtmgridOptions = null;
    var eMtmGridDiv = document.querySelector('#trade-mtm-grid-analytics');
    var spotRefGridOptions = null;
    var eSpotRefGridDiv = document.querySelector('#spot-rate-table-analytics');
    var spotRefGridOptions = null;
    var eSpotRefGridDiv = document.querySelector('#spot-rate-table-analytics');

    function numberToColor(val) {
//        if (val>100) {
            return '#B2BEB5';
//        } else if (val >= 80) {
//            return '#ffae42';
//        } else {
//            return '#aaffaa';
//        }
     }
    /// Auxiliary functions
    // converting cell value to two decimal places
    function toFormatTwo(params) {
        if (typeof params.value != 'undefined'){
            return parseFloat(params.value).toFixed(2);
        }
    }

    // converting cell value to two decimal places
    function commaFormat(params) {
        if (typeof params.value != 'undefined'){
            return (accounting.formatMoney(params.value,"", 2, ",", "."));
        }
    }

    // converting cell value to two decimal places
    function toFormatFour(params) {
        if (typeof params.value != 'undefined' & params.value != null){
            return (accounting.formatMoney(params.value,"", 4, "", "."));
        }
    }
    // We can put this code in the common place if required so.
    const monthNames = ["January", "February", "March", "April", "May", "June",
              "July", "August", "September", "October", "November", "December"
            ];
     // last 2 year only for the moment
     var qntYears = 1;
     var selectYear = $("#year_select");
     var selectMonth = $("#month_select");
     var currentYear = new Date().getFullYear();

     for (var y = 0; y < qntYears; y++){
        let date = new Date(currentYear);
        var yearElem = document.createElement("option");
        yearElem.value = currentYear
        yearElem.textContent = currentYear;
        selectYear.append(yearElem);
        currentYear--;
      }

      for (var m = 0; m < 12; m++){
          let monthNum = new Date(2019, m).getMonth()
          let month = monthNames[monthNum];
          var monthElem = document.createElement("option");
          monthElem.value = monthNum+1;
          monthElem.textContent = month;
          selectMonth.append(monthElem);
        }


    if (params.investor)
    {
        var d = new Date();
        var month = d.getMonth();
        var year = d.getFullYear();
        document.getElementById("year_select").selectedIndex = "0";
        document.getElementById("month_select").selectedIndex = d.getMonth();
    }
    /// Main functions

   function fetchLevelDropDownOptions(){
         $.ajax({
            type: 'POST',
            url: params.fetch_hedge_valuation_level_options,
            data: {
              'csrfmiddlewaretoken': params.csrf_token,
              'client': params.client,
              'fund':$('#fund-select').val(),
            },
            success: function (data) {
                if (data.data != null || data.data.length > 0) {
                    dropdown_data = data.data;
                    fund_analytics_option.empty();
                    fund_analytics_option.prop('selectedIndex', 0);
                    var count = 1 ;
                    document.getElementById("level_data_para").innerHTML = "";
                    data.data.forEach(function (element) {
                       if ( element.id  != 'Entity'){
                            fund_analytics_option.append($('<option value="' + element.id + '">' + element.id + '</option>'));
                            $('#level_data_para').append('<p>'+count+'. '+element.id+' Level Analysis.</p>')
                            count++;
                       }
                    });
                    populateLevelDropDown();

                }
                else{
                    fund_analytics_option.empty();
                    analysisLevelDropDown.empty();
                }
            }
        });
   }

   function populateLevelDropDown() {
        analysisLevelDropDown.empty();
        analysisLevelDropDown.prop('selectedIndex', 0);
        if (investorGridOptions!= null){
                investorGridOptions.api.destroy();
                investorGridOptions = null;
        }
        dropdown_data.forEach(function (element) {
            if (element.id == fund_analytics_option.val()){
                for (var value in element.value){
                        analysisLevelDropDown.append($('<option value="' +element.value[ value]+ '">' +element.value[ value]+ '</option>'));
                }
                if (element.id == 'Fund'){
                    analysisLevelDropDown.prop('disabled', true);
                    if  (element.value[0] == "GSS Holding (Asset)"){
                        $('#investor_content').html(" Asset Return Analysis");
                        fetch_investor_summary();
                        $('#investor_display').show();
                    }
                    else{
                     $('#investor_display').hide();
                     }
                 }
                else
                {
                    if(element.id.toLowerCase() == 'investor'){
                        $('#investor_content').html(fund_analytics_option.val()  +" Return Analysis");
                        fetch_investor_summary();
                        $('#investor_display').show();

                    }
                    else{

                        if  ( $('#fund-select').val() == "GSS Holding (Asset)"){
                            $('#investor_content').html(" Asset Return Analysis");
                            fetch_investor_summary();
                            $('#investor_display').show();
                        }
                        else{

                            $('#investor_display').hide();
                        }
                    }
                    analysisLevelDropDown.prop('disabled', false);
                }
            }
        });
        fetchMetrics();
    }

    function mtmGrid(data) {
        if (mtmgridOptions != null) {
            mtmgridOptions.api.setRowData(data.data);
        } else {
            mtmgridOptions = {
                columnDefs: [
                    { headerName: 'Trade ID', field: 'trade__trade_id' },
                    { headerName: 'Fund', field: 'trade__fund' },
                    { headerName: 'Trade Date', field: 'trade__trade_date' },
                    { headerName: 'Value Date', field: 'trade__value_date' },
                    { headerName: 'Delivery Date', field: 'trade__delivery_date' },
                    { headerName: 'Style', field: 'trade__style' },
                    { headerName: 'Underlying', field: 'trade__underlying' },
                    { headerName: 'Direction', field: 'trade__direction' },
                    { headerName: 'Notional Currency', field: 'trade__notional_currency' },
                    { headerName: 'Notional Amounts', field: 'trade__notional_amounts', valueFormatter: commaFormat, filter: 'agNumberColumnFilter', cellStyle: { 'text-align': "right" } },
                    { headerName: 'Strike', field: 'trade__strike', valueFormatter: toFormatFour },
                    { headerName: 'Premium', field: 'trade__premium' },
                    { headerName: 'Premium Currency', field: 'trade__premium_currency' },
                    { headerName: 'Premium Payment Date', field: 'trade__premium_payment_date' },
                    { headerName: 'Counterparty', field: 'trade__counterparty' },
                    { headerName: 'Valuation Date', field: 'valuation_date' },
                    { headerName: 'Spot Ref', field: 'spot_ref', valueFormatter: toFormatFour },
                    { headerName: 'Outright Forward', field: 'outright_forward', valueFormatter: toFormatFour },
                    { headerName: 'MTM Currency', field: 'mtm_currency' },
                    { headerName: 'MTM', field: 'mtm', valueFormatter: commaFormat, filter: 'agNumberColumnFilter', cellStyle: { 'text-align': "right" } },
                ],
                rowData: data.data,
                domLayout: 'autoHeight',
                enableFilter: true,
                headerHeight: 35,
                enableSorting: true,
                pagination: true,
                paginationPageSize: 15,
                enableColResize: true,
                defaultColDef: {
                    suppressMovable: true,
                    suppressFilter: true,
                    suppressMenu : true,
                },
                suppressColumnVirtualisation: true,
                onGridReady: function (gridOptions) {
                    gridOptions.columnApi.autoSizeAllColumns();

                    var debouncedResize = _.debounce(function () {
                        gridOptions.columnApi.autoSizeAllColumns();
                    }, 300);

                    window.addEventListener('resize', function () {
                        debouncedResize();
                    })
                }
            };
            mtmGrid_obj = new agGrid.Grid(eMtmGridDiv, mtmgridOptions);
        }

        if (typeof data.mtm_sum != 'Undefined' & data.mtm_sum != null) {
            if (data.currency != null){
                $('#TotalMTM').text(data.currency.toUpperCase() + ' : ' + (parseFloat(data.mtm_sum).toFixed(2)).toLocaleString() +' million')
            }else{
                $('#TotalMTM').text('');
            }
        } else {
            $('#TotalMTM').text('');
        }
    }

    function spotRefGrid(data) {
        if (spotRefGridOptions != null) {
            spotRefGridOptions.api.setRowData(data.data);
        } else {
            spotRefGridOptions = {
                columnDefs: [
                    {
                        headerName: 'Underlying',
                        field: 'trade__underlying'
                    },
                    {
                        headerName: 'Spot Ref',
                        field: 'average_spot_rate',
                        editable: true,
                        cellClass: 'text-right',
                        headerClass: 'text-right',
                        cellEditor: 'SpotRefCellEditor',
                        valueFormatter: window.formatters.SpotRatesTable.spotRef
                    },
                ],
                rowData: data.data,
                domLayout: 'autoHeight',
                headerHeight: 35,
                enableColResize: false,
                defaultColDef: {
                    suppressMovable: true,
                    suppressFilter: true,
                    suppressMenu : true,
                },
                components:{
                    SpotRefCellEditor: SpotRefCellEditor
                },
                onGridReady: function (gridOptions) {
                    gridOptions.api.sizeColumnsToFit();

                    var debouncedResize = _.debounce(function () {
                        gridOptions.api.sizeColumnsToFit();
                    }, 300);

                    window.addEventListener('resize', function () {
                        debouncedResize();
                    })
                }
            };
            spotRefGrid_obj = new agGrid.Grid(eSpotRefGridDiv, spotRefGridOptions);
        }
    }

    let KEY_BACKSPACE = 8;
    let KEY_F2 = 113;
    let KEY_DELETE = 46;
    function SpotRefCellEditor() {}

    SpotRefCellEditor.prototype.init = function(params) {
        this.gui = document.createElement('input');
        this.gui.type = 'text';
        this.gui.classList.add('my-simple-editor');

        this.params = params;

        var startValue;

        let keyPressBackspaceOrDelete =
            params.keyPress === KEY_BACKSPACE
            || params.keyPress === KEY_DELETE;

        if (keyPressBackspaceOrDelete) {
            startValue = '';
        } else if (params.charPress) {
            startValue = params.charPress;
        } else {
            startValue = params.value;
            if (params.keyPress !== KEY_F2) {
                this.highlightAllOnFocus = true;
            }
        }

        if (startValue!==null && startValue!==undefined) {
            this.gui.value = startValue;
        }
    };

    SpotRefCellEditor.prototype.getGui = function() {
        return this.gui;
    };

    SpotRefCellEditor.prototype.getValue = function() {
        return this.gui.value;
    };

    SpotRefCellEditor.prototype.afterGuiAttached = function() {
        this.gui.focus();
    };

    SpotRefCellEditor.prototype.myCustomFunction = function() {
        return {
            rowIndex: this.params.rowIndex,
            colId: this.params.column.getId()
        };
    };

    setInterval( function(){
    if (spotRefGridOptions != null){
        var instances = spotRefGridOptions.api.getCellEditorInstances();
        if (instances.length>0) {
            var instance = instances[0];
            if (instance.myCustomFunction) {
                var result = instance.myCustomFunction();
                //console.log('found editing cell: row index = ' + result.rowIndex + ', column = ' + result.colId + '.');
            } else {
                // console.log('found editing cell, but method myCustomFunction not found, must be the default editor.');
            }
        } else {
            // console.log('found not editing cell.');
        }
        }
    }, 1000);

    function updateMtmGrid() {
        var spot_ref_data = getGridData(spotRefGridOptions)

        $.ajax({
            type: 'POST',
            url: params.recalculate_mtm_on_spotref_url,
            data: {
                'csrfmiddlewaretoken': params.csrf_token,
                'spot_ref_data': spot_ref_data,
                'client': params.client,
                'fund':$('#fund-select').val(),
                'level': fund_analytics_option.val().toLowerCase(),
                'level_value': analysisLevelDropDown.val(),
                'valuation_date': $('#SpotRate_valuationDate_select').val()
                },
            success: function(updated_mtm_data) {
                mtmGrid(updated_mtm_data);
            },
            error: function(updated_mtm_data) {
                console.log("Error");
            }
        })

    };

    function getGridData(gridOptions) {
        var rowData = [];
        gridOptions.api.forEachNode( function(node) {
            rowData.push(node.data);
        });
        return JSON.stringify(rowData);
    };

    function initFundPercentagePieChart(datum,ccy,fund_sum) {
        var title = 'Investment Summary';

        Highcharts.chart('fund-percentage-container', {
            chart: {
                backgroundColor: null,
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: title
            },
            subtitle: {
                text: '<b> Current NAV by Currency in </b>' +ccy +'<b> - Total :</b>'+ (fund_sum/1000000).toFixed(2).toLocaleString()+ ' mn'
            },
            tooltip: {
                pointFormat: '<b>{point.name} Invested Equity('+ ccy +'): {point.value}</b>',
                backgroundColor: 'rgba(255, 255, 255, 1)'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.value} - {point.y} %',
                        style: {
                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                        }
                    }
                }
            },
            credits: {
                enabled: false
            },
            exporting: {
                enabled: true
            },
            series: [{
                name: ' Fund Currency',
                colorByPoint: true,
                data: datum
            }]
        });
    }

    function initPortfolioDealHedgeMTMChart(data){
        Highcharts.chart('fund-hedge-vs-unhedged-container',{
            chart: {
                zoomType: 'x'
            },
            title: {
                text: 'Portfolio Valuation (Unhedged vs. Hedged)'
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                type: 'datetime',
                categories: data.time
            },
            yAxis: {
                title: {
                    text: data.ccy
                }
            },
            legend: {
                enabled: true
            },
            plotOptions: {
                area: {
                    marker: {
                        radius: 2
                    },
                    lineWidth: 1,
                    states: {
                        hover: {
                            lineWidth: 1
                        }
                    },
                    threshold: null
                }
            },
            credits: {
                    enabled: false
            },
            exporting: {
                enabled: true
            },
            series: [{
                type: 'line',
                name: 'Hedged',
                data: data.hedged
            },
            {
                type: 'line',
                name: 'Unhedged',
                data: data.unhedged
            }]
        })
    }



   function initInvestorAnalysis(data){
         if (investorGridOptions != null) {
            investorGridOptions.api.setRowData(data.data);
            investorGridOptions.api.sizeColumnsToFit();
        } else {
            investorGridOptions = {
                columnDefs: [
                    { headerName: 'Return Invest Currency', field: 'return_invest_ccy' },
                    { headerName: 'FX Market Impact ', field: 'fx_market_impact', },
                    { headerName: 'Return Fund Currency', field: 'return_fund_ccy', cellStyle: function(params) {
                     if (params.value === undefined) {
                        // nothing to be done at the moment
                     }else{
                            var color = numberToColor(params.value);
                            return {
                                backgroundColor: color,
                                textAlign: "center"
                            }
                        }}
                    },
                    { headerName: 'Cost of Hedging', field: 'cost_of_hedging',   },
                    { headerName: 'Hedging Impact', field: 'hedging_impact',   },
                    { headerName: 'Final Returns', field: 'final_return',  cellStyle: function(params) {
                         if (params.value === undefined) {
                            // nothing to be done at the moment
                         }else{
                            var color = numberToColor(params.value);
                            return {
                                backgroundColor: color,
                                textAlign: "center"
                            }
                        }}
                    },
                ],
                rowData: data.data,
                domLayout: 'autoHeight',
                enableFilter: true,
                headerHeight: 45,
                enableSorting: true,
                pagination: true,
                paginationPageSize: 15,
                defaultColDef: {
                    suppressMovable: true,
                    suppressFilter: true,
                    suppressMenu: true,
                    editable: false,
                    onGridReady: function (params) {
                        params.columnApi.autoSizeAllColumns();
                        window.addEventListener('resize', function () {
                            setTimeout(function () {
                                params.columnApi.autoSizeAllColumns();
                            })
                        })
                    }
                },
            };
            var investorgridDiv = document.querySelector('#investorGridOptions_table');
           investorgrid = new agGrid.Grid(investorgridDiv, investorGridOptions);

        }

        Highcharts.chart('investoranalysis', {
            chart: {
                type: 'waterfall'
            },

            title: {
                text: 'Investor hedge Performance'
            },

            xAxis: {
                type: 'category'
            },

            yAxis: {
                title: {
                    text: 'Return %'
                }
            },

            legend: {
                enabled: false
            },

            tooltip: {
                pointFormat: '<b>{point.y:.2f}</b>%'
            },
             credits: {
                  enabled: false
            },
            exporting: {
                enabled: true
            },
            series: [{
                upColor: '#42BA97',
                color: '#D62839',
                borderColor:'#CCCCCC',
                data: [ {
                            name: 'Return Invest Currency',
                            y: data.results.return_invest_ccy
                        },
                        {
                            name: 'FX Market Impact ',
                            y: data.results.fx_market_impact
                        },
                        {
                            name: 'Return Fund Currency',
                            isIntermediateSum: true,
                            y: data.results.return_fund_ccy,
                            color: Highcharts.getOptions().colors[1]
                        },
                        {
                            name: 'Cost of Hedging',
                             y: data.results.cost_of_hedging
                        },
                        {
                            name: 'Hedging Impact',
                            y: data.results.hedging_impact
                         },
                        {
                           name: 'Final Return',
                           isSum: true,
                           y: data.results.final_return,
                           color: Highcharts.getOptions().colors[1]
                         }],
                dataLabels: {
                    enabled: true,
                    formatter: function () {
                        return Highcharts.numberFormat(this.y, 2, '.') ;
                    },
                    style: {
                        fontSize:16,
                        fontWeight: 'bold'
                    }
                },
                pointPadding: 0
                }]
        });
   }

    function initSummaryBaseCaseRisk(data){

        Highcharts.chart('summary-base-case-risk', {
            chart: {
                type: 'waterfall'
            },
            title: {
                text: 'Risk breakdown'
            },
            xAxis: {
                type: 'category'
            },

            yAxis: {
                title: {
                    text: 'IRR (95% CL)'
                }
            },

            legend: {
                enabled: false
            },

            tooltip: {
                pointFormat: '<b>{point.y:,.2f}</b>%'
            },
            credits: {
                    enabled: false
            },
            exporting: {
                enabled: true
            },
            series: [{
                upColor: '#42BA97',
                color: '#D62839',
                borderColor:'#CCCCCC',
                data: [{
                    name: 'Base Case',
                    y: data.base_case
                },
                {
                    name: 'Current FX Gain/Loss',
                    y: data.current_fx_gain_loss
                },
                {
                    name: '@ Risk',
                    y: data.at_risk
                },
                {
                    name: 'Hedging Effect',
                    y: data.hedging_effect
                },
                {
                    name: 'Cost',
                    y: data.cost
                },
                {
                    name: 'Worst Case',
                    isSum: true
                }],
                dataLabels: {
                    enabled: true,
                    formatter: function () {
                        return Highcharts.numberFormat(this.y, 2, '.') + '%';
                    },
                    style: {
                        fontSize:16,
                        fontWeight: 'bold'
                    }
                },
                pointPadding: 0
            }]
        });
    }

    function initMoicBaseCaseRisk(data){

        Highcharts.chart('moic-base-case-risk', {
            chart: {
                type: 'waterfall'
            },

            title: {
                text: 'Risk breakdown'
            },

            xAxis: {
                type: 'category'
            },

            yAxis: {
                title: {
                    text:  'MOIC'
                }
            },

            legend: {
                enabled: false
            },

            tooltip: {
                pointFormat: '<b>{point.y:,.2f}</b>'
            },
                credits: {
                    enabled: false
            },
            exporting: {
                enabled: true
            },
            series: [{
                upColor: '#42BA97',
                color: '#D62839',
                borderColor:'#CCCCCC',
                data: [{
                    name: 'Base Case',
                    y: data.base_case
                },
                {
                    name: 'Current FX Gain/Loss',
                    y: data.current_fx_gain_loss
                },
                {
                    name: '@ Risk',
                    y: data.at_risk
                },
                {
                    name: 'Hedging Effect',
                    y: data.hedging_effect
                },
                {
                    name: 'Cost',
                    y: data.cost
                },
                {
                    name: 'Worst Case',
                    isSum: true
                }],
                dataLabels: {
                    enabled: true,
                    formatter: function () {
                        return Highcharts.numberFormat(this.y, 2, '.') ;
                    },
                    style: {
                        fontSize:16,
                        fontWeight: 'bold'
                    }
                },
                pointPadding: 0
            }]
        });
    }

    /// Data fetching
    function fetch_fund_summary(select_date=null){
        if  ( params.portfolio){
            $.ajax({
                type: 'POST',
                url: params.fundlevel_investment_summary,
                data: {
                    'csrfmiddlewaretoken': params.csrf_token,
                    'client': params.client,
                    'fund':$('#fund-select').val(),
                    'level': fund_analytics_option.val().toLowerCase(),
                    'level_value': analysisLevelDropDown.val(),
                    'select_date':select_date,
                },
                success: function (data) {
                    var mappedFundPercentageData = data.data.map(function (fund_summary_data) {
                        return {
                            name: fund_summary_data.ccy,
                            y: fund_summary_data.percentage,
                            value:Math.round(fund_summary_data.value).toLocaleString()
                        }
                    });
                    initFundPercentagePieChart(mappedFundPercentageData,data.fund_ccy, data.fund_sum);
                }
            });
        }
    }

    function fetch_historical_valuation_data() {
        if  ( params.portfolio){
            $.ajax({
                type:'POST',
                url: params.fundlevel_portfolio_mtm_chart_chart,
                data: {
                    'csrfmiddlewaretoken': params.csrf_token,
                    'client': params.client,
                    'fund':$('#fund-select').val(),
                    'level': fund_analytics_option.val().toLowerCase(),
                    'level_value': analysisLevelDropDown.val(),
                },
                success: function(data) {
                    initPortfolioDealHedgeMTMChart(data);
                }
            });
        }
    }

    function fetch_base_risk_data (){
        if  ( params.irr){
            $.ajax({
                type:'POST',
                url: params.fundlevel_summary_base_case_risk,
                data: {
                    'csrfmiddlewaretoken': params.csrf_token,
                    'client': params.client,
                        'fund':$('#fund-select').val(),
                    'level': fund_analytics_option.val().toLowerCase(),
                    'level_value': analysisLevelDropDown.val(),
                    'confidence_level': $('#CF_select').val(),
                },
                success: function(data) {
                    initSummaryBaseCaseRisk(data);
                }
            });
        }
    }

    function fetch_investor_summary (){
        $.ajax({
            type:'POST',
            url: params.fundlevel_investor_summary_analysis,
            data: {
                'csrfmiddlewaretoken': params.csrf_token,
                'client': params.client,
                 'fund':$('#fund-select').val(),
                'level': fund_analytics_option.val().toLowerCase(),
                'level_value': analysisLevelDropDown.val(),
                'date': selectYear.val() +' '+selectMonth.val(),
                'overall': document.getElementById("overall").checked
            },
            success: function(data) {
                initInvestorAnalysis(data);
            }
        });
    }

    function fetch_moic_risk_data (){
        if  ( params.moic){
            $.ajax({
                type:'POST',
                url: params.fundlevel_moic_base_case_risk,
                data: {
                    'csrfmiddlewaretoken': params.csrf_token,
                    'client': params.client,
                        'fund':$('#fund-select').val(),
                    'level': fund_analytics_option.val().toLowerCase(),
                    'level_value': analysisLevelDropDown.val(),
                    'confidence_level': $('#moic_select').val(),
                },
                success: function(data) {
                    initMoicBaseCaseRisk(data);
                }
            });
        }
    }

    function fetch_mtm_data(select=null) {
        $.ajax({
            type: 'POST',
            url: params.fundlevel_analytics_mtm_table_url,
            data: {
                'csrfmiddlewaretoken': params.csrf_token,
                'client': params.client,
                    'fund':$('#fund-select').val(),
                'level': fund_analytics_option.val().toLowerCase(),
                'level_value': analysisLevelDropDown.val(),
                'valuation_date': select,
                'is_proportional_tab': true,
            },
            success: function(data) {
                var dateList = data.date_list || [];
                var targetDate = new Date();
                if (data.valuation_date) {
                    targetDate = new Date(moment(data.valuation_date, 'YYYY-MM-DD').format())
                }

                $('#SpotRate_valuationDate_select').datepicker('setDate', targetDate);
                $('#SpotRate_valuationDate_select').datepicker('option', 'dateFormat', 'yy-mm-dd');
                $('#SpotRate_valuationDate_select').datepicker('option', 'beforeShowDay', function(date) {
                    return [dateList.find(function(val) { return val === moment(date).format('YYYY-MM-DD'); }), '', ''];
                });

                mtmGrid(data);
            }
        });
        $.ajax({
            type: 'POST',
            url: params.fundlevel_spot_ref_url,
            data: {
                'csrfmiddlewaretoken': params.csrf_token,
                'client': params.client,
                'fund':$('#fund-select').val(),
                'level': fund_analytics_option.val().toLowerCase(),
                'level_value': analysisLevelDropDown.val(),
                'valuation_date': select,
            },
            success: function(data) {
                spotRefGrid(data);
            }
        });
    }

    $('#fund-select').selectpicker('val', params.selected_fund);

    function addOption(id_name, data){
        var select = document.getElementById(id_name);
        select.options.length = 0;
        data.forEach(function(element) {
            select.options[select.options.length] = new Option(element.trim(), element.trim(), false, false);
        });
    }

    function fetchMetrics(){
        fetch_fund_summary();
        fetch_historical_valuation_data();
        fetch_base_risk_data();
        fetch_moic_risk_data();
        fetch_mtm_data();
    };

    // Event Bindings
    fund_analytics_option.change( function() {
        populateLevelDropDown();
    });

    $('#updateSpotData-analytics').on('click', function() {
        updateMtmGrid();
    });

    $('#resetSpotData-analytics').on('click', function() {
        fetch_mtm_data();
    });

    $('#downloadSpotData-analytics').on('click', function() {
        mtmgridOptions.api.exportDataAsCsv();
    });

    $('#SpotRate_valuationDate_select').change(function () {
        fetch_mtm_data($('#SpotRate_valuationDate_select').val());
    });

    $('#CF_select').change( function () {
        fetch_base_risk_data();
    });

    $('#moic_select').change( function () {
        fetch_moic_risk_data();
    });

    analysisLevelDropDown.change( function () {
        fetchMetrics();
    });

    // Fund selected
    $('#fund-select').selectpicker('val', params.selected_fund);
    fetchLevelDropDownOptions();

    // Initializing Date picker
    $('#SpotRate_valuationDate_select').datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "yy-mm-dd",
    });

     $('#fund-select').change(function(){
       fetchLevelDropDownOptions();
    });

    // If investor is displayed and Month or Year or Overall is selected
    $("#overall").change(function() {
        fetch_investor_summary();
    });

    selectMonth.change(function(){
        fetch_investor_summary();
    });

    selectYear.change(function(){
        fetch_investor_summary();
    });

});

