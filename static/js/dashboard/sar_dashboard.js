"use strict";

(function($) {
    const params = window.sarJsParams;
    let value_date = window.sarJsParams.value_date.replace(new RegExp("&#39;", "g"), "").replace("[","").replace("]","").split(", ") || [];
    let gridOptions = null;
    let mtmgridOptions = null;
    let rollinggridOptions = null;
    let cashSettlementGridOptions = null;
    let cashSummationGridOptions = null;
    let fund_deselection = [];
    let cp_deselection = [];

    function getDatePicker() {
        // function to act as a class
        function Datepicker() {}

        // gets called once before the renderer is used
        Datepicker.prototype.init = function(params) {
            // create the cell
            this.eInput = document.createElement('input');
            this.eInput.value = params.value;

            // https://jqueryui.com/datepicker/
            $(this.eInput).datepicker({
                dateFormat: 'yy-mm-dd'
            });
        };

        // gets called once when grid ready to insert the element
        Datepicker.prototype.getGui = function() {
            return this.eInput;
        };

        // focus and select can be done after the gui is attached
        Datepicker.prototype.afterGuiAttached = function() {
            this.eInput.focus();
            this.eInput.select();
        };

        // returns the new value after editing
        Datepicker.prototype.getValue = function() {
            return this.eInput.value;
        };

        // any cleanup we need to be done here
        Datepicker.prototype.destroy = function() {
            // but this example is simple, no cleanup, we could
            // even leave this method out as it's optional
        };

        // if true, then this editor will appear in a popup
        Datepicker.prototype.isPopup = function() {
            // and we could leave this method out also, false is the default
            return false;
        };

        return Datepicker;
    }

    function rollinggrid(data) {
        if (rollinggridOptions != null) {
            rollinggridOptions.api.setRowData(data.list_rolling_period);
        } else {
            rollinggridOptions = {
                columnDefs: [
                    { headerName: 'Fund', field: 'fund' },
                    { headerName: 'Underlying', field: 'underlying' },
                    { headerName: 'Direction', field: 'direction' },
                    { headerName: 'Notional Currency', field: 'notional_currency' },
                    { headerName: 'Counterparty', field: 'counterparty' },
                    {
                        headerName: 'Max Rolling Amounts',
                        field: 'notional_amounts__sum',
                        valueFormatter: window.formatters.VrmHedge.notional,
                        filter: 'agNumberColumnFilter',
                        cellStyle: {
                            'text-align': 'right'
                        }
                    },
                    {
                        headerName: 'Rolling Amounts',
                        field: 'notional_amounts__newsum',
                        valueFormatter: window.formatters.VrmHedge.notional,
                        editable: true,
                        filter: 'agNumberColumnFilter',
                        cellStyle: {
                            'text-align': 'right',
                            'backgroundColor': 'orange'
                        },
                        cellEditor: 'mySimpleCellEditor'
                    },
                    {
                        headerName: 'Near Leg Strike',
                        field: 'strike',
                        valueFormatter: window.formatters.VrmHedge.strike,
                        editable: true,
                        cellEditor: 'mySimpleCellEditor'
                    },
                    {
                        headerName: 'New Value Date',
                        field: 'value_date',
                        editable: true,
                        cellStyle: {
                            'text-align': 'center',
                            'backgroundColor': 'orange'
                        },
                        cellEditor: 'datePicker'
                    },
                ],
                rowData: data.list_rolling_period,
                domLayout: 'autoHeight',
                enableFilter: true,
                headerHeight: 35,
                enableSorting: true,
                pagination: true,
                paginationPageSize: 15,
                components: {
                    mySimpleCellEditor: MySimpleCellEditor,
                    datePicker: getDatePicker()
                },
                defaultColDef: {
                    suppressMovable: true,
                    suppressFilter: true,
                    suppressMenu: true,
                    editable: false,
                    onGridReady: function (params) {
                        params.columnApi.autoSizeAllColumns();

                        window.addEventListener('resize', function () {
                            setTimeout(function () {
                                params.columnApi.autoSizeAllColumns();
                            })
                        })
                    }
                },
                suppressColumnVirtualisation: true,
                onGridReady: function (gridOptions) {
                    gridOptions.columnApi.autoSizeAllColumns();
                }
            };
            var eRollingGridDiv = document.querySelector('#rolling-grid');
            new agGrid.Grid(eRollingGridDiv, rollinggridOptions);
        }
    }

    function cashsettlementgrid(data) {
        if (cashSettlementGridOptions != null) {
            cashSettlementGridOptions.api.setRowData(data.data);
        } else {
            cashSettlementGridOptions = {
                columnDefs: [
                    { headerName: 'Trade ID', field: 'trade_id', pinned: 'left' },
                    { headerName: 'Fund', field: 'fund' },
                    { headerName: 'Counterparty', field: 'counterparty' },
                    { headerName: 'CCY', field: 'notional_currency' },
                    {
                        headerName: 'Notional Amounts',
                        field: 'notional_amounts',
                        valueFormatter: window.formatters.VrmHedge.notional,
                        filter: 'agNumberColumnFilter',
                        cellStyle: {
                            'text-align': 'right'
                        }
                    },
                    { headerName: 'CCY', field: 'in_notional_currency' },
                    {
                        headerName: 'Notional Amounts',
                        field: 'in_notional_amounts',
                        valueFormatter: window.formatters.VrmHedge.notional,
                        filter: 'agNumberColumnFilter',
                        cellStyle: {
                            'text-align': 'right'
                        }
                    },
                    { headerName: 'Direction', field: 'direction', hide: true},
                    { headerName: 'Style', field: 'style', hide: true},
                ],
                rowData: data.data,
                domLayout: 'autoHeight',
                enableFilter: true,
                headerHeight: 35,
                enableSorting: true,
                pagination: true,
                paginationPageSize: 15,
                defaultColDef: {
                    suppressMovable: true,
                    suppressFilter: true,
                    suppressMenu: true,
                },
                suppressColumnVirtualisation: true,
                onGridReady: function (gridOptions) {
                    gridOptions.columnApi.autoSizeAllColumns();
                }
            };
            var eCashSettlementGridDiv = document.querySelector('#cashSettlementOptions');
            new agGrid.Grid(eCashSettlementGridDiv, cashSettlementGridOptions);
        }
    }

    function cashSummation(data) {
        if (cashSummationGridOptions != null) {
            cashSummationGridOptions.api.setRowData(data.cash_summation);
        } else {
            cashSummationGridOptions = {
                columnDefs: [
                    { headerName: 'Fund', field: 'fund' },
                    { headerName: 'Counterparty', field: 'counterparty' },
                    { headerName: 'CCY', field: 'ccy' },
                    {
                        headerName: 'Notional Amounts',
                        field: 'ccy_amunt',
                        valueFormatter: commaFormat,
                        filter: 'agNumberColumnFilter',
                        cellStyle: {
                            'text-align': 'right'
                        }
                    },
                ],
                rowData: data.cash_summation,
                domLayout: 'autoHeight',
                enableFilter: true,
                headerHeight: 35,
                enableSorting: true,
                pagination: true,
                paginationPageSize: 15,
                defaultColDef: {
                    suppressMovable: true,
                    suppressFilter: true,
                    suppressMenu: true,
                },
                suppressColumnVirtualisation: true,
                onGridReady: function (gridOptions) {
                    gridOptions.columnApi.autoSizeAllColumns();
                }
            };
            var eCashSummationGridDiv = document.querySelector('#cashSummationOptions');
            new agGrid.Grid(eCashSummationGridDiv, cashSummationGridOptions);
        }
    }

    function mtmgrid(data) {
        if (mtmgridOptions != null) {
            mtmgridOptions.api.setRowData(data.data);
        } else {
            mtmgridOptions = {
                columnDefs: [
                    { headerName: 'Trade ID', field: 'trade_id' },
                    { headerName: 'Fund', field: 'fund' },
                    { headerName: 'Trade Date', field: 'trade_date', },
                    { headerName: 'Value Date', field: 'value_date', },
                    { headerName: 'Delivery Date', field: 'delivery_date', },
                    { headerName: 'Style', field: 'style' },
                    { headerName: 'Underlying', field: 'underlying' },
                    { headerName: 'Direction', field: 'direction' },
                    { headerName: 'Notional Currency', field: 'notional_currency' },
                    {
                        headerName: 'Notional Amounts',
                        field: 'notional_amounts',
                        valueFormatter: window.formatters.VrmHedge.notional,
                        filter: 'agNumberColumnFilter',
                        cellStyle: {
                            'text-align': 'right'
                        }
                    },
                    {
                        headerName: 'Strike',
                        field: 'strike',
                        valueFormatter: window.formatters.VrmHedge.strike,
                        cellStyle: {
                            'text-align': 'right'
                        }
                    },
                    {
                        headerName: 'Premium',
                        field: 'premium',
                        valueFormatter: window.formatters.VrmHedge.premium
                    },
                    { headerName: 'Premium Payment Date', field: 'premium_payment_date' },
                    { headerName: 'Counterparty', field: 'counterparty' },
                    { headerName: 'Legal Entity', field: 'legal_entity' },
                    { headerName: 'Unwind', field: 'unwind' },
                    { headerName: 'Unwind Proceeds', field: 'unwind_proceeds' },
                ],
                rowData: data.data,
                domLayout: 'autoHeight',
                enableFilter: true,
                headerHeight: 35,
                enableSorting: true,
                pagination: true,
                paginationPageSize: 15,
                defaultColDef: {
                    suppressMovable: true,
                    suppressFilter: true,
                    suppressMenu: true,
                },
                suppressColumnVirtualisation: true,
                onGridReady: function (gridOptions) {
                    gridOptions.columnApi.autoSizeAllColumns();
                }
            };
            var eMtmGridDiv = document.querySelector('#trade-mtm-grid');
            new agGrid.Grid(eMtmGridDiv, mtmgridOptions);
        }
    }

    function grid(rowData) {
        if (gridOptions != null) {
            gridOptions.api.setRowData(rowData);
        } else {
            gridOptions = {
                columnDefs: [
                    { headerName: 'Underlying', field: 'underlying' },
                    {
                        headerName: 'Mid',
                        field: 'mid',
                        valueFormatter: window.formatters.LiveSpotRateTable.spotRef
                    },
                    { headerName: 'TimeStamp', field: 'timestamp', valueFormatter: toTimeFormatter},
                ],
                rowData: rowData,
                domLayout: 'autoHeight',
                headerHeight: 35,
                enableColResize: false,
                defaultColDef: {
                    suppressMovable: true,
                    suppressFilter: true,
                    suppressMenu: true,
                },
                suppressColumnVirtualisation: true,
                onGridReady: function (gridOptions) {
                    gridOptions.columnApi.autoSizeAllColumns();
                }
            };

            var eGridDiv = document.querySelector('#myGrid');
            new agGrid.Grid(eGridDiv, gridOptions);
        }
    }

    // converting cell value to two decimal places
    function commaFormat(params) {
        return (Math.round(parseFloat(params.value))).toLocaleString();
    }

    // converting cell value to UTC time
    function toTimeFormatter(params) {
        return new Date(params.value).toLocaleString();
    }

    let KEY_BACKSPACE = 8;
    let KEY_F2 = 113;
    let KEY_DELETE = 46;

    function MySimpleCellEditor() { }

    MySimpleCellEditor.prototype.init = function (params) {
        this.gui = document.createElement('input');
        this.gui.type = 'text';
        this.gui.classList.add('my-simple-editor');

        this.params = params;

        var startValue;

        let keyPressBackspaceOrDelete =
            params.keyPress === KEY_BACKSPACE
            || params.keyPress === KEY_DELETE;

        if (keyPressBackspaceOrDelete) {
            startValue = '';
        } else if (params.charPress) {
            startValue = params.charPress;
        } else {
            startValue = params.value;
            if (params.keyPress !== KEY_F2) {
                this.highlightAllOnFocus = true;
            }
        }

        if (startValue !== null && startValue !== undefined) {
            this.gui.value = startValue;
        }
    };

    MySimpleCellEditor.prototype.getGui = function () {
        return this.gui;
    };

    MySimpleCellEditor.prototype.getValue = function () {
        return this.gui.value;
    };

    MySimpleCellEditor.prototype.afterGuiAttached = function () {
        this.gui.focus();
    };

    MySimpleCellEditor.prototype.myCustomFunction = function () {
        return {
            rowIndex: this.params.rowIndex,
            colId: this.params.column.getId()
        };
    };

    setInterval(function () {
        if (gridOptions != null){
            var instances = gridOptions.api.getCellEditorInstances();
            if (instances.length > 0) {
                var instance = instances[0];
                if (instance.myCustomFunction) {
                    var result = instance.myCustomFunction();
                    console.log('found editing cell: row index = ' + result.rowIndex + ', column = ' + result.colId + '.');
                } else {
                    console.log('found editing cell, but method myCustomFunction not found, must be the default editor.');
                }
            } else {
                console.log('found not editing cell.');
            }
        }
    }, 1000);

    function fetch_tradedeals_data(select_date = null) {
        if (select_date != null) {
            var rmdata = { data: [], cash_summation: [] };
            cashsettlementgrid(rmdata);
            cashSummation(rmdata);
        }

        if ($("#SRInputValueDate").val() === "") {
            select_date = null;
        } else{
            select_date = $('#SRInputValueDate').val();
        }

        $.ajax({
            type: 'POST',
            url: params.tradeTable,
            data: {
                'csrfmiddlewaretoken': params.csrf_token,
                'client': params.client,
                'fund': $('#fund-select').val(),
                'valuation_date': select_date,
                'fund_deselection': JSON.stringify(fund_deselection),
                'cp_deselection': JSON.stringify(cp_deselection),
                'rollpnl':$('#SRInputRollPNL').val(),
                'farleg':$('#SRInputFarLeg').val(),
                'nettingval':$('#SRInputNettingVal').val(),
            },
            success: function (data) {
                mtmgrid(data);
                rollinggrid(data);
                $('#SRInputValueDate').datepicker('setDate', data.value_date);
                $('#SRInputValueDate').datepicker('option', 'dateFormat', 'yy-mm-dd');
                $('#SRInputValueDate').datepicker('option', 'beforeShowDay', function(date) {
                    return [value_date.find(function(val) { return val === moment(date).format('YYYY-MM-DD'); }), '', ''];
                });
            }
        });
    }

    function ajaxlivespotrate() {
        if (!document.getElementById('pause').checked) {
            $.ajax({
                type: 'POST',
                url: params.liveSpotRates,
                data: {
                    'csrfmiddlewaretoken': params.csrf_token,
                    'client': params.client,
                    'valuation_date': $('#SRInputValueDate').val()
                },
                success: function (data) {
                    grid(data['data']);
                }
            });
        }
    }

    function handleFilterChange(el) {
        fund_deselection = $("[name=SRInputFund]")
            .filter(function(index, el) {
                return !$(el).is(':checked');
            })
            .map(function(index, el) {
                return $(el).val();
            })
            .get()

        cp_deselection = $("[name=SRInputCounterparty]")
            .filter(function(index, el) {
                return !$(el).is(':checked');
            })
            .map(function(index, el) {
                return $(el).val();
            })
            .get()

        // For wiping the tables
        const data = {
            data: [],
            cash_summation: []
        }
        cashsettlementgrid(data);
        cashSummation(data);

        // Fetch the real data
        fetch_tradedeals_data();

        // Unpause?
        $('#pause').prop('checked', false);
    }

    function srCashCall() {
        ajaxlivespotrate();
        const current_rollingvalues = {};
        const current_livespotrates = {};

        rollinggridOptions.api.forEachNode(function (rowNode, index) {
            current_rollingvalues[index] = rowNode.data;
        });

        gridOptions.api.forEachNode(function (rowNode, index) {
            current_livespotrates[rowNode.data.underlying] = rowNode.data.mid;
        });

        $.ajax({
            type: 'POST',
            url: params.cashSettlement,
            data: {
                'csrfmiddlewaretoken': params.csrf_token,
                'client': params.client,
                'valuation_date': $('#SRInputValueDate').val(),
                'current_rollingvalues': JSON.stringify(current_rollingvalues),
                'current_livespotrates': JSON.stringify(current_livespotrates),
                'rollpnl':$('#SRInputRollPNL').val(),
                'farleg':$('#SRInputFarLeg').val(),
                'nettingval':$('#SRInputNettingVal').val(),
            },
            success: function (data) {
                cashsettlementgrid(data);
                cashSummation(data);
            }
        });
    }

    function downloadRollingReport() {
        const current_rollingvalues = {};
        const current_livespotrates = {};
        const current_trades = {};
        const cash_settlement = {};
        const cash_summation = {};

        rollinggridOptions.api.forEachNode(function (rowNode, index) {
            current_rollingvalues[index] = rowNode.data;
        });
        gridOptions.api.forEachNode(function (rowNode, index) {
            current_livespotrates[rowNode.data.underlying] = rowNode.data.mid;
        });
        mtmgridOptions.api.forEachNode(function (rowNode, index) {
            current_trades[index] = rowNode.data;
        });

        if (cashSettlementGridOptions != null) {
            cashSettlementGridOptions.api.forEachNode(function (rowNode, index) {
                cash_settlement[index] = rowNode.data;
            });
        }
        if (cashSummationGridOptions != null) {
            cashSummationGridOptions.api.forEachNode(function (rowNode, index) {
               cash_summation[index] = rowNode.data;
            });
        }

        $.ajax({
            type: 'POST',
            url: params.prepareRollingData,
            data: {
                'csrfmiddlewaretoken': params.csrf_token,
                'client': params.client,
                'valuation_date': $('#SRInputValueDate').val(),
                'current_rollingvalues': JSON.stringify(current_rollingvalues),
                'current_livespotrates': JSON.stringify(current_livespotrates),
                'current_trades': JSON.stringify(current_trades),
                'cashSettlementGridOptions': JSON.stringify(cash_settlement),
                'cash_summation': JSON.stringify(cash_summation),
                'rollpnl':$('#SRInputRollPNL').val(),
                'farleg':$('#SRInputFarLeg').val(),
                'nettingval':$('#SRInputNettingVal').val(),

            },
            success: function (data) {
                var url = params.downloadRollingData + data.data;
                window.location.assign(url);
            }
        });
    }

    /**
     * Initialization
     */
    // You don't need the ability to change funds on this page.
    $('#fund-select').prop('disabled', true);
    $('#fund-select').selectpicker('val', params.selectedFund);

    // Call Spot rate immediately, and on intervals
    ajaxlivespotrate();
    setInterval(ajaxlivespotrate, 5000);
    // Fetch Deals immediately
    fetch_tradedeals_data();

    /**
     * Event Bindings
     */
    $('#SRCashCal').on('click', function () {
        srCashCall()
    });

    $('#downloadRollingReport').on('click', function () {
        downloadRollingReport();
    });

    $('#downloadSpotData').on('click', function () {
        gridOptions.api.exportDataAsCsv();
    });

    $('#SRInputValueDate').on('change', function(e) {
        fetch_tradedeals_data(e.target.value);
        $('#pause').prop('checked', false);
    })

     $('#SRInputRollPNL').on('change', function() {
          if ($(this).is(':checked')) {
            $(this).attr('value', 'true');
          } else {
            $(this).attr('value', 'false');
          }
        fetch_tradedeals_data( $('#SRInputValueDate').val());

    })
    $('#SRInputNettingVal').on('change', function() {
          if ($(this).is(':checked')) {
            $(this).attr('value', 'true');
          } else {
            $(this).attr('value', 'false');
          }
        fetch_tradedeals_data( $('#SRInputNettingVal').val());

    })
    $('#SRInputFarLeg').on('change', function() {
      if ($(this).is(':checked')) {
            $(this).attr('value', 'true');
          } else {
            $(this).attr('value', 'false');
          }
        fetch_tradedeals_data( $('#SRInputValueDate').val());

    })


    $('.form-group-funds').on('change', 'input', function(e) {
        handleFilterChange(this);
    });

    $('.form-group-counterparties').on('change', 'input', function(e) {
        handleFilterChange(this);
    });
     $('#SRInputValueDate').datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "yy-mm-dd"
    });

})(jQuery);