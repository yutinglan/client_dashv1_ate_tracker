$(function() {
    var params = analyticsJsParams;
    var countDecimals = function (value) {
        if (Math.floor(value) === value) return 0;
        if (value.toString().split(".").length == 1) return 1;
        return value.toString().split(".")[1].length || 0;
    };

    var mtmgridOptions = null;
    var eMtmGridDiv = document.querySelector('#trade-mtm-grid-analytics');

    var spotRefGridOptions = null;
    var eSpotRefGridDiv = document.querySelector('#spot-rate-table-analytics');

    function addOption(id_name, data) {
        var select = document.getElementById(id_name);
        select.options.length = 0;
        data.forEach(function (element) {
            select.options[select.options.length] = new Option(element.trim(), element.trim(), false, false);
        });
    }

    // converting cell value to two decimal places
    function commaFormat(params) {
        if (typeof params.value != 'undefined') {
            return (accounting.formatMoney(params.value,"", 2, ",", "."));
        }
    }

    // converting cell value to two decimal places
    function toFormatFour(params) {
        if (typeof params.value != 'undefined' & params.value != null) {
            return parseFloat(params.value).toFixed(4);
        }
    }

    /// Main functions
    function mtmGrid(data) {
        if (mtmgridOptions != null) {
            mtmgridOptions.api.setRowData(data.data);
        } else {
            mtmgridOptions = {
                columnDefs: [
                    {
                        headerName: 'Trade ID',
                        field: 'trade__trade_id'
                    },
                    {
                        headerName: 'Fund',
                        field: 'trade__fund'
                    },
                    {
                        headerName: 'Trade Date',
                        field: 'trade__trade_date'
                    },
                    {
                        headerName: 'Value Date',
                        field: 'trade__value_date'
                    },
                    {
                        headerName: 'Delivery Date',
                        field: 'trade__delivery_date'
                    },
                    {
                        headerName: 'Style',
                        field: 'trade__style'
                    },
                    {
                        headerName: 'Underlying',
                        field: 'trade__underlying'
                    },
                    {
                        headerName: 'Direction',
                        field: 'trade__direction'
                    },
                    {
                        headerName: 'Notional Currency',
                        field: 'trade__notional_currency'
                    },
                    {
                        headerName: 'Notional Amounts',
                        field: 'trade__notional_amounts',
                        valueFormatter: window.formatters.VrmMtm.notional,
                        filter: 'agNumberColumnFilter',
                        cellClass: 'text-right'
                    },
                    {
                        headerName: 'Strike',
                        field: 'trade__strike',
                        valueFormatter: window.formatters.VrmMtm.strike,
                        cellClass: 'text-right'
                    },
                    {
                        headerName: 'Premium',
                        field: 'trade__premium',
                        valueFormatter: window.formatters.VrmMtm.premium
                    },
                    {
                        headerName: 'Premium Currency',
                        field: 'trade__premium_currency'
                    },
                    {
                        headerName: 'Premium Payment Date',
                        field: 'trade__premium_payment_date'
                    },
                    {
                        headerName: 'Counterparty',
                        field: 'trade__counterparty'
                    },
                    {
                        headerName: 'Valuation Date',
                        field: 'valuation_date'
                    },
                    {
                        headerName: 'Spot Ref',
                        field: 'spot_ref',
                        valueFormatter: window.formatters.VrmMtm.spotRef,
                        cellClass: 'text-right'
                    },
                    {
                        headerName: 'Outright Forward',
                        field: 'outright_forward',
                        valueFormatter: window.formatters.VrmMtm.outrightForward,
                        cellClass: 'text-right'
                    },
                    {
                        headerName: 'MTM Currency',
                        field: 'mtm_currency'
                    },
                    {
                        headerName: 'MTM',
                        field: 'mtm',
                        valueFormatter: commaFormat,
                        filter: 'agNumberColumnFilter',
                        cellClass: 'text-right'
                    }
                ],
                rowData: data.data,
                domLayout: 'autoHeight',
                enableFilter: true,
                headerHeight: 35,
                enableSorting: true,
                pagination: true,
                paginationPageSize: 15,
                enableColResize: true,
                defaultColDef: {
                    suppressMovable: true,
                    suppressFilter: true,
                    suppressMenu: true
                },
                suppressColumnVirtualisation: true,
                onGridReady: function (gridOptions) {
                    gridOptions.columnApi.autoSizeAllColumns();

                    var debouncedResize = _.debounce(function () {
                        gridOptions.columnApi.autoSizeAllColumns();
                    }, 300);

                    window.addEventListener('resize', function () {
                        debouncedResize();
                    })
                }
            };
            mtmGrid_obj = new agGrid.Grid(eMtmGridDiv, mtmgridOptions);
        }

        if (typeof data.mtm_sum != 'Undefined' & data.mtm_sum != null) {
         if (data.currency != null){
                $('#TotalMTM').text(data.currency.toUpperCase() + ' : ' + (parseFloat(data.mtm_sum).toFixed(2)).toLocaleString() + ' million')
            }else{
                $('#TotalMTM').text('');
            }
        }
        else {
            $('#TotalMTM').text('');
        }
    }

    function spotRefGrid(data) {
        if (spotRefGridOptions != null) {
            spotRefGridOptions.api.setRowData(data.data);
        } else {
            spotRefGridOptions = {
                columnDefs: [
                    {
                        headerName: 'Underlying',
                        field: 'trade__underlying'
                    },
                    {
                        headerName: 'Spot Ref',
                        field: 'average_spot_rate',
                        editable: true,
                        cellClass: 'text-right',
                        headerClass: 'text-right',
                        cellEditor: 'SpotRefCellEditor',
                        valueFormatter: window.formatters.SpotRatesTable.spotRef
                    },
                ],
                rowData: data.data,
                domLayout: 'autoHeight',
                headerHeight: 35,
                enableColResize: false,
                defaultColDef: {
                    suppressMovable: true,
                    suppressFilter: true,
                    suppressMenu: true,
                },
                components: {
                    SpotRefCellEditor: SpotRefCellEditor
                },
                onGridReady: function (gridOptions) {
                    gridOptions.api.sizeColumnsToFit();

                    var debouncedResize = _.debounce(function () {
                        gridOptions.api.sizeColumnsToFit();
                    }, 300);

                    window.addEventListener('resize', function () {
                        debouncedResize();
                    })
                }
            };
            spotRefGrid_obj = new agGrid.Grid(eSpotRefGridDiv, spotRefGridOptions);
        }
    }

    let KEY_BACKSPACE = 8;
    let KEY_F2 = 113;
    let KEY_DELETE = 46;
    function SpotRefCellEditor() { }

    SpotRefCellEditor.prototype.init = function (params) {
        this.gui = document.createElement('input');
        this.gui.type = 'text';
        this.gui.classList.add('my-simple-editor');

        this.params = params;

        var startValue;

        let keyPressBackspaceOrDelete =
            params.keyPress === KEY_BACKSPACE
            || params.keyPress === KEY_DELETE;

        if (keyPressBackspaceOrDelete) {
            startValue = '';
        } else if (params.charPress) {
            startValue = params.charPress;
        } else {
            startValue = params.value;
            if (params.keyPress !== KEY_F2) {
                this.highlightAllOnFocus = true;
            }
        }

        if (startValue !== null && startValue !== undefined) {
            this.gui.value = startValue;
        }
    };

    SpotRefCellEditor.prototype.getGui = function () {
        return this.gui;
    };

    SpotRefCellEditor.prototype.getValue = function () {
        return this.gui.value;
    };

    SpotRefCellEditor.prototype.afterGuiAttached = function () {
        this.gui.focus();
    };

    SpotRefCellEditor.prototype.myCustomFunction = function () {
        return {
            rowIndex: this.params.rowIndex,
            colId: this.params.column.getId()
        };
    }

    setInterval(function () {
        if (spotRefGridOptions != null) {
            var instances = spotRefGridOptions.api.getCellEditorInstances();
            if (instances.length > 0) {
                var instance = instances[0];
                if (instance.myCustomFunction) {
                    var result = instance.myCustomFunction();
                    //console.log('found editing cell: row index = ' + result.rowIndex + ', column = ' + result.colId + '.');
                } else {
                    // console.log('found editing cell, but method myCustomFunction not found, must be the default editor.');
                }
            } else {
                // console.log('found editing cell, but method myCustomFunction not found, must be the default editor.');
            }
        }
    }, 1000);

    function initFundPercentagePieChart(datum, ccy, fund_sum) {
        // Highcharts title formatting isn't very good
        var title = 'Fund Investment Summary';

        Highcharts.chart('fund-percentage-container', {
            chart: {
                backgroundColor: null,
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: title
            },
            subtitle: {
                text: '<b> Current NAV by Currency in </b>' + ccy + '<b> - Total :</b>' + (fund_sum / 1000000).toFixed(2).toLocaleString() + ' mn'
            },
            tooltip: {
                pointFormat: '<b>{point.name} Invested Equity(' + ccy + '): {point.value}</b>',
                backgroundColor: 'rgba(255, 255, 255, 1)'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.value} - {point.y} %',
                        style: {
                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                        }
                    }
                }
            },
            credits: {
                enabled: false
            },
            exporting: {
                enabled: true
            },
            series: [{
                name: ' Fund Currency',
                colorByPoint: true,
                data: datum
            }]
        });
    }

    function initPortfolioDealHedgeMTMChart(data) {
        Highcharts.chart('fund-hedge-vs-unhedged-container', {
            chart: {
                zoomType: 'x'
            },
            title: {
                text: 'Portfolio Valuation (Unhedged vs. Hedged)'
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                type: 'datetime',
                categories: data.time
            },
            yAxis: {
                title: {
                    text: data.ccy
                }
            },
            legend: {
                enabled: true
            },
            plotOptions: {
                area: {
                    marker: {
                        radius: 2
                    },
                    lineWidth: 1,
                    states: {
                        hover: {
                            lineWidth: 1
                        }
                    },
                    threshold: null
                }
            },

            credits: {
                enabled: false
            },
            exporting: {
                enabled: true
            },

            series: [
                {
                    type: 'line',
                    name: 'Hedged',
                    data: data.hedged
                },
                {
                    type: 'line',
                    name: 'Unhedged',
                    data: data.unhedged
                }
            ]
        })
    }

    function initSummaryBaseCaseRisk(data) {
        Highcharts.chart('summary-base-case-risk', {
            chart: {
                type: 'waterfall'
            },

            title: {
                text: 'Risk breakdown'
            },

            xAxis: {
                type: 'category'
            },

            yAxis: {
                title: {
                    text: 'IRR (95% CL)'
                }
            },

            legend: {
                enabled: false
            },

            tooltip: {
                pointFormat: '<b>{point.y:,.2f}</b>%'
            },
            credits: {
                enabled: false
            },
            exporting: {
                enabled: true
            },
            series: [{
                upColor: '#42BA97',
                color: '#D62839',
                borderColor: '#CCCCCC',
                data: [{
                    name: 'Base Case',
                    y: data.base_case
                },
                {
                    name: 'Current FX Gain/Loss',
                    y: data.current_fx_gain_loss
                },
                {
                    name: '@ Risk',
                    y: data.at_risk
                },
                {
                    name: 'Hedging Effect',
                    y: data.hedging_effect
                },
                {
                    name: 'Cost',
                    y: data.cost
                },
                {
                    name: 'Worst Case',
                    isSum: true
                }],
                dataLabels: {
                    enabled: true,
                    formatter: function () {
                        return Highcharts.numberFormat(this.y, 0, ',') + '%';
                    },
                    style: {
                        fontWeight: 'bold'
                    }
                },
                pointPadding: 0
            }]
        });
    }

    function fetch_fund_summary(select_date = null) {
        if  ( params.portfolio){
            $.ajax({
                type: 'POST',
                url: params.fund_investment_summary,
                data: {
                    'csrfmiddlewaretoken': params.csrf_token,
                    'client': params.client,
                    'fund': $('#fund-select').val(),
                    'select_date': select_date
                },
                success: function (data) {
                    //setup_cpsummation(data);
                    var mappedFundPercentageData = data.data.map(function (fund_summary_data) {
                        return {
                            name: fund_summary_data.ccy,
                            y: fund_summary_data.percentage,
                            value: Math.round(fund_summary_data.value).toLocaleString()
                        }
                    });
                    initFundPercentagePieChart(mappedFundPercentageData, data.fund_ccy, data.fund_sum);
                }
            });
        }

    }

    function fetch_historical_valuation_data() {
        if  ( params.portfolio){
            $.ajax({
                type: 'POST',
                url: params.api_portfolio_deal_hedge_mtm_chart,
                data: {
                    'csrfmiddlewaretoken': params.csrf_token,
                    'client': params.client,
                    'fund': $('#fund-select').val(),
                },
                success: function (data) {
                    initPortfolioDealHedgeMTMChart(data);
                }
            });
        }
    }

    function fetch_base_risk_data() {
     if  ( params.irr){
            $.ajax({
                type: 'POST',
                url: params.api_summary_base_case_risk,
                data: {
                    'csrfmiddlewaretoken': params.csrf_token,
                    'client': params.client,
                    'fund': $('#fund-select').val(),
                    'confidence_level': $('#CF_select').val(),
                },
                success: function (data) {
                    initSummaryBaseCaseRisk(data);
                }
            });
        }
    }

    function fetch_mtm_data(select = null) {
        $.ajax({
            type: 'POST',
            url: params.trade_mtm_url,
            data: {
                'csrfmiddlewaretoken': params.csrf_token,
                'client': params.client,
                'fund': $('#fund-select').val(),
                'valuation_date': select,
            },
            success: function (data) {
                var dateList = data.date_list || [];
                var targetDate = new Date();
                if (data.valuation_date) {
                    targetDate = new Date(moment(data.valuation_date, 'YYYY-MM-DD').format())
                }

                $('#SpotRate_valuationDate_select').datepicker('setDate', targetDate);
                $('#SpotRate_valuationDate_select').datepicker('option', 'dateFormat', 'yy-mm-dd');
                $('#SpotRate_valuationDate_select').datepicker('option', 'beforeShowDay', function(date) {
                    return [dateList.find(function(val) { return val === moment(date).format('YYYY-MM-DD'); }), '', ''];
                });

                mtmGrid(data);
            }
        });
        $.ajax({
            type: 'POST',
            url: params.spot_ref_url,
            data: {
                'csrfmiddlewaretoken': params.csrf_token,
                'client': params.client,
                'fund': $('#fund-select').val(),
                'valuation_date': select,
            },
            success: function (data) {
                spotRefGrid(data);
            }
        });
    }

    /// Button action functions
    function updateMtmGrid() {
        var spot_ref_data = getGridData(spotRefGridOptions)
        var mtm_data = getGridData(mtmgridOptions)

        $.ajax({
            type: 'POST',
            url: params.recalculate_mtm_on_spotref_url,
            data: {
                'csrfmiddlewaretoken': params.csrf_token,
                'spot_ref_data': spot_ref_data,
                // TODO: @Carlos this needs to get resolved
                // We're not sure what should be in this call.
                // The MTM being returned from the API seems wildly off.
                'mtm_data': mtm_data,
                'client': params.client,
                'fund': $('#fund-select').val(),
                'valuation_date': $('#SpotRate_valuationDate_select').val()
            },
            success: function (updated_mtm_data) {
                mtmGrid(updated_mtm_data);
            }
        })
    };

    function getGridData(gridOptions) {
        var rowData = [];
        // document.querySelector('#spot-rate-table-analytics')
        gridOptions.api.forEachNode(function (node) {
            rowData.push(node.data);
        });
        return JSON.stringify(rowData);
    };

    // Event Bindings
    $('#updateSpotData-analytics').on('click', function () {
        updateMtmGrid();
        fetch_mtm_data;
    });

    $('#resetSpotData-analytics').on('click', function () {
        fetch_mtm_data();
    });

    $('#downloadSpotData-analytics').on('click', function () {
        mtmgridOptions.api.exportDataAsCsv();
    });

    $('#SpotRate_valuationDate_select').change(function () {
        fetch_mtm_data($('#SpotRate_valuationDate_select').val());
    });

    $('#CF_select').change(function () {
        fetch_base_risk_data();
    });

    $('#fund-select').change(function () {
        if (! params.display_mtm_only) {
            fetch_fund_summary();
            fetch_historical_valuation_data();
            fetch_base_risk_data();
        }
        fetch_mtm_data();
    });

    $('#fund-select').selectpicker('val', params.selected_fund);
    if (! params.display_mtm_only) {
        fetch_fund_summary();
        fetch_historical_valuation_data();
        fetch_base_risk_data();
    }

    fetch_mtm_data();

    // Initializing Date Picker
    $('#SpotRate_valuationDate_select').datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "yy-mm-dd"
    });
});