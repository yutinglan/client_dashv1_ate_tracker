$(function() {
    var params = window.analyticsJsParams;
    var mtmgridOptions = null;
    var eMtmGridDiv = document.querySelector('#pickup_credit-grid-analytics');

    // converting cell value to two decimal places
    function commaFormat(params) {
        if (typeof params.value != 'undefined') {
            return (accounting.formatMoney(params.value,"", 2, ",", "."));
        }
    }

    // converting cell value to two decimal places
    function toFormatFour(params) {
        if (typeof params.value != 'undefined' & params.value != null) {
            return parseFloat(params.value).toFixed(4);
        }
    }


    function mtmGrid(data) {
        if (mtmgridOptions != null) {
            mtmgridOptions.api.setRowData(data.data);
        } else {
            mtmgridOptions = {
                columnDefs: [
                    { headerName: 'Trade Date', field: 'trade_date' },
                    { headerName: 'Value Date', field: 'value_date' },
                    { headerName: 'Underlying', field: 'underlying' },
                    { headerName: 'Direction', field: 'direction' },
                    { headerName: 'Notional Currency', field: 'notional_currency' },
                    { headerName: 'Notional Amounts', field: 'notional_amounts', valueFormatter: commaFormat, filter: 'agNumberColumnFilter',  cellClass: 'twoDecimalPlaces', cellStyle: { 'text-align': "right" } },
                    {
                        headerName: 'Trade Forward',
                        field: 'traded_forward',
                        valueFormatter: toFormatFour,
                        cellClass: 'text-right'
                    },
                    {
                        headerName: 'Trade Spot',
                        field: 'traded_spot',
                        valueFormatter: toFormatFour,
                        cellClass: 'text-right'
                    },
                    {
                        headerName: 'Strike',
                        field: 'strike',
                        valueFormatter: toFormatFour,
                        cellClass: 'text-right'
                    },
                    { headerName: 'Premium', field: 'premium', valueFormatter: commaFormat, cellClass: 'twoDecimalPlaces',  cellStyle: { 'text-align': "right" }  },
                     {
                        headerName: 'Pick Up',
                        field: 'pick_up',
                        valueFormatter: commaFormat,
                        cellStyle: { 'text-align': "right" } ,
                        cellClass: 'twoDecimalPlaces'
                    },
                    {
                        headerName: 'Credit Cost',
                        field: 'credit_cost',
                        valueFormatter: commaFormat,
                        cellStyle: { 'text-align': "right" } ,
                        cellClass: 'twoDecimalPlaces'
                    },
                ],
                rowData: data.data,
                domLayout: 'autoHeight',
                enableFilter: true,
                headerHeight: 35,
                enableSorting: true,
                pagination: true,
                paginationPageSize: 15,
                enableColResize: true,
                defaultColDef: {
                    suppressMovable: true,
                    suppressFilter: true,
                    suppressMenu: true,
                },
                suppressColumnVirtualisation: true,
                onGridReady: function (gridOptions) {
                    gridOptions.columnApi.autoSizeAllColumns();

                    var debouncedResize = _.debounce(function () {
                        gridOptions.columnApi.autoSizeAllColumns();
                    }, 300);

                    window.addEventListener('resize', function () {
                        debouncedResize();
                    })
                },
                 excelStyles: [
                  {
                    id: 'booleanType',
                    dataType: 'boolean'
                },{
                    id: 'stringType',
                    dataType: 'string'
                },{
                    id: 'dateType',
                    dataType: 'dateTime'
                },
                {
                    id: 'twoDecimalPlaces',
                    numberFormat: {
                        format: '#,##0.00'
                    }
                },

                {
                    id: 'header',
                    interior: {
                        color: '#CCCCCC',
                        pattern: 'Solid'
                    },
                    font: {
                        size: 14
                    },
                    borders: {
                        borderBottom: {
                            color: '#5687f5',
                            lineStyle: 'Continuous',
                            weight: 1
                        },
                        borderLeft: {
                            color: '#5687f5',
                            lineStyle: 'Continuous',
                            weight: 1
                        },
                        borderRight: {
                            color: '#5687f5',
                            lineStyle: 'Continuous',
                            weight: 1
                        },
                        borderTop: {
                            color: '#5687f5',
                            lineStyle: 'Continuous',
                            weight: 1
                        }
                    }
                },

              ],
            };
            mtmGrid_obj = new agGrid.Grid(eMtmGridDiv, mtmgridOptions);
        }

        if (typeof data.pick_up_total != 'Undefined' & data.pick_up_total != null) {
            $('#PickupMTM').text(data.currency.toUpperCase() + ' : ' + accounting.formatMoney(data.pick_up_total,"", 2, ",", "."))
        }
        else {
            $('#PickupMTM').text('');
        }
        if (typeof data.credit_cost_total != 'Undefined' & data.credit_cost_total != null) {
            $('#CreditMTM').text(data.currency.toUpperCase() + ' : ' + accounting.formatMoney(data.credit_cost_total,"", 2, ",", ".") )
        }
        else {
            $('#CreditMTM').text('');
        }
    }





    /// Data fetching

    function fetch_mtm_data(select=null) {
        $.ajax({
            type: 'POST',
            url: params.pucanalytics,
            data: {
                'csrfmiddlewaretoken': params.csrf_token,
                'client': params.client,
                'fund':$('#fund-select').val(),
                'start_valuation_date': $('#Startdate_valuationDate_select').val() || null,
                'end_valuation_date': $('#EndDate_valuationDate_select').val() || null,
            },
            success: function (data) {
                var dateList = data.date_list || [];
                var reverse_datList = Array.prototype.reverse.apply(data.date_list || [])
                var targetStartDate = new Date();
                var targetEndDate = new Date();
                if (data.start_valuation_date) {
                    targetStartDate = new Date(moment(data.start_valuation_date, 'YYYY-MM-DD').format())
                }
                 if (data.end_valuation_date) {
                    targetEndDate = new Date(moment(data.end_valuation_date, 'YYYY-MM-DD').format())
                }
                $('#Startdate_valuationDate_select').datepicker('setDate', targetStartDate);
                $('#Startdate_valuationDate_select').datepicker('option', 'dateFormat', 'yy-mm-dd');
                $('#Startdate_valuationDate_select').datepicker('option', 'beforeShowDay', function(date) {
                    return [reverse_datList.find(function(val) { return val === moment(date).format('YYYY-MM-DD'); }), '', ''];
                });
                $('#EndDate_valuationDate_select').datepicker('setDate', targetEndDate);
                $('#EndDate_valuationDate_select').datepicker('option', 'dateFormat', 'yy-mm-dd');
                $('#EndDate_valuationDate_select').datepicker('option', 'beforeShowDay', function(date) {
                    return [dateList.find(function(val) { return val === moment(date).format('YYYY-MM-DD'); }), '', ''];
                });
                mtmGrid(data);
            }
        });

    }

    function addOption(id_name, data) {
        var select = document.getElementById(id_name);
        select.options.length = 0;
        data.forEach(function (element) {
            select.options[select.options.length] = new Option(element.trim(), element.trim(), false, false);
        });
    }

    function fetchMetrics() {
        fetch_mtm_data();
    };



    $('#download-analytics').on('click', function () {

        var fileparams = {
            fileName: 'PickupCost - Analysis '+ $('#fund-select').val(),
            sheetName: params.client +' - '+$('#fund-select').val(),
            exportMode:'xlsx'
         };
         params.processHeaderCallback = function(params) {
            return params.column.getColDef().headerName.toUpperCase();
        };
        mtmgridOptions.api.exportDataAsExcel(fileparams);
    });

    $('#Startdate_valuationDate_select').change(function () {
        fetchMetrics();
    });

    $('#EndDate_valuationDate_select').change(function () {
        fetchMetrics();
    });
    $('#fund-select').change(function () {
        $('#Startdate_valuationDate_select').val('');
        $('#EndDate_valuationDate_select').val('');

        fetchMetrics();
    });
    fetchMetrics();
//    fetchLevelDropDownOptions($('#manalytics_option').val());

    // if funds changes we need to perform the action
    //$('#fund-select').prop('disabled', true);

//     Initializing Date picker
    $('#Startdate_valuationDate_select').datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "yy-mm-dd"
    });
    $('#EndDate_valuationDate_select').datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "yy-mm-dd"
    });
});