(function ($) {

    var params = portfolioDashboardJsParams;

    function fetchValuationDates (select=null) {
        $.ajax({
            type: 'POST',
            url: params.trade_mtm_url,
            data: {
                'csrfmiddlewaretoken': params.csrf_token,
                'client': params.client,
                'fund': $('#fund-select').val(),
                'valuation_date': select,
            },
            success: function(data) {
                var dateList = data.date_list || [];
                var targetDate = new Date();
                if (data.valuation_date) {
                    targetDate = new Date(moment(data.valuation_date, 'YYYY-MM-DD').format())
                }

                $('#ValuationDate_select').datepicker({
                    changeMonth: true,
                    changeYear: true,
                    dateFormat: "yy-mm-dd"
                });
                $('#ValuationDate_select').datepicker('setDate', targetDate);
                $("#ValuationDate_select").datepicker("option", "dateFormat", "yy-mm-dd");
                $("#ValuationDate_select").datepicker("option", "beforeShowDay", function(date) {
                    return [dateList.find(function(val) { return val === moment(date).format('YYYY-MM-DD'); }), '', ''];
                });
            }
        });
    };

    function prepare_deal_level_valuation_report () {
        $.ajax({
                type: 'POST',
                url: params.prepare_deal_level_valuation_report_url,
                data: {
                    'csrfmiddlewaretoken': params.csrf_token,
                    'label': params.label,
                    'client': params.client,
                    'fund': $('#fund-select').val(),
                    'valuation_date': $('#ValuationDate_select').val(),
                },
                success: function(data) {
                    var url = params.download_file_data_url + data.file_name;
                    if (urlExists(url)) {
                        window.location.assign(url);
                    } else {
                        console.log('Error trying to download ' + data.file_name);
                    }
                }
        });
    };

    function urlExists(url) {
        var http = $.ajax({
            type:"HEAD",
            url: url,
            async: false
        });
        var status = http.status;
        var exists = false;
        if (status == 200) {
            exists = true
        }
        return exists
    }

    $('#downloadDataPLMTM').click(function () {
        prepare_deal_level_valuation_report ();
    });

    $('#fund-select').change(function () {
        fetchValuationDates ();
    });

    $(document).ready(function () {
        $('#fund-select').selectpicker('val', params.selected_fund);
        fetchValuationDates ();
    });
    $('#fund-select').prop('disabled', true);

})(jQuery);