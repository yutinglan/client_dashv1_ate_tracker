'use strict';

window.formatters = (function () {
    // These currencies do not have the concept of fractions of dollars,
    // they need two less points of precision accordingly.
    var currencyVariants = ['JPY', 'NGN'];

    function modifiedCompose() {
        var funcs = Array.prototype.slice.call(arguments);
        return function () {
            return funcs.slice(1).reduce(function (res, fn) {
                return fn.apply(undefined, res);
            }, funcs[0].apply(undefined, arguments));
        }
    }

    function countDecimals(value) {
        // Copied from the wonderful contributor at:
        // https://stackoverflow.com/questions/17369098/simplest-way-of-getting-the-number-of-decimals-in-a-number-in-javascript#answer-17369245
        if (Math.floor(value) === value) return 0;
        return value.toString().split(".")[1].length || 0;
    }

    /**
     * From an email chain, John Glover as the source of knowledge.
     * *********************************************************************
     * For all currencies with the exception of JPY, we should use a minimum
     * of 6 decimal places, and a maximum of however many decimal places
     * were used in the actual contracted deal rate.
     * For JPY, which has a market convention of 2 decimal places, we
     * should use a minimum of 4 and a maximum of the actual contracted rate.
     * *********************************************************************
     *
     * @param {float} value
     * @param {string} currency
     */
    function strikeFormatter(value, currency) {
        if (value==null)
        {
            return '';
        }
        var minimumPrecision = (currencyVariants.indexOf(currency) === -1) ? 6 : 4;
        var valuePrecision = countDecimals(value);
        var precision = (valuePrecision > minimumPrecision)
            ? valuePrecision
            : minimumPrecision;
        return value.toFixed(precision);
    }

    /**
     * From an email chain, John Glover as the source of knowledge.
     *
     * *********************************************************************
     * This should be shown as four decimal places  for all but JPY,
     * which should be truncated to 2
     * *********************************************************************
     *
     * @param {float} value
     * @param {string} currency
     */
    function spotRefFormatter(value, currency) {
        // This function needs string checking because it can be modified with
        // a cell editor.
        var castedValue = Number(value);
        if (isNaN(castedValue)) {
            return "Invalid Input."
        }

        var precision = (currencyVariants.indexOf(currency) === -1) ? 4 : 2;

        return castedValue.toFixed(precision);
    }

    /**
     * From an email chain, John Glover as the source of knowledge.
     *
     * *********************************************************************
     * Forward points can be as many as 6 decimal places
     * (except JPY which can be 4)
     * *********************************************************************
     *
     * @param {float} value
     * @param {string} currency
     */
    function outrightForwardFormatter(value, currency) {
        var precision = (currencyVariants.indexOf(currency) === -1) ? 6 : 4;
        if (value === null) {
            return value;
        }
        return value.toFixed(precision);
    }

    /**
     * Formats a value, currency pair so it's comma separated for easy reading.
     *
     * @param {float} value
     * @param {string} currency
     */
    function notionalFormatter(value, currency) {
        var minimumPrecision = (currencyVariants.indexOf(currency) === -1)
            ? 2 : 0;

        return value.toLocaleString(undefined, {
            minimumFractionDigits: minimumPrecision,
            maximumFractionDigits: minimumPrecision
        });
    }

    /**
     * Formats a value, currency pair so it's comma separated for easy reading.
     *
     * Currently we are not concerned about variant formatting.
     *
     * @param {float} value
     * @param {string} currency
     */
    function premiumFormatter(value, currency) {
        if (value === null) {
            return value;
        }
        // We aren't worrying about variants yet.
        return value.toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2});
    }

    /**
     * Maps the API shape for VrmMtm1 objects into a general clean format.
     *
     * This should isolate the complexity of mapping data out of the formatters.
     *
     * @param {object} params
     */
    function VrmMtmMapper(params) {
        var value = params.value;
        var data = params.data;

        if (data === undefined) {
            throw "VrmMtmMapper has invalid parameters.";
        }

        var currency = data['trade__notional_currency'];
        if (currency == undefined){
            currency = data['notional_currency'];
        }

        if (value === undefined || currency === undefined) {
            throw 'VrmMtmMapper is mapping an incorrect structure.';
        }

        return [value, currency];
    }

    /**
     * Maps the API shape for VrmMtm1 objects into a general clean format.
     *
     * This should isolate the complexity of mapping data out of the formatters.
     *
     * @param {object} params
     */
    function FutureIndividualMapper(params) {
        var value = params.value;
        var data = params.data;

        if (data === undefined) {
            throw "FutureIndividualMapper has invalid parameters.";
        }

        var currency = 'CAD';

        if (value === undefined || currency === undefined) {
            throw 'FutureIndividualMapper is mapping an incorrect structure.';
        }

        return [value, currency];
    }

    /**
     * Maps the API shape for VrmHedge1CopyCopy objects into a general clean format.
     *
     * This should isolate the complexity of mapping data out of the formatters.
     *
     * @param {object} params
     */
    function VrmHedgeMapper(params) {
        var value = params.value;
        var data = params.data;

        if (data === undefined) {
            throw 'VrmHedgeMapper has invalid parameters.';
        }

        var currency = data['notional_currency'];

        if (value === undefined || currency === undefined) {
            throw 'VrmHedgeMapper is mapping an incorrect structure.';
        }

        return [value, currency];
    }

    /**
     * Maps the API shape for the Spot Rate Table reponse objects into a general
     * clean format.
     *
     * This should isolate the complexity of mapping data out of the formatters.
     *
     * @param {object} params
     */
    function SpotRateTableMapper(params) {
        var value = params.value;
        var data = params.data;

        if (data === undefined) {
            throw 'SpotRateTableMapper has invalid parameters.';
        }

        // USDGBP => GBP
        let key = 'trade__underlying'
        if (data.hasOwnProperty("underlying")){
            key = 'underlying'
        }
        var currency = data[key].substring(3, 6);

        if (value === undefined || currency === undefined) {
            throw 'SpotRateTableMapper is mapping an incorrect structure.';
        }

        return [value, currency];
    }

    /**
     * Maps the API shape for the Live Spot Rate Table reponse objects into a general
     * clean format.
     *
     * This should isolate the complexity of mapping data out of the formatters.
     *
     * @param {object} params
     */
    function LiveSpotRateTableMapper(params) {
        const value = params.value;
        const data = params.data;

        if (data === undefined) {
            throw 'LiveSpotRateTableMapper has invalid parameters.';
        }

        // USDGBP => GBP
        var currency = data['underlying'].substring(3, 6);

        if (value === undefined || currency === undefined) {
            throw 'LiveSpotRateTableMapper is mapping an incorrect structure.';
        }

        return [value, currency];
    }

    // A more general approach may be useful in the future...
    // This will obviously become a repetetive error proned method as this grows
    return {
        raw: {
            strike: strikeFormatter,
            spotRef: spotRefFormatter,
            outrightForward: outrightForwardFormatter,
            notional: notionalFormatter,
            premium: premiumFormatter
        },
        VrmHedge: {
            strike: modifiedCompose(VrmHedgeMapper, strikeFormatter),
            spotRef: modifiedCompose(VrmHedgeMapper, spotRefFormatter),
            outrightForward: modifiedCompose(VrmHedgeMapper, outrightForwardFormatter),
            notional: modifiedCompose(VrmHedgeMapper, notionalFormatter),
            premium: modifiedCompose(VrmHedgeMapper, premiumFormatter)
        },
        VrmMtm: {
            strike: modifiedCompose(VrmMtmMapper, strikeFormatter),
            spotRef: modifiedCompose(VrmMtmMapper, spotRefFormatter),
            outrightForward: modifiedCompose(VrmMtmMapper, outrightForwardFormatter),
            notional: modifiedCompose(VrmMtmMapper, notionalFormatter),
            premium: modifiedCompose(VrmMtmMapper, premiumFormatter)
        },
        futureSpotRef: {
            strike: modifiedCompose(FutureIndividualMapper, strikeFormatter),
            closing_rate: modifiedCompose(FutureIndividualMapper, strikeFormatter),
            spotRef: modifiedCompose(FutureIndividualMapper, spotRefFormatter),
            outrightForward: modifiedCompose(FutureIndividualMapper, outrightForwardFormatter),
            notional: modifiedCompose(FutureIndividualMapper, notionalFormatter),
            premium: modifiedCompose(FutureIndividualMapper, premiumFormatter)
        },
        SpotRatesTable: {
            spotRef: modifiedCompose(SpotRateTableMapper, spotRefFormatter),
        },
        LiveSpotRateTable: {
            spotRef: modifiedCompose(LiveSpotRateTableMapper, spotRefFormatter),
        }
    };
})();
