**Validus Client links**
---

##  Install the application using pip -r  requirements.txt
1. source the environment  like source ~/venv/bin/activate
2. to run in local check the databases it is connected. Run command python manage.py runserver

## AG-Grid License

Add your AG-Grid license to your [environment](#Environment\ Variables) variables, preferably in your activate file.

To find the license key see the company password management software.

```bash
export AG_GRID_LICENSE="<LICENSE_KEY>"
```

# Migrations

**DO NOT RUN ANY MIGRATIONS**

This project uses a shared database because of how the data is calculated and saved on a daily basis. If you have any concerns, ask a coworker.

# Environment Variables

```bash
# AG-Grid License
export AG_GRID_LICENSE="<LICENSE_KEY>"

# Debug:
# If production nothing is needed, if development, set to true
export DEBUG="[True|False]"

# Django Configuration
export SCHEMA="vrmhome"
export DBHOST="<DBHOST>" # devuk in AWS console for development
export PASSWORD="<PASSWORD>"
export USER="<USER>"
```

# Front End

## Node v11.1.0

Downloading a node version manager can be very useful when building multiple front end projects. I would recommend using this [link](https://github.com/creationix/nvm) for linux/unix environments

```bash
nvm install stable
# Stable is v11.1.0 at the time of this comment
nvm use stable
```

## Install required packages

```
npm install
```

## Ag-Grid

[https://www.ag-grid.com/](https://www.ag-grid.com/documentation-main/documentation.php)

*This is a temporary solution until there is a separate front-end build system.*

Move both node_modules directories: `ag-grid` and `ag-grid-enterprise` into your `static/node_modules` directory.

# Overview / Notes

This covers the defined modules of the project. Specific clients will not be listed in this table.

|Module|Current Purpose|Future Intended Purpose|
|-|-|-|
|Accounts|Users, Profiles, Permissions|-|
|Client|-|-|
|Client.Dashboard|This houses the dashboard templates, some filters + context information that should be refactored out to the webapi in the future.|-|
|Database Schema|-|-|
|DSS Schema|-|-|
|Etc|Server Configuration|-|
|General|-|-|
|Links|-|-|
|Logs|Server logs go here.|-|
|MarketData1|-|-|
|Validus|This is the root Django "App"|-|
|Validus_Demo|This is a placeholder "Client" for demo purposes|-|
|WebApi|This houses all web API calls.|-|

# Creating a new Client

## Creating Users

### Step 1: User Creation

The first step required is to create the User objects for the people we are provided
information for. We use the link below for this.

This method is used to offload client specific work until theres is a more complete
and automated system in place.

[Sign Up Link](https://app.validus-riskview.com/accounts/signup/)

### Step 2: Activation + Permissions

Create a Profile object for the User.

> Accounts > Profiles

- Select the User
- Approve Email
- Select the Client(s)
- Any other required field setup.

[Admin Link](https://app.validus-riskview.com/admin/)

**Deactivating a user**

To deactivate a User, set the `Active` flag to false on the User object. It goes against Django philosophy to delete User objects.

**For Auditing: Be sure to remove the reference to the profile, this is to remove account references to clients.**

This process should be automated in the future.

### Step 3: Password Setup + Notifying Users

When there has been signoff and we are ready to notify new Users about their accounts. Someone will go to the link below and enter the passwords into the
forgot password form. This will send an automated email out with a link to
reset password, and will redirect them to the login page.

[Reset Link](https://app.validus-riskview.com/accounts/password_reset/)


## Create a new client module

TODO: Flesh this out fully

```
* is required

new_client
- __init__.py* # Python requirement for imports
- admin.py # TODO: Is this required?
- apps.py*
- models.py* # Clients will need custom data
- routers.py* # Routers control where the ORM will search
- tests.py # TODO: Will we be testing?
- views.py # TODO: Do we use client view imports at all?
```