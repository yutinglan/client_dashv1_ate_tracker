from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Q
from django.views.generic import ListView
from django.contrib.auth.models import User, Group


class Counter:
    count = 0

    def is_zero(self):
        if self.count == 0:
            return True
        else:
            return False

    def increment(self):
        self.count += 1
        return ''

    def decrement(self):
        self.count -= 1
        return ''

    def reset(self):
        self.count = 0
        return ''

    def divisible_by(self):
        if self.count % 3 == 0:
            return True
        else:
            return False


class ContactPage(LoginRequiredMixin, ListView):
    login_url = 'login'
    context_object_name = 'staff'
    template_name = 'contacts/contacts.html'

    def get_context_data(self, *, object_list=None, **kwargs):
        data = super(ContactPage, self).get_context_data(**kwargs)
        data['groups'] = Group.objects.all()
        c = Counter()
        partner = Group.objects.get(name='Partner')
        data['partners'] = User.objects.filter(groups=partner)
        data['counter'] = c
        return data

    def get_queryset(self):
        partner = Group.objects.get(name='Partner')
        return User.objects.filter(~Q(groups=partner), is_staff=True)


