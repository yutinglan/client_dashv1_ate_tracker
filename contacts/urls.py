from django.urls import path
from . import views


urlpatterns = [
    path('', views.ContactPage.as_view(), name= 'contacts_page'),
]
